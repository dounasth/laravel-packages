<?php
namespace Dounasth\Widgets\Base;

/**
 * Created by PhpStorm.
 * User: nimda
 * Date: 3/27/15
 * Time: 9:23 PM
 */

class TableWidget extends ViewWidget {

	public $view;
	public $data;
	public $columns;

	public function __construct($id='', $title='Untitled', $rows=[], $columns='', $wrapClass='', $view='widgets::table-widget') {
		$this->id = $id;
		$this->html = '';
		$this->view = $view;
		$this->data = [
			'title' => $title,
			'columns' => explode(',', $columns),
			'rows' => $rows,
		];
		$this->wrapClass = $wrapClass;
	}

	public function prepare() { }

}