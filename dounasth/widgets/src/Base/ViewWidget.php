<?php
namespace Dounasth\Widgets\Base;
use Illuminate\Support\Facades\View;

/**
 * Created by PhpStorm.
 * User: nimda
 * Date: 3/27/15
 * Time: 9:23 PM
 */

class ViewWidget extends Widget {

	public $view;
	public $data;

	public function __construct($id='', $view='', $data=array(), $wrapClass='') {
		$this->id = $id;
		$this->type = Widget::TYPE_VIEW;
		$this->html = '';
		$this->view = $view;
		$this->data = $data;
		$this->wrapClass = $wrapClass;
	}

	public function prepare() {
		parent::prepare();
	}

	public function html() {
		$this->prepare();
		$this->html = View::make($this->view, $this->data);
		$this->html = parent::html($this);
		return $this->html;
	}

}