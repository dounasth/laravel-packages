<?php
namespace Dounasth\Widgets\Base;

use Illuminate\Support\Facades\View;

class Widget {

    const TYPE_VIEW = 'ViewWidget';
    const TYPE_HTML = 'HtmlWidget';

    public $id;
    public $type;
    public $html;
    public $wrapClass;

    public function __construct($id='', $type=Widget::TYPE_VIEW, $html='', $wrapClass='') {
        $this->id = $id;
        $this->type = $type;
        $this->html = $html;
        $this->wrapClass = $wrapClass;
    }

    public function prepare() { }

    public function html() {
    	$this->prepare();
        if ($this->wrapClass) {
	        //$this->html = "<div class='{$this->wrapClass}'>{$this->html}</div>";
	        $this->html = $this->wrap();
        }
        return $this->html;
    }

    public function wrap() {
    	return View::make('widgets::wrappers.simple', [
		    'html' => $this->html,
		    'wrapClass' => $this->wrapClass,
	    ]);
    }
}