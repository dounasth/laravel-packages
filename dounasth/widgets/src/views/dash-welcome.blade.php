<div class="small-box bg-aqua">
    <div class="inner">
        <h3>
            Welcome
        </h3>
        <p>
            {{Auth::user()->name}}
        </p>
    </div>
    <div class="icon">
        :)
    </div>
    <a href="#" class="small-box-footer">
        More info <i class="fa fa-arrow-circle-right"></i>
    </a>
</div>