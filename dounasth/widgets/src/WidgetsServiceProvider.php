<?php

namespace Dounasth\Widgets;

use Illuminate\Support\ServiceProvider;

class WidgetsServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot(\Illuminate\Routing\Router $router)
    {
	    // LOAD THE VIEWS
	    // - first the published views (in case they have any changes)
	    $this->loadViewsFrom(resource_path('views/vendor/dounasth/widgets'), 'widgets');
	    // - then the stock views that come with the package, in case a published view might be missing
	    $this->loadViewsFrom(realpath(__DIR__.'/views'), 'widgets');

        $this->registerMiddleware($router);
        $this->setupRoutes($router);
        $this->publishFiles();
        $this->loadHelpers();
    }

    public function registerMiddleware(\Illuminate\Routing\Router $router)
    {
    }

    public function setupRoutes(\Illuminate\Routing\Router $router)
    {
    }

    public function publishFiles()
    {
    }

    public function loadHelpers()
    {
        require_once realpath(__DIR__ . '/functions.php');
    }

    public function register()
    {
    }

    public function provides()
    {
        return array(
        );
    }

}
