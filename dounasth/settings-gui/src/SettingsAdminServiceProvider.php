<?php

namespace Dounasth\SettingsAdmin;

use Illuminate\Support\ServiceProvider;

class SettingsAdminServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot(\Illuminate\Routing\Router $router)
    {
	    // LOAD THE VIEWS
	    // - first the published views (in case they have any changes)
	    $this->loadViewsFrom(resource_path('views/vendor/dounasth/settings-gui'), 'settings-gui');
	    // - then the stock views that come with the package, in case a published view might be missing
	    $this->loadViewsFrom(realpath(__DIR__.'/views'), 'settings-gui');


	    $this->registerMiddleware($router);
        $this->setupRoutes($router);
        $this->publishFiles();
        $this->loadHelpers();

	    \Eventy::addFilter('my.hook', function($what) {
		    $what = 'You are '. $what;
		    return $what;
	    }, 30, 1);
    }

    public function registerMiddleware(\Illuminate\Routing\Router $router)
    {
    }

    public function setupRoutes(\Illuminate\Routing\Router $router)
    {
	    // by default, use the routes file provided in vendor
	    $routeFilePathInUse = __DIR__ . '/routes.5.6.php';
	    $this->loadRoutesFrom($routeFilePathInUse);

	    // but if there's a file with the use that one
	    if (file_exists(base_path().'/routes/settings-gui.php')) {
		    $routeFilePathInUse = base_path().'/routes/settings-gui.php';
		    $this->loadRoutesFrom($routeFilePathInUse);
	    }
    }

    public function publishFiles()
    {
    }

    public function loadHelpers()
    {
        require_once realpath(__DIR__ . '/functions.php');
    }

    public function register()
    {
    }

    public function provides()
    {
        return array(
        );
    }

}
