<?php

namespace Dounasth\SettingsAdmin\App;

use Dounasth\Crud\Traits\CrudModelTrait;
use Dounasth\Settings\Models\Setting;

class SettingsCrud extends Setting {
	use CrudModelTrait;
}