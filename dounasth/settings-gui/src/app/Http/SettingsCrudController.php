<?php

namespace Dounasth\SettingsAdmin\App\Http;

use Dounasth\Crud\App\Controllers\AdminCrudController;
use Dounasth\Settings\Models\Setting;
use Dounasth\SettingsAdmin\App\SettingsCrud;

class SettingsCrudController extends AdminCrudController
{
    public $model = SettingsCrud::class;
    public $title = 'Custom Settings';
    public $subtitle = '';
    public $crudName = 'settings';
	public $route = 'backend.settings.index';
	public $routesPrefix = 'backend.settings.';

    public function init()
    {
        parent::init();
	    $this->gui['pagination'] = false;
	    $this->gui['toolbar'] = false;
	    $this->gui['search'] = false;
	    $this->gui['table_head'] = false;

        $this->data['fields']['list'] = [
            'id' => [ 'title' => 'id', 'value' => 'id', 'sorter'=>'id' ],
            'name' => [ 'title' => 'name', 'value' => function(Setting $row) {
	            return "
                {$row->name}
                <p class='text-sm text-muted'>
                    {$row->description}
                </p>
                ";
            }, 'sorter'=>'name' ],
            'key' => [ 'title' => 'key', 'value' => 'key', 'sorter'=>'key' ],
            'value' => [ 'title' => 'value', 'value' => 'value', 'sorter'=>'value' ],
            'status' => columntype_status(),
        ];

        $this->data['fields']['update'] = [
            'key' => [ 'title' => 'key', 'name' => 'key', 'type' => 'text',  'value' => 'key', ],
            'name' => [ 'title' => 'name', 'name' => 'name', 'type' => 'text',  'value' => 'name', ],
            'description' => [ 'title' => 'description', 'name' => 'description', 'type' => 'text',  'value' => 'description', ],
            'value' => [ 'title' => 'value', 'name' => 'value', 'type' => 'text',  'value' => 'value', ],
            'status' => fieldtype_status(),
        ];
    }

}
