<?php
namespace Dounasth\SettingsAdmin\App\Http;

use Dounasth\Backend\App\Http\Controllers\Admin\BackendBaseController;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class SettingsAdminController extends BackendBaseController {

    public function manageSettings($config_file) {
        $settings = Config::get($config_file, []);
        return View::make('settings-gui::manage')->withSettings($settings)->withConfigfile($config_file);
    }

    public function saveSettings() {
        $file = Input::get('configfile');
        list($package, $file) = explode('::', $file);
        $settings = Input::get('settings');

        $content = '<?php return '.var_export($settings, true).'; ?>';

        $config_file = Config::get($package.'::general.config_path').'/'.$file.'.php';

        if (file_exists($config_file)) {
            file_put_contents($config_file, $content);
        }
        return redirect()->route('settings', [Input::get('configfile')]);
    }

}
