<?php

\Eventy::addFilter('admin.top-right-menu', function($tree) {
	$tree = $tree + [
		'config' => [
			'label'   => 'Config',
			'href'    => route('config'),
			'icon'    => 'fa-cogs',
		],
		'settings' => [
			'label'   => 'Settings',
			'href'    => '#',
			'icon'    => 'fa-cogs',
			'submenu' => array(
				'general'     => array(
					'label' => 'General',
					'href'  => route( 'settings', array( 'backend-general' ) ),
					'icon'  => 'fa-user',
				),
				'site'        => array(
					'label' => 'Site',
					'href'  => route( 'settings', array( 'backend::site' ) ),
					'icon'  => 'fa-user',
				),
				'permissions' => array(
					'label' => 'Permissions',
					'href'  => route( 'settings', array( 'backend::permissions' ) ),
					'icon'  => 'fa-user',
				),
				'crud' => array(
					'label' => 'CRUD',
					'href'  => route( 'backend.settings.index' ),
					'icon'  => 'fa-cogs',
				),
			),
		]
	];
	return $tree;
}, 11, 1);

Route::middleware(['web', 'auth', 'auth.admin'])->namespace('Dounasth\SettingsAdmin\App\Http')->prefix('admin')->group(function() {
    Route::get('settings/{config_file}', array('as' => 'settings', 'uses' => 'SettingsAdminController@manageSettings'));
    Route::any('settings/save', array('as' => 'settings_save', 'uses' => 'SettingsAdminController@saveSettings'));

	Route::any('backend/settings/index', 'SettingsCrudController@index')->name('backend.settings.index');
	Route::any('backend/settings/add', 'SettingsCrudController@edit')->name('backend.settings.add');
	Route::any('backend/settings/edit/{id}', 'SettingsCrudController@edit')->name('backend.settings.edit');
	Route::any('backend/settings/save/{id?}', 'SettingsCrudController@save')->name('backend.settings.save');
	Route::any('backend/settings/delete/{id}', 'SettingsCrudController@delete')->name('backend.settings.delete');
});