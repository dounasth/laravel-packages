#SettingsAdmin


# install the package
composer require dounasth/settings-gui

# run the migration
php artisan vendor:publish --provider="Dounasth\SettingsAdmin\SettingsServiceProvider"
php artisan migrate

# [optional] insert some example dummy data to the database
php artisan db:seed --class="Dounasth\SettingsAdmin\database\seeds\SettingsTableSeeder"