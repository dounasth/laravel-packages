<?php

namespace Dounasth\Filesystems;

use Dounasth\Filesystems\Providers\BoxcomServiceProvider;
use Dounasth\Filesystems\Providers\GoogleDriveServiceProvider;
use Illuminate\Support\ServiceProvider;

class FilesystemsServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->app->register(GoogleDriveServiceProvider::class);
        $this->app->register(BoxcomServiceProvider::class);

//        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
//        $loader->alias('Breadcrumbs', 'DaveJamesMiller\Breadcrumbs\Facade');

        $this->registerMiddleware($router);
        $this->setupRoutes($router);
        $this->publishFiles();
        $this->loadHelpers();
    }

    public function registerMiddleware(\Illuminate\Routing\Router $router)
    {
    }

    public function setupRoutes(\Illuminate\Routing\Router $router)
    {
    }

    public function publishFiles()
    {
    }

    public function loadHelpers()
    {
        require_once realpath(__DIR__ . '/functions.php');
    }

    public function register()
    {
    }

    public function provides()
    {
        return array(
        );
    }

}
