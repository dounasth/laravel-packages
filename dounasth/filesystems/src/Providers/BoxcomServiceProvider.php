<?php

namespace Dounasth\Filesystems\Providers;

use FlysystemBox\BoxAdapter;
use Illuminate\Support\ServiceProvider;
use LaravelBox\LaravelBox;
use League\Flysystem\Filesystem;

class BoxcomServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \Storage::extend('boxcom', function($app, $config) {
            $adapter = new BoxAdapter(new LaravelBox('QGZfJJfhXV22IiORA9xhMa4Vg5B4GSqS'));
            return new Filesystem($adapter);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}





/*
'disks' => [

    // ...

    'google' => [
        'driver' => 'google',
        'clientId' => env('MAIN_GOOGLE_DRIVE_CLIENT_ID'),
        'clientSecret' => env('MAIN_GOOGLE_DRIVE_CLIENT_SECRET'),
        'refreshToken' => env('MAIN_GOOGLE_DRIVE_REFRESH_TOKEN'),
        'folderId' => env('MAIN_GOOGLE_DRIVE_FOLDER_ID'),
    ],

    'google_backup' => [
        'driver' => 'google',
        'clientId' => env('BACKUP_GOOGLE_DRIVE_CLIENT_ID'),
        'clientSecret' => env('BACKUP_GOOGLE_DRIVE_CLIENT_SECRET'),
        'refreshToken' => env('BACKUP_GOOGLE_DRIVE_REFRESH_TOKEN'),
        'folderId' => env('BACKUP_GOOGLE_DRIVE_FOLDER_ID'),
    ],

    // ...

],
*/