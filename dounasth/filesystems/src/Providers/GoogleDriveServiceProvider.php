<?php

namespace Dounasth\Filesystems\Providers;

use Illuminate\Support\ServiceProvider;

class GoogleDriveServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \Storage::extend('google', function($app, $config) {
            $client = new \Google_Client();
            $client->setClientId($config['clientId']);
            $client->setClientSecret($config['clientSecret']);
            $client->refreshToken($config['refreshToken']);
            $service = new \Google_Service_Drive($client);

            $options = [];
            if(isset($config['teamDriveId'])) {
                $options['teamDriveId'] = $config['teamDriveId'];
            }

            $adapter = new GoogleDriveAdapter($service, $config['folderId'], $options);

            return new \League\Flysystem\Filesystem($adapter);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}





/*
'disks' => [

    // ...

    'google' => [
        'driver' => 'google',
        'clientId' => env('MAIN_GOOGLE_DRIVE_CLIENT_ID'),
        'clientSecret' => env('MAIN_GOOGLE_DRIVE_CLIENT_SECRET'),
        'refreshToken' => env('MAIN_GOOGLE_DRIVE_REFRESH_TOKEN'),
        'folderId' => env('MAIN_GOOGLE_DRIVE_FOLDER_ID'),
    ],

    'google_backup' => [
        'driver' => 'google',
        'clientId' => env('BACKUP_GOOGLE_DRIVE_CLIENT_ID'),
        'clientSecret' => env('BACKUP_GOOGLE_DRIVE_CLIENT_SECRET'),
        'refreshToken' => env('BACKUP_GOOGLE_DRIVE_REFRESH_TOKEN'),
        'folderId' => env('BACKUP_GOOGLE_DRIVE_FOLDER_ID'),
    ],

    // ...

],
*/