<?php
/*
Event::listen('admin.left-menu', function(){
    if ( Route::getCurrentRoute()->getPrefix() == 'admin' ) {
        return [
            '<li class="header">CONTENT</li>',
            'tags' => array(
                'label' => 'Tags',
                'href' => route('content.tags.index'),
                'icon' => 'fa-cog',
            ),
            'photos' => array(
                'label' => 'Photos',
                'href' => route('content.photos.index'),
                'icon' => 'fa-cog',
            ),
        ];
    }
    else return [];
}, 999990);

*/

Route::middleware(['web'])->prefix( config('frontend-site.routes_prefix') )->namespace('Dounasth\Frontend\App\Http\Controllers')->group(function() {
    Route::any('/', 'HomeController@home')->name('site.home');
});