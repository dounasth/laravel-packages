<?php
namespace Dounasth\Frontend\App\Http\Controllers;

use Dounasth\Frontend\App\Http\Controllers\Admin\FrontendBaseController;
use Illuminate\Support\Facades\Event;

class HomeController extends FrontendBaseController
{
    public function home() {
//        $ews = Event::fire('admin.dashboard.widgets');
        $widgets = array();
//        foreach ($ews as $k => $v) {
//            $v->prepare();
//            $widgets[$k] = $v->html();
//        }
        return view()->make('frontend::home')->withWidgets($widgets);
    }
}
