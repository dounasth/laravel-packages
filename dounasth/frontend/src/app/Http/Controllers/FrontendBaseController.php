<?php
namespace Dounasth\Frontend\App\Http\Controllers;

use Dounasth\Backend\App\Http\Controllers\BootableBaseController;
use Illuminate\Support\Facades\Request;

class FrontendBaseController extends BootableBaseController
{

    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    protected function isRss()
    {
        return Request::get('rss', false);
    }

    protected function rss($data)
    {

        $feed = Rss::feed('2.0', 'UTF-8');
        $feed->channel(array('title' => 'Channel\'s title', 'description' => 'Channel\'s description', 'link' => 'http://www.test.com/'));
        foreach ($data as $row) {
            $feed->item(array(
                'title' => $row['title'],
                'description|cdata' => $row['description'],
                'link' => $row['link'],
                'enclosure' => $row['image'],
            ));
        }

        return Response::make($feed, 200, array('Content-Type' => 'text/xml'));
    }
}
