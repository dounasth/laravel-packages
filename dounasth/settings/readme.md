#Settings


# install the package
composer require dounasth/settings

# run the migration
php artisan vendor:publish --provider="Dounasth\Settings\SettingsServiceProvider"
php artisan migrate

# [optional] insert some example dummy data to the database
php artisan db:seed --class="Dounasth\Settings\database\seeds\SettingsTableSeeder"