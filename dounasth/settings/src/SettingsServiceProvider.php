<?php

namespace Dounasth\Settings;

use Dounasth\Settings\Models\Setting;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class SettingsServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot(\Illuminate\Routing\Router $router)
    {
	    // only use the Settings package if the Settings table is present in the database
	    if (!\App::runningInConsole() && count(Schema::getColumnListing('settings'))) {
		    // get all settings from the database
		    $settings = Setting::all();
		    // bind all settings to the Laravel config, so you can call them like
		    // Config::get('settings.contact_email')
		    foreach ($settings as $key => $setting) {
			    Config::set('settings.'.$setting->key, $setting->value);
		    }
	    }
	    // publish the migrations and seeds
	    $this->publishes([__DIR__.'/database/migrations/' => database_path('migrations')], 'migrations');

        $this->registerMiddleware($router);
        $this->setupRoutes($router);
        $this->publishFiles();
        $this->loadHelpers();
    }

    public function registerMiddleware(\Illuminate\Routing\Router $router)
    {
    }

    public function setupRoutes(\Illuminate\Routing\Router $router)
    {
    }

    public function publishFiles()
    {
    }

    public function loadHelpers()
    {
        require_once realpath(__DIR__ . '/functions.php');
    }

    public function register()
    {
    }

    public function provides()
    {
        return array(
        );
    }

}
