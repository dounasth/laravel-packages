<?php

namespace Dounasth\Filefinder;

use Illuminate\Support\ServiceProvider;

class FilefinderServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot(\Illuminate\Routing\Router $router)
    {
        // LOAD THE VIEWS
        // - first the published views (in case they have any changes)
        $this->loadViewsFrom(resource_path('views/vendor/dounasth/filefinder'), 'filefinder');
        // - then the stock views that come with the package, in case a published view might be missing
        $this->loadViewsFrom(realpath(__DIR__.'/views'), 'filefinder');

        // use the vendor configuration file as fallback
        $this->mergeConfigFrom(realpath(__DIR__ . '/config/filefinder.php'), 'filefinder');

        $this->registerMiddleware($router);
        $this->setupRoutes($router);
        $this->publishFiles();
        $this->loadHelpers();
    }

    public function registerMiddleware(\Illuminate\Routing\Router $router)
    {
    }

    public function setupRoutes(\Illuminate\Routing\Router $router)
    {
        // by default, use the routes file provided in vendor
        $routeFilePathInUse = __DIR__ . '/routes.5.6.php';
        $this->loadRoutesFrom($routeFilePathInUse);

        // but if there's a file with the use that one
        if (file_exists(base_path().'/routes/filefinder.php')) {
            $routeFilePathInUse = base_path().'/routes/filefinder.php';
            $this->loadRoutesFrom($routeFilePathInUse);
        }
    }

    public function publishFiles()
    {
        // publish config file
        $this->publishes([
            realpath(__DIR__ . '/config/filefinder.php') => config_path('filefinder.php')
        ], 'filefinder');
    }

    public function loadHelpers()
    {
        require_once realpath(__DIR__ . '/functions.php');
    }

    public function register()
    {
    }

    public function provides()
    {
        return array(
        );
    }

}
