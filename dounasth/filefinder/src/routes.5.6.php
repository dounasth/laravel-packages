<?php



\Eventy::addFilter('admin.left-menu', function($tree) {
	$tree = $tree + [
		'files_menu' => '<li class="header">FileSystems</li>',
		'files1' => array(
			'label' => 'Files',
			'href' => route('filesystem.elfinder'),
			'icon' => 'fa-cog',
		),
	];
	return $tree;
}, 100, 1);



Route::middleware(['web', 'auth', 'auth.admin'])->namespace('Dounasth\Filefinder\App\Controllers')->prefix('admin')->group(function() {
    Route::any('filesystem', ['as' => 'filesystem.elfinder', 'uses' => 'Filefinder@index']);
});
