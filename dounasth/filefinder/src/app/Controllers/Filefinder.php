<?php namespace Dounasth\Filefinder\App\Controllers;

use Barryvdh\Elfinder\ElfinderController;

class Filefinder extends ElfinderController
{
    public function index()
    {
        return view()->make('filefinder::elfinder')->with($this->getViewVars());
    }
}
