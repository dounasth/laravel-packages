<!-- text input -->
<div>
    <label>{!! $field['title'] !!}</label>
    <div class="input-group">
        <input
            type="text" class="form-control"
            id="data_{{ $field['name'] }}"
            name="data[{{ $field['name'] }}]"
            value="{{ old($field['name']) ? old($field['name']) : (is_string($field['value']) ? $row->{$field['value']} : $field['value'] ) }}"
        >
        <span class="input-group-btn">
            <button type="button" class="select-file-button btn btn-default" data-upateimage="#data_{{ $field['name'] }}">Image</button>
        </span>
    </div>
    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>
