@extends('backend::layout')

@include('crud::update.header')

@section('page-menu')
@stop

@include('crud::parts.show_fields_scripts')

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                {{ Form::open(array('route' => [$crud->routesPrefix.'save', $row->getKey()], 'method' => 'POST', 'role' => 'form', 'enctype'=>'multipart/form-data')) }}
                @csrf
                <div class="box-body">
                    @include('crud::parts.show_fields', ['fields' => $fields['update']])
                </div>
                <div class="box-footer">
                    <button class="btn btn-success" type="submit"><i class="fa fa-check-square-o"></i> Save</button>
                    <a class="btn btn-danger btn-cancel"><i class="fa fa-square-o"></i> Cancel</a>
                    <button class="btn btn-warning pull-right" type="submit" name="saveNew" value="1"><i class="fa fa-plus"></i> Save as new</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection