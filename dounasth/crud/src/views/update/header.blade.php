@section('page-title')
    Edit
@stop

@section('page-subtitle')
    dashboard subtitle, some description must be here
@stop

@push('breadcrumb')
<li class=""><a href="{{ url()->previous() }}">Manage</a></li>
<li class="active">Manage</li>
@endpush