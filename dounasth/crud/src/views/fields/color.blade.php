<!-- html5 color input -->
<div class="form-group">
    <label for="data_{{ $field['name'] }}" class="control-label col-lg-3">{!! $field['title'] !!}</label>
    <div class="col-lg-9">
        <input
            class="form-control"
            type="color"
            name="{{ $field['name'] }}"
            value="{{ old($field['name']) ? old($field['name']) : (is_string($field['value']) ? $row->{$field['value']} : $field['value'] ) }}"
            >
    </div>
    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>