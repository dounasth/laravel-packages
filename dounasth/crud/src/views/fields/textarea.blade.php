<!-- textarea -->
<div class="form-group">
    <label for="data_{{ $field['name'] }}" class="control-label col-lg-3">{!! $field['title'] !!}</label>
    <div class="col-lg-9">
        <textarea class="form-control ckeditor" id="data_{{ $field['name'] }}" name="data[{{ $field['name'] }}]" >{!! old($field['name']) ? old($field['name']) : (is_string($field['value']) ? $row->{$field['value']} : $field['value'] ) !!}</textarea>
        {{-- HINT --}}
        @if (isset($field['hint']))
            <p class="help-block">{!! $field['hint'] !!}</p>
        @endif
    </div>
</div>