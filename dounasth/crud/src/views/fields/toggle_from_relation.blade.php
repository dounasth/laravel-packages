<?php
$rel = $field['relation'];
$kf = resolveRelationKey($crud->model::first()->$rel());
$vf = $field['relation_texts'];
$values = $crud->model::first()->$rel()->getRelated()->pluck($vf, $kf);
$field_value = (is_string($field['value']) ? $row->{$field['value']} : $field['value'] );
?>
<div class="form-group">
    <label for="data_{{ $field['name'] }}" class="control-label col-lg-3">{!! $field['title'] !!}</label>
    <div class="col-lg-9">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            {{--@if ($entity_model::isColumnNullable($field['name']))--}}
            {{--<option value="">-</option>--}}
            {{--@endif--}}
            @if (fn_is_not_empty($values))
                @foreach ($values as $value => $text)
                    @if(old($field['name']) == $value || (is_null(old($field['name'])) && isset($field_value) && $field_value == $value))
                    <label class="btn btn-default active">
                        <input type="radio" name="data[{{ $field['name'] }}]" value="{{$value}}" id="option_{{$value}}" autocomplete="off" checked> {{$text}}
                    </label>
                    @else
                    <label class="btn btn-default">
                        <input type="radio" name="data[{{ $field['name'] }}]" value="{{$value}}" id="option_{{$value}}" autocomplete="off"> {{$text}}
                    </label>
                    @endif
                @endforeach
            @endif
        </div>
    </div>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>
<?php
$rel = $kf = $vf = $values = $field_value = $field_classes
	= null;
?>