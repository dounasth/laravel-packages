<div class="btn-group btn-group-sm pull-right">
    @if ($crud->can_edit)
        <a class="btn btn-info" href="{{ $crud->editRoute($row) }}"><i class="fa fa-edit"></i></a>
    @endif
    @foreach( $row_buttons as $button )
        <a class="btn {{ $button['class'] }}" href="{{ $button['route']($crud->routesPrefix, $row) }}"><i class="{{ $button['icon'] }}"></i> {{ $button['label'] or '' }}</a>
    @endforeach
    @if ($crud->can_delete || count($row_drop_buttons))
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu">
            @if ($crud->can_delete)
                @if ($crud->hasTrash() && $row->trashed())
                    <a class="btn btn-flat btn-info" href="{{route($crud->routesPrefix.'restore', [$row->id])}}"><i class="fa fa-refresh"></i> {{trans('backend::actions.restore')}}</a>
                    <li><a class="btn" href="{{ $crud->deleteRoute($row) }}"><i class="fa fa-trash"></i> Delete</a></li>
                @else
                    <li><a class="btn" href="{{ $crud->deleteRoute($row) }}"><i class="fa fa-trash"></i> Trash</a></li>
                @endif
            @endif
            @foreach( $row_drop_buttons as $button )
                <li><a class="btn {{ $button['class'] }}" href="{{ $button['route']($crud->routesPrefix, $row) }}"><i class="{{ $button['icon'] }}"></i> {{ $button['label'] or '' }}</a></li>
            @endforeach
        </ul>
    @endif
</div>