@if ($objects && $crud->gui['pagination'])
    <div class="row">
        <div class="col-md-4">
            <span class="like-btn-default">
                <a class="btn btn-default">
                Σύνολο {{ $objects->total() }} αποτελεσματα
                </a>
            </span>
            <div class="btn-group">
                <a class="btn btn-default">
                    <i class="fa fa-sort"></i> {{ itemsPerPage() . ' ανά σελίδα' }}
                </a>
                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle"><span class="caret"></span> <span class="sr-only"></span></button>
                <ul class="dropdown-menu">
                    @foreach (Config::get('app.items_per_page', [10,25,50,100]) as $v)
                        <li><a href="{{route('crud.funcs.set_items_per_page', [Route::currentRouteName(), $v])}}">{{ $v . ' ανά σελίδα' }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-8">
            <div class="pull-right">{{ $objects->appends(queryParamsForLinks())->links() }}</div>
        </div>
    </div>
@endif