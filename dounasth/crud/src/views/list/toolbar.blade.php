@if ($crud->gui['toolbar'])
    <div class="panel-heading clearfix" style="padding: 1px;">
        <a class="btn btn-default btn-xs" target="_blank" href="{{addParamToCurrentUrl('export', 'xls')}}">
            <i class="fa fa-download"></i> Excel
        </a>
    </div>
@endif