@extends('backend::layout')

@include('crud::list.header')

@section('page-menu')
    @if ($crud->gui['search'])
    <li role="presentation">
        <a role="menuitem" tabindex="-1"  class="btn" data-toggle="modal" data-target="#ordersSearchModal">
            <span class="fa fa-search"></span> Πλήρης Αναζήτηση
        </a>
    </li>
    @endif
    @if ($crud->can_add)
        <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ $crud->addRoute() }}"><i class="fa fa-plus"></i> Add new</a></li>
    @endif
    @if ($crud->can_delete)
        <li role="presentation"><a role="menuitem" tabindex="-1" href="#delete-many" data-root="#example1 tbody" class="delete-selected"><i class="fa fa-trash-o"></i> Delete selected</a></li>
    @endif
    @if ($crud->hasTrash())
        <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ route($crud->route) }}?trash=only"><i class="fa fa-trash-o"></i> Only Trashed</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ route($crud->route) }}?trash=with"><i class="fa fa-trash-o"></i> With Trashed</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ route($crud->route) }}"><i class="fa fa-trash-o"></i> Not Trashed</a></li>
    @endif
    {{--<li role="presentation" class="divider"></li>--}}
    @foreach( $grid_buttons as $button )
        <li role="presentation"><a class="{{ $button['class'] or '' }}" role="menuitem" tabindex="-1" href="{{ $button['route']($crud->routesPrefix, null) }}"><i class="{{ $button['icon'] or '' }}"></i> {{ $button['label'] or '' }}</a></li>
    @endforeach
@stop

@section('content')
@if ($crud->gui['search'])
    <!-- Modal -->
    <div class="modal fade" id="ordersSearchModal" tabindex="-1" role="dialog" aria-labelledby="ordersSearchModalLabel">
        <form class="clearfix" action="{{$form_route or route($route)}}" method="get">
            <input type="hidden" name="search[doing_search]" value="1"/>
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="ordersSearchModalLabel">Πλήρης Αναζήτηση</h4>
                    </div>
                    <div class="modal-body">
                        search form here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Άκυρο</button>
                        <button type="submit" class="btn btn-primary">Αναζήτηση</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endif

@include('crud::list.pagination', ['objects'=>$rows, 'divclass'=>'bottomside'])

<div class="row">
    <div class="panel panel-default">
        @include('crud::list.toolbar')
        <div class="panel-body">
            <table class="table table-responsive table-striped table-condensed" id="cvresults">
                @if ($crud->gui['table_head'])
                    <thead>
                    <tr>
                        <th>
                            <div class="btn-group btn-group-xs with-selected">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <input id="select_alll" type="checkbox" class="select_all"/>
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a class="ws-delete" href="#XX"><i class="fa fa-trash"></i> Διαγραφή</a></li>
                                </ul>
                            </div>
                        </th>
                        @foreach($fields['list'] as $k=>$v)
                            <th>
                                @if (isset($v['sorter']))
                                    {!! sortLabel($v['sorter'], $v['title']) !!}
                                @else
                                    {!! $v['title'] !!}
                                @endif
                            </th>
                        @endforeach
                        <th></th>
                    </tr>
                    </thead>
                @endif
                <tbody>
                @foreach( $rows as $row )
                    <?php $rowdot = array_dot( $row->toArray() ) ?>
                    <tr data-for="row_select_{{$row->getKey()}}">
                        <td class="dontselect">
                            <input id="row_select_{{$row->getKey()}}" data-id="{{$row->getKey()}}" type="checkbox"
                                   name="ad{{$row->getKey()}}" class="row_selector"/>
                        </td>
                        @foreach($fields['list'] as $k=>$v)
                            <td>
                                @if ( is_string($v['value']) )
                                    {!! $rowdot[$v['value']] or '--' !!}
                                @else
                                    {!! $v['value']($row) !!}
                                @endif
                            </td>
                        @endforeach
                        <td class="dontselect pull-right" style="width: 160px;">
                            @include('crud::list.buttons')
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@include('crud::list.pagination', ['objects'=>$rows, 'divclass'=>'bottomside'])

@endsection