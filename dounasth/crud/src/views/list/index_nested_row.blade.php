<?php $rowdot = array_dot($row->toArray()) ?>
<li class="dd-item dd3-item" data-id="{{$row->id}}">
    <div class="dd-handle dd3-handle "></div>
    <div class="dd3-content ">
        <div class="pull-left">
            <a class="translatable" href="#update">
                <?php $v = array_first($fields['list']) ?>
                @if ( is_callable($v['value']) )
                    {!! $v['value']($row) !!}
                @else
                    {!! $rowdot[$v['value']] or '--' !!}
                @endif
            </a>
            ( {{ $row->count  }} )
        </div>
        @if (@$checkable)
        <div class="pull-right">
            <div class="radio-inline">
                <label><input type="radio" name="selection" value="before"/> before</label>
            </div>
            <div class="radio-inline">
                <label><input type="radio" name="selection" value="after"/> after</label>
            </div>
        </div>
        @endif
        <div class="btn-group pull-right">
            @foreach($fields['list'] as $k=>$v)
                @if ( $v != array_first($fields['list']) )
                <a class="btn btn-sm btn-default hidden-sm hidden-xs" >
                    @if ( is_callable($v['value']) )
                        {!! $v['value']($row) !!}
                    @else
                        {!! $rowdot[$v['value']] or '--' !!}
                    @endif
                </a>
                @endif
            @endforeach

            {{--<a class="btn btn-sm btn-default hidden-sm hidden-xs" >{{@$row->seo->title}}</a>--}}
            {{--<a class="btn btn-sm btn-default hidden-sm hidden-xs" >{{$row->slug}}</a>--}}
            {{--<a class="btn btn-sm btn-default hidden-sm hidden-xs" href="#update"><i class="fa fa-edit"></i></a>--}}
            {{--<a class="btn btn-sm btn-danger" href="#delete"><i class="fa fa-trash"></i></a>--}}
            @include('crud::list.buttons')
        </div>
    </div>

    @if(count($row->children)>0)
    <ol class="dd-list">
        @foreach($row->children as $sub)
        @include('crud::list.index_nested_row', ['row'=>$sub])
        @endforeach
    </ol>
    @endif
</li>