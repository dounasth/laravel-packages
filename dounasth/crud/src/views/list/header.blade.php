@section('page-title')
    {{ $crud->title or '$crud->title'}}
@stop

@section('page-subtitle')
    {{ $crud->subtitle or '$crud->subtitle' }}
@stop

@push('breadcrumb')
<li class="active">Manage {{ $crud->crudName or '$crud->crudName' }} </li>
@endpush