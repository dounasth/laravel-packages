@push('after_styles')
<link href="{{ asset('vendor/adminlte/plugins/select2/select2.css') }}" rel="stylesheet">
<link href="{{ asset('packages/barryvdh/elfinder/colorbox.css') }}"  rel="stylesheet">
@endpush

@push('after_scripts')
<link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
<script src="{{ asset('vendor/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/plugins/ckeditor/ckeditor.js') }}"></script>

<script src="{{ asset('vendor/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('packages/barryvdh/elfinder/jquery.colorbox-min.js') }}"></script>

<script src="{{ asset('packages/barryvdh/elfinder/js/elfinder.full.js') }}"></script>
<script src="{{ asset('packages/barryvdh/elfinder/js/i18n/elfinder.'.app()->getLocale().'.js') }}"></script>

<script type="text/javascript" charset="UTF-8">

    function processSelectedFile(filePath, requestingField) {
        $('#' + requestingField).val(filePath).trigger('change');
    }

    jQuery(document).ready(function(){

        jQuery('.select-file-button').click(function () {
            var updateobj = jQuery(this).attr('data-upateimage');
        });

        jQuery('.select2-single').each(function(i, elm) {
            jQuery(elm).select2({
                templateResult: window[jQuery(elm).data('template') || 'select2Generic']
            });
        });
        jQuery('.select2-ajax').select2({
            ajax: {
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        query: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    var x = {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                    return x;
                }
            }
        });
        jQuery('.select2-multiple').select2({
            multiple: true
            ,closeOnSelect: false
        });
        jQuery('.select2-tags').select2({
            multiple: true
            ,closeOnSelect: false
            ,tags: true
        });
        jQuery('.select2-ajax-multiple').select2({

            multiple: true,
            closeOnSelect: false,
            ajax: {
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        query: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    var x = {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                    return x;
                }
            }
        });

//        jQuery("textarea, .textarea").wysihtml5();
//        CKEDITOR.replace('editor1');

    });
</script>
<div id="elfinder"></div>
@endpush