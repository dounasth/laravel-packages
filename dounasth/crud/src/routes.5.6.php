<?php

Route::middleware(['web', 'auth', 'auth.admin'])->namespace('Dounasth\Crud\App\Controllers')->prefix('admin')->group(function() {
    Route::any(
        '/crud/setItemsPerPage/{route_name}/{items_per_page}',
        'CrudFuncsController@setItemsPerPage'
    )->name('crud.funcs.set_items_per_page');
});


\Eventy::addFilter('config.menu', function($tree) {
	$tree = $tree + [
			'crud' => array(
				'label' => 'CRUD',
				'href' => '#',
				'icon' => 'fa-cogs',
				'submenu' => array(
				)
			),
		];
	return $tree;
}, 40, 1);

//  Just reference
//Route::any('admin/crud/save-node-pos', 'CrudController@savePositionedNode')->name('crud.list.save-node-pos');



Route::get('/data/models', 'Dounasth\Crud\App\Controllers\ModelController@index')->name('api.data.model.index');
Route::get('/data/models/{model}/all/{with_relations?}', 'Dounasth\Crud\App\Controllers\ModelController@all')->name('api.data.model.all');
Route::get('/data/models/{model}/one/{id}/{with_relations?}', 'Dounasth\Crud\App\Controllers\ModelController@one')->name('api.data.model.one');
Route::get('/data/models/{model}/select2', 'Dounasth\Crud\App\Controllers\ModelController@select2')->name('api.data.model.select2');