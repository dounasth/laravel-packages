<?php

function resolveRelationKey($relation) {
    if ( ends_with(get_class($relation), 'BelongsTo') ) {
        return $relation->getOwnerKey();
    }
    else die('not defined a way to get the key');
}

function columntype_status() {
	return [ 'title' => 'status', 'value' => function($row) {
		if ($row->status == 'A') { return "<i class='fa fa-check text-success'></i>"; }
		elseif ($row->status == 'D') { return "<i class='fa fa-ban text-danger'></i>"; }
		else return $row->status;
	}, 'sorter'=>'status' ];
}
function fieldtype_status() {
	return [
		'title' => 'status', 'name' => 'status', 'value' => 'status',
		'type' => 'toggle_from_array', 'values' => \Illuminate\Support\Collection::make([
			'A' => 'Active',
			'D' => 'Disabled',
		])
	];
}