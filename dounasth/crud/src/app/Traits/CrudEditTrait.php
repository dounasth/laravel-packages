<?php

namespace Dounasth\Crud\Traits;

use Dounasth\Backend\App\Helpers\AlertMessage;
use Dounasth\Commerce\App\Models\Order\Order;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;

trait CrudEditTrait
{
    public $can_add = true;
    public $can_edit = true;
    public $view_add = 'crud::update.add';
    public $view_edit = 'crud::update.edit';

    public function bootCrudEditTrait(){
        /*Event::listen('crud.list.row.buttons', function(){
            return [ 'icon'=>'fa fa-edit', 'class'=>'btn-info', 'route'=> function($routesPrefix, $row){  return CrudEditTrait::editRoute($routesPrefix, $row); }, ];
        });*/
        /*Event::listen('crud.list.grid.buttons', function(){
            return [ 'label'=>'Add new ', 'icon'=>'fa fa-plus', 'route'=>function($routesPrefix, $row){  return CrudEditTrait::addRoute($routesPrefix, $row); }, ];
        });*/
    }

    //----------------------------------------------------------------
    //  ADD
    //----------------------------------------------------------------
    public function addRoute()
    {
        return route($this->routesPrefix.'add');
    }

    //----------------------------------------------------------------
    //  EDIT
    //----------------------------------------------------------------
    public function editRoute($row)
    {
        return route($this->routesPrefix.'edit', [$row->getKey()]);
    }
    public function edit($id=0)
    {
        $this->data['row'] = $this->model::find($id);
        if (!$this->data['row']) {
            $this->data['row'] = new $this->model;
        }
        return view($this->view_edit, $this->data);
    }

    public function save($id=0)
    {
        $data = Input::get('data', false);

        if (Input::get('saveNew', 0)) {
            $object = new $this->model();
        }
        else {
            $object = $this->model::find($id);
            if (!$object) {
                $object = new $this->model();
            }
        }

        $object->fill( $data );
        $object->save();
        return redirect()->route($this->routesPrefix.'edit', [$object->getKey()])->withMessage( AlertMessage::success('Saved') );
    }

}
