<?php
namespace Dounasth\Crud\Traits;

trait CrudModelTrait
{

    public function scopeSearchAnything($query, $params)
    {
        $params = $this->searchDefaultParams($params);
        extract($params);
        return $query;
    }

    public static function searchDefaultParams($params = [])
    {
        $params = $params != null ? $params : [];
        return $params = array_merge(array(
//            'id' => '',
//            'id_op' => '=',
        ), array_filter($params));
    }

    public function scopeSetOrder($query)
    {
        return $query->orderBy(
            orderType($this->getKeyName()),
            orderDirection()
        );
    }


}