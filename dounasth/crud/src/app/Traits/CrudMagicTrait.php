<?php
namespace Dounasth\Crud\Traits;

trait CrudMagicTrait
{
    public function __get($field) {
        if( in_array( $field, ['can_delete', 'can_add', 'can_edit', 'can_list']) ) { return false; }
        else return false;
    }

}