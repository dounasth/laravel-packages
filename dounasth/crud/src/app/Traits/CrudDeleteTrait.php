<?php

namespace Dounasth\Crud\Traits;

use Dounasth\Backend\App\Helpers\AlertMessage;
use Illuminate\Support\Facades\Event;

trait CrudDeleteTrait
{
    public $can_delete = true;

    public function bootCrudDeleteTrait(){
        /*Event::listen('crud.list.row.drop_buttons', function(){
            return [ 'label'=>'Delete', 'icon'=>'fa fa-trash', 'class'=>'', 'route'=>function($routesPrefix, $row){  return CrudDeleteTrait::deleteRoute($routesPrefix, $row); }, ];
        });*/
    }

    public function deleteRoute($row)
    {
        return route($this->routesPrefix.'delete', [$row->getKey()]);
    }

//    public function delete($id)
//    {
//        return (string) $this->model->findOrFail($id)->delete();
//    }

    public function delete($id) {
        $o = ($this->hasTrash()) ? $this->model::withTrashed() : new $this->model;
        $o = $o->where($o->getKeyName(),'=',$id)->first();
        if ($o) {
            if ($this->hasTrash() && $o->trashed()) {
                $o->forceDelete();
            }
            else {
                $o->delete();
            }
            $message = AlertMessage::success("$this->model {$o->name} ({$o->getKey()}) deleted");
            return redirect()->back()->withMessage( $message );
        }
        else {
            return redirect()->back()->withMessage( $message = AlertMessage::error("$this->model for deletion not found") );
        }
    }

    public function deleteMany($ids) {
        $ids = explode(',', $ids);

        foreach ($ids as $id) {
            $o = ($this->hasTrash()) ? $this->model::withTrashed() : $this->model;
            $o = $o->where('id','=',$id)->first();
            if ($o->id) {
//                if ($o->trashed()) {
                $o->forceDelete();
//                }
//                else {
//                    $o->delete();
//                }
            }
        }
        $message = AlertMessage::success("$this->models deleted");
        return redirect()->back()->withMessage( $message );
    }

    public function restoreTrashed($id) {
        if ($this->hasTrash()) {
            $o = $this->model::withTrashed();
            $o = $o->where($o->getKeyName(),'=',$id)->first();
            $o->restore();
            $message = AlertMessage::success("$this->model {$o->getKey()} ({$o->getKey()}) deleted");
        }
        else $message = AlertMessage::error("$this->model has no trash");
        return redirect()->back()->withMessage( $message );
    }
}
