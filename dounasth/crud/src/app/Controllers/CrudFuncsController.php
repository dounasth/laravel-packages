<?php

namespace Dounasth\Crud\App\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class CrudFuncsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function setItemsPerPage($route_name, $items_per_page)
    {
        Session::put($route_name . '.items_per_page', Input::get('items_per_page', $items_per_page ? $items_per_page : 10));
        return redirect()->back();
    }

}
