<?php

namespace Dounasth\Crud\App\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ModelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function index()
    {
        $path = app_path() . "/Models";
        dd($this->getModels($path));
        exit;
    }

    public function all($model, $with_relations='')
    {
        $model = $this->find($model);
        $model = new $model();
        $data = $model->with(array_filter(explode(',', $with_relations)))->paginate(5);
        echo $data->toJson(JSON_UNESCAPED_UNICODE);
        exit;
    }

    public function one($model, $id, $with_relations='')
    {
        $model = $this->find($model);
        $model = new $model();
        $data = $model->with(array_filter(explode(',', $with_relations)))->where('item_id', '=', $id)->first();
        if (!$data) {
            $data = new $model();
        }
        echo $data->toJson(JSON_UNESCAPED_UNICODE);
        exit;
    }

    public function select2($model)
    {
        $model = $this->find($model);
        $query = Input::get('query', null);
//        $model = "App\\Models\\$model";
        $data['items'] = $model::select2($query);
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit;
    }

    public function getModels($path){
        $out = [];
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.' or $result === '..') continue;
            $filename = $path . '/' . $result;
            if (is_dir($filename)) {
                $out = array_merge($out, $this->getModels($filename));
            }else{
                $out[] = substr($result,0,-4);
            }
        }
        return $out;
    }

    private function find($model) {
        $model = str_ireplace('.','\\',$model);
        return $model;
    }
}
