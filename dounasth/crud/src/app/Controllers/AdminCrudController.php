<?php

namespace Dounasth\Crud\App\Controllers;

use Dounasth\Crud\Traits\CrudDeleteTrait;
use Dounasth\Crud\Traits\CrudEditTrait;

class AdminCrudController extends BaseCrudController
{
    use CrudDeleteTrait, CrudEditTrait;

    public $title = 'Title';
    public $subtitle = 'subtitle';
    public $breadcrumb = 'Breadcrumb';
}
