<?php

namespace Dounasth\Crud\App\Controllers;

use Dounasth\Backend\App\Http\Controllers\BootableBaseController;
use Dounasth\Crud\App\Exceptions\ModelNotSetException;
use Dounasth\Crud\Traits\CrudMagicTrait;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;

class BaseCrudController extends BootableBaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    use CrudMagicTrait;

    public $model = null;
    public function hasTrash() {
        return in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($this->model)) && ! $this->forceDeleting;
    }
    public $view = 'crud::list.index';
    public $route = '';
    public $routesPrefix = '';
    public $crudName = '';
    public $data = [];
    public $with = null;

    public $isNested = false;
    public $view_nested = 'crud::list.index_nested';

    public $gui = [
    	'pagination' => true,
    	'toolbar' => true,
    	'search' => true,
    	'table_head' => true,
    ];

    public $buttons = [];

    public function __construct()
    {
        if (!$this->model) throw new ModelNotSetException();
        parent::__construct();
        $this->prep();
        $this->init();
    }

    public function prep()
    {
        $this->data = array(
            'crud' => $this,
            'crudName' => $this->crudName,
            'route' => $this->route,
            'row_buttons' => Event::fire('crud.list.row.buttons'),
            'row_drop_buttons' => Event::fire('crud.list.row.drop_buttons'),
            'grid_buttons' => Event::fire('crud.list.grid.buttons'),
        );
    }

    public function init() {}

    public function joins($query) { return $query; }

    public function index()
    {
        $this->data['params'] = $params = $this->model::searchDefaultParams(Input::get('search'));
        $this->data['rows'] = $this->model::searchAnything($params)->setOrder();
        $this->data['rows'] = $this->joins($this->data['rows']);

        if ($this->hasTrash()) {
            $trash = Input::get('trash', false);
            if ($trash == 'only') {
                $this->data['rows'] = $this->data['rows']->onlyTrashed();
            }
            elseif ($trash == 'with') {
                $this->data['rows'] = $this->data['rows']->withTrashed();
            }
        }

        if ($this->with) {
            $this->data['rows'] = $this->data['rows']->with($this->with);
        }
        if ($this->isNested) {
            $this->data['rows'] = $this->data['rows']->defaultOrder()->get()->toTree();
        }
        else $this->data['rows'] = $this->data['rows']->paginate(itemsPerPage());
        return view( $this->isNested ? $this->view_nested : $this->view, $this->data);
    }

    public function savePositionedNode()
    {
        $item = Input::get('item', false);
        $parent = Input::get('parent', 0);
        $sibling = Input::get('sibling', false);
        $pos = Input::get('pos', '');

        $node = $this->model::findOrFail($item);
        $neighbor = $this->model::findOrFail($sibling);

        $node->parent_id = $parent;
        $node->save();

        $node = $this->model::findOrFail($item);
        if ($neighbor) {
            if ($pos == 'before') {
                $node->beforeNode($neighbor);
            } elseif ($pos == 'after') {
                $node->afterNode($neighbor);
            }
        }
        $node->save();

        return response()->json($node->toArray());
    }
}
