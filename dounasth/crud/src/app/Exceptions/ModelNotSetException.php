<?php
namespace Dounasth\Crud\App\Exceptions;

use Exception;

class ModelNotSetException extends Exception
{
	public function __construct( $message = "", $code = 0, Throwable $previous = null ) {
		$message = (fn_is_not_empty($message)) ? $message : 'Model not set for controller';
		parent::__construct( $message, $code, $previous );
	}
}