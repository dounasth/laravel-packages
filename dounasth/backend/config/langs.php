<?php return array (
    'default_site' => 'el',
    'default_admin' => 'el',
    'fallback_site' => 'en',
    'fallback_admin' => 'en',
    'list' =>
        array (
            'el' =>
                array (
                    'name' => 'Ελληνικά',
                    'locale' => 'el',
                    'enabled' => '1',
                ),
            'en' =>
                array (
                    'name' => 'English',
                    'locale' => 'en',
                    'enabled' => '1',
                ),
        ),
); ?>