<?php
return array(
    'all' => array(
        'user_can_register_for' => 'backend',
        'user.view' => 'backend',
        'user.create' => 'backend',
        'user.delete' => 'backend',
        'user.update' => 'backend',
    ),
);
