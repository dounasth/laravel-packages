<?php

return array(
    'path' => dirname(__FILE__).'/../',
    'asset_path' => env('APP_URL', 'http://localhost'),
    'theme_path' => env('APP_URL', 'http://localhost'),
    'config_path' => dirname(__FILE__),

    'mail.from.address' => 'info@laraport.local',
    'mail.from.name' => 'Laraport',

    'notify_mail' => '',
    'pingo.title' => 'LuxuryTales.gr',
    'pingo.blogurl' => 'https://www.luxurytales.gr',
    'pingo.rssurl' => '',





    /*
|--------------------------------------------------------------------------
| Look & feel customizations
|--------------------------------------------------------------------------
|
| Make it yours.
|
*/

    // Project name. Shown in the breadcrumbs and a few other places.
    'project_name' => 'Backend',

    // Menu logos
    'logo_lg'   => '<b>Back</b>end',
    'logo_mini' => '<b>B</b>E',

    // Developer or company name. Shown in footer.
    'developer_name' => 'Thodoris Ntounas',

    // Developer website. Link in footer.
    'developer_link' => 'http://www.webondemand.gr',

    // Show powered by Laravel Backpack in the footer?
    'show_powered_by' => false,

    // The AdminLTE skin. Affects menu color and primary/secondary colors used throughout the application.
    'skin' => 'skin-green',
    'sidebar-mini' => '',
    // Options: skin-black, skin-blue, skin-purple, skin-red, skin-yellow, skin-green, skin-blue-light, skin-black-light, skin-purple-light, skin-green-light, skin-red-light, skin-yellow-light

    // Date & Datetime Format Syntax: https://github.com/jenssegers/date#usage
    // (same as Carbon)
    'default_date_format'     => 'j F Y',
    'default_datetime_format' => 'j F Y H:i',

    /*
    |--------------------------------------------------------------------------
    | Registration Open
    |--------------------------------------------------------------------------
    |
    | Choose whether new users are allowed to register.
    | This will show up the Register button in the menu and allow access to the
    | Register functions in AuthController.
    |
    */

    'registration_open' => (env('APP_ENV') == 'local') ? true : false,

    /*
    |--------------------------------------------------------------------------
    | Routing
    |--------------------------------------------------------------------------
    */

    // The prefix used in all base routes (the 'admin' in admin/dashboard)
    'route_prefix' => 'admin',

    // Set this to false if you would like to use your own AuthController and PasswordController
    // (you then need to setup your auth routes manually in your routes.php file)
    'setup_auth_routes' => true,

    // Set this to false if you would like to skip adding the dashboard routes
    // (you then need to overwrite the login route on your AuthController)
    'setup_dashboard_routes' => true,

    /*
    |--------------------------------------------------------------------------
    | User Model
    |--------------------------------------------------------------------------
    */

    // Fully qualified namespace of the User model
    'user_model_fqn' => '\Dounasth\Backend\App\Models\User',

	// What kind of avatar will you like to show to the user?
	// Default: gravatar (automatically use the gravatar for his email)
	// Other options:
	// - placehold (generic image with his first letter)
	// - example_method_name (specify the method on the User model that returns the URL)
    'avatar_type' => 'gravatar',
);
