# Installation

Make a project e.g. composer create-project laravel/laravel {directory} 4.2 --prefer-dist

- Add `"dounasth/backend": "dev-master"` to your composer.json
- Set `"minimum-stability": "dev"` to your composer.json
- Set `"prefer": "dist"` to your composer.json
- Run `php composer.phar install` or `composer update`
- Add `'Dounasth\Backend\BackendServiceProvider',` to your `config/app.php` file under `$providers`
- Go to /install register admin user and you are ready to go

- Backend defines a home route, so if you have another home route defined in app/routes.php, you will need to remove it or copy the backend home route if you want to extend it.

# Optional
- Add your service credentials to `app/config/packages/atticmedia/anvard/hybridauth.php`




php artisan key:generate 
php artisan vendor:publish --provider="Dounasth\Backend\BackendServiceProvider" --force
php artisan migrate
php artisan db:seed --class="Dounasth\Backend\BackendDatabaseSeeder"


remove Auth::routes(); from routes/web.php if you have ran make:auth

For the user model either:
1 delete App\User (default laravel user model)
2 replace references with Dounasth\Backend\App\Models\User

or:
change App\User to extend Dounasth\Backend\App\Models\User
