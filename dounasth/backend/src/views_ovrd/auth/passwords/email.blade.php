@extends('backend::layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('backend::b.reset_password') }}</h3>

                    <div class="box-tools pull-right">
                        @if (config('backend-general.registration_open'))
                            <a href="{{ route('login') }}" class="btn btn-box-tool" >
                                <i class="fa fa-sign-in"></i>
                                {{ trans('backend::b.login') }}
                            </a>
                            <a href="{{ route('register') }}" class="btn btn-box-tool" >
                                <i class="fa fa-user-plus"></i>
                                {{ trans('backend::b.register') }}
                            </a>
                        @endif
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ trans('backend::b.reset_password_button') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
