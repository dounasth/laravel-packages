<?php

define( 'MINUTE_IN_SECONDS', 60 );
define( 'HOUR_IN_SECONDS',   60 * MINUTE_IN_SECONDS );
define( 'DAY_IN_SECONDS',    24 * HOUR_IN_SECONDS   );
define( 'WEEK_IN_SECONDS',    7 * DAY_IN_SECONDS    );
define( 'MONTH_IN_SECONDS',  30 * DAY_IN_SECONDS    );
define( 'YEAR_IN_SECONDS',  365 * DAY_IN_SECONDS    );

function canPreview() {
    return ($_SESSION['preview'] == 'Y');
}

if (! function_exists('ff')) {
	/**
	 * Dump the passed variables and end the script.
	 *
	 * @param  mixed  $args
	 * @return void
	 */
	function ff(...$args)
	{
		foreach ($args as $x) {
			(new \Illuminate\Support\Debug\Dumper())->dump($x);
		}
	}
}

function niceprintr($v) {
    global $command;
    if ($command) {
        $command->line($v);
    }
    else {
        echo '<pre>';
        print_r($v);
        echo '</pre>';
    }
}

function verbose($v) {
    if (isset($_REQUEST['verbose'])) {
        echo '<pre>';
        print_r($v);
        echo '</pre>';
    }
}

function fn_to_array($array) {
    $res = array();
    if ($array) {
        foreach ($array as $k => $v) {
            if (!is_string($v)) {
                $res[$k] = fn_to_array($v);
            } else
                $res[$k] = $v;
        }
    }
    return $res;
}

function seems_utf8($str) {
    $length = strlen($str);
    for ($i = 0; $i < $length; $i++) {
        $c = ord($str[$i]);
        if ($c < 0x80)
            $n = 0;# 0bbbbbbb
        elseif (($c & 0xE0) == 0xC0)
            $n = 1;# 110bbbbb
        elseif (($c & 0xF0) == 0xE0)
            $n = 2;# 1110bbbb
        elseif (($c & 0xF8) == 0xF0)
            $n = 3;# 11110bbb
        elseif (($c & 0xFC) == 0xF8)
            $n = 4;# 111110bb
        elseif (($c & 0xFE) == 0xFC)
            $n = 5;# 1111110b
        else
            return false;# Does not match any model
        for ($j = 0; $j < $n; $j++) { # n bytes matching 10bbbbbb follow ?
            if (( ++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
                return false;
        }
    }
    return true;
}

function remove_accents($string) {
    if (!preg_match('/[\x80-\xff]/', $string))
        return $string;

    if (seems_utf8($string)) {
        $chars = array(
            // Decompositions for Latin-1 Supplement
            chr(195) . chr(128) => 'A', chr(195) . chr(129) => 'A',
            chr(195) . chr(130) => 'A', chr(195) . chr(131) => 'A',
            chr(195) . chr(132) => 'A', chr(195) . chr(133) => 'A',
            chr(195) . chr(134) => 'AE', chr(195) . chr(135) => 'C',
            chr(195) . chr(136) => 'E', chr(195) . chr(137) => 'E',
            chr(195) . chr(138) => 'E', chr(195) . chr(139) => 'E',
            chr(195) . chr(140) => 'I', chr(195) . chr(141) => 'I',
            chr(195) . chr(142) => 'I', chr(195) . chr(143) => 'I',
            chr(195) . chr(144) => 'D', chr(195) . chr(145) => 'N',
            chr(195) . chr(146) => 'O', chr(195) . chr(147) => 'O',
            chr(195) . chr(148) => 'O', chr(195) . chr(149) => 'O',
            chr(195) . chr(150) => 'O', chr(195) . chr(153) => 'U',
            chr(195) . chr(154) => 'U', chr(195) . chr(155) => 'U',
            chr(195) . chr(156) => 'U', chr(195) . chr(157) => 'Y',
            chr(195) . chr(158) => 'TH', chr(195) . chr(159) => 's',
            chr(195) . chr(160) => 'a', chr(195) . chr(161) => 'a',
            chr(195) . chr(162) => 'a', chr(195) . chr(163) => 'a',
            chr(195) . chr(164) => 'a', chr(195) . chr(165) => 'a',
            chr(195) . chr(166) => 'ae', chr(195) . chr(167) => 'c',
            chr(195) . chr(168) => 'e', chr(195) . chr(169) => 'e',
            chr(195) . chr(170) => 'e', chr(195) . chr(171) => 'e',
            chr(195) . chr(172) => 'i', chr(195) . chr(173) => 'i',
            chr(195) . chr(174) => 'i', chr(195) . chr(175) => 'i',
            chr(195) . chr(176) => 'd', chr(195) . chr(177) => 'n',
            chr(195) . chr(178) => 'o', chr(195) . chr(179) => 'o',
            chr(195) . chr(180) => 'o', chr(195) . chr(181) => 'o',
            chr(195) . chr(182) => 'o', chr(195) . chr(184) => 'o',
            chr(195) . chr(185) => 'u', chr(195) . chr(186) => 'u',
            chr(195) . chr(187) => 'u', chr(195) . chr(188) => 'u',
            chr(195) . chr(189) => 'y', chr(195) . chr(190) => 'th',
            chr(195) . chr(191) => 'y',
            // Decompositions for Latin Extended-A
            chr(196) . chr(128) => 'A', chr(196) . chr(129) => 'a',
            chr(196) . chr(130) => 'A', chr(196) . chr(131) => 'a',
            chr(196) . chr(132) => 'A', chr(196) . chr(133) => 'a',
            chr(196) . chr(134) => 'C', chr(196) . chr(135) => 'c',
            chr(196) . chr(136) => 'C', chr(196) . chr(137) => 'c',
            chr(196) . chr(138) => 'C', chr(196) . chr(139) => 'c',
            chr(196) . chr(140) => 'C', chr(196) . chr(141) => 'c',
            chr(196) . chr(142) => 'D', chr(196) . chr(143) => 'd',
            chr(196) . chr(144) => 'D', chr(196) . chr(145) => 'd',
            chr(196) . chr(146) => 'E', chr(196) . chr(147) => 'e',
            chr(196) . chr(148) => 'E', chr(196) . chr(149) => 'e',
            chr(196) . chr(150) => 'E', chr(196) . chr(151) => 'e',
            chr(196) . chr(152) => 'E', chr(196) . chr(153) => 'e',
            chr(196) . chr(154) => 'E', chr(196) . chr(155) => 'e',
            chr(196) . chr(156) => 'G', chr(196) . chr(157) => 'g',
            chr(196) . chr(158) => 'G', chr(196) . chr(159) => 'g',
            chr(196) . chr(160) => 'G', chr(196) . chr(161) => 'g',
            chr(196) . chr(162) => 'G', chr(196) . chr(163) => 'g',
            chr(196) . chr(164) => 'H', chr(196) . chr(165) => 'h',
            chr(196) . chr(166) => 'H', chr(196) . chr(167) => 'h',
            chr(196) . chr(168) => 'I', chr(196) . chr(169) => 'i',
            chr(196) . chr(170) => 'I', chr(196) . chr(171) => 'i',
            chr(196) . chr(172) => 'I', chr(196) . chr(173) => 'i',
            chr(196) . chr(174) => 'I', chr(196) . chr(175) => 'i',
            chr(196) . chr(176) => 'I', chr(196) . chr(177) => 'i',
            chr(196) . chr(178) => 'IJ', chr(196) . chr(179) => 'ij',
            chr(196) . chr(180) => 'J', chr(196) . chr(181) => 'j',
            chr(196) . chr(182) => 'K', chr(196) . chr(183) => 'k',
            chr(196) . chr(184) => 'k', chr(196) . chr(185) => 'L',
            chr(196) . chr(186) => 'l', chr(196) . chr(187) => 'L',
            chr(196) . chr(188) => 'l', chr(196) . chr(189) => 'L',
            chr(196) . chr(190) => 'l', chr(196) . chr(191) => 'L',
            chr(197) . chr(128) => 'l', chr(197) . chr(129) => 'L',
            chr(197) . chr(130) => 'l', chr(197) . chr(131) => 'N',
            chr(197) . chr(132) => 'n', chr(197) . chr(133) => 'N',
            chr(197) . chr(134) => 'n', chr(197) . chr(135) => 'N',
            chr(197) . chr(136) => 'n', chr(197) . chr(137) => 'N',
            chr(197) . chr(138) => 'n', chr(197) . chr(139) => 'N',
            chr(197) . chr(140) => 'O', chr(197) . chr(141) => 'o',
            chr(197) . chr(142) => 'O', chr(197) . chr(143) => 'o',
            chr(197) . chr(144) => 'O', chr(197) . chr(145) => 'o',
            chr(197) . chr(146) => 'OE', chr(197) . chr(147) => 'oe',
            chr(197) . chr(148) => 'R', chr(197) . chr(149) => 'r',
            chr(197) . chr(150) => 'R', chr(197) . chr(151) => 'r',
            chr(197) . chr(152) => 'R', chr(197) . chr(153) => 'r',
            chr(197) . chr(154) => 'S', chr(197) . chr(155) => 's',
            chr(197) . chr(156) => 'S', chr(197) . chr(157) => 's',
            chr(197) . chr(158) => 'S', chr(197) . chr(159) => 's',
            chr(197) . chr(160) => 'S', chr(197) . chr(161) => 's',
            chr(197) . chr(162) => 'T', chr(197) . chr(163) => 't',
            chr(197) . chr(164) => 'T', chr(197) . chr(165) => 't',
            chr(197) . chr(166) => 'T', chr(197) . chr(167) => 't',
            chr(197) . chr(168) => 'U', chr(197) . chr(169) => 'u',
            chr(197) . chr(170) => 'U', chr(197) . chr(171) => 'u',
            chr(197) . chr(172) => 'U', chr(197) . chr(173) => 'u',
            chr(197) . chr(174) => 'U', chr(197) . chr(175) => 'u',
            chr(197) . chr(176) => 'U', chr(197) . chr(177) => 'u',
            chr(197) . chr(178) => 'U', chr(197) . chr(179) => 'u',
            chr(197) . chr(180) => 'W', chr(197) . chr(181) => 'w',
            chr(197) . chr(182) => 'Y', chr(197) . chr(183) => 'y',
            chr(197) . chr(184) => 'Y', chr(197) . chr(185) => 'Z',
            chr(197) . chr(186) => 'z', chr(197) . chr(187) => 'Z',
            chr(197) . chr(188) => 'z', chr(197) . chr(189) => 'Z',
            chr(197) . chr(190) => 'z', chr(197) . chr(191) => 's',
            // Decompositions for Latin Extended-B
            chr(200) . chr(152) => 'S', chr(200) . chr(153) => 's',
            chr(200) . chr(154) => 'T', chr(200) . chr(155) => 't',
            // Euro Sign
            chr(226) . chr(130) . chr(172) => 'E',
            // GBP (Pound) Sign
            chr(194) . chr(163) => '');

        $string = strtr($string, $chars);
    } else {
        // Assume ISO-8859-1 if not UTF-8
        $chars['in'] = chr(128) . chr(131) . chr(138) . chr(142) . chr(154) . chr(158)
                . chr(159) . chr(162) . chr(165) . chr(181) . chr(192) . chr(193) . chr(194)
                . chr(195) . chr(196) . chr(197) . chr(199) . chr(200) . chr(201) . chr(202)
                . chr(203) . chr(204) . chr(205) . chr(206) . chr(207) . chr(209) . chr(210)
                . chr(211) . chr(212) . chr(213) . chr(214) . chr(216) . chr(217) . chr(218)
                . chr(219) . chr(220) . chr(221) . chr(224) . chr(225) . chr(226) . chr(227)
                . chr(228) . chr(229) . chr(231) . chr(232) . chr(233) . chr(234) . chr(235)
                . chr(236) . chr(237) . chr(238) . chr(239) . chr(241) . chr(242) . chr(243)
                . chr(244) . chr(245) . chr(246) . chr(248) . chr(249) . chr(250) . chr(251)
                . chr(252) . chr(253) . chr(255);

        $chars['out'] = "EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy";

        $string = strtr($string, $chars['in'], $chars['out']);
        $double_chars['in'] = array(chr(140), chr(156), chr(198), chr(208), chr(222), chr(223), chr(230), chr(240), chr(254));
        $double_chars['out'] = array('OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th');
        $string = str_replace($double_chars['in'], $double_chars['out'], $string);
    }

    return $string;
}

function slugify($title) {
    $title = greeklish($title);
    $title = strip_tags($title);
    $title = remove_accents($title);
    // Preserve escaped octets.
    $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
    // Remove percent signs that are not part of an octet.
    $title = str_replace('%', '', $title);
    // Restore octets.
    $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);
    $title = strtolower($title);
    $title = preg_replace('/&.+?;/', '', $title); // kill entities
    $title = str_replace('.', '-', $title);
    $title = str_replace(',', '-', $title);
    $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
    $title = preg_replace('/\s+/', '-', $title);
    $title = preg_replace('|-+|', '-', $title);
    $title = trim($title, '-');

    return $title;
}

function greeklish($str) {
    return strtolower(strtr($str, array(
        'Α' => 'A', 'Ά' => 'A', 'α' => 'a', 'ά' => 'a',
        'Β' => 'V', 'β' => 'v',
        'Γ' => 'G', 'γ' => 'g',
        'Δ' => 'D', 'δ' => 'd',
        'Ε' => 'E', 'Έ' => 'E', 'ε' => 'e', 'έ' => 'e',
        'Ζ' => 'Z', 'ζ' => 'z',
        'Η' => 'I', 'Ή' => 'I', 'η' => 'i', 'ή' => 'i',
        'Θ' => 'TH', 'θ' => 'th',
        'Ι' => 'I', 'Ί' => 'I', 'ι' => 'i', 'ί' => 'i', 'ϊ' => 'i', 'ΐ' => 'i',
        'Κ' => 'K', 'κ' => 'k',
        'Λ' => 'L', 'λ' => 'l',
        'Μ' => 'M', 'μ' => 'm',
        'Ν' => 'N', 'ν' => 'n',
        'Ξ' => 'KS', 'ξ' => 'ks',
        'Ο' => 'O', 'Ό' => 'O', 'ο' => 'o', 'ό' => 'o',
        'Π' => 'P', 'π' => 'p',
        'Ρ' => 'R', 'ρ' => 'r',
        'Σ' => 'S', 'σ' => 's', 'ς' => 's',
        'Τ' => 'T', 'τ' => 't',
        'Υ' => 'Y', 'Ύ' => 'Y', 'υ' => 'y', 'ύ' => 'y', 'ϋ' => 'y', 'ΰ' => 'y',
        'Φ' => 'F', 'φ' => 'f',
        'Χ' => 'X', 'χ' => 'x',
        'Ψ' => 'PS', 'ψ' => 'ps',
        'Ω' => 'O', 'Ώ' => 'O', 'ω' => 'o', 'ώ' => 'o'
    )));
}

function startsWith($haystack, $needle) {
    return !strncmp($haystack, $needle, strlen($needle));
}

function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

//
// Advanced checking for variable emptyness
//
function fn_is_empty($var) {
    if (!is_array($var)) {
        return (empty($var));
    } else {
        foreach ($var as $k => $v) {
            if (empty($v)) {
                unset($var[$k]);
                continue;
            }

            if (is_array($v) && fn_is_empty($v)) {
                unset($var[$k]);
            }
        }
        return (empty($var)) ? true : false;
    }
}

function fn_is_not_empty($var) {
    return !fn_is_empty($var);
}

function print_array_as_rows($array) {
    $out = '';
    foreach ($array as $k => $v) {
        if (!empty($v) && !is_array($v)) {
            $out .= "{$k}: {$v}<br/>";
        }
    }
    return $out;
}

function cast($destination, $sourceObject)
{
    if (is_string($destination)) {
        $destination = new $destination();
    }
    $sourceReflection = new ReflectionObject($sourceObject);
    $destinationReflection = new ReflectionObject($destination);
    $sourceProperties = $sourceReflection->getProperties();
    foreach ($sourceProperties as $sourceProperty) {
        $sourceProperty->setAccessible(true);
        $name = $sourceProperty->getName();
        $value = $sourceProperty->getValue($sourceObject);
        if ($destinationReflection->hasProperty($name)) {
            $propDest = $destinationReflection->getProperty($name);
            $propDest->setAccessible(true);
            $propDest->setValue($destination,$value);
        } else {
            $destination->$name = $value;
        }
    }
    return $destination;
}

function human_time_diff( $from, $to = '' ) {
    if ( empty( $to ) ) {
        $to = time();
    }

    $diff = (int) abs( $to - $from );

    if ( $diff < HOUR_IN_SECONDS ) {
        $mins = round( $diff / MINUTE_IN_SECONDS );
        if ( $mins <= 1 )
            $mins = 1;
        /* translators: min=minute */
        $since = sprintf( '%s mins', $mins );
    } elseif ( $diff < DAY_IN_SECONDS && $diff >= HOUR_IN_SECONDS ) {
        $hours = round( $diff / HOUR_IN_SECONDS );
        if ( $hours <= 1 )
            $hours = 1;
        $since = sprintf( '%s hours', $hours );
    } elseif ( $diff < WEEK_IN_SECONDS && $diff >= DAY_IN_SECONDS ) {
        $days = round( $diff / DAY_IN_SECONDS );
        if ( $days <= 1 )
            $days = 1;
        $since = sprintf( '%s days', $days );
    } elseif ( $diff < MONTH_IN_SECONDS && $diff >= WEEK_IN_SECONDS ) {
        $weeks = round( $diff / WEEK_IN_SECONDS );
        if ( $weeks <= 1 )
            $weeks = 1;
        $since = sprintf( '%s weeks', $weeks );
    } elseif ( $diff < YEAR_IN_SECONDS && $diff >= MONTH_IN_SECONDS ) {
        $months = round( $diff / MONTH_IN_SECONDS );
        if ( $months <= 1 )
            $months = 1;
        $since = sprintf( '%s months', $months );
    } elseif ( $diff >= YEAR_IN_SECONDS ) {
        $years = round( $diff / YEAR_IN_SECONDS );
        if ( $years <= 1 )
            $years = 1;
        $since = sprintf( '%s years', $years );
    }

    return $since;
}


function getSql($builder)
{
//    $builder = $this->getBuilder();
    $sql = $builder->toSql();
    foreach($builder->getBindings() as $binding)
    {
        $value = is_numeric($binding) ? $binding : "'".$binding."'";
        $sql = preg_replace('/\?/', $value, $sql, 1);
    }
    return $sql;
}

















/**
 *  Greek Stemmer.
 *  @author Magarisiotis Konstantinos <magarisiotis.kostas@hotmail.com>
 *
 *  Original Stemmer by "Spyros Saroukos".
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */





/**
 *  The call function of the stemmer.
 *  It receives the text and returnes the stemmed words.
 *
 *  @param string $word         :   The text we want to stem
 *  @param bool $simple_stemmer :   If TRUE then it returns all the stemmed words.
 *                                  If FALSE, then it returns only the words that have meaning for
 *                                  the content of the whole text.
 *  @return array               :   The stemmed words in an array
 */
function stem_start($word, $simple_stemmer) {
    // The array that holds the stemmed words
    $stem_con = array();

    // Pre - manipulation
    // Remove the EndLines and dots (.) from the ending of the words.
    $w = trim($word);         // Remove empty spaces
    $w = explode("\n", $w);    // Explode the string based on new line characters
    $w = implode(" ", $w);     // Reunite the string (it does not contain any new lines)
    $w = explode(".", $w);     // Explode the string based on dots.
    $w = implode(" ", $w);     // Reunite the string
    $w = explode(" ", $w);     // Explode it into an array
    // Stem each one word
    for ($i = 0; $i < sizeof($w); $i++) {

        // Pass each word into the stemmer
        $stemmed = stem_stemWord($w[$i], $simple_stemmer);

        // Push the stemmed word into the array, only if a value is returned
        if ($stemmed === null)
            continue;
        else
            array_push($stem_con, $stemmed);
    }

    if($simple_stemmer == FALSE){
        $content_stem_con = [];
        foreach($stem_con as $word){
            if(array_key_exists($word, $content_stem_con)){
                $value = $content_stem_con[$word];
                $value++;
                $content_stem_con[$word] = $value;
            }
            else
                $content_stem_con[$word] = 1;
        }
        return $content_stem_con;
    }

    // Return the stemmed words
    return $stem_con;
}






/**
 *  The stemmer
 *
 *  @param type $w  :   The word to be stemmed
 */
function stem_stemWord($w, $simple_stemmer) {
    // Change the encoding to "ISO-8859-7" and keep track of the change.
    $encoding_changed = FALSE;
    if (mb_check_encoding($w, "UTF-8")) {
        // Flag - the encoding has changed
        $encoding_changed = TRUE;
        $w = mb_convert_encoding($w, "ISO-8859-7", "UTF-8");
    }


    // Remove all special characters and numbers
    $w = str_replace("<br />", "", $w);
    $w = str_replace(".", "", $w);
    $w = str_replace(",", "", $w);
    $w = str_replace(";", "", $w);
    $w = str_replace("/", "", $w);
    $w = str_replace("?", "", $w);
    $w = str_replace(":", "", $w);
    $w = str_replace("'", "", $w);
    $w = str_replace("\"", "", $w);
    $w = str_replace("\\", "", $w);
    $w = str_replace("[", "", $w);
    $w = str_replace("{", "", $w);
    $w = str_replace("]", "", $w);
    $w = str_replace("}", "", $w);
    $w = str_replace("|", "", $w);
    $w = str_replace("~", "", $w);
    $w = str_replace("`", "", $w);
    $w = str_replace("!", "", $w);
    $w = str_replace("@", "", $w);
    $w = str_replace("#", "", $w);
    $w = str_replace("$", "", $w);
    $w = str_replace("%", "", $w);
    $w = str_replace("%", "", $w);
    $w = str_replace("^", "", $w);
    $w = str_replace("&", "", $w);
    $w = str_replace("*", "", $w);
    $w = str_replace("(", "", $w);
    $w = str_replace(")", "", $w);
    $w = str_replace("-", "", $w);
    $w = str_replace("_", "", $w);
    $w = str_replace("+", "", $w);
    $w = str_replace("=", "", $w);
    $w = str_replace("0", "", $w);
    $w = str_replace("1", "", $w);
    $w = str_replace("2", "", $w);
    $w = str_replace("3", "", $w);
    $w = str_replace("4", "", $w);
    $w = str_replace("5", "", $w);
    $w = str_replace("6", "", $w);
    $w = str_replace("7", "", $w);
    $w = str_replace("8", "", $w);
    $w = str_replace("9", "", $w);
    $w = str_replace("«", "", $w);
    $w = str_replace("»", "", $w);

    // Convert to uppercase
    $w = mb_strtoupper($w, 'ISO-8859-7');


    // Replace some values - remove the tonnation
    $unacceptedLetters = [ "α"=>"Α", "β"=>"Β", "γ"=>"Γ", "δ"=>"Δ", "ε"=>"Ε", "ζ"=>"Ζ", "η"=>"Η", "θ"=>"Θ", "ι"=>"Ι", "κ"=>"Κ",
        "λ"=>"Λ", "μ"=>"Μ", "ν"=>"Ν", "ξ"=>"Ξ", "ο"=>"Ο", "π"=>"Π", "ρ"=>"Ρ", "σ"=>"Σ", "τ"=>"Τ", "υ"=>"Υ", "φ"=>"Φ", "χ"=>"Χ",
        "ψ"=>"Ψ", "ω"=>"Ω", "ά"=>"Α", "έ"=>"Ε", "ή"=>"Η", "ί"=>"Ι", "ό"=>"Ο", "ύ"=>"Υ", "ώ"=>"Ω", "ϊ"=>"Ι", "ϋ"=>"Υ",
        "¶"=>"Α", "Έ"=>"Ε", "Ή"=>"Η", "Ί"=>"Ι", "Ό"=>"Ο", "Ύ"=>"Υ", "Ώ"=>"Ω","ς"=>"Σ"];

    // Replace any occurrance of the above letters
    for ($i = 0; $i < strlen($w); $i++) {
        if (array_key_exists($w[$i],$unacceptedLetters))
            $w[$i] = $unacceptedLetters[$w[$i]];
    }


    // If we choose to return only the valuable words, then we remove the Stop Words.
    if( $simple_stemmer == FALSE){
        // ********** The following rules could be modified.
        // Stop Words Removal
        $stop_w = array("ΕΚΟ", "ΑΒΑ", "ΑΓΑ", "ΑΓΗ", "ΑΓΩ", "ΑΔΗ", "ΑΔΩ", "ΑΕ", "ΑΕΙ", "ΑΘΩ", "ΑΙ", "ΑΙΚ", "ΑΚΗ", "ΑΚΟΜΑ", "ΑΚΟΜΗ", "ΑΚΡΙΒΩΣ", "ΑΛΑ", "ΑΛΗΘΕΙΑ", "ΑΛΗΘΙΝΑ", "ΑΛΛΑΧΟΥ", "ΑΛΛΙΩΣ", "ΑΛΛΙΩΤΙΚΑ", "ΑΛΛΟΙΩΣ", "ΑΛΛΟΙΩΤΙΚΑ", "ΑΛΛΟΤΕ", "ΑΛΤ", "ΑΛΩ", "ΑΜΑ", "ΑΜΕ", "ΑΜΕΣΑ", "ΑΜΕΣΩΣ", "ΑΜΩ", "ΑΝ", "ΑΝΑ", "ΑΝΑΜΕΣΑ", "ΑΝΑΜΕΤΑΞΥ", "ΑΝΕΥ", "ΑΝΤΙ", "ΑΝΤΙΠΕΡΑ", "ΑΝΤΙΣ", "ΑΝΩ", "ΑΝΩΤΕΡΩ", "ΑΞΑΦΝΑ", "ΑΠ", "ΑΠΕΝΑΝΤΙ", "ΑΠΟ", "ΑΠΟΨΕ", "ΑΠΩ", "ΑΡΑ", "ΑΡΑΓΕ", "ΑΡΕ", "ΑΡΚ", "ΑΡΚΕΤΑ", "ΑΡΛ", "ΑΡΜ", "ΑΡΤ", "ΑΡΥ", "ΑΡΩ", "ΑΣ", "ΑΣΑ", "ΑΣΟ", "ΑΤΑ", "ΑΤΕ", "ΑΤΗ", "ΑΤΙ", "ΑΤΜ", "ΑΤΟ", "ΑΥΡΙΟ", "ΑΦΗ", "ΑΦΟΤΟΥ", "ΑΦΟΥ", "ΑΧ", "ΑΧΕ", "ΑΧΟ", "ΑΨΑ", "ΑΨΕ", "ΑΨΗ", "ΑΨΥ", "ΑΩΕ", "ΑΩΟ", "ΒΑΝ", "ΒΑΤ", "ΒΑΧ", "ΒΕΑ", "ΒΕΒΑΙΟΤΑΤΑ", "ΒΗΞ", "ΒΙΑ", "ΒΙΕ", "ΒΙΗ", "ΒΙΟ", "ΒΟΗ", "ΒΟΩ", "ΒΡΕ", "ΓΑ", "ΓΑΒ", "ΓΑΡ", "ΓΕΝ", "ΓΕΣ", "", "ΓΗ", "ΓΗΝ", "ΓΙ", "ΓΙΑ", "ΓΙΕ", "ΓΙΝ", "ΓΙΟ", "ΓΚΙ", "ΓΙΑΤΙ", "ΓΚΥ", "ΓΟΗ", "ΓΟΟ", "ΓΡΗΓΟΡΑ", "ΓΡΙ", "ΓΡΥ", "ΓΥΗ", "ΓΥΡΩ", "ΔΑ", "ΔΕ", "ΔΕΗ", "ΔΕΙ", "ΔΕΝ", "ΔΕΣ", "ΔΗ", "ΔΗΘΕΝ", "ΔΗΛΑΔΗ", "ΔΗΩ", "ΔΙ", "ΔΙΑ", "ΔΙΑΡΚΩΣ", "ΔΙΟΛΟΥ", "ΔΙΣ", "ΔΙΧΩΣ", "ΔΟΛ", "ΔΟΝ", "ΔΡΑ", "ΔΡΥ", "ΔΡΧ", "ΔΥΕ", "ΔΥΟ", "ΔΩ", "ΕΑΜ", "ΕΑΝ", "ΕΑΡ", "ΕΘΗ", "ΕΙ", "ΕΙΔΕΜΗ", "ΕΙΘΕ", "ΕΙΜΑΙ", "ΕΙΜΑΣΤΕ", "ΕΙΝΑΙ", "ΕΙΣ", "ΕΙΣΑΙ", "ΕΙΣΑΣΤΕ", "ΕΙΣΤΕ", "ΕΙΤΕ", "ΕΙΧΑ", "ΕΙΧΑΜΕ", "ΕΙΧΑΝ", "ΕΙΧΑΤΕ", "ΕΙΧΕ", "ΕΙΧΕΣ", "ΕΚ", "ΕΚΕΙ", "ΕΛΑ", "ΕΛΙ", "ΕΜΠ", "ΕΝ", "ΕΝΤΕΛΩΣ", "ΕΝΤΟΣ", "ΕΝΤΩΜΕΤΑΞΥ", "ΕΝΩ", "ΕΞ", "ΕΞΑΦΝΑ", "ΕΞΙ", "ΕΞΙΣΟΥ", "ΕΞΩ", "ΕΟΚ", "ΕΠΑΝΩ", "ΕΠΕΙΔΗ", "ΕΠΕΙΤΑ", "ΕΠΗ", "ΕΠΙ", "ΕΠΙΣΗΣ", "ΕΠΟΜΕΝΩΣ", "ΕΡΑ", "ΕΣ", "ΕΣΑΣ", "ΕΣΕ", "ΕΣΕΙΣ", "ΕΣΕΝΑ", "ΕΣΗ", "ΕΣΤΩ", "ΕΣΥ", "ΕΣΩ", "ΕΤΙ", "ΕΤΣΙ", "ΕΥ", "ΕΥΑ", "ΕΥΓΕ", "ΕΥΘΥΣ", "ΕΥΤΥΧΩΣ", "ΕΦΕ", "ΕΦΕΞΗΣ", "ΕΦΤ", "ΕΧΕ", "ΕΧΕΙ", "ΕΧΕΙΣ", "ΕΧΕΤΕ", "ΕΧΘΕΣ", "ΕΧΟΜΕ", "ΕΧΟΥΜΕ", "ΕΧΟΥΝ", "ΕΧΤΕΣ", "ΕΧΩ", "ΕΩΣ", "ΖΕΑ", "ΖΕΗ", "ΖΕΙ", "ΖΕΝ", "ΖΗΝ", "ΖΩ", "Η", "ΗΔΗ", "ΗΔΥ", "ΗΘΗ", "ΗΛΟ", "ΗΜΙ", "ΗΠΑ", "ΗΣΑΣΤΕ", "ΗΣΟΥΝ", "ΗΤΑ", "ΗΤΑΝ", "ΗΤΑΝΕ", "ΗΤΟΙ", "ΗΤΤΟΝ", "ΗΩ", "ΘΑ", "ΘΥΕ", "ΘΩΡ", "Ι", "ΙΑ", "ΙΒΟ", "ΙΔΗ", "ΙΔΙΩΣ", "ΙΕ", "ΙΙ", "ΙΙΙ", "ΙΚΑ", "ΙΛΟ", "ΙΜΑ", "ΙΝΑ", "ΙΝΩ", "ΙΞΕ", "ΙΞΟ", "ΙΟ", "ΙΟΙ", "ΙΣΑ", "ΙΣΑΜΕ", "ΙΣΕ", "ΙΣΗ", "ΙΣΙΑ", "ΙΣΟ", "ΙΣΩΣ", "ΙΩΒ", "ΙΩΝ", "ΙΩΣ", "ΙΑΝ", "ΚΑΘ", "ΚΑΘΕ", "ΚΑΘΕΤΙ", "ΚΑΘΟΛΟΥ", "ΚΑΘΩΣ", "ΚΑΙ", "ΚΑΝ", "ΚΑΠΟΤΕ", "ΚΑΠΟΥ", "ΚΑΠΩΣ", "ΚΑΤ", "ΚΑΤΑ", "ΚΑΤΙ", "ΚΑΤΙΤΙ", "ΚΑΤΟΠΙΝ", "ΚΑΤΩ", "ΚΑΩ", "ΚΒΟ", "ΚΕΑ", "ΚΕΙ", "ΚΕΝ", "ΚΙ", "ΚΙΜ", "ΚΙΟΛΑΣ", "ΚΙΤ", "ΚΙΧ", "ΚΚΕ", "ΚΛΙΣΕ", "ΚΛΠ", "ΚΟΚ", "ΚΟΝΤΑ", "ΚΟΧ", "ΚΤΛ", "ΚΥΡ", "ΚΥΡΙΩΣ", "ΚΩ", "ΚΩΝ", "ΛΑ", "ΛΕΑ", "ΛΕΝ", "ΛΕΟ", "ΛΙΑ", "ΛΙΓΑΚΙ", "ΛΙΓΟΥΛΑΚΙ", "ΛΙΓΟ", "ΛΙΓΩΤΕΡΟ", "ΛΙΟ", "ΛΙΡ", "ΛΟΓΩ", "ΛΟΙΠΑ", "ΛΟΙΠΟΝ", "ΛΟΣ", "ΛΣ", "ΛΥΩ", "ΜΑ", "ΜΑΖΙ", "ΜΑΚΑΡΙ", "ΜΑΛΙΣΤΑ", "ΜΑΛΛΟΝ", "ΜΑΝ", "ΜΑΞ", "ΜΑΣ", "ΜΑΤ", "ΜΕ", "ΜΕΘΑΥΡΙΟ", "ΜΕΙ", "ΜΕΙΟΝ", "ΜΕΛ", "ΜΕΛΕΙ", "ΜΕΛΛΕΤΑΙ", "ΜΕΜΙΑΣ", "ΜΕΝ", "ΜΕΣ", "ΜΕΣΑ", "ΜΕΤ", "ΜΕΤΑ", "ΜΕΤΑΞΥ", "ΜΕΧΡΙ", "ΜΗ", "ΜΗΔΕ", "ΜΗΝ", "ΜΗΠΩΣ", "ΜΗΤΕ", "ΜΙ", "ΜΙΞ", "ΜΙΣ", "ΜΜΕ", "ΜΝΑ", "ΜΟΒ", "ΜΟΛΙΣ", "ΜΟΛΟΝΟΤΙ", "ΜΟΝΑΧΑ", "ΜΟΝΟΜΙΑΣ", "ΜΙΑ", "ΜΟΥ", "ΜΠΑ", "ΜΠΟΡΕΙ", "ΜΠΟΡΟΥΝ", "ΜΠΡΑΒΟ", "ΜΠΡΟΣ", "ΜΠΩ", "ΜΥ", "ΜΥΑ", "ΜΥΝ", "ΝΑ", "ΝΑΕ", "ΝΑΙ", "ΝΑΟ", "ΝΔ", "ΝΕΐ", "ΝΕΑ", "ΝΕΕ", "ΝΕΟ", "ΝΙ", "ΝΙΑ", "ΝΙΚ", "ΝΙΛ", "ΝΙΝ", "ΝΙΟ", "ΝΤΑ", "ΝΤΕ", "ΝΤΙ", "ΝΤΟ", "ΝΥΝ", "ΝΩΕ", "ΝΩΡΙΣ", "ΞΑΝΑ", "ΞΑΦΝΙΚΑ", "ΞΕΩ", "ΞΙ", "Ο", "ΟΑ", "ΟΑΠ", "ΟΔΟ", "ΟΕ", "ΟΖΟ", "ΟΗΕ", "ΟΙ", "ΟΙΑ", "ΟΙΗ", "ΟΚΑ", "ΟΛΟΓΥΡΑ", "ΟΛΟΝΕΝ", "ΟΛΟΤΕΛΑ", "ΟΛΩΣΔΙΟΛΟΥ", "ΟΜΩΣ", "ΌΜΩΣ", "ΌΠΩΣ", "ΟΝ", "ΟΝΕ", "ΟΝΟ", "ΟΠΑ", "ΟΠΕ", "ΟΠΗ", "ΟΠΟ", "ΟΠΟΙΑΔΗΠΟΤΕ", "ΟΠΟΙΑΝΔΗΠΟΤΕ", "ΟΠΟΙΑΣΔΗΠΟΤΕ", "ΟΠΟΙΔΗΠΟΤΕ", "ΟΠΟΙΕΣΔΗΠΟΤΕ", "ΟΠΟΙΟΔΗΠΟΤΕ", "ΟΠΟΙΟΝΔΗΠΟΤΕ", "ΟΠΟΙΟΣΔΗΠΟΤΕ", "ΟΠΟΙΟΥΔΗΠΟΤΕ", "ΟΠΟΙΟΥΣΔΗΠΟΤΕ", "ΟΠΟΙΩΝΔΗΠΟΤΕ", "ΟΠΟΤΕΔΗΠΟΤΕ", "ΟΠΟΥ", "ΟΠΟΥΔΗΠΟΤΕ", "ΟΠΩΣ", "ΟΡΑ", "ΟΡΕ", "ΟΡΗ", "ΟΡΟ", "ΟΡΦ", "ΟΡΩ", "ΟΣΑ", "ΟΣΑΔΗΠΟΤΕ", "ΟΣΕ", "ΟΣΕΣΔΗΠΟΤΕ", "ΟΣΗΔΗΠΟΤΕ", "ΟΣΗΝΔΗΠΟΤΕ", "ΟΣΗΣΔΗΠΟΤΕ", "ΟΣΟΔΗΠΟΤΕ", "ΟΣΟΙΔΗΠΟΤΕ", "ΟΣΟΝΔΗΠΟΤΕ", "ΟΣΟΣΔΗΠΟΤΕ", "ΟΣΟΥΔΗΠΟΤΕ", "ΟΣΟΥΣΔΗΠΟΤΕ", "ΟΣΩΝΔΗΠΟΤΕ", "ΟΤΑΝ", "ΟΤΕ", "ΟΤΙ", "ΟΤΙΔΗΠΟΤΕ", "ΟΥ", "ΟΥΔΕ", "ΟΥΚ", "ΟΥΣ", "ΟΥΤΕ", "ΟΥΦ", "ΟΧΙ", "ΟΨΑ", "ΟΨΕ", "ΟΨΗ", "ΟΨΙ", "ΟΨΟ", "ΠΑ", "ΠΑΛΙ", "ΠΑΝ", "ΠΑΝΤΟΤΕ", "ΠΑΝΤΟΥ", "ΠΑΝΤΩΣ", "ΠΑΠ", "ΠΑΡ", "ΠΑΡΑ", "ΠΕΙ", "ΠΕΡ", "ΠΕΡΑ", "ΠΕΡΙ", "ΠΕΡΙΠΟΥ", "ΠΕΡΣΙ", "ΠΕΡΥΣΙ", "ΠΕΣ", "ΠΙ", "ΠΙΑ", "ΠΙΘΑΝΟΝ", "ΠΙΚ", "ΠΙΟ", "ΠΙΣΩ", "ΠΙΤ", "ΠΙΩ", "ΠΛΑΙ", "ΠΛΕΟΝ", "ΠΛΗΝ", "ΠΛΩ", "ΠΜ", "ΠΟΑ", "ΠΟΕ", "ΠΟΛ", "ΠΟΛΥ", "ΠΟΠ", "ΠΟΤΕ", "ΠΟΥ", "ΠΟΥΘΕ", "ΠΟΥΘΕΝΑ", "ΠΡΕΠΕΙ", "ΠΡΙ", "ΠΡΙΝ", "ΠΡΟ", "ΠΡΟΚΕΙΜΕΝΟΥ", "ΠΡΟΚΕΙΤΑΙ", "ΠΡΟΠΕΡΣΙ", "ΠΡΟΣ", "ΠΡΟΤΟΥ", "ΠΡΟΧΘΕΣ", "ΠΡΟΧΤΕΣ", "ΠΡΩΤΥΤΕΡΑ", "ΠΥΑ", "ΠΥΞ", "ΠΥΟ", "ΠΥΡ", "ΠΧ", "ΠΩ", "ΠΩΛ", "ΠΩΣ", "ΡΑ", "ΡΑΙ", "ΡΑΠ", "ΡΑΣ", "ΡΕ", "ΡΕΑ", "ΡΕΕ", "ΡΕΙ", "ΡΗΣ", "ΡΘΩ", "ΡΙΟ", "ΡΟ", "ΡΟΐ", "ΡΟΕ", "ΡΟΖ", "ΡΟΗ", "ΡΟΘ", "ΡΟΙ", "ΡΟΚ", "ΡΟΛ", "ΡΟΝ", "ΡΟΣ", "ΡΟΥ", "ΣΑΙ", "ΣΑΝ", "ΣΑΟ", "ΣΑΣ", "ΣΕ", "ΣΕΙΣ", "ΣΕΚ", "ΣΕΞ", "ΣΕΡ", "ΣΕΤ", "ΣΕΦ", "ΣΗΜΕΡΑ", "ΣΙ", "ΣΙΑ", "ΣΙΓΑ", "ΣΙΚ", "ΣΙΧ", "ΣΚΙ", "ΣΟΙ", "ΣΟΚ", "ΣΟΛ", "ΣΟΝ", "ΣΟΣ", "ΣΟΥ", "ΣΡΙ", "ΣΤΑ", "ΣΤΗ", "ΣΤΗΝ", "ΣΤΗΣ", "ΣΤΙΣ", "ΣΤΟ", "ΣΤΟΝ", "ΣΤΟΥ", "ΣΤΟΥΣ", "ΣΤΩΝ", "ΣΥ", "ΣΥΓΧΡΟΝΩΣ", "ΣΥΝ", "ΣΥΝΑΜΑ", "ΣΥΝΕΠΩΣ", "ΣΥΝΗΘΩΣ", "ΣΧΕΔΟΝ", "ΣΩΣΤΑ", "ΤΑ", "ΤΑΔΕ", "ΤΑΚ", "ΤΑΝ", "ΤΑΟ", "ΤΑΥ", "ΤΑΧΑ", "ΤΑΧΑΤΕ", "ΤΕ", "ΤΕΙ", "ΤΕΛ", "ΤΕΛΙΚΑ", "ΤΕΛΙΚΩΣ", "ΤΕΣ", "ΤΕΤ", "ΤΖΟ", "ΤΗ", "ΤΗΛ", "ΤΗΝ", "ΤΗΣ", "ΤΙ", "ΤΙΚ", "ΤΙΜ", "ΤΙΠΟΤΑ", "ΤΙΠΟΤΕ", "ΤΙΣ", "ΤΝΤ", "ΤΟ", "ΤΟΙ", "ΤΟΚ", "ΤΟΜ", "ΤΟΝ", "ΤΟΠ", "ΤΟΣ", "ΤΟΣ?Ν", "ΤΟΣΑ", "ΤΟΣΕΣ", "ΤΟΣΗ", "ΤΟΣΗΝ", "ΤΟΣΗΣ", "ΤΟΣΟ", "ΤΟΣΟΙ", "ΤΟΣΟΝ", "ΤΟΣΟΣ", "ΤΟΣΟΥ", "ΤΟΣΟΥΣ", "ΤΟΤΕ", "ΤΟΥ", "ΤΟΥΛΑΧΙΣΤΟ", "ΤΟΥΛΑΧΙΣΤΟΝ", "ΤΟΥΣ", "ΤΣ", "ΤΣΑ", "ΤΣΕ", "ΤΥΧΟΝ", "ΤΩ", "ΤΩΝ", "ΤΩΡΑ", "ΥΑΣ", "ΥΒΑ", "ΥΒΟ", "ΥΙΕ", "ΥΙΟ", "ΥΛΑ", "ΥΛΗ", "ΥΝΙ", "ΥΠ", "ΥΠΕΡ", "ΥΠΟ", "ΥΠΟΨΗ", "ΥΠΟΨΙΝ", "ΥΣΤΕΡΑ", "ΥΦΗ", "ΥΨΗ", "ΦΑ", "ΦΑΐ", "ΦΑΕ", "ΦΑΝ", "ΦΑΞ", "ΦΑΣ", "ΦΑΩ", "ΦΕΖ", "ΦΕΙ", "ΦΕΤΟΣ", "ΦΕΥ", "ΦΙ", "ΦΙΛ", "ΦΙΣ", "ΦΟΞ", "ΦΠΑ", "ΦΡΙ", "ΧΑ", "ΧΑΗ", "ΧΑΛ", "ΧΑΝ", "ΧΑΦ", "ΧΕ", "ΧΕΙ", "ΧΘΕΣ", "ΧΙ", "ΧΙΑ", "ΧΙΛ", "ΧΙΟ", "ΧΛΜ", "ΧΜ", "ΧΟΗ", "ΧΟΛ", "ΧΡΩ", "ΧΤΕΣ", "ΧΩΡΙΣ", "ΧΩΡΙΣΤΑ", "ΨΕΣ", "ΨΗΛΑ", "ΨΙ", "ΨΙΤ", "Ω", "ΩΑ", "ΩΑΣ", "ΩΔΕ", "ΩΕΣ", "ΩΘΩ", "ΩΜΑ", "ΩΜΕ", "ΩΝ", "ΩΟ", "ΩΟΝ", "ΩΟΥ", "ΩΣ", "ΩΣΑΝ", "ΩΣΗ", "ΩΣΟΤΟΥ", "ΩΣΠΟΥ", "ΩΣΤΕ", "ΩΣΤΟΣΟ", "ΩΤΑ", "ΩΧ", "ΩΩΝ", "ΑΔΙΑΚΟΠΑ", "ΑΙ", "ΑΚΟΜΑ", "ΑΚΟΜΗ", "ΑΚΡΙΒΩΣ", "ΑΛΗΘΕΙΑ", "ΑΛΗΘΙΝΑ", "ΑΛΛΑ", "ΑΛΛΑΧΟΥ", "ΑΛΛΕΣ", "ΑΛΛΗ", "ΑΛΛΗΝ", "ΑΛΛΗΣ", "ΑΛΛΙΩΣ", "ΑΛΛΙΩΤΙΚΑ", "ΑΛΛΟ", "ΑΛΛΟΙ", "ΑΛΛΟΙΩΣ", "ΑΛΛΟΙΩΤΙΚΑ", "ΑΛΛΟΝ", "ΑΛΛΟΣ", "ΑΛΛΟΤΕ", "ΑΛΛΟΥ", "ΑΛΛΟΥΣ", "ΑΛΛΩΝ", "ΑΜΑ", "ΑΜΕΣΑ", "ΑΜΕΣΩΣ", "ΑΝ", "ΑΝΑ", "ΑΝΑΜΕΣΑ", "ΑΝΑΜΕΤΑΞΥ", "ΑΝΕΥ", "ΑΝΤΙ", "ΑΝΤΙΠΕΡΑ", "ΑΝΤΙΣ", "ΑΝΩ", "ΑΝΩΤΕΡΩ", "ΑΞΑΦΝΑ", "ΑΠ", "ΑΠΕΝΑΝΤΙ", "ΑΠΟ", "ΑΠΟΨΕ", "ΑΡΑ", "ΑΡΑΓΕ", "ΑΡΓΑ", "ΑΡΓΟΤΕΡΟ", "ΑΡΙΣΤΕΡΑ", "ΑΡΚΕΤΑ", "ΑΡΧΙΚΑ", "ΑΣ", "ΑΥΡΙΟ", "ΑΥΤΑ", "ΑΥΤΕΣ", "ΑΥΤΗ", "ΑΥΤΗΝ", "ΑΥΤΗΣ", "ΑΥΤΟ", "ΑΥΤΟΙ", "ΑΥΤΟΝ", "ΑΥΤΟΣ", "ΑΥΤΟΥ", "ΑΥΤΟΥΣ", "ΑΥΤΩΝ", "ΑΦΟΤΟΥ", "ΑΦΟΥ", "ΒΕΒΑΙΑ", "ΒΕΒΑΙΟΤΑΤΑ", "ΓΙ", "ΓΙΑ", "ΓΡΗΓΟΡΑ", "ΓΥΡΩ", "ΔΑ", "ΔΕ", "ΔΕΙΝΑ", "ΔΕΝ", "ΔΕΞΙΑ", "ΔΗΘΕΝ", "ΔΗΛΑΔΗ", "ΔΙ", "ΔΙΑ", "ΔΙΑΡΚΩΣ", "ΔΙΚΑ", "ΔΙΚΟ", "ΔΙΚΟΙ", "ΔΙΚΟΣ", "ΔΙΚΟΥ", "ΔΙΚΟΥΣ", "ΔΙΟΛΟΥ", "ΔΙΠΛΑ", "ΔΙΧΩΣ", "ΕΑΝ", "ΕΑΥΤΟ", "ΕΑΥΤΟΝ", "ΕΑΥΤΟΥ", "ΕΑΥΤΟΥΣ", "ΕΑΥΤΩΝ", "ΕΓΚΑΙΡΑ", "ΕΓΚΑΙΡΩΣ", "ΕΓΩ", "ΕΔΩ", "ΕΙΔΕΜΗ", "ΕΙΘΕ", "ΕΙΜΑΙ", "ΕΙΜΑΣΤΕ", "ΕΙΝΑΙ", "ΕΙΣ", "ΕΙΣΑΙ", "ΕΙΣΑΣΤΕ", "ΕΙΣΤΕ", "ΕΙΤΕ", "ΕΙΧΑ", "ΕΙΧΑΜΕ", "ΕΙΧΑΝ", "ΕΙΧΑΤΕ", "ΕΙΧΕ", "ΕΙΧΕΣ", "ΕΚΑΣΤΑ", "ΕΚΑΣΤΕΣ", "ΕΚΑΣΤΗ", "ΕΚΑΣΤΗΝ", "ΕΚΑΣΤΗΣ", "ΕΚΑΣΤΟ", "ΕΚΑΣΤΟΙ", "ΕΚΑΣΤΟΝ", "ΕΚΑΣΤΟΣ", "ΕΚΑΣΤΟΥ", "ΕΚΑΣΤΟΥΣ", "ΕΚΑΣΤΩΝ", "ΕΚΕΙ", "ΕΚΕΙΝΑ", "ΕΚΕΙΝΕΣ", "ΕΚΕΙΝΗ", "ΕΚΕΙΝΗΝ", "ΕΚΕΙΝΗΣ", "ΕΚΕΙΝΟ", "ΕΚΕΙΝΟΙ", "ΕΚΕΙΝΟΝ", "ΕΚΕΙΝΟΣ", "ΕΚΕΙΝΟΥ", "ΕΚΕΙΝΟΥΣ", "ΕΚΕΙΝΩΝ", "ΕΚΤΟΣ", "ΕΜΑΣ", "ΕΜΕΙΣ", "ΕΜΕΝΑ", "ΕΜΠΡΟΣ", "ΕΝ", "ΕΝΑ", "ΕΝΑΝ", "ΕΝΑΣ", "ΕΝΟΣ", "ΕΝΤΕΛΩΣ", "ΕΝΤΟΣ", "ΕΝΤΩΜΕΤΑΞΥ", "ΕΝΩ", "ΕΞ", "ΕΞΑΦΝΑ", "ΕΞΗΣ", "ΕΞΙΣΟΥ", "ΕΞΩ", "ΕΠΑΝΩ", "ΕΠΕΙΔΗ", "ΕΠΕΙΤΑ", "ΕΠΙ", "ΕΠΙΣΗΣ", "ΕΠΟΜΕΝΩΣ", "ΕΣΑΣ", "ΕΣΕΙΣ", "ΕΣΕΝΑ", "ΕΣΤΩ", "ΕΣΥ", "ΕΤΕΡΑ", "ΕΤΕΡΑΙ", "ΕΤΕΡΑΣ", "ΕΤΕΡΕΣ", "ΕΤΕΡΗ", "ΕΤΕΡΗΣ", "ΕΤΕΡΟ", "ΕΤΕΡΟΙ", "ΕΤΕΡΟΝ", "ΕΤΕΡΟΣ", "ΕΤΕΡΟΥ", "ΕΤΕΡΟΥΣ", "ΕΤΕΡΩΝ", "ΕΤΟΥΤΑ", "ΕΤΟΥΤΕΣ", "ΕΤΟΥΤΗ", "ΕΤΟΥΤΗΝ", "ΕΤΟΥΤΗΣ", "ΕΤΟΥΤΟ", "ΕΤΟΥΤΟΙ", "ΕΤΟΥΤΟΝ", "ΕΤΟΥΤΟΣ", "ΕΤΟΥΤΟΥ", "ΕΤΟΥΤΟΥΣ", "ΕΤΟΥΤΩΝ", "ΕΤΣΙ", "ΕΥΓΕ", "ΕΥΘΥΣ", "ΕΥΤΥΧΩΣ", "ΕΦΕΞΗΣ", "ΕΧΕΙ", "ΕΧΕΙΣ", "ΕΧΕΤΕ", "ΕΧΘΕΣ", "ΕΧΟΜΕ", "ΕΧΟΥΜΕ", "ΕΧΟΥΝ", "ΕΧΤΕΣ", "ΕΧΩ", "ΕΩΣ", "Η", "ΗΔΗ", "ΗΜΑΣΤΑΝ", "ΗΜΑΣΤΕ", "ΗΜΟΥΝ", "ΗΣΑΣΤΑΝ", "ΗΣΑΣΤΕ", "ΗΣΟΥΝ", "ΗΤΑΝ", "ΗΤΑΝΕ", "ΗΤΟΙ", "ΗΤΤΟΝ", "ΘΑ", "Ι", "ΙΔΙΑ", "ΙΔΙΑΝ", "ΙΔΙΑΣ", "ΙΔΙΕΣ", "ΙΔΙΟ", "ΙΔΙΟΙ", "ΙΔΙΟΝ", "ΙΔΙΟΣ", "ΙΔΙΟΥ", "ΙΔΙΟΥΣ", "ΙΔΙΩΝ", "ΙΔΙΩΣ", "ΙΙ", "ΙΙΙ", "ΙΣΑΜΕ", "ΙΣΙΑ", "ΙΣΩΣ", "ΚΑΘΕ", "ΚΑΘΕΜΙΑ", "ΚΑΘΕΜΙΑΣ", "ΚΑΘΕΝΑ", "ΚΑΘΕΝΑΣ", "ΚΑΘΕΝΟΣ", "ΚΑΘΕΤΙ", "ΚΑΘΟΛΟΥ", "ΚΑΘΩΣ", "ΚΑΙ", "ΚΑΚΑ", "ΚΑΚΩΣ", "ΚΑΛΑ", "ΚΑΛΩΣ", "ΚΑΜΙΑ", "ΚΑΜΙΑΝ", "ΚΑΜΙΑΣ", "ΚΑΜΠΟΣΑ", "ΚΑΜΠΟΣΕΣ", "ΚΑΜΠΟΣΗ", "ΚΑΜΠΟΣΗΝ", "ΚΑΜΠΟΣΗΣ", "ΚΑΜΠΟΣΟ", "ΚΑΜΠΟΣΟΙ", "ΚΑΜΠΟΣΟΝ", "ΚΑΜΠΟΣΟΣ", "ΚΑΜΠΟΣΟΥ", "ΚΑΜΠΟΣΟΥΣ", "ΚΑΜΠΟΣΩΝ", "ΚΑΝΕΙΣ", "ΚΑΝΕΝ", "ΚΑΝΕΝΑ", "ΚΑΝΕΝΑΝ", "ΚΑΝΕΝΑΣ", "ΚΑΝΕΝΟΣ", "ΚΑΠΟΙΑ", "ΚΑΠΟΙΑΝ", "ΚΑΠΟΙΑΣ", "ΚΑΠΟΙΕΣ", "ΚΑΠΟΙΟ", "ΚΑΠΟΙΟΙ", "ΚΑΠΟΙΟΝ", "ΚΑΠΟΙΟΣ", "ΚΑΠΟΙΟΥ", "ΚΑΠΟΙΟΥΣ", "ΚΑΠΟΙΩΝ", "ΚΑΠΟΤΕ", "ΚΑΠΟΥ", "ΚΑΠΩΣ", "ΚΑΤ", "ΚΑΤΑ", "ΚΑΤΙ", "ΚΑΤΙΤΙ", "ΚΑΤΟΠΙΝ", "ΚΑΤΩ", "ΚΙΟΛΑΣ", "ΚΛΠ", "ΚΟΝΤΑ", "ΚΤΛ", "ΚΥΡΙΩΣ", "ΛΙΓΑΚΙ", "ΛΙΓΟ", "ΛΙΓΩΤΕΡΟ", "ΛΟΓΩ", "ΛΟΙΠΑ", "ΛΟΙΠΟΝ", "ΜΑ", "ΜΑΖΙ", "ΜΑΚΑΡΙ", "ΜΑΚΡΥΑ", "ΜΑΛΙΣΤΑ", "ΜΑΛΛΟΝ", "ΜΑΣ", "ΜΕ", "ΜΕΘΑΥΡΙΟ", "ΜΕΙΟΝ", "ΜΕΛΕΙ", "ΜΕΛΛΕΤΑΙ", "ΜΕΜΙΑΣ", "ΜΕΝ", "ΜΕΡΙΚΑ", "ΜΕΡΙΚΕΣ", "ΜΕΡΙΚΟΙ", "ΜΕΡΙΚΟΥΣ", "ΜΕΡΙΚΩΝ", "ΜΕΣΑ", "ΜΕΤ", "ΜΕΤΑ", "ΜΕΤΑΞΥ", "ΜΕΧΡΙ", "ΜΗ", "ΜΗΔΕ", "ΜΗΝ", "ΜΗΠΩΣ", "ΜΗΤΕ", "ΜΙΑ", "ΜΙΑΝ", "ΜΙΑΣ", "ΜΟΛΙΣ", "ΜΟΛΟΝΟΤΙ", "ΜΟΝΑΧΑ", "ΜΟΝΕΣ", "ΜΟΝΗ", "ΜΟΝΗΝ", "ΜΟΝΗΣ", "ΜΟΝΟ", "ΜΟΝΟΙ", "ΜΟΝΟΜΙΑΣ", "ΜΟΝΟΣ", "ΜΟΝΟΥ", "ΜΟΝΟΥΣ", "ΜΟΝΩΝ", "ΜΟΥ", "ΜΠΟΡΕΙ", "ΜΠΟΡΟΥΝ", "ΜΠΡΑΒΟ", "ΜΠΡΟΣ", "ΝΑ", "ΝΑΙ", "ΝΩΡΙΣ", "ΞΑΝΑ", "ΞΑΦΝΙΚΑ", "Ο", "ΟΙ", "ΟΛΑ", "ΟΛΕΣ", "ΟΛΗ", "ΟΛΗΝ", "ΟΛΗΣ", "ΟΛΟ", "ΟΛΟΓΥΡΑ", "ΟΛΟΙ", "ΟΛΟΝ", "ΟΛΟΝΕΝ", "ΟΛΟΣ", "ΟΛΟΤΕΛΑ", "ΟΛΟΥ", "ΟΛΟΥΣ", "ΟΛΩΝ", "ΟΛΩΣ", "ΟΛΩΣΔΙΟΛΟΥ", "ΟΜΩΣ", "ΟΠΟΙΑ", "ΟΠΟΙΑΔΗΠΟΤΕ", "ΟΠΟΙΑΝ", "ΟΠΟΙΑΝΔΗΠΟΤΕ", "ΟΠΟΙΑΣ", "ΟΠΟΙΑΣΔΗΠΟΤΕ", "ΟΠΟΙΔΗΠΟΤΕ", "ΟΠΟΙΕΣ", "ΟΠΟΙΕΣΔΗΠΟΤΕ", "ΟΠΟΙΟ", "ΟΠΟΙΟΔΗΠΟΤΕ", "ΟΠΟΙΟΙ", "ΟΠΟΙΟΝ", "ΟΠΟΙΟΝΔΗΠΟΤΕ", "ΟΠΟΙΟΣ", "ΟΠΟΙΟΣΔΗΠΟΤΕ", "ΟΠΟΙΟΥ", "ΟΠΟΙΟΥΔΗΠΟΤΕ", "ΟΠΟΙΟΥΣ", "ΟΠΟΙΟΥΣΔΗΠΟΤΕ", "ΟΠΟΙΩΝ", "ΟΠΟΙΩΝΔΗΠΟΤΕ", "ΟΠΟΤΕ", "ΟΠΟΤΕΔΗΠΟΤΕ", "ΟΠΟΥ", "ΟΠΟΥΔΗΠΟΤΕ", "ΟΠΩΣ", "ΟΡΙΣΜΕΝΑ", "ΟΡΙΣΜΕΝΕΣ", "ΟΡΙΣΜΕΝΩΝ", "ΟΡΙΣΜΕΝΩΣ", "ΟΣΑ", "ΟΣΑΔΗΠΟΤΕ", "ΟΣΕΣ", "ΟΣΕΣΔΗΠΟΤΕ", "ΟΣΗ", "ΟΣΗΔΗΠΟΤΕ", "ΟΣΗΝ", "ΟΣΗΝΔΗΠΟΤΕ", "ΟΣΗΣ", "ΟΣΗΣΔΗΠΟΤΕ", "ΟΣΟ", "ΟΣΟΔΗΠΟΤΕ", "ΟΣΟΙ", "ΟΣΟΙΔΗΠΟΤΕ", "ΟΣΟΝ", "ΟΣΟΝΔΗΠΟΤΕ", "ΟΣΟΣ", "ΟΣΟΣΔΗΠΟΤΕ", "ΟΣΟΥ", "ΟΣΟΥΔΗΠΟΤΕ", "ΟΣΟΥΣ", "ΟΣΟΥΣΔΗΠΟΤΕ", "ΟΣΩΝ", "ΟΣΩΝΔΗΠΟΤΕ", "ΟΤΑΝ", "ΟΤΙ", "ΟΤΙΔΗΠΟΤΕ", "ΟΤΟΥ", "ΟΥ", "ΟΥΔΕ", "ΟΥΤΕ", "ΟΧΙ", "ΠΑΛΙ", "ΠΑΝΤΟΤΕ", "ΠΑΝΤΟΥ", "ΠΑΝΤΩΣ", "ΠΑΡΑ", "ΠΕΡΑ", "ΠΕΡΙ", "ΠΕΡΙΠΟΥ", "ΠΕΡΙΣΣΟΤΕΡΟ", "ΠΕΡΣΙ", "ΠΕΡΥΣΙ", "ΠΙΑ", "ΠΙΘΑΝΟΝ", "ΠΙΟ", "ΠΙΣΩ", "ΠΛΑΙ", "ΠΛΕΟΝ", "ΠΛΗΝ", "ΠΟΙΑ", "ΠΟΙΑΝ", "ΠΟΙΑΣ", "ΠΟΙΕΣ", "ΠΟΙΟ", "ΠΟΙΟΙ", "ΠΟΙΟΝ", "ΠΟΙΟΣ", "ΠΟΙΟΥ", "ΠΟΙΟΥΣ", "ΠΟΙΩΝ", "ΠΟΛΥ", "ΠΟΣΕΣ", "ΠΟΣΗ", "ΠΟΣΗΝ", "ΠΟΣΗΣ", "ΠΟΣΟΙ", "ΠΟΣΟΣ", "ΠΟΣΟΥΣ", "ΠΟΤΕ", "ΠΟΥ", "ΠΟΥΘΕ", "ΠΟΥΘΕΝΑ", "ΠΡΕΠΕΙ", "ΠΡΙΝ", "ΠΡΟ", "ΠΡΟΚΕΙΜΕΝΟΥ", "ΠΡΟΚΕΙΤΑΙ", "ΠΡΟΠΕΡΣΙ", "ΠΡΟΣ", "ΠΡΟΤΟΥ", "ΠΡΟΧΘΕΣ", "ΠΡΟΧΤΕΣ", "ΠΡΩΤΥΤΕΡΑ", "ΠΩΣ", "ΣΑΝ", "ΣΑΣ", "ΣΕ", "ΣΕΙΣ", "ΣΗΜΕΡΑ", "ΣΙΓΑ", "ΣΟΥ", "ΣΤΑ", "ΣΤΗ", "ΣΤΗΝ", "ΣΤΗΣ", "ΣΤΙΣ", "ΣΤΟ", "ΣΤΟΝ", "ΣΤΟΥ", "ΣΤΟΥΣ", "ΣΤΩΝ", "ΣΥΓΧΡΟΝΩΣ", "ΣΥΝ", "ΣΥΝΑΜΑ", "ΣΥΝΕΠΩΣ", "ΣΥΝΗΘΩΣ", "ΣΥΧΝΑ", "ΣΥΧΝΑΣ", "ΣΥΧΝΕΣ", "ΣΥΧΝΗ", "ΣΥΧΝΗΝ", "ΣΥΧΝΗΣ", "ΣΥΧΝΟ", "ΣΥΧΝΟΙ", "ΣΥΧΝΟΝ", "ΣΥΧΝΟΣ", "ΣΥΧΝΟΥ", "ΣΥΧΝΟΥ", "ΣΥΧΝΟΥΣ", "ΣΥΧΝΩΝ", "ΣΥΧΝΩΣ", "ΣΧΕΔΟΝ", "ΣΩΣΤΑ", "ΤΑ", "ΤΑΔΕ", "ΤΑΥΤΑ", "ΤΑΥΤΕΣ", "ΤΑΥΤΗ", "ΤΑΥΤΗΝ", "ΤΑΥΤΗΣ", "ΤΑΥΤΟ", "ΤΑΥΤΟΝ", "ΤΑΥΤΟΣ", "ΤΑΥΤΟΥ", "ΤΑΥΤΩΝ", "ΤΑΧΑ", "ΤΑΧΑΤΕ", "ΤΕΛΙΚΑ", "ΤΕΛΙΚΩΣ", "ΤΕΣ", "ΤΕΤΟΙΑ", "ΤΕΤΟΙΑΝ", "ΤΕΤΟΙΑΣ", "ΤΕΤΟΙΕΣ", "ΤΕΤΟΙΟ", "ΤΕΤΟΙΟΙ", "ΤΕΤΟΙΟΝ", "ΤΕΤΟΙΟΣ", "ΤΕΤΟΙΟΥ", "ΤΕΤΟΙΟΥΣ", "ΤΕΤΟΙΩΝ", "ΤΗ", "ΤΗΝ", "ΤΗΣ", "ΤΙ", "ΤΙΠΟΤΑ", "ΤΙΠΟΤΕ", "ΤΙΣ", "ΤΟ", "ΤΟΙ", "ΤΟΝ", "ΤΟΣ", "ΤΟΣΑ", "ΤΟΣΕΣ", "ΤΟΣΗ", "ΤΟΣΗΝ", "ΤΟΣΗΣ", "ΤΟΣΟ", "ΤΟΣΟΙ", "ΤΟΣΟΝ", "ΤΟΣΟΣ", "ΤΟΣΟΥ", "ΤΟΣΟΥΣ", "ΤΟΣΩΝ", "ΤΟΤΕ", "ΤΟΥ", "ΤΟΥΛΑΧΙΣΤΟ", "ΤΟΥΛΑΧΙΣΤΟΝ", "ΤΟΥΣ", "ΤΟΥΤΑ", "ΤΟΥΤΕΣ", "ΤΟΥΤΗ", "ΤΟΥΤΗΝ", "ΤΟΥΤΗΣ", "ΤΟΥΤΟ", "ΤΟΥΤΟΙ", "ΤΟΥΤΟΙΣ", "ΤΟΥΤΟΝ", "ΤΟΥΤΟΣ", "ΤΟΥΤΟΥ", "ΤΟΥΤΟΥΣ", "ΤΟΥΤΩΝ", "ΤΥΧΟΝ", "ΤΩΝ", "ΤΩΡΑ", "ΥΠ", "ΥΠΕΡ", "ΥΠΟ", "ΥΠΟΨΗ", "ΥΠΟΨΙΝ", "ΥΣΤΕΡΑ", "ΦΕΤΟΣ", "ΧΑΜΗΛΑ", "ΧΘΕΣ", "ΧΤΕΣ", "ΧΩΡΙΣ", "ΧΩΡΙΣΤΑ", "ΨΗΛΑ", "Ω", "ΩΡΑΙΑ", "ΩΣ", "ΩΣΑΝ", "ΩΣΟΤΟΥ", "ΩΣΠΟΥ", "ΩΣΤΕ", "ΩΣΤΟΣΟ", "ΩΧ", "ΕΝΟΣ", "ΔΥΟ", "ΤΡΙΑ", "ΤΡΙΩΝ", "ΤΕΣΣΕΡΑ", "ΤΕΣΣΑΡΩΝ", "ΠΕΝΤΕ", "ΕΞΙ", "ΕΦΤΑ", "ΟΚΤΩ", "ΟΧΤΩ", "ΕΝΝΙΑ");
        if (in_array($w, $stop_w))
            return null;

        // Verbs Removal
        $re = '/(.+?)(ΙΖΑ|ΙΣΑ|ΙΣΩ|ΙΣΟΥΝ|ΙΖΕΣ|ΙΖΕ|ΙΖΑΜΕ|ΙΖΑΤΕ|ΙΖΑΝ|ΙΖΑΝΕ|ΙΖΩ|ΙΖΕΙΣ|ΙΖΕΙ|ΙΖΟΥΜΕ|ΙΖΕΤΕ|ΙΖΟΥΝ|ΙΖΟΥΝΕ|ΕΙ|ΙΣΕ|ΗΣΕ|ΟΝΤΑΙ|ΟΥΝ|ΟΝΤΑΣ)$/';
        if (preg_match($re, $w))
            return null;
    }





    // ------------------------- Stemming Rules : Original Stemmer


    // step1list is used in Step 1. 41 stems
    $step1list = Array();
    $step1list["ΦΑΓΙΑ"] = "ΦΑ";
    $step1list["ΦΑΓΙΟΥ"] = "ΦΑ";
    $step1list["ΦΑΓΙΩΝ"] = "ΦΑ";
    $step1list["ΣΚΑΓΙΑ"] = "ΣΚΑ";
    $step1list["ΣΚΑΓΙΟΥ"] = "ΣΚΑ";
    $step1list["ΣΚΑΓΙΩΝ"] = "ΣΚΑ";
    $step1list["ΟΛΟΓΙΟΥ"] = "ΟΛΟ";
    $step1list["ΟΛΟΓΙΑ"] = "ΟΛΟ";
    $step1list["ΟΛΟΓΙΩΝ"] = "ΟΛΟ";
    $step1list["ΣΟΓΙΟΥ"] = "ΣΟ";
    $step1list["ΣΟΓΙΑ"] = "ΣΟ";
    $step1list["ΣΟΓΙΩΝ"] = "ΣΟ";
    $step1list["ΤΑΤΟΓΙΑ"] = "ΤΑΤΟ";
    $step1list["ΤΑΤΟΓΙΟΥ"] = "ΤΑΤΟ";
    $step1list["ΤΑΤΟΓΙΩΝ"] = "ΤΑΤΟ";
    $step1list["ΚΡΕΑΣ"] = "ΚΡΕ";
    $step1list["ΚΡΕΑΤΟΣ"] = "ΚΡΕ";
    $step1list["ΚΡΕΑΤΑ"] = "ΚΡΕ";
    $step1list["ΚΡΕΑΤΩΝ"] = "ΚΡΕ";
    $step1list["ΠΕΡΑΣ"] = "ΠΕΡ";
    $step1list["ΠΕΡΑΤΟΣ"] = "ΠΕΡ";
    $step1list["ΠΕΡΑΤΗ"] = "ΠΕΡ"; //Added by Spyros . also at $re in step1
    $step1list["ΠΕΡΑΤΑ"] = "ΠΕΡ";
    $step1list["ΠΕΡΑΤΩΝ"] = "ΠΕΡ";
    $step1list["ΤΕΡΑΣ"] = "ΤΕΡ";
    $step1list["ΤΕΡΑΤΟΣ"] = "ΤΕΡ";
    $step1list["ΤΕΡΑΤΑ"] = "ΤΕΡ";
    $step1list["ΤΕΡΑΤΩΝ"] = "ΤΕΡ";
    $step1list["ΦΩΣ"] = "ΦΩ";
    $step1list["ΦΩΤΟΣ"] = "ΦΩ";
    $step1list["ΦΩΤΑ"] = "ΦΩ";
    $step1list["ΦΩΤΩΝ"] = "ΦΩ";
    $step1list["ΚΑΘΕΣΤΩΣ"] = "ΚΑΘΕΣΤ";
    $step1list["ΚΑΘΕΣΤΩΤΟΣ"] = "ΚΑΘΕΣΤ";
    $step1list["ΚΑΘΕΣΤΩΤΑ"] = "ΚΑΘΕΣΤ";
    $step1list["ΚΑΘΕΣΤΩΤΩΝ"] = "ΚΑΘΕΣΤ";
    $step1list["ΓΕΓΟΝΟΣ"] = "ΓΕΓΟΝ";
    $step1list["ΓΕΓΟΝΟΤΟΣ"] = "ΓΕΓΟΝ";
    $step1list["ΓΕΓΟΝΟΤΑ"] = "ΓΕΓΟΝ";
    $step1list["ΓΕΓΟΝΟΤΩΝ"] = "ΓΕΓΟΝ";

    $v = '(Α|Ε|Η|Ι|Ο|Υ|Ω)'; // vowel
    $v2 = '(Α|Ε|Η|Ι|Ο|Ω)';      //vowel without Y

    $test1 = true;              // Variable needed later


    /*
     * preg_match definition
     *
     * preg_match( $pattern, $input_string, $matches);
     * $pattern     :   the pattern to search for as a string
     * $input_string:   the input string
     * $matches     :   if $matches is provided, then it is filled with the results of search
     */


    $match = [];

    //Step S1. 14 stems
    $re = '/^(.+?)(ΙΖΑ|ΙΖΕΣ|ΙΖΕ|ΙΖΑΜΕ|ΙΖΑΤΕ|ΙΖΑΝ|ΙΖΑΝΕ|ΙΖΩ|ΙΖΕΙΣ|ΙΖΕΙ|ΙΖΟΥΜΕ|ΙΖΕΤΕ|ΙΖΟΥΝ|ΙΖΟΥΝΕ)$/';
    $exceptS1 = '/^(ΑΝΑΜΠΑ|ΕΜΠΑ|ΕΠΑ|ΞΑΝΑΠΑ|ΠΑ|ΠΕΡΙΠΑ|ΑΘΡΟ|ΣΥΝΑΘΡΟ|ΔΑΝΕ)$/';
    $exceptS2 = '/^(ΜΑΡΚ|ΚΟΡΝ|ΑΜΠΑΡ|ΑΡΡ|ΒΑΘΥΡΙ|ΒΑΡΚ|Β|ΒΟΛΒΟΡ|ΓΚΡ|ΓΛΥΚΟΡ|ΓΛΥΚΥΡ|ΙΜΠ|Λ|ΛΟΥ|ΜΑΡ|Μ|ΠΡ|ΜΠΡ|ΠΟΛΥΡ|Π|Ρ|ΠΙΠΕΡΟΡ)$/';
    if (preg_match($re, $w, $match)) {
        $stem = $match[1];
        $suffix = $match[2];
        $w = $stem . $step1list[$suffix];
        $test1 = false;
        if (preg_match($exceptS1, $w))
            $w = $w . 'I';
        if (preg_match($exceptS2, $w))
            $w = $w . 'IΖ';


        return stem_returnStem($w, $encoding_changed);
    }




    //Step S2. 7 stems
    $re = '/^(.+?)(ΩΘΗΚΑ|ΩΘΗΚΕΣ|ΩΘΗΚΕ|ΩΘΗΚΑΜΕ|ΩΘΗΚΑΤΕ|ΩΘΗΚΑΝ|ΩΘΗΚΑΝΕ)$/';
    $exceptS1 = '/^(ΑΛ|ΒΙ|ΕΝ|ΥΨ|ΛΙ|ΖΩ|Σ|Χ)$/';
    if (preg_match($re, $w, $match)) {
        $stem = $match[1];
        $suffix = $match[2];
        $w = $stem . $step1list[$suffix];
        $test1 = false;
        if (preg_match($exceptS1, $w)) {
            $w = $w . 'ΩΝ';
        }

        return stem_returnStem($w, $encoding_changed);
    }





    //Step S3. 7 stems
    $re = '/^(.+?)(ΙΣΑ|ΙΣΕΣ|ΙΣΕ|ΙΣΑΜΕ|ΙΣΑΤΕ|ΙΣΑΝ|ΙΣΑΝΕ)$/';
    $exceptS1 = '/^(ΑΝΑΜΠΑ|ΑΘΡΟ|ΕΜΠΑ|ΕΣΕ|ΕΣΩΚΛΕ|ΕΠΑ|ΞΑΝΑΠΑ|ΕΠΕ|ΠΕΡΙΠΑ|ΑΘΡΟ|ΣΥΝΑΘΡΟ|ΔΑΝΕ|ΚΛΕ|ΧΑΡΤΟΠΑ|ΕΞΑΡΧΑ|ΜΕΤΕΠΕ|ΑΠΟΚΛΕ|ΑΠΕΚΛΕ|ΕΚΛΕ|ΠΕ|ΠΕΡΙΠΑ)$/';
    $exceptS2 = '/^(ΑΝ|ΑΦ|ΓΕ|ΓΙΓΑΝΤΟΑΦ|ΓΚΕ|ΔΗΜΟΚΡΑΤ|ΚΟΜ|ΓΚ|Μ|Π|ΠΟΥΚΑΜ|ΟΛΟ|ΛΑΡ)$/';
    if ($w == "ΙΣΑ") {
        $w = "ΙΣ";
        return $w;
    }
    if (preg_match($re, $w, $match)) {
        $stem = $match[1];
        $suffix = $match[2];
        $w = $stem . $step1list[$suffix];
        $test1 = false;
        if (preg_match($exceptS1, $w)) {
            $w = $w . 'Ι';
        }

        return stem_returnStem($w, $encoding_changed);
    }


    //Step S4. 7 stems
    $re = '/^(.+?)(ΙΣΩ|ΙΣΕΙΣ|ΙΣΕΙ|ΙΣΟΥΜΕ|ΙΣΕΤΕ|ΙΣΟΥΝ|ΙΣΟΥΝΕ)$/';
    $exceptS1 = '/^(ΑΝΑΜΠΑ|ΕΜΠΑ|ΕΣΕ|ΕΣΩΚΛΕ|ΕΠΑ|ΞΑΝΑΠΑ|ΕΠΕ|ΠΕΡΙΠΑ|ΑΘΡΟ|ΣΥΝΑΘΡΟ|ΔΑΝΕ|ΚΛΕ|ΧΑΡΤΟΠΑ|ΕΞΑΡΧΑ|ΜΕΤΕΠΕ|ΑΠΟΚΛΕ|ΑΠΕΚΛΕ|ΕΚΛΕ|ΠΕ|ΠΕΡΙΠΑ)$/';
    if (preg_match($re, $w, $match)) {
        $stem = $match[1];
        $suffix = $match[2];
        $w = $stem . $step1list[$suffix];
        $test1 = false;
        if (preg_match($exceptS1, $w)) {
            $w = $w . 'Ι';
        }
        return stem_returnStem($w, $encoding_changed);
    }



    //Step S5. 11 stems
    $re = '/^(.+?)(ΙΣΤΟΣ|ΙΣΤΟΥ|ΙΣΤΟ|ΙΣΤΕ|ΙΣΤΟΙ|ΙΣΤΩΝ|ΙΣΤΟΥΣ|ΙΣΤΗ|ΙΣΤΗΣ|ΙΣΤΑ|ΙΣΤΕΣ)$/';
    $exceptS1 = '/^(Μ|Π|ΑΠ|ΑΡ|ΗΔ|ΚΤ|ΣΚ|ΣΧ|ΥΨ|ΦΑ|ΧΡ|ΧΤ|ΑΚΤ|ΑΟΡ|ΑΣΧ|ΑΤΑ|ΑΧΝ|ΑΧΤ|ΓΕΜ|ΓΥΡ|ΕΜΠ|ΕΥΠ|ΕΧΘ|ΗΦΑ|ΉΦΑ|ΚΑΘ|ΚΑΚ|ΚΥΛ|ΛΥΓ|ΜΑΚ|ΜΕΓ|ΤΑΧ|ΦΙΛ|ΧΩΡ)$/';
    $exceptS2 = '/^(ΔΑΝΕ|ΣΥΝΑΘΡΟ|ΚΛΕ|ΣΕ|ΕΣΩΚΛΕ|ΑΣΕ|ΠΛΕ)$/';
    if (preg_match($re, $w, $match)) {
        $stem = $match[1];
        $suffix = $match[2];
        $w = $stem . $step1list[$suffix];
        $test1 = false;
        if (preg_match($exceptS1, $w)) {
            $w = $w . 'ΙΣΤ';
        }
        if (preg_match($exceptS2, $w)) {
            $w = $w . 'Ι';
        }
        return stem_returnStem($w, $encoding_changed);
    }



    //Step S6. 6 stems
    $re = '/^(.+?)(ΙΣΜΟ|ΙΣΜΟΙ|ΙΣΜΟΣ|ΙΣΜΟΥ|ΙΣΜΟΥΣ|ΙΣΜΩΝ)$/';
    $exceptS1 = '/^(ΑΓΝΩΣΤΙΚ|ΑΤΟΜΙΚ|ΓΝΩΣΤΙΚ|ΕΘΝΙΚ|ΕΚΛΕΚΤΙΚ|ΣΚΕΠΤΙΚ|ΤΟΠΙΚ)$/';
    $exceptS2 = '/^(ΣΕ|ΜΕΤΑΣΕ|ΜΙΚΡΟΣΕ|ΕΓΚΛΕ|ΑΠΟΚΛΕ)$/';
    $exceptS3 = '/^(ΔΑΝΕ|ΑΝΤΙΔΑΝΕ)$/';
    $exceptS4 = '/^(ΑΛΕΞΑΝΔΡΙΝ|ΒΥΖΑΝΤΙΝ|ΘΕΑΤΡΙΝ)$/';
    if (preg_match($re, $w, $match)) {
        $stem = $match[1];
        $suffix = $match[2];
        $w = $stem;
        $test1 = false;
        if (preg_match($exceptS1, $w)) {
            $w = str_replace('ΙΚ', "", $w);
        }
        if (preg_match($exceptS2, $w)) {
            $w = $w . "ΙΣΜ";
        }
        if (preg_match($exceptS3, $w)) {
            $w = $w . "Ι";
        }
        if (preg_match($exceptS4, $w)) {
            $w = str_replace('ΙΝ', "", $w);
        }
        return stem_returnStem($w, $encoding_changed);
    }





    //Step S7. 4 stems
    $re = '/^(.+?)(ΑΡΑΚΙ|ΑΡΑΚΙΑ|ΟΥΔΑΚΙ|ΟΥΔΑΚΙΑ)$/';
    $exceptS1 = '/^(Σ|Χ)$/';
    if (preg_match($re, $w, $match)) {
        $stem = $match[1];
        $suffix = $match[2];
        $w = $stem;
        $test1 = false;
        if (preg_match($exceptS1, $w)) {
            $w = $w . "AΡΑΚ";
        }

        return stem_returnStem($w, $encoding_changed);
    }





    //Step S8. 8 stems
    $re = '/^(.+?)(ΑΚΙ|ΑΚΙΑ|ΙΤΣΑ|ΙΤΣΑΣ|ΙΤΣΕΣ|ΙΤΣΩΝ|ΑΡΑΚΙ|ΑΡΑΚΙΑ)$/';
    $exceptS1 = '/^(ΑΝΘΡ|ΒΑΜΒ|ΒΡ|ΚΑΙΜ|ΚΟΝ|ΚΟΡ|ΛΑΒΡ|ΛΟΥΛ|ΜΕΡ|ΜΟΥΣΤ|ΝΑΓΚΑΣ|ΠΛ|Ρ|ΡΥ|Σ|ΣΚ|ΣΟΚ|ΣΠΑΝ|ΤΖ|ΦΑΡΜ|Χ|ΚΑΠΑΚ|ΑΛΙΣΦ|ΑΜΒΡ|ΑΝΘΡ|Κ|ΦΥΛ|ΚΑΤΡΑΠ|ΚΛΙΜ|ΜΑΛ|ΣΛΟΒ|Φ|ΣΦ|ΤΣΕΧΟΣΛΟΒ)$/';
    $exceptS2 = '/^(Β|ΒΑΛ|ΓΙΑΝ|ΓΛ|Ζ|ΗΓΟΥΜΕΝ|ΚΑΡΔ|ΚΟΝ|ΜΑΚΡΥΝ|ΝΥΦ|ΠΑΤΕΡ|Π|ΣΚ|ΤΟΣ|ΤΡΙΠΟΛ)$/';
    $exceptS3 = '/(ΚΟΡ)$/'; // for words like ΠΛΟΥΣΙΟΚΟΡΙΤΣΑ, ΠΑΛΙΟΚΟΡΙΤΣΑ etc
    if (preg_match($re, $w, $match)) {
        $stem = $match[1];
        $suffix = $match[2];
        $w = $stem;
        $test1 = false;
        if (preg_match($exceptS1, $w)) {
            $w = $w . "ΑΚ";
        }
        if (preg_match($exceptS2, $w)) {
            $w = $w . "ΙΤΣ";
        }
        if (preg_match($exceptS3, $w)) {
            $w = $w . "ΙΤΣ";
        }
        return stem_returnStem($w, $encoding_changed);
    }






    //Step S9. 3 stems
    $re = '/^(.+?)(ΙΔΙΟ|ΙΔΙΑ|ΙΔΙΩΝ)$/';
    $exceptS1 = '/^(ΑΙΦΝ|ΙΡ|ΟΛΟ|ΨΑΛ)$/';
    $exceptS2 = '/(Ε|ΠΑΙΧΝ)$/';
    if (preg_match($re, $w, $match)) {
        $stem = $match[1];
        $suffix = $match[2];
        $w = $stem;
        $test1 = false;
        if (preg_match($exceptS1, $w)) {
            $w = $w . "ΙΔ";
        }
        if (preg_match($exceptS2, $w)) {
            $w = $w . "ΙΔ";
        }
        return stem_returnStem($w, $encoding_changed);
    }





    //Step S10. 4 stems
    $re = '/^(.+?)(ΙΣΚΟΣ|ΙΣΚΟΥ|ΙΣΚΟ|ΙΣΚΕ)$/';
    $exceptS1 = '/^(Δ|ΙΒ|ΜΗΝ|Ρ|ΦΡΑΓΚ|ΛΥΚ|ΟΒΕΛ)$/';
    if (preg_match($re, $w, $match)) {
        $stem = $match[1];
        $suffix = $match[2];
        $w = $stem;
        $test1 = false;
        if (preg_match($exceptS1, $w)) {
            $w = $w . "ΙΣΚ";
        }

        return stem_returnStem($w, $encoding_changed);
    }






    //Step1
    $re = '/(.*)(ΦΑΓΙΑ|ΦΑΓΙΟΥ|ΦΑΓΙΩΝ|ΣΚΑΓΙΑ|ΣΚΑΓΙΟΥ|ΣΚΑΓΙΩΝ|ΟΛΟΓΙΟΥ|ΟΛΟΓΙΑ|ΟΛΟΓΙΩΝ|ΣΟΓΙΟΥ|ΣΟΓΙΑ|ΣΟΓΙΩΝ|ΤΑΤΟΓΙΑ|ΤΑΤΟΓΙΟΥ|ΤΑΤΟΓΙΩΝ|ΚΡΕΑΣ|ΚΡΕΑΤΟΣ|ΚΡΕΑΤΑ|ΚΡΕΑΤΩΝ|ΠΕΡΑΣ|ΠΕΡΑΤΟΣ|ΠΕΡΑΤΗ|ΠΕΡΑΤΑ|ΠΕΡΑΤΩΝ|ΤΕΡΑΣ|ΤΕΡΑΤΟΣ|ΤΕΡΑΤΑ|ΤΕΡΑΤΩΝ|ΦΩΣ|ΦΩΤΟΣ|ΦΩΤΑ|ΦΩΤΩΝ|ΚΑΘΕΣΤΩΣ|ΚΑΘΕΣΤΩΤΟΣ|ΚΑΘΕΣΤΩΤΑ|ΚΑΘΕΣΤΩΤΩΝ|ΓΕΓΟΝΟΣ|ΓΕΓΟΝΟΤΟΣ|ΓΕΓΟΝΟΤΑ|ΓΕΓΟΝΟΤΩΝ)$/';
    if (preg_match($re, $w, $match)) {
        //debug($w,1);
        $stem = $match[1];
        $suffix = $match[2];
        $w = $stem . $step1list[$suffix];
        $test1 = false;
    }




    // Step 2a. 2 stems
    $re = '/^(.+?)(ΑΔΕΣ|ΑΔΩΝ)$/';
    if (preg_match($re, $w, $match)) {
        $stem = $match[1];
        $w = $stem;
        $re = '/(ΟΚ|ΜΑΜ|ΜΑΝ|ΜΠΑΜΠ|ΠΑΤΕΡ|ΓΙΑΓΙ|ΝΤΑΝΤ|ΚΥΡ|ΘΕΙ|ΠΕΘΕΡ)$/';
        if (!preg_match($re, $w)) {
            $w = $w . "ΑΔ";
        }
    }





    //Step 2b. 2 stems
    $re = '/^(.+?)(ΕΔΕΣ|ΕΔΩΝ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $exept2 = '/(ΟΠ|ΙΠ|ΕΜΠ|ΥΠ|ΓΗΠ|ΔΑΠ|ΚΡΑΣΠ|ΜΙΛ)$/';
        if (preg_match($exept2, $w)) {
            $w = $w . 'ΕΔ';
        }
    }






    //Step 2c
    $re = '/^(.+?)(ΟΥΔΕΣ|ΟΥΔΩΝ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;

        $exept3 = '/(ΑΡΚ|ΚΑΛΙΑΚ|ΠΕΤΑΛ|ΛΙΧ|ΠΛΕΞ|ΣΚ|Σ|ΦΛ|ΦΡ|ΒΕΛ|ΛΟΥΛ|ΧΝ|ΣΠ|ΤΡΑΓ|ΦΕ)$/';
        if (preg_match($exept3, $w)) {
            $w = $w . 'ΟΥΔ';
        }
    }




    //Step 2d
    $re = '/^(.+?)(ΕΩΣ|ΕΩΝ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $exept4 = '/^(Θ|Δ|ΕΛ|ΓΑΛ|Ν|Π|ΙΔ|ΠΑΡ)$/';
        if (preg_match($exept4, $w)) {
            $w = $w . 'Ε';
        }
    }



    $fp = [];
    //Step 3
    $re = '/^(.+?)(ΙΑ|ΙΟΥ|ΙΩΝ)$/';
    if (preg_match($re, $w, $fp)) {
        $stem = $fp[1];
        $w = $stem;
        $re = '/' . $v . '$/';
        $test1 = false;
        if (preg_match($re, $w)) {
            $w = $stem . 'Ι';
        }
    }




    //Step 4
    $re = '/^(.+?)(ΙΚΑ|ΙΚΟ|ΙΚΟΥ|ΙΚΩΝ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;

        $test1 = false;
        $re = '/' . $v . '$/';
        $exept5 = '/^(ΑΛ|ΑΔ|ΕΝΔ|ΑΜΑΝ|ΑΜΜΟΧΑΛ|ΗΘ|ΑΝΗΘ|ΑΝΤΙΔ|ΦΥΣ|ΒΡΩΜ|ΓΕΡ|ΕΞΩΔ|ΚΑΛΠ|ΚΑΛΛΙΝ|ΚΑΤΑΔ|ΜΟΥΛ|ΜΠΑΝ|ΜΠΑΓΙΑΤ|ΜΠΟΛ|ΜΠΟΣ|ΝΙΤ|ΞΙΚ|ΣΥΝΟΜΗΛ|ΠΕΤΣ|ΠΙΤΣ|ΠΙΚΑΝΤ|ΠΛΙΑΤΣ|ΠΟΣΤΕΛΝ|ΠΡΩΤΟΔ|ΣΕΡΤ|ΣΥΝΑΔ|ΤΣΑΜ|ΥΠΟΔ|ΦΙΛΟΝ|ΦΥΛΟΔ|ΧΑΣ)$/';
        if (preg_match($re, $w) || preg_match($exept5, $w)) {
            $w = $w . 'ΙΚ';
        }
    }





    //step 5a
    $re = '/^(.+?)(ΑΜΕ)$/';
    $re2 = '/^(.+?)(ΑΓΑΜΕ|ΗΣΑΜΕ|ΟΥΣΑΜΕ|ΗΚΑΜΕ|ΗΘΗΚΑΜΕ)$/';
    if ($w == "ΑΓΑΜΕ") {
        $w = "ΑΓΑΜ";
    }
    if (preg_match($re2, $w)) {
        preg_match($re2, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;
    }
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $exept6 = '/^(ΑΝΑΠ|ΑΠΟΘ|ΑΠΟΚ|ΑΠΟΣΤ|ΒΟΥΒ|ΞΕΘ|ΟΥΛ|ΠΕΘ|ΠΙΚΡ|ΠΟΤ|ΣΙΧ|Χ)$/';
        if (preg_match($exept6, $w)) {
            $w = $w . "ΑΜ";
        }
    }






    //Step 5b
    $re2 = '/^(.+?)(ΑΝΕ)$/';
    $re3 = '/^(.+?)(ΑΓΑΝΕ|ΗΣΑΝΕ|ΟΥΣΑΝΕ|ΙΟΝΤΑΝΕ|ΙΟΤΑΝΕ|ΙΟΥΝΤΑΝΕ|ΟΝΤΑΝΕ|ΟΤΑΝΕ|ΟΥΝΤΑΝΕ|ΗΚΑΝΕ|ΗΘΗΚΑΝΕ)$/';
    if (preg_match($re3, $w)) {
        preg_match($re3, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $re3 = '/^(ΤΡ|ΤΣ)$/';
        if (preg_match($re3, $w)) {
            $w = $w . "ΑΓΑΝ";
        }
    }
    if (preg_match($re2, $w)) {
        preg_match($re2, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $re2 = '/' . $v2 . '$/';
        $exept7 = '/^(ΒΕΤΕΡ|ΒΟΥΛΚ|ΒΡΑΧΜ|Γ|ΔΡΑΔΟΥΜ|Θ|ΚΑΛΠΟΥΖ|ΚΑΣΤΕΛ|ΚΟΡΜΟΡ|ΛΑΟΠΛ|ΜΩΑΜΕΘ|Μ|ΜΟΥΣΟΥΛΜ|Ν|ΟΥΛ|Π|ΠΕΛΕΚ|ΠΛ|ΠΟΛΙΣ|ΠΟΡΤΟΛ|ΣΑΡΑΚΑΤΣ|ΣΟΥΛΤ|ΤΣΑΡΛΑΤ|ΟΡΦ|ΤΣΙΓΓ|ΤΣΟΠ|ΦΩΤΟΣΤΕΦ|Χ|ΨΥΧΟΠΛ|ΑΓ|ΟΡΦ|ΓΑΛ|ΓΕΡ|ΔΕΚ|ΔΙΠΛ|ΑΜΕΡΙΚΑΝ|ΟΥΡ|ΠΙΘ|ΠΟΥΡΙΤ|Σ|ΖΩΝΤ|ΙΚ|ΚΑΣΤ|ΚΟΠ|ΛΙΧ|ΛΟΥΘΗΡ|ΜΑΙΝΤ|ΜΕΛ|ΣΙΓ|ΣΠ|ΣΤΕΓ|ΤΡΑΓ|ΤΣΑΓ|Φ|ΕΡ|ΑΔΑΠ|ΑΘΙΓΓ|ΑΜΗΧ|ΑΝΙΚ|ΑΝΟΡΓ|ΑΠΗΓ|ΑΠΙΘ|ΑΤΣΙΓΓ|ΒΑΣ|ΒΑΣΚ|ΒΑΘΥΓΑΛ|ΒΙΟΜΗΧ|ΒΡΑΧΥΚ|ΔΙΑΤ|ΔΙΑΦ|ΕΝΟΡΓ|ΘΥΣ|ΚΑΠΝΟΒΙΟΜΗΧ|ΚΑΤΑΓΑΛ|ΚΛΙΒ|ΚΟΙΛΑΡΦ|ΛΙΒ|ΜΕΓΛΟΒΙΟΜΗΧ|ΜΙΚΡΟΒΙΟΜΗΧ|ΝΤΑΒ|ΞΗΡΟΚΛΙΒ|ΟΛΙΓΟΔΑΜ|ΟΛΟΓΑΛ|ΠΕΝΤΑΡΦ|ΠΕΡΗΦ|ΠΕΡΙΤΡ|ΠΛΑΤ|ΠΟΛΥΔΑΠ|ΠΟΛΥΜΗΧ|ΣΤΕΦ|ΤΑΒ|ΤΕΤ|ΥΠΕΡΗΦ|ΥΠΟΚΟΠ|ΧΑΜΗΛΟΔΑΠ|ΨΗΛΟΤΑΒ)$/';
        if (preg_match($re2, $w) || preg_match($exept7, $w)) {
            $w = $w . "ΑΝ";
        }
    }




    //Step 5c
    $re3 = '/^(.+?)(ΕΤΕ)$/';
    $re4 = '/^(.+?)(ΗΣΕΤΕ)$/';
    if (preg_match($re4, $w)) {
        preg_match($re4, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;
    }
    if (preg_match($re3, $w)) {
        preg_match($re3, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $re3 = '/' . $v2 . '$/';
        $exept8 = '/(ΟΔ|ΑΙΡ|ΦΟΡ|ΤΑΘ|ΔΙΑΘ|ΣΧ|ΕΝΔ|ΕΥΡ|ΤΙΘ|ΥΠΕΡΘ|ΡΑΘ|ΕΝΘ|ΡΟΘ|ΣΘ|ΠΥΡ|ΑΙΝ|ΣΥΝΔ|ΣΥΝ|ΣΥΝΘ|ΧΩΡ|ΠΟΝ|ΒΡ|ΚΑΘ|ΕΥΘ|ΕΚΘ|ΝΕΤ|ΡΟΝ|ΑΡΚ|ΒΑΡ|ΒΟΛ|ΩΦΕΛ)$/';
        $exept9 = '/^(ΑΒΑΡ|ΒΕΝ|ΕΝΑΡ|ΑΒΡ|ΑΔ|ΑΘ|ΑΝ|ΑΠΛ|ΒΑΡΟΝ|ΝΤΡ|ΣΚ|ΚΟΠ|ΜΠΟΡ|ΝΙΦ|ΠΑΓ|ΠΑΡΑΚΑΛ|ΣΕΡΠ|ΣΚΕΛ|ΣΥΡΦ|ΤΟΚ|Υ|Δ|ΕΜ|ΘΑΡΡ|Θ)$/';

        if (preg_match($re3, $w) || preg_match($exept8, $w) || preg_match($exept9, $w)) {
            $w = $w . "ΕΤ";
        }
    }







    //Step 5d
    $re = '/^(.+?)(ΟΝΤΑΣ|ΩΝΤΑΣ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $exept10 = '/^(ΑΡΧ)$/';
        $exept11 = '/(ΚΡΕ)$/';
        if (preg_match($exept10, $w)) {
            $w = $w . "ΟΝΤ";
        }
        if (preg_match($exept11, $w)) {
            $w = $w . "ΩΝΤ";
        }
    }







    //Step 5e
    $re = '/^(.+?)(ΟΜΑΣΤΕ|ΙΟΜΑΣΤΕ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $exept11 = '/^(ΟΝ)$/';
        if (preg_match($exept11, $w)) {
            $w = $w . "ΟΜΑΣΤ";
        }
    }








    //Step 5f
    $re = '/^(.+?)(ΕΣΤΕ)$/';
    $re2 = '/^(.+?)(ΙΕΣΤΕ)$/';

    if (preg_match($re2, $w)) {
        preg_match($re2, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $re2 = '/^(Π|ΑΠ|ΣΥΜΠ|ΑΣΥΜΠ|ΑΚΑΤΑΠ|ΑΜΕΤΑΜΦ)$/';
        if (preg_match($re2, $w)) {
            $w = $w . "ΙΕΣΤ";
        }
    }

    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $exept12 = '/^(ΑΛ|ΑΡ|ΕΚΤΕΛ|Ζ|Μ|Ξ|ΠΑΡΑΚΑΛ|ΑΡ|ΠΡΟ|ΝΙΣ)$/';
        if (preg_match($exept12, $w)) {
            $w = $w . "ΕΣΤ";
        }
    }









    //Step 5g
    $re = '/^(.+?)(ΗΚΑ|ΗΚΕΣ|ΗΚΕ)$/';
    $re2 = '/^(.+?)(ΗΘΗΚΑ|ΗΘΗΚΕΣ|ΗΘΗΚΕ)$/';

    if (preg_match($re2, $w)) {
        preg_match($re2, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;
    }

    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $exept13 = '/(ΣΚΩΛ|ΣΚΟΥΛ|ΝΑΡΘ|ΣΦ|ΟΘ|ΠΙΘ)$/';
        $exept14 = '/^(ΔΙΑΘ|Θ|ΠΑΡΑΚΑΤΑΘ|ΠΡΟΣΘ|ΣΥΝΘ|)$/';
        if (preg_match($exept13, $w) || preg_match($exept14, $w)) {
            $w = $w . "ΗΚ";
        }
    }





    //Step 5h
    $re = '/^(.+?)(ΟΥΣΑ|ΟΥΣΕΣ|ΟΥΣΕ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $exept15 = '/^(ΦΑΡΜΑΚ|ΧΑΔ|ΑΓΚ|ΑΝΑΡΡ|ΒΡΟΜ|ΕΚΛΙΠ|ΛΑΜΠΙΔ|ΛΕΧ|Μ|ΠΑΤ|Ρ|Λ|ΜΕΔ|ΜΕΣΑΖ|ΥΠΟΤΕΙΝ|ΑΜ|ΑΙΘ|ΑΝΗΚ|ΔΕΣΠΟΖ|ΕΝΔΙΑΦΕΡ|ΔΕ|ΔΕΥΤΕΡΕΥ|ΚΑΘΑΡΕΥ|ΠΛΕ|ΤΣΑ)$/';
        $exept16 = '/(ΠΟΔΑΡ|ΒΛΕΠ|ΠΑΝΤΑΧ|ΦΡΥΔ|ΜΑΝΤΙΛ|ΜΑΛΛ|ΚΥΜΑΤ|ΛΑΧ|ΛΗΓ|ΦΑΓ|ΟΜ|ΠΡΩΤ)$/';
        if (preg_match($exept15, $w) || preg_match($exept16, $w)) {
            $w = $w . "ΟΥΣ";
        }
    }

    //Step 5i
    $re = '/^(.+?)(ΑΓΑ|ΑΓΕΣ|ΑΓΕ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $exept17 = '/^(ΨΟΦ|ΝΑΥΛΟΧ)$/';
        $exept20 = '/(ΚΟΛΛ)$/';
        $exept18 = '/^(ΑΒΑΣΤ|ΠΟΛΥΦ|ΑΔΗΦ|ΠΑΜΦ|Ρ|ΑΣΠ|ΑΦ|ΑΜΑΛ|ΑΜΑΛΛΙ|ΑΝΥΣΤ|ΑΠΕΡ|ΑΣΠΑΡ|ΑΧΑΡ|ΔΕΡΒΕΝ|ΔΡΟΣΟΠ|ΞΕΦ|ΝΕΟΠ|ΝΟΜΟΤ|ΟΛΟΠ|ΟΜΟΤ|ΠΡΟΣΤ|ΠΡΟΣΩΠΟΠ|ΣΥΜΠ|ΣΥΝΤ|Τ|ΥΠΟΤ|ΧΑΡ|ΑΕΙΠ|ΑΙΜΟΣΤ|ΑΝΥΠ|ΑΠΟΤ|ΑΡΤΙΠ|ΔΙΑΤ|ΕΝ|ΕΠΙΤ|ΚΡΟΚΑΛΟΠ|ΣΙΔΗΡΟΠ|Λ|ΝΑΥ|ΟΥΛΑΜ|ΟΥΡ|Π|ΤΡ|Μ)$/';
        $exept19 = '/(ΟΦ|ΠΕΛ|ΧΟΡΤ|ΛΛ|ΣΦ|ΡΠ|ΦΡ|ΠΡ|ΛΟΧ|ΣΜΗΝ)$/';

        if ((preg_match($exept18, $w) || preg_match($exept19, $w)) && !(preg_match($exept17, $w) || preg_match($exept20, $w))) {
            $w = $w . "ΑΓ";
        }
    }





    //Step 5j
    $re = '/^(.+?)(ΗΣΕ|ΗΣΟΥ|ΗΣΑ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $exept21 = '/^(Ν|ΧΕΡΣΟΝ|ΔΩΔΕΚΑΝ|ΕΡΗΜΟΝ|ΜΕΓΑΛΟΝ|ΕΠΤΑΝ)$/';
        if (preg_match($exept21, $w)) {
            $w = $w . "ΗΣ";
        }
    }






    //Step 5k
    $re = '/^(.+?)(ΗΣΤΕ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $exept22 = '/^(ΑΣΒ|ΣΒ|ΑΧΡ|ΧΡ|ΑΠΛ|ΑΕΙΜΝ|ΔΥΣΧΡ|ΕΥΧΡ|ΚΟΙΝΟΧΡ|ΠΑΛΙΜΨ)$/';
        if (preg_match($exept22, $w)) {
            $w = $w . "ΗΣΤ";
        }
    }






    //Step 5l
    $re = '/^(.+?)(ΟΥΝΕ|ΗΣΟΥΝΕ|ΗΘΟΥΝΕ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $exept23 = '/^(Ν|Ρ|ΣΠΙ|ΣΤΡΑΒΟΜΟΥΤΣ|ΚΑΚΟΜΟΥΤΣ|ΕΞΩΝ)$/';
        if (preg_match($exept23, $w)) {
            $w = $w . "ΟΥΝ";
        }
    }





    //Step 5l
    $re = '/^(.+?)(ΟΥΜΕ|ΗΣΟΥΜΕ|ΗΘΟΥΜΕ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
        $test1 = false;

        $exept24 = '/^(ΠΑΡΑΣΟΥΣ|Φ|Χ|ΩΡΙΟΠΛ|ΑΖ|ΑΛΛΟΣΟΥΣ|ΑΣΟΥΣ)$/';
        if (preg_match($exept24, $w)) {
            $w = $w . "ΟΥΜ";
        }
    }





    // Step 6
    $re = '/^(.+?)(ΜΑΤΑ|ΜΑΤΩΝ|ΜΑΤΟΣ)$/';
    $re2 = '/^(.+?)(Α|ΑΓΑΤΕ|ΑΓΑΝ|ΑΕΙ|ΑΜΑΙ|ΑΝ|ΑΣ|ΑΣΑΙ|ΑΤΑΙ|ΑΩ|Ε|ΕΙ|ΕΙΣ|ΕΙΤΕ|ΕΣΑΙ|ΕΣ|ΕΤΑΙ|Ι|ΙΕΜΑΙ|ΙΕΜΑΣΤΕ|ΙΕΤΑΙ|ΙΕΣΑΙ|ΙΕΣΑΣΤΕ|ΙΟΜΑΣΤΑΝ|ΙΟΜΟΥΝ|ΙΟΜΟΥΝΑ|ΙΟΝΤΑΝ|ΙΟΝΤΟΥΣΑΝ|ΙΟΣΑΣΤΑΝ|ΙΟΣΑΣΤΕ|ΙΟΣΟΥΝ|ΙΟΣΟΥΝΑ|ΙΟΤΑΝ|ΙΟΥΜΑ|ΙΟΥΜΑΣΤΕ|ΙΟΥΝΤΑΙ|ΙΟΥΝΤΑΝ|Η|ΗΔΕΣ|ΗΔΩΝ|ΗΘΕΙ|ΗΘΕΙΣ|ΗΘΕΙΤΕ|ΗΘΗΚΑΤΕ|ΗΘΗΚΑΝ|ΗΘΟΥΝ|ΗΘΩ|ΗΚΑΤΕ|ΗΚΑΝ|ΗΣ|ΗΣΑΝ|ΗΣΑΤΕ|ΗΣΕΙ|ΗΣΕΣ|ΗΣΟΥΝ|ΗΣΩ|Ο|ΟΙ|ΟΜΑΙ|ΟΜΑΣΤΑΝ|ΟΜΟΥΝ|ΟΜΟΥΝΑ|ΟΝΤΑΙ|ΟΝΤΑΝ|ΟΝΤΟΥΣΑΝ|ΟΣ|ΟΣΑΣΤΑΝ|ΟΣΑΣΤΕ|ΟΣΟΥΝ|ΟΣΟΥΝΑ|ΟΤΑΝ|ΟΥ|ΟΥΜΑΙ|ΟΥΜΑΣΤΕ|ΟΥΝ|ΟΥΝΤΑΙ|ΟΥΝΤΑΝ|ΟΥΣ|ΟΥΣΑΝ|ΟΥΣΑΤΕ|Υ|ΥΣ|Ω|ΩΝ)$/';
    if (preg_match($re, $w, $match)) {
        //debug($w,6);
        $stem = $match[1];
        $w = $stem . "ΜΑ";

    }

    if (preg_match($re2, $w) && $test1) {
        //debug($w,"6-re2");
        preg_match($re2, $w, $match);
        $stem = $match[1];
        $w = $stem;

    }





    // Step 7 (ΠΑΡΑΘΕΤΙΚΑ)
    $re = '/^(.+?)(ΕΣΤΕΡ|ΕΣΤΑΤ|ΟΤΕΡ|ΟΤΑΤ|ΥΤΕΡ|ΥΤΑΤ|ΩΤΕΡ|ΩΤΑΤ)$/';
    if (preg_match($re, $w)) {
        preg_match($re, $w, $match);
        $stem = $match[1];
        $w = $stem;
    }

    return stem_returnStem($w, $encoding_changed);
}






/**
 *  Reverts the encoding if needed and returns the stemmed
 *
 *  @param type $w                  :   The stemmed word
 *  @param type $encoding_changed   :   The encoding has changed
 *  @return type                    :   The stemmed word
 */
function stem_returnStem($w, $encoding_changed) {
    // If the size of the word is less than two,
    // then the word is important for the text.
    if (strlen($w) > 2) {
        // If the encoding was changed, revert to the original.
        // Then return the stemmed word.
        if ($encoding_changed == TRUE)
            $w = mb_convert_encoding($w, "UTF-8", "ISO-8859-7");
        return
            $w;
    }
    else {
        return null;
    }
}

if (!function_exists('backend_avatar')) {
	/**
	 * Returns the avatar URL of a user.
	 *
	 * @param $user
	 *
	 * @return string
	 */
	function backend_avatar_url($user)
	{
		switch (config('backend-general.avatar_type')) {
			case 'gravatar':
				return Gravatar::fallback('https://placehold.it/160x160/00a65a/ffffff/&text='.$user->name[0])->get($user->email);
				break;

			case 'placehold':
				return 'https://placehold.it/160x160/00a65a/ffffff/&text='.$user->name[0];
				break;

			default:
				return $user->{config('backend.base.avatar_type')};
				break;
		}
	}
}













function dateConvert($date, $delimiter='/') {
	//  FROM DD/MM/YYYY to YYYY-MM-DD
	$date = explode($delimiter, $date);
	$date = implode('-', array_reverse($date));
	return $date;
}
function reverseDateConvert($date, $delimiter='-') {
	//  FROM YYYY-MM-DD to DD/MM/YYYY
	$date = explode($delimiter, $date);
	$date = implode('/', array( $date[2],$date[1],$date[0] ));
	return $date;
}

function printDateRange($daterange, $today) {
	$html = ' ';
	$html .= ( $daterange && $daterange['from'] != $daterange['to'] ) ? trans('admin.dash.range') : trans('admin.dash.today') ;
	$html .= ' ';
	$html .= $daterange ? ( $daterange['from'] == $daterange['to'] ? reverseDateConvert($daterange['from']) : reverseDateConvert($daterange['from']).' - '.reverseDateConvert($daterange['to']) ) : reverseDateConvert($today);
	return $html;
}
function printDateRangeValue($daterange, $today) {
	if ($daterange) {
		$value = reverseDateConvert($daterange['from']).' - '.reverseDateConvert($daterange['to']);
	}
	else $value = reverseDateConvert($today);
	return $value;
}

function getDatesBetween($daterange){
	$period = new DatePeriod(
		new DateTime($daterange['from']),
		new DateInterval('P1D'),
		new DateTime($daterange['to'])
	);
	$dates = array();
	foreach($period as $date){
		$dates[] = $date->format("Y-m-d");
	}
	$dates[] = $daterange['to'];
	return $dates;
}




function get_array_values($data, $key) {
	$res = array();
	foreach ($data as $row) {
		$res[] = $row[$key];
	}
	return $res;
}
function get_array_as_dates($data, $format='d/m/Y') {
	array_walk($data, function(&$item1) use ($format) {
		$item1 = strtotime($item1);
		$item1 = date($format, $item1);
	});
	return "'".implode("','", $data)."'";
}
function get_array_values_as_dates($data, $key, $format='d/m/Y') {
	$res = get_array_values($data, $key);
	array_walk($res, function(&$item1, $key) use ($format) {
		$item1 = strtotime($item1);
		$item1 = date($format, $item1);
	});
	return $res;
//	return "'".implode("','", $res)."'";
}
function get_array_values_as_text($data, $key) {
	$res = get_array_values($data, $key);
	return "'".implode("','", $res)."'";
}
function get_array_values_as_numbers($data, $key) {
	$res = get_array_values($data, $key);
	return $res;
	return implode(",", $res);
}
function get_array_values_as_euro($data, $key) {
	$res = get_array_values($data, $key);
	array_walk($res, function(&$item1, $key) {
		$item1 = number($item1, 2, '.', '');
	});
	return $res;
	return implode(",", $res);
}