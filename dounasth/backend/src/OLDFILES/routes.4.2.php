<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::group(array('before' => 'is.installed', 'after' => '', 'prefix' => 'install'), function() {
//    Route::get('/', array('as' => 'install-main', 'uses' => 'BackendInitController@main'));
//    Route::post('/run', array('as' => 'install-run', 'uses' => 'BackendInitController@runMigration'));
//});

Route::group(array('before' => '', 'after' => ''), function() {
    Route::get('/', array('as'=>'home', function()
    {
        return View::make('backend::site.home');
    }));

    Route::get('/f', array('as'=>'search', function()
    {
        $q = Input::get('q', '');
        $search = [];
        if (fn_is_not_empty($q)) {
//            $search['pages'] = [
//                'title' => 'Pages',
//                'template' => 'backend::site.search.parts.page',
//                'items' => \Dounasth\Backend\Page::search($q)->get()
//            ];
//            $search['products'] = [
//                'title' => 'Products',
//                'template' => 'backend::site.search.parts.product',
//                'items' => \Dounasth\Laracart\Product::search($q)->with('seo', 'photos', 'affiliateUrl', 'prices', 'metas', 'imported', 'imported.merchant')->paginate(24)
//            ];
//            $search['categories'] = [
//                'title' => 'Categories',
//                'template' => 'backend::site.search.parts.category',
//                'items' => \Dounasth\Laracart\Category::search($q)->get()
//            ];
        }

        return View::make('backend::site.search.search', compact('search'));
    }));

//    Route::any('login', array('as' => 'login', 'uses' => 'BackendUserController@login'));
//    Route::any('register', array('as' => 'register', 'uses' => 'BackendUserController@register'));
//    Route::any('activate', array('as' => 'activate', 'uses' => 'BackendUserController@activate'));
//    Route::any('logout', array('as' => 'logout', 'uses' => 'BackendUserController@logout'));

    Route::get('/contactus', array('as' => 'contact', 'uses' => 'BackendContactController@contact'));
    Route::post('/contactus', array('as' => 'sendContact', 'uses' => 'BackendContactController@sendContact'));

    Route::any('/user/activate', array('as' => 'activate_user', 'uses' => 'BackendUserController@activate'));
    Route::any('/user/resend-activation/{id}', array('as' => 'resend_activation', 'uses' => 'BackendUserController@resendActivation'));
    Route::any('/user/reset-password/{id}', array('as' => 'reset_password', 'uses' => 'BackendUserController@resetPassword'));
    Route::any('/user/do-reset-password', array('as' => 'do_reset_password', 'uses' => 'BackendUserController@doResetPassword'));

    Route::get('/sitemap-index.xml', array('as' => 'site.sitemap.index', 'uses' => 'BackendSitemapsController@index'));
});

Route::group(array('before' => 'auth', 'after' => '', 'prefix' => 'user'), function() {
    Route::any('account', array('as' => 'site.user.account', 'uses' => 'BackendUserController@account'));
    Route::any('details', array('as' => 'site.user.details', 'uses' => 'BackendUserController@details'));
});

Route::group(array('before' => 'auth|auth.admin|init.admin', 'after' => '', 'prefix' => 'admin'), function() {
    Route::get('/', array('as' => 'admin', 'uses' => 'BackendDashboardController@dashboard'));

    Route::get('/search', array('as' => 'admin_search', 'uses' => 'BackendDashboardController@search'));

    Route::get('users/list', array('as' => 'users', 'uses' => 'BackendUserController@listUsers'));
    Route::any('users/manage/{id}', array('as' => 'edit_user', 'uses' => 'BackendUserController@manageUser'));
    Route::any('users/manage/add', array('as' => 'add_user', 'uses' => 'BackendUserController@manageUser'));
    Route::any('users/manage/delete/{id}', array('as' => 'delete_user', 'uses' => 'BackendUserController@deleteUser'));

    Route::get('usergroups/list', array('as' => 'usergroups', 'uses' => 'BackendUserController@listUsergroups'));
    Route::any('usergroups/manage/{id}', array('as' => 'edit_usergroup', 'uses' => 'BackendUserController@manageUsergroup'));
    Route::any('usergroups/manage/add', array('as' => 'add_usergroup', 'uses' => 'BackendUserController@manageUsergroup'));
    Route::any('usergroups/manage/delete/{id}', array('as' => 'delete_usergroup', 'uses' => 'BackendUserController@deleteUsergroup'));

    Route::get('permissions/list', array('as' => 'permissions', 'uses' => 'BackendUserController@managePermissions'));

    Route::get('languages/list', array('as' => 'languages_list', 'uses' => 'BackendLanguagesController@listLangs'));
    Route::get('languages/status/{locale}/{status}', array('as' => 'language_set_status', 'uses' => 'BackendLanguagesController@setStatus'))->where('status', '[0-1]');
    Route::get('languages/default/{locale}/{area}', array('as' => 'language_set_default', 'uses' => 'BackendLanguagesController@setDefault'))->where('area', '(site|admin)');
    Route::get('languages/fallback/{locale}/{area}', array('as' => 'language_set_fallback', 'uses' => 'BackendLanguagesController@setFallback'))->where('area', '(site|admin)');

    Route::get('languages/translations/list/{file?}', array('as' => 'translations_list', 'uses' => 'BackendLanguagesController@listTranslations'));
    Route::any('languages/translations/save/{locale}/{file}', array('as' => 'translations_save', 'uses' => 'BackendLanguagesController@saveTranslations'));
    Route::any('languages/translations/delete/{path}/{file}/{key}', array('as' => 'translations_delete', 'uses' => 'BackendLanguagesController@deleteTranslation'));

    Route::get('pingEngines', function(){
        \Dounasth\Backend\Util::pingEngines();
        exit;
    });
});

Route::get('cc', function(){
    Cache::flush();
    die('all cache deleted');
    exit;
});



Route::get('stem', function(){
    $text_to_stem = "ΠΛΗΡΩΘΗΚΑΜΕ";
    $simple_stemmer = false;
    $stem = stem_stemWord($text_to_stem, $simple_stemmer);
    niceprintr($stem);
    die('done');
    exit;
});
