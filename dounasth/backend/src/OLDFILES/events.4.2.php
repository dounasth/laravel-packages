<?php

/*Event::listen('admin.top-left-menu', function(){
    if ( Route::getCurrentRoute()->getPrefix() == 'admin' ) {
    return array();
    }
    else return [];
}, 1000000);*/

use Dounasth\Widgets\Base\ViewWidget;
use Dounasth\Widgets\Base\Widget;
use Dounasth\Backend\App\Models\User;

//Event::listen('admin.left-menu', function(){
//    return array(
//        '<li class="header">MAIN NAVIGATION</li>',
//        'dashboard' => array(
//            'label' => 'Dashboard',
//            'href' => route('admin'),
//            'icon' => 'fa-dashboard',
//        ),
//        'config' => array(
//            'label' => 'Config',
//            'href' => '#',
//            'icon' => 'fa-cogs',
//            'submenu' => array(
//                'settings' => array(
//                    'label' => 'Settings',
//                    'href' => '#',
//                    'icon' => 'fa-cogs',
//                    'submenu' => array(
//                        'general' => array(
//                            'label' => 'General',
//                            'href' => route('settings', array('backend-general')),
//                            'icon' => 'fa-user',
//                        ),
//                        'site' => array(
//                            'label' => 'Site',
//                            'href' => route('settings', array('backend::site')),
//                            'icon' => 'fa-user',
//                        ),
//                        'permissions' => array(
//                            'label' => 'Permissions',
//                            'href' => route('settings', array('backend::permissions')),
//                            'icon' => 'fa-user',
//                        ),
//                    ),
//                ),
//                'languages' => array(
//                    'label' => 'Languages',
//                    'href' => '#',
//                    'icon' => 'fa-cogs',
//                    'submenu' => array(
//                        'manage' => array(
//                            'label' => 'Manage',
//                            'href' => route('languages_list'),
//                            'icon' => 'fa-cogs',
//                        ),
//                        'translate' => array(
//                            'label' => 'Translate',
//                            'href' => route('translations_list'),
//                            'icon' => 'fa-cogs',
//                        ),
//                    ),
//                ),
//                'users' => array(
//                    'label' => 'Users',
//                    'href' => '#',
//                    'icon' => 'fa-users',
//                    'submenu' => array(
//                        'users' => array(
//                            'label' => 'Users',
//                            'href' => route('users'),
//                            'icon' => 'fa-user',
//                        ),
//                        'usergroups' => array(
//                            'label' => 'User Groups',
//                            'href' => route('usergroups'),
//                            'icon' => 'fa-users',
//                        ),
//                        'permissions' => array(
//                            'label' => 'Permissions',
//                            'href' => route('permissions'),
//                            'icon' => 'fa-check-square-o',
//                        ),
//                    ),
//                ),
//            )
//        )
//    );
//}, 1000000);

Event::listen('admin.dashboard.widgets', function(){
    $widget = new ViewWidget('welcome');
    $widget->view = 'backend::widgets.dash-welcome';
    $widget->wrapClass = 'col-lg-3 col-xs-6';
    $widget->data = array(
        'user' => Auth::user()
    );
    return $widget;
}, 1000000);

Event::listen('admin.dashboard.widgets', function(){
    $widget = new ViewWidget('users-count');
    $widget->view = 'backend::widgets.dash-users-count';
    $widget->wrapClass = 'col-lg-3 col-xs-6';
    $widget->data = array(
        'users_count' => User::count()
    );
    return $widget;
}, 1000000);

Event::listen('admin.dashboard.widgets', function(){
    $widget = new ViewWidget('analytics-bounce-rate');
    $widget->view = 'backend::widgets.dash-analytics-bounce-rate';
    $widget->wrapClass = 'col-lg-3 col-xs-6';
    $widget->data = array();
    return $widget;
}, 1000000);

Event::listen('admin.dashboard.widgets', function() {
    $widget = new ViewWidget('analytics-visitors');
    $widget->view = 'backend::widgets.dash-analytics-visitors';
    $widget->wrapClass = 'col-lg-3 col-xs-6';
    $widget->data = array();
    return $widget;
}, 1000000);

/*Event::listen('admin.dashboard.widgets', function(){
    return array(
        'server-load' => array(
            'view' => 'backend::widgets.dash-server-load',
            'wrap_class' => 'col-lg-6 connectedSortable',
            'data' => array(
            ),
        )
    );
}, 1000000);

Event::listen('admin.dashboard.widgets', function(){
    return array(
        'analytics-map' => array(
            'view' => 'backend::widgets.dash-analytics-map',
            'wrap_class' => 'col-lg-6 connectedSortable',
            'data' => array(
            ),
        )
    );
}, 1000000);*/

Event::listen('admin.translations', function() {
    return array(
        'prefix' => 'backend',
        'path' => dirname(__FILE__),
    );
}, 1000000);

