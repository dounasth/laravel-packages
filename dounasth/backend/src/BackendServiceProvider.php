<?php namespace Dounasth\Backend;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
	    if ( str_contains($this->app->request->getRequestUri(), '/admin/') ) {
		    Config::set( 'app.locale', config('backend-langs.default_admin') );
		    Config::set( 'app.fallback_locale', config('backend-langs.fallback_admin') );
	    }
	    else {
		    Config::set( 'app.locale', config('backend-langs.default_site') );
		    Config::set( 'app.fallback_locale', config('backend-langs.fallback_site') );
	    }

        // LOAD THE VIEWS
        // - first the published views (in case they have any changes)
        $this->loadViewsFrom(resource_path('views/vendor/dounasth/backend'), 'backend');
        // - then the stock views that come with the package, in case a published view might be missing
        $this->loadViewsFrom(realpath(__DIR__.'/views'), 'backend');

        $this->loadTranslationsFrom(realpath(__DIR__.'/lang'), 'backend');

        // use the vendor configuration file as fallback
        $this->mergeConfigFrom(realpath(__DIR__ . '/../config/general.php'), 'backend-general');
        $this->mergeConfigFrom(realpath(__DIR__ . '/../config/langs.php'), 'backend-langs');
        $this->mergeConfigFrom(realpath(__DIR__ . '/../config/permissions.php'), 'backend-permissions');
        $this->mergeConfigFrom(realpath(__DIR__ . '/../config/site.php'), 'backend-site');

        $this->registerMiddleware($router);
        $this->setupRoutes($router);
        $this->publishFiles();
        $this->loadHelpers();

        $this->loadMigrationsFrom(realpath(__DIR__.'/migrations'));
    }

    public function registerMiddleware(\Illuminate\Routing\Router $router)
    {
        $router->aliasMiddleware('auth.admin', "Dounasth\\Backend\\App\\Http\\Middleware\\AuthAdminMdl");
    }

    public function setupRoutes(\Illuminate\Routing\Router $router)
    {
        // by default, use the routes file provided in vendor
        $routeFilePathInUse = __DIR__ . '/routes.5.6.php';
        $this->loadRoutesFrom($routeFilePathInUse);

        // but if there's a file with the use that one
        if (file_exists(base_path().'/routes/backend.php')) {
            $routeFilePathInUse = base_path().'/routes/backend.php';
            $this->loadRoutesFrom($routeFilePathInUse);
        }
    }

    public function publishFiles()
    {
        // publish config file
        $this->publishes([
            realpath(__DIR__ . '/../config/general.php') => config_path('backend-general.php'),
            realpath(__DIR__ . '/../config/langs.php') => config_path('backend-langs.php'),
            realpath(__DIR__ . '/../config/permissions.php') => config_path('backend-permissions.php'),
            realpath(__DIR__ . '/../config/site.php') => config_path('backend-site.php'),
        ], 'config');

        // publish lang files
        // $this->publishes([__DIR__.'/resources/lang' => resource_path('lang/vendor/backend')], 'lang');

        // publish views
        $this->publishes([
            realpath(__DIR__.'/views_ovrd/auth') => resource_path('views/auth'),
            realpath(__DIR__.'/views') => resource_path('views/backend'),
        ]);
        // publish error views
//        $this->publishes([__DIR__.'/resources/error_views' => resource_path('views/errors')], 'errors');

        // publish public Backpack assets
        $this->publishes([__DIR__.'/public' => public_path('vendor/backend')], 'public');

        // calculate the path from current directory to get the vendor path

        // publish public AdminLTE assets
        $this->publishes([base_path('vendor/almasaeed2010/adminlte') => public_path('vendor/adminlte')], 'adminlte');
    }

    public function loadHelpers()
    {
        require_once realpath(__DIR__ . '/functions.php');
        require_once realpath(__DIR__ . '/events.5.6.php');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        /*
         * Register the service provider for the dependency.
         */
//        $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
//        $this->app->register('Barryvdh\Debugbar\ServiceProvider');
        /*
         * Create aliases for the dependency.
         */
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
//        $loader->alias('Debugbar', 'Barryvdh\Debugbar\Facade');
//        $loader->alias('Sitemap', 'Watson\Sitemap\Facades\Sitemap');
//        $this->app['rss'] = $this->app->share(function($app)
//        {
//            return new Rss;
//        });
//        $loader->alias('Rss', 'Dounasth\Backend\RssFacade');
	    $this->commands([
		    \Dounasth\Backend\app\Console\Commands\BackendInit::class,
	    ]);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array(
//            'rss'
        );
    }

}
