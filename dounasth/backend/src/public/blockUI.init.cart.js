// override these in your code to change the default behavior and style
$.blockUI.defaults = {
    message:  '<p class="ajaxloader-big">&nbsp;</p>',
    draggable: false,    // only used when theme == true (requires jquery-ui.js to be loaded)
    css: {
        padding:        0,
        margin:         0,
        width:          '30%',
        top:            '45%',
        left:           '35%',
        textAlign:      'center',
        color:          '#000',
        border:         '0px solid #aaa',
        backgroundColor:'transparent',
        cursor:         'wait'
    },
    themedCSS: {
        width:  '30%',
        top:    '40%',
        left:   '35%'
    },
    overlayCSS:  {
        backgroundColor: '#000',
        opacity:         0.1,
        cursor:          'wait'
    },
    cursorReset: 'default',
    growlCSS: {
        width:    '350px',
        top:      '10px',
        left:     '',
        right:    '10px',
        border:   'none',
        padding:  '5px',
        opacity:   0.6,
        cursor:    null,
        color:    '#fff',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius':    '10px'
    },
    constrainTabKey: true,
    timeout: 10000,
    showOverlay: true,
    blockMsgClass: 'blockMsg',
    ignoreIfBlocked: true
};
