<ul class="sidebar-menu">
    @section('sidebar.left')
        <?php $sidetree = \Eventy::filter('admin.left-menu', []); ?>
        @if ($sidetree)
            @foreach ($sidetree as $item)
                    @if (is_array($item))
                        <li {{ (!empty($item['submenu'])) ? 'class="treeview"' : ''}}>
                            <a href="{{$item['href']}}">
                                <i class="fa {{$item['icon']}}"></i>
                                <span>{{$item['label']}}</span>
                                @if (!empty($item['submenu']))
                                    <span class="pull-right-container">
                                      <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                @endif
                            </a>
                            @if (!empty($item['submenu']))
                                <ul class="treeview-menu">
                                    @foreach ($item['submenu'] as $itemm)
                                        @if (is_array($itemm))
                                            <li {{ (!empty($itemm['submenu'])) ? 'class="treeview"' : ''}}>
                                                <a href="{{$itemm['href']}}">
                                                    <i class="fa {{$itemm['icon']}}"></i>
                                                    <span>{{$itemm['label']}}</span>
                                                    @if (!empty($itemm['submenu']))
                                                        <span class="pull-right-container">
                                                          <i class="fa fa-angle-left pull-right"></i>
                                                        </span>
                                                    @endif
                                                </a>
                                                @if (!empty($itemm['submenu']))
                                                    <ul class="treeview-menu">
                                                        @foreach ($itemm['submenu'] as $itemmm)
                                                            @if (is_array($itemmm))
                                                                <li {{ (!empty($itemmm['submenu'])) ? 'class="treeview"' : ''}}>
                                                                    <a href="{{$itemmm['href']}}">
                                                                        <i class="fa {{$itemmm['icon']}}"></i>
                                                                        <span>{{$itemmm['label']}}</span>
                                                                        @if (!empty($itemmm['submenu']))
                                                                            <span class="pull-right-container">
                                                          <i class="fa fa-angle-left pull-right"></i>
                                                        </span>
                                                                        @endif
                                                                    </a>
                                                                    @if (!empty($itemmm['submenu']))

                                                                    @endif
                                                                </li>
                                                            @else
                                                                {!! $itemmm !!}
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @else
                                            {!! $itemm !!}
                                        @endif
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @else
                        {!! $item !!}
                    @endif
            @endforeach
        @endif
    @show
</ul>