<ul class="nav navbar-nav">
    {{--<li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>--}}
    {{--<li><a href="#">Link</a></li>--}}
    {{--<li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gears"></i> Settings <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
            <li><a href="{{ trans('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
            <li class="divider"></li>
            <li><a href="{{ trans('crud/locations') }}"><i class="fa fa-dashboard"></i> <span>Θέσεις</span></a></li>
            <li><a href="{{ trans('crud/categories') }}"><i class="fa fa-dashboard"></i> <span>Κατηγορίες</span></a></li>
            <li><a href="{{ trans('crud/items') }}"><i class="fa fa-dashboard"></i> <span>Είδη</span></a></li>
            <li><a href="{{ trans('crud/metrics') }}"><i class="fa fa-dashboard"></i> <span>Μονάδες</span></a></li>
            <li><a href="{{ trans('crud/tmpl_pasteriosi') }}"><i class="fa fa-dashboard"></i> <span>Template Παστερίωσης</span></a></li>
            <li><a href="{{ trans('crud/transaction_types') }}"><i class="fa fa-dashboard"></i> <span>Transaction Types</span></a></li>
        </ul>
    </li>--}}
	<?php $top_left_tree = \Eventy::filter('admin.top-left-menu', []); ?>
    @foreach ($top_left_tree as $item)
        @if (is_array($item))
            <li {{ (!empty($item['submenu'])) ? 'class="dropdown"' : ''}}>
                <a href="{{$item['href']}}"
                   @if (!empty($item['submenu']))
                   class="dropdown-toggle" data-toggle="dropdown"
                        @endif
                >
                    <i class="fa {{$item['icon']}}"></i> {{$item['label']}}
                    @if (!empty($item['submenu']))
                        <span class="caret"></span>
                    @endif
                </a>
                @if (!empty($item['submenu']))
                    <ul class="dropdown-menu" role="menu">
                        @foreach ($item['submenu'] as $itemm)
                            @if (is_array($itemm))
                                <li {{ (!empty($itemm['submenu'])) ? 'class="dropdown"' : ''}}>
                                    <a href="{{$itemm['href']}}">
                                        <i class="fa {{$itemm['icon']}}"></i> {{$itemm['label']}}
                                        @if (!empty($itemm['submenu']))
                                            <span class="caret"></span>
                                        @endif
                                    </a>
                                </li>
                            @else
                                {!! $itemm !!}
                            @endif
                        @endforeach
                    </ul>
                @endif
            </li>
        @else
            {!! $item !!}
        @endif
    @endforeach
</ul>
