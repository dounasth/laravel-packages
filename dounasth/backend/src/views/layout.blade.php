<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>
        {{ isset($title) ? $title.' :: '.config('backend-general.project_name').' Admin' : config('backend-general.project_name').' Admin' }}
    </title>

    @stack('before_styles')

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/bootstrap/css/bootstrap.min.css">
    {{--<link rel="stylesheet" href="{{ asset('vendor/backend/') }}/fontawesome-5.4.2/css/all.css">--}}
    {{--<link rel="stylesheet" href="{{ asset('vendor/backend/') }}/ionicons.min.css">--}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/plugins/pace/pace.min.css">
    <link rel="stylesheet" href="{{ asset('vendor/backend/pnotify/pnotify.custom.min.css') }}">

    <!-- BackPack Base CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/backend/backend.base.css') }}?v=2">
    <link rel="stylesheet" href="{{ asset('vendor/backend/overlays/backend.bold.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/backend/custom.css') }}">

@stack('after_styles')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('vendor/backend/') }}/html5shiv.min.js"></script>
    <script src="{{ asset('vendor/backend/') }}/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition {{ config('backend-general.skin') }} sidebar-mini {{ (Auth::user()) ? '' : 'layout-top-nav' }}">
<script type="text/javascript">
    /* Recover sidebar state */
    (function () {
        if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
            var body = document.getElementsByTagName('body')[0];
            body.className = body.className + ' sidebar-collapse';
        }
    })();
</script>
<!-- Site wrapper -->
<div class="wrapper">
    @if (Auth::user())
        <header class="main-header">
            <!-- Logo -->
            <a href="{{ url('') }}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">{!! config('backend-general.logo_mini') !!}</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">{!! config('backend-general.logo_lg') !!}</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->

            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">{{ trans('backend::g.toggle_navigation') }}</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu pull-left">
                    <ul class="nav navbar-nav">
                        <!-- =================================================== -->
                        <!-- ========== Top menu items (ordered left) ========== -->
                        <!-- =================================================== -->

                    <!-- <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span>Home</span></a></li> -->
                    @include('backend::parts.top-nav-left')

                    <!-- ========== End of top menu left items ========== -->
                    </ul>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                    @include('backend::parts.menu-top-left')
                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
                        </div>
                    </form>
                </div>
                <!-- /.navbar-collapse -->

                <div class="navbar-custom-menu">
                    @include('backend::parts.menu-top-right')
                </div>


            </nav>
        </header>
@endif

<!-- =============================================== -->

@if (Auth::user())
    <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <a class="pull-left image" href="{{ route('backend.user.account') }}">
                        <img src="{{ backend_avatar_url(Auth::user()) }}" class="img-circle" alt="User Image">
                    </a>
                    <div class="pull-left info">
                        <p><a href="{{ route('backend.user.account') }}">{{ Auth::user()->name }}</a></p>
                        <small>
                            <small>
                                <a href="{{ route('backend.user.account') }}"><span><i
                                                class="fa fa-user-circle-o"></i> {{ trans('backend::b.my_account') }}</span></a>
                                &nbsp; &nbsp;
                                <a href="{{ route('logout_get') }}"><i class="fa fa-sign-out"></i>
                                    <span>{{ trans('backend::b.logout') }}</span></a>
                            </small>
                        </small>
                    </div>
                </div>

                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu tree" data-widget="tree">
                {{-- <li class="header">{{ trans('backend::g.administration') }}</li> --}}
                <!-- ================================================ -->
                    <!-- ==== Recommended place for admin menu items ==== -->
                    <!-- ================================================ -->

                @include('backend::parts.sidebar-left')

                <!-- ======================================= -->
                    {{-- <li class="header">Other menus</li> --}}
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>
@endif

<!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    @if (Auth::user())
        <!-- Content Header (Page header) -->
            <section class="content-header">
                @if (trim($__env->yieldContent('page-menu')))
                    <div class="dropdown pull-left">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown">
                            <i class="fa fa-gear"></i>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                            @yield('page-menu', '<li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-ban"></i> Nothing to do here</a></li>')
                        </ul>
                    </div>
                @endif
                <h1>
                    &nbsp;
                    @yield('page-title')
                    <small>@yield('page-subtitle')</small>
                </h1>
                <ol class="breadcrumb">
                    @include('backend::parts.breadcrumb')
                </ol>
            </section>
    @endif

    <!-- Main content -->
        <section class="content clearfix">

            @yield('content')

            @if (trim($__env->yieldContent('page-menu')))
                <ul class="nav nav-pills">
                    @yield('page-menu')
                </ul>
            @endif

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer clearfix">
        @if (config('backend-general.show_powered_by'))
            <div class="pull-right hidden-xs">
                {{ trans('backend::g.powered_by') }} <a target="_blank"
                                                        href="http://backendforlaravel.com?ref=panel_footer_link">Backpack
                    for Laravel</a>
            </div>
        @endif
        {{ trans('backend::g.handcrafted_by') }} <a target="_blank"
                                                    href="{{ config('backend-general.developer_link') }}">{{ config('backend-general.developer_name') }}</a>.
    </footer>
</div>
<!-- ./wrapper -->


@stack('before_scripts')

<!-- jQuery 2.2.3 -->
{{--<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>--}}
<script>window.jQuery || document.write('<script src="{{ asset('vendor/adminlte') }}/plugins/jQuery/jQuery-2.2.3.min.js"><\/script>')</script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ asset('vendor/adminlte') }}/bootstrap/js/bootstrap.min.js"></script>
<script src="{{ asset('vendor/adminlte') }}/plugins/pace/pace.min.js"></script>
<script src="{{ asset('vendor/adminlte') }}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="{{ asset('vendor/adminlte') }}/plugins/fastclick/fastclick.js"></script>
<script src="{{ asset('vendor/adminlte') }}/dist/js/app.min.js"></script>

<!-- page script -->
<script type="text/javascript">
    /* Store sidebar state */
    $('.sidebar-toggle').click(function (event) {
        event.preventDefault();
        if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
            sessionStorage.setItem('sidebar-toggle-collapsed', '');
        } else {
            sessionStorage.setItem('sidebar-toggle-collapsed', '1');
        }
    });
    // To make Pace works on Ajax calls
    $(document).ajaxStart(function () {
        Pace.restart();
    });

    // Ajax calls should always have the CSRF token attached to them, otherwise they won't work
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Set active state on menu element
    var current_url = "{{ Request::fullUrl() }}";
    var full_url = current_url + location.search;
    var $navLinks = $("ul.sidebar-menu li a");
    // First look for an exact match including the search string
    var $curentPageLink = $navLinks.filter(
        function () {
            return $(this).attr('href') === full_url;
        }
    );
    // If not found, look for the link that starts with the url
    if (!$curentPageLink.length > 0) {
        $curentPageLink = $navLinks.filter(
            function () {
                return $(this).attr('href').startsWith(current_url) || current_url.startsWith($(this).attr('href'));
            }
        );
    }

    $curentPageLink.parents('li').addClass('active');
            {{-- Enable deep link to tab --}}
    var activeTab = $('[href="' + location.hash.replace("#", "#tab_") + '"]');
    location.hash && activeTab && activeTab.tab('show');
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        location.hash = e.target.hash.replace("#tab_", "#");
    });
</script>

{{--@include('backend::parts.messages')--}}

@stack('after_scripts')

<!-- JavaScripts -->
{{-- <script src="{{ mix('js/app.js') }}"></script> --}}
</body>
</html>
