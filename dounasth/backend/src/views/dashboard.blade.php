@extends('backend::layout')

@section('page-title')
Dashboard
@stop

@section('page-subtitle')
@stop

@section('content')
<section class="content">
    <div class="row">
        @foreach ($widgets as $widget)
            {!! $widget->html() !!}
        @endforeach
    </div>
</section>

@stop