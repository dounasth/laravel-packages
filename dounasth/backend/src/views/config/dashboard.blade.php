@extends('backend::layout')

@section('page-title')
Configuration
@stop

@section('page-subtitle')
@stop

@section('content')
    {{--<section class="content">--}}
        {{--<div class="row">--}}
            {{--@foreach ($widgets as $widget)--}}
                {{--{!! $widget->html() !!}--}}
            {{--@endforeach--}}
        {{--</div>--}}
    {{--</section>--}}

    <ul class="">
        <?php $sidetree = \Eventy::filter('config.menu', []); ?>
        @if ($sidetree)
            @foreach ($sidetree as $item)
                @if (is_array($item))
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <span class="info-box-text">{{$item['label']}}</span>
                        <span class="info-box-number">{{$item['href']}}</span>

                        @if (!empty($item['submenu']))
                            <div class="list-group list-group-flush">
                                @foreach ($item['submenu'] as $itemm)
                                    @if (is_array($itemm))
                                            <a href="{{$itemm['href']}}" class="list-group-item list-group-item-action">
                                                <i class="fa {{$itemm['icon']}} pull-right"></i>
                                                <p class="list-group-item-heading">{{$itemm['label']}}</p>
                                                @if (isset($itemm['hint']))
                                                <p class="list-group-item-text">{{$itemm['hint']}}</p>
                                                @endif
                                            </a>
                                            @if (!empty($itemm['submenu']))
                                                <div class="list-group list-group-flush" style="padding-left: 20px">
                                                    @foreach ($itemm['submenu'] as $itemmm)
                                                        @if (is_array($itemmm))
                                                                <a href="{{$itemmm['href']}}" class="list-group-item list-group-item-action">
                                                                    <i class="fa {{$itemmm['icon']}} pull-right"></i>
                                                                    <p class="list-group-item-heading">{{$itemmm['label']}}</p>
                                                                    @if (isset($itemm['hint']))
                                                                        <p class="list-group-item-text text-muted">{{$itemm['hint']}}</p>
                                                                    @endif
                                                                </a>
                                                                @if (!empty($itemmm['submenu']))

                                                                @endif
                                                        @else
                                                            {!! $itemmm !!}
                                                        @endif
                                                    @endforeach
                                                </div>
                                            @endif
                                    @else
                                        {!! $itemm !!}
                                    @endif
                                @endforeach
                            </div>
                        @endif
                    </div>
                @else
                    {!! $item !!}
                @endif
            @endforeach
        @endif
    </ul>
@stop