@extends('backend::layout')

@section('page-title')
@if ($mode == 'add')
Add User
@else
Edit User :: {{ $user->name }}
@endif
@stop

@section('page-subtitle')
dashboard subtitle, some description must be here
@stop

@section('breadcrumb')
@parent
    <li><a href="{{ route('users') }}" class="goOnCancel"><i class="fa fa-group"></i> Manage Users</a></li>
    <li class="active">{{ ($mode == 'add') ? 'Add' : 'Edit' }} user</li>
@stop

@section('page-menu')
<li role="presentation"><a role="menuitem" tabindex="-1" href="{{ route('resend_activation', [$user->id]) }}"><i class="fa fa-plus"></i> Resend Activation Mail</a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('reset_password', [$user->id])}}"><i class="fa fa-trash-o"></i> Reset password</a></li>
<li role="presentation" class="divider"></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="{{route('delete_user', [$user->id])}}"><i class="fa fa-trash-o"></i> Delete user</a></li>
@stop

@section('content')
<div class="box box-warning">
    <form role="form" method="post" action="{{ ($mode == 'add') ? route('add_user') : route('edit_user', [$user->id]) }}">
        @csrf
        <div class="box-body">
            <div class="row">
                <div class="col-lg-8 col-xs-12">
                    <h3>User Data</h3>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6 col-xs-12">
                                <label for="first_name">First name</label>
                                <input type="text" id="first_name" placeholder="User's first name" class="form-control" name="user[extra][first_name]" value="{{ $user->extra ? $user->extra->first_name : '' }}">
                            </div>
                            <div class="col-lg-6 col-xs-12">
                                <label for="last_name">Last name</label>
                                <input type="text" id="last_name" placeholder="User's last name" class="form-control" name="user[extra][last_name]" value="{{ $user->extra ? $user->extra->last_name : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">{{ trans('backend::g.email') }}</label>
                        <div class="input-group">
                            <span class="input-group-addon">@</span>
                            <input id="email" type="text" placeholder="Email" class="form-control" name="user[email]" value="{{ $user->email }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password1">Password</label>
                        <input type="password" placeholder="Password" id="password1" class="form-control" name="pass[word]">
                        <br/>
                        <input type="password" placeholder="Password again. Leave empty to keep same password" id="password2" class="form-control" name="pass[verify]">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-4 col-xs-12">
                                <label for="activated">Status</label>
                                <select id="activated" name="user[extra][activated]" class="form-control">
                                    <option value="1" {{ ($user->extra && $user->extra->activated) ? 'selected="selected"' : '' }} >Activated</option>
                                    <option value="0" {{ ($user->extra && !$user->extra->activated) ? 'selected="selected"' : '' }} >Disabled</option>
                                </select>
                            </div>
                            <div class="col-lg-4 col-xs-12">
                                <label for="activation_code">{{ trans('backend::g.activation_code')}}</label>
                                <input type="text" id="activation_code" placeholder="Activation Code. Empty for activated users." class="form-control" name="user[activation_code]" value="{{ $user->activation_code }}">
                            </div>
                            <div class="col-lg-4 col-xs-12">
                                <label for="reset_password_code">{{ trans('backend::g.reset_password_code')}}</label>
                                <input type="text" id="reset_password_code" class="form-control" name="user[reset_password_code]" value="{{ $user->reset_password_code }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12">
                    @if ($mode != 'add')
                        <h3>more info...</h3><br/>

                        <div class="form-group">
                            <label for="password1">Usergroups</label>
                            {{ Form::select('usergroups[]', \Dounasth\Backend\App\Models\Usergroup::pluck('name', 'id'), $user->usergroups()->pluck('id'), ['class'=>'form-control select2-multiple', 'multiple'=>true]) }}
                        </div>

                        Created: {{ $user->created_at }}<br/><br/>
                        Updated: {{ $user->updated_at }}<br/><br/>
                        Activated At: {{ $user->activated_at }}<br/><br/>
                        Persist Code: {{ $user->persist_code }}<br/><br/>
                        Remember Token: {{ $user->remember_token }}<br/><br/>
                        {{--Usergroups: @forelse ($user->getGroups() as $k => $group)
                        <a href="{{ route('edit_usergroup', [$group->id]) }}">{{ $group->name }}</a>
                        @if ( $k < count($user->getGroups())-1 )
                        ,
                        @endif
                        @empty
                        {{ trans('backend::g.no_groups_assigned') }}
                        @endforelse<br/><br/>--}}
                        {{--Permissions: @forelse($user->getMergedPermissions() as $permission=>$value)
                        {{ $permission }} /
                        @empty
                        {{ trans('backend::g.no_permissions_assigned') }}
                        @endforelse<br/><br/>--}}
                    @endif
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <button class="btn btn-success" type="submit"><i class="fa fa-check-square-o"></i> Save</button>
            <a class="btn btn-danger btn-cancel"><i class="fa fa-square-o"></i> Cancel</a>
            <button class="btn btn-warning pull-right" type="submit"><i class="fa fa-plus"></i> Save as new</button>
        </div>
        @csrf
    </form>
</div>
@stop

@push('after_styles')
<link href="{{ asset('vendor/adminlte/plugins/select2/select2.css') }}" rel="stylesheet">
@endpush

@push('after_scripts')
<script src="{{ asset('vendor/adminlte/plugins/select2/select2.full.min.js') }}"></script>

<script type="text/javascript" charset="UTF-8">

    jQuery(document).ready(function(){

        jQuery('.select2-single').each(function(i, elm) {
            jQuery(elm).select2({
                templateResult: window[jQuery(elm).data('template') || 'select2Generic']
            });
        });
        jQuery('.select2-ajax').select2({
            ajax: {
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        query: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    var x = {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                    return x;
                }
            }
        });
        jQuery('.select2-multiple').select2({
            multiple: true
            ,closeOnSelect: false
        });
        jQuery('.select2-ajax-multiple').select2({
            multiple: true,
            closeOnSelect: false,
            ajax: {
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        query: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    var x = {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                    return x;
                }
            }
        });

    });
</script>
@endpush