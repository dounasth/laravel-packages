<?php
return array(
    'my_account' => 'Λογαριασμός',
    'forgot_password' => 'Ξεχάσατε τον κωδικό σας;',
    'reset_password' => 'Επαναφορά κωδικού',
    'reset_password_button' => 'Αποστολή συνδέσμου',
    'register' => 'Εγγραφή',
    'login' => 'Σύνδεση',
    'logout' => 'Αποσύνδεση',
);
?>