<?php

namespace Dounasth\Backend\App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthAdminMdl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

//        if (Auth::guest()) {
//            return redirect()->route('login');
//        }

        return $next($request);
    }
}
