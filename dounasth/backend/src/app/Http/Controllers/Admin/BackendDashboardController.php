<?php
namespace Dounasth\Backend\App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\View;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 26/9/2014
 * Time: 12:37 μμ
 */


class BackendDashboardController extends BackendBaseController {

    public function dashboard() {
        $ews = Event::fire('admin.dashboard.widgets');
        $widgets = array();
        foreach ($ews as $k => $v) {
            $v->prepare();
            $widgets[$k] = $v;//->html();
        }
        return View::make('backend::dashboard')->withWidgets($widgets);
    }

    public function search() {
        $q = Input::get('q', '');
        $search = [];
        if (fn_is_not_empty($q)) {
//            $search['pages'] = [
//                'title' => 'Pages',
//                'template' => 'backend::search.parts.page',
//                'items' => Page::search($q)->get()
//            ];
//            $search['products'] = [
//                'title' => 'Products',
//                'template' => 'backend::search.parts.product',
//                'items' => Product::search($q)->get()
//            ];
//            $search['categories'] = [
//                'title' => 'Categories',
//                'template' => 'backend::search.parts.category',
//                'items' => Category::search($q)->get()
//            ];
        }
        return View::make('backend::search.admin-search', compact('search'));
    }

}