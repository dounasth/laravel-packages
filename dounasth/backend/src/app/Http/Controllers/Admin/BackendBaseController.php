<?php
namespace Dounasth\Backend\App\Http\Controllers\Admin;


use Dounasth\Backend\App\Http\Controllers\BootableBaseController;
use Dounasth\Commerce\App\Models\Order\Order;
use Dounasth\Commerce\App\Models\Order\OrderItem;
use Dounasth\Commerce\App\Models\Order\OrderTotals;
use Dounasth\Commerce\App\Models\Payment;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;

class BackendBaseController extends BootableBaseController
{

    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('auth.admin');
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }
}
