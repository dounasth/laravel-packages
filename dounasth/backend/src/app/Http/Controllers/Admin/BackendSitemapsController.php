<?php
namespace Dounasth\Backend\App\Http\Controllers\Admin;


class BackendSitemapsController extends BackendBaseController
{
    public function index()
    {
        /*
        // Get a general sitemap.
        Sitemap::addSitemap('/sitemap-categories.xml');
        // You can use the route helpers too.
        Sitemap::addSitemap(URL::route('sitemaps.posts'));
        Sitemap::addSitemap(route('sitemaps.users'));
        */

//        Sitemap::addSitemap(route('site.sitemap.pages'));
//        Sitemap::addSitemap(route('site.sitemap.categories'));
//        Sitemap::addSitemap(route('site.sitemap.coupons'));

        // Return the sitemap to the client.
        return Sitemap::renderSitemapIndex();
    }

}
