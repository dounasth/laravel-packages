<?php
namespace Dounasth\Backend\App\Http\Controllers\Admin;

use Dounasth\Backend\BackendDatabaseSeeder;

class BackendInitController extends BackendBaseController {

    public function main() {
        return View::make('backend::install.main');
    }
    public function runMigration()
    {

    	/*
		php artisan key:generate
		php artisan vendor:publish --provider="Dounasth\Backend\BackendServiceProvider" --force
		php artisan migrate
		php artisan db:seed --class="Dounasth\Backend\BackendDatabaseSeeder"
    	 */

/*        Artisan::call('vendor:publish', ['provider'=>'Dounasth\Backend\BackendServiceProvider', '--force' => true,]);

        Artisan::call('migrate', [
            '--force' => true,
        ]);
        $seeder = new BackendDatabaseSeeder();
        $seeder->run();*/

	    Artisan::call('backend:init');

        return Redirect::route('login')->withMessage('Backend install is complete. Login with your admin account now');
    }

}
?>