<?php
namespace Dounasth\Backend\App\Http\Controllers\Admin;

use Dounasth\Backend\App\Exceptions\PasswordsDontMatchException;
use Dounasth\Backend\App\Exceptions\UsergroupExistsException;
use Dounasth\Backend\App\Exceptions\UsergroupNameRequiredException;
use Dounasth\Backend\App\Helpers\AlertMessage;
use Dounasth\Backend\App\Models\User;
use Dounasth\Backend\App\Models\UserExtra;
use Dounasth\Backend\App\Models\Usergroup;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class BackendUserController extends BackendBaseController {

    public function listUsers() {
        $users = User::all();
        return View::make('backend::users.list_users')->with('users', $users);
    }

    public function manageUser($id=0) {
        if (Request::isMethod('post')) {
            if ( is_numeric($id) ) {
                try
                {
                    // Find the user using the user id
                    $user = User::findOrFail($id);
                    $user->fill(Input::get('user'));

                    if (Input::get('pass.word', false) && Input::get('pass.verify', false) && Input::get('pass.word') == Input::get('pass.verify')) {
                        $user->password = Hash::make(Input::get('pass.word'));
                    }

                    // Update the user
                    if ($user->save())
                    {
                        $user->extra->fill(Input::get('user.extra'));
                        $user->extra->save();
                    }
                    else
                    {
                        // User information was not updated
                    }

	                $user->usergroups()->sync(Input::get('usergroups', []));
                    return redirect()->route('edit_user', [$id]);
                }
                catch (Cartalyst\Sentry\Users\UserExistsException $e)
                {
                    $message = AlertMessage::make(AlertMessage::TYPE_ERROR, 'User with this login already exists.');
                }
                catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
                {
                    $message = AlertMessage::make(AlertMessage::TYPE_ERROR, 'User was not found.');
                }
                return redirect()->route('edit_user', [$id])->withMessage($message);
            }
            else {
                $message = '';
                try
                {
                    $data = Input::get('user', []);
                    $pass = Input::get('pass', []);
                    $extra = Input::get('user.extra', []);
	                $user = new User();
	                $user->fill($data);
	                $user->name = "{$data['extra']['first_name']} {$data['extra']['last_name']}";
	                if ($pass['word'] == $pass['verify']) {
	                	$user->password = Hash::make($pass['word']);
	                }
	                else throw new PasswordsDontMatchException();
	                $user->save();
	                $user_extra = new UserExtra();
	                $user_extra->fill($extra);
	                $user->extra()->save($user_extra);
	                $user->usergroups()->sync(Input::get('usergroups', []));
                }
                catch (PasswordsDontMatchException $e)
                {
                    $message = AlertMessage::make(AlertMessage::TYPE_ERROR, 'Passwords dont match.');
                }

                if ($message) {
                    return redirect()->route('add_user')->withMessage($message);
                }
                else {
                    $message = AlertMessage::make(AlertMessage::TYPE_SUCCESS, 'User created successfully.');
                    return redirect()->route('edit_user', [$user->getId()])->withMessage($message);
                };
            }

        }
        $user = User::findOrNew($id);
        $mode = ($id == 'add') ? 'add' : 'edit';
        return View::make('backend::users.manage_user')->with('user', $user)->withMode($mode);
    }

    public function listUsergroups() {
        $groups = Usergroup::all();
        return View::make('backend::users.list_usergroups')->with('groups', $groups);
    }
    public function manageUsergroup($id=0) {
        if (Request::isMethod('post')) {
	        $message = '';
            try
            {
                if (is_numeric($id)) {
                    $group = Usergroup::findOrFail($id);
                    $group->name = Input::get('name');
                    unset($group->permissions);
                    $group->permissions = Input::get('permissions', array());
                    $group->save();
                }
                else {
                    $data = Input::all();
                    if (fn_is_empty($data['name'])) {
	                    throw new UsergroupNameRequiredException();
                    }
                    elseif (Usergroup::whereName($data['name'])->exists()) {
	                    throw new UsergroupExistsException();
                    }
                    else {
	                    $group = new Usergroup();
	                    $group->fill($data);
	                    $group->save();
                    }
                }
            }
            catch (\Exception $e)
            {
                $message = AlertMessage::make(AlertMessage::TYPE_ERROR, $e->getMessage());
            }
            return redirect()->route('usergroups')->withMessage($message);
        }

        $mode = ($id == 'add') ? 'add' : 'edit';
	    $group = Usergroup::findOrNew($id);
        return View::make('backend::users.manage_usergroup')
                    ->withGroup($group)
                    ->withMode($mode);
    }
    public function deleteUsergroup($id) {
        niceprintr($id);
        exit;
    }

    public function managePermissions() {
        return View::make('backend::users.list_permissions')->with('permissions', Config::get('backend-permissions.all'));
    }


    public function activate() {
        $email = Input::get('email', '');
        $activationCode = Input::get('activationCode', '');
        if ($email && $activationCode) {
            $message = '';
            try
            {
                // Find the user using the user id
                $user = Sentry::findUserByLogin($email);

                // Attempt to activate the user
                if ($user->attemptActivation($activationCode))
                {
                    // User activation passed
                    niceprintr('User activation passed');
                }
                else
                {
                    // User activation failed
                    niceprintr('User activation failed');
                }
            }
            catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
            {
                $message = AlertMessage::make(AlertMessage::TYPE_ERROR, 'User not found');
            }
            catch (Cartalyst\Sentry\Users\UserAlreadyActivatedException $e)
            {
                $message = AlertMessage::make(AlertMessage::TYPE_ERROR, 'User is already activated');
            }
            return redirect()->route('login')->withMessage($message);
        }
        else {
            return View::make('backend::activate');
        }
    }

    public function resendActivation($id=0) {
        $user = Sentry::findUserById($id);
        Mail::send('backend::emails.activation', array('user' => $user), function($message) use ($user)
        {
            $message->from(Config::get('backend-general.mail.from.address'), Config::get('backend-general.mail.from.name'));
            $message->to($user->email, "{$user->last_name} {$user->first_name}")->subject('Here is your activation link');
        });
        return redirect()->to($_SERVER['HTTP_REFERER']);
    }

    public function deleteUser($id) {
        // Find the user using the user id
        $user = User::findOrFail(1);
        // Delete the user
        $user->delete();
        return redirect()->back();
    }

    public function logout() {
        Auth::logout();
        return redirect()->to('/');
    }

    public function account() {
        return $this->manageUser(\Auth::user()->id);
    }

    public function details() {
        $id = Auth::user()->id;
        if (Request::isMethod('post')) {
            if ($id) {
                try
                {
                    // Find the user using the user id
                    $user = Sentry::findUserById($id);

                    // Update the user details
//                $user->email = Input::get('user.email');
//                $user->first_name = Input::get('user.first_name');
//                $user->last_name = Input::get('user.last_name');
//                $user->activation_code = Input::get('user.activation_code');
//                $user->reset_password_code = Input::get('user.reset_password_code');

                    $user->fill(Input::get('user'));

                    // Update the user
                    if ($user->save())
                    {
                        // User information was updated
                        $message = AlertMessage::success('User information was updated');
                    }
                    else
                    {
                        // User information was not updated
                        $message = AlertMessage::error('User information was updated');
                    }
                    return redirect()->back()->withMessage($message);
                }
                catch (Cartalyst\Sentry\Users\UserExistsException $e)
                {
                    $message = AlertMessage::make(AlertMessage::TYPE_ERROR, 'User with this login already exists.');
                }
                catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
                {
                    $message = AlertMessage::make(AlertMessage::TYPE_ERROR, 'User was not found.');
                }
                return redirect()->back()->withMessage($message);
            }
        }
        try { $user = Sentry::findUserById($id); }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e) { }
        return View::make('backend::site.user.details')->with('user', $user);
    }


}
