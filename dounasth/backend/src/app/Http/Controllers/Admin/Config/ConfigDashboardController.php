<?php
namespace Dounasth\Backend\App\Http\Controllers\Admin\Config;

use Dounasth\Backend\App\Http\Controllers\Admin\BackendBaseController;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\View;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 26/9/2014
 * Time: 12:37 μμ
 */


class ConfigDashboardController extends BackendBaseController {

    public function dashboard() {
        $ews = Event::fire('config.dashboard.widgets');
        $widgets = array();
        foreach ($ews as $k => $v) {
            $v->prepare();
            $widgets[$k] = $v;//->html();
        }
        return View::make('backend::config.dashboard')->withWidgets($widgets);
    }

}