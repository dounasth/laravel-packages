<?php
namespace Dounasth\Backend\App\Exceptions;

use Exception;
use Throwable;

class UsergroupExistsException extends Exception
{
	public function __construct( $message = "", $code = 0, Throwable $previous = null ) {
		$message = (fn_is_not_empty($message)) ? $message : 'Group already exists';
		parent::__construct( $message, $code, $previous );
	}
}