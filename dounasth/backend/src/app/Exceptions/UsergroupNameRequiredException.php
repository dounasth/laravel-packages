<?php
namespace Dounasth\Backend\App\Exceptions;

use Exception;

class UsergroupNameRequiredException extends Exception
{
	public function __construct( $message = "", $code = 0, Throwable $previous = null ) {
		$message = (fn_is_not_empty($message)) ? $message : 'Name field is required';
		parent::__construct( $message, $code, $previous );
	}
}