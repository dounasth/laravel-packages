<?php
namespace Dounasth\Backend\App\Exceptions;

use Exception;

class PasswordsDontMatchExc extends Exception
{
	public function __construct( $message = "", $code = 0, Throwable $previous = null ) {
		$message = (fn_is_not_empty($message)) ? $message : 'Passwords dont match';
		parent::__construct( $message, $code, $previous );
	}
}