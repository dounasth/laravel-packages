<?php

namespace Dounasth\Backend;

/**
 * Created by PhpStorm.
 * User: nimda
 * Date: 4/17/15
 * Time: 5:02 PM
 */

class Util {

    public static function saveArrayToFile($array, $file) {
        $content = '<?php return '.var_export($array, true).'; ?>';
//        if (file_exists($file)) {
            file_put_contents($file, $content);
//        }
    }

    public static function pingEngines() {
        if (\Config::get("backend::site.is-open")) {
            $title = \Config::get("backend-general.pingo.title");
            $url = \Config::get("backend-general.pingo.blogurl");
            $rss = \Config::get("backend-general.pingo.rssurl");
            $pinger = new Pinger();
            $pinger->pingAll($title, $url, $rss);
            return true;
        }
        else return false;
    }

    public static function socialButtons() {
        $socialButtons = '';
        if (class_exists( 'Atticmedia\Anvard\Anvard' )) {
//            $request = Request::create(Config::get('anvard::routes.index'), 'GET');
//            $socialButtons = Route::dispatch($request)->getContent();
            $anvard = \App::make('anvard');
            $providers = $anvard->getProviders();
            $socialButtons = \View::make('backend::social-buttons', compact('providers'))->render();
        }
        return $socialButtons;
    }

    public static function keywordio($search) {
//        https://www.google.gr/complete/search?client=serp&hl=el&q=%CF%86%CE%BF%CF%81%CE%AD%CE%BC%CE%B1%CF%84%CE%B1%20%CE%BC%CE%AC%CE%BE%CE%B9&callback=fnhbhk3rbbp3b
        $url = 'https://www.google.gr/complete/search?';
        $client = new \Dounasth\Laraffiliate\HttpClient();
        $params = http_build_query(array('client'=>'firefox','hl'=>'el','q'=>$search,));

//        $request = $client->createRequest('GET', $url.$params);
//        $request->addHeader('Accept-Encoding','GZIP');
//        $request->addHeader('Content-Type','application/json; charset=UTF-8');
//        $response = $client->send($request);
//        $str = $response->getBody()->getContents();

        set_time_limit(0);
        ini_set("memory_limit","4048M");
        ini_set('max_execution_time',3600);
        error_reporting(0);
        date_default_timezone_set('Europe/Athens');
        setlocale(LC_TIME, 'el_GR.UTF-8');
        $start = time();
        $a = $b = array(
            'α',
            'β',
            'γ',
            'δ',
            'ε',
            'ζ',
            'η',
            'θ',
            'ι',
            'κ',
            'λ',
            'μ',
            'ν',
            'ξ',
            'ο',
            'π',
            'ρ',
            'σ',
            'τ',
            'υ',
            'φ',
            'χ',
            'ψ',
            'ω'
        );
        $godeep = true;
        $q = $_GET['q'];
        try{
            for($i=-1;$i<=23;$i++){

                if($i>-1){
                    $q = urlencode($_GET['q']." {$a[$i]}");
                }else{
                    $q = urlencode($q);
                }
                //echo urldecode($q)." <br/>";
                $file = "http://www.google.com/complete/search?output=toolbar&q={$q}";
                $file = file_get_contents($file);

                $file = simplexml_load_string($file,'SimpleXMLElement',LIBXML_NOCDATA);
                $file = json_encode($file);
                $file = json_decode($file,1);

                $current = array();
                foreach ($file['CompleteSuggestion'] as $k=>$v){

                    $current[] = $v[0]['@attributes']['data'];
                    $json[] = $v[0]['@attributes']['data'];
                }
                if(count($current)>5){
                    for($j=0;$j<=23;$j++){

                        $qo = urlencode($_GET['q']." {$a[$i]}{$b[$j]}");


                        //echo urldecode($qo)." <br/>";

                        $file = "http://www.google.com/complete/search?output=toolbar&q={$qo}";
                        $file = file_get_contents($file);

                        $file = simplexml_load_string($file,'SimpleXMLElement',LIBXML_NOCDATA);
                        $file = json_encode($file);
                        $file = json_decode($file,1);
                        //echo "<pre>";
                        //print_r($file);
                        $jcurrent = array();
                        foreach ($file['CompleteSuggestion'] as $k=>$v){
                            $jcurrent[] = $v[0]['@attributes']['data'];
                            $json[] = $v[0]['@attributes']['data'];
                        }
                        sleep(1);
                    }
                }
                sleep(2);
                $check = time();
                if(($check-$start)>= 20){
                    break;
                }
            }
        }catch (Exception $e){
            echo "<pre>";
            print_r($e);
        }
        foreach ($json as $k=>$v){
            if(!trim($v)){
                unset($json[$k]);
            }
        }
        if(count($json)>0){
            niceprintr($json);
            $file = json_encode($json);
            echo $file;
        }

    }

}