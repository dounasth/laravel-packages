<?php

namespace Dounasth\Backend\App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserExtra extends \Eloquent
{
    use Notifiable;

    public $table = 'user_extra';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'activated',
    ];


    public function user()
    {
        return $this->belongsTo('Dounasth\Backend\App\Models\User', 'id', 'user_id');
    }

}
