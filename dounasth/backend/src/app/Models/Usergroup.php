<?php

namespace Dounasth\Backend\App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usergroup extends \Eloquent
{
    protected $table = 'usergroups';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'permissions',
    ];

	public function getPermissionsAttribute($value)
	{
		return json_decode($value, true);
	}
	public function setPermissionsAttribute($value)
	{
		$this->attributes['permissions'] = json_encode($value);
	}

	public function users()
	{
		return $this->belongsToMany('Dounasth\Backend\App\Models\User', 'users_on_groups', 'usergroup_id', 'user_id');
	}

}
