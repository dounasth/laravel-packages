<?php

namespace Dounasth\Backend\App\Models;

class Seo extends \Eloquent {

    protected $table = 'seo';
    protected $fillable = ['seoble_id','seoble_type','title', 'description', 'keywords'];

    public function seoble()
    {
        return $this->morphTo();
    }

}