<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class BackendCreateMetasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('metas', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name')->unique('idx_name');
			$table->integer('num_items')->default(0);
			$table->timestamps();
			$table->softDeletes();
			$table->index(['num_items','deleted_at','created_at'], 'popular_metas');
			$table->index(['name','deleted_at','num_items','created_at'], 'alpha_metas');
			$table->index(['updated_at','deleted_at','num_items','created_at'], 'updated_metas');
			$table->index(['created_at','deleted_at','num_items'], 'newest_metas');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('metas');
	}

}
