<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class BackendCreateUsersOnGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_on_groups', function(Blueprint $table)
		{
			$table->integer('user_id');
			$table->integer('usergroup_id');
			$table->primary(['user_id','usergroup_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_on_groups');
	}

}
