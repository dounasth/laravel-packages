<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class BackendCreateUserExtraTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_extra', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->unique('user_unique');
			$table->string('permissions')->nullable();
			$table->integer('activated')->default(0);
			$table->decimal('last_login', 10, 0)->nullable();
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_extra');
	}

}
