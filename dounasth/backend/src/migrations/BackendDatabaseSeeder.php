<?php
namespace Dounasth\Backend;

use Illuminate\Database\Seeder;

class BackendDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call('Dounasth\Backend\BackendGroupsTableSeeder');
	}

}

