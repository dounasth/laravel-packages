<?php

Route::middleware(['web'])->namespace('Dounasth\Backend\App\Http\Controllers')->group(function() {

    Auth::routes();
    Route::get('logout', 'Auth\LoginController@logout')->name('logout_get');

    Route::any('/user/activate', array('as' => 'activate_user', 'uses' => 'BackendUserController@activate'));
    Route::any('/user/resend-activation/{id}', array('as' => 'resend_activation', 'uses' => 'BackendUserController@resendActivation'));
    Route::any('/user/reset-password/{id}', array('as' => 'reset_password', 'uses' => 'BackendUserController@resetPassword'));
    Route::any('/user/do-reset-password', array('as' => 'do_reset_password', 'uses' => 'BackendUserController@doResetPassword'));
});

Route::middleware(['web', 'auth', 'auth.admin'])->namespace('Dounasth\Backend\App\Http\Controllers\Admin')->prefix('admin')->group(function() {
    Route::get('/', 'BackendDashboardController@dashboard')->name('admin');

    Route::get('/search', array('as' => 'admin_search', 'uses' => 'BackendDashboardController@search'));

    Route::get('users/list', array('as' => 'users', 'uses' => 'BackendUserController@listUsers'));
    Route::any('users/manage/{id}', array('as' => 'edit_user', 'uses' => 'BackendUserController@manageUser'));
    Route::any('users/manage/add', array('as' => 'add_user', 'uses' => 'BackendUserController@manageUser'));
    Route::any('users/manage/delete/{id}', array('as' => 'delete_user', 'uses' => 'BackendUserController@deleteUser'));

	Route::any('user/account', 'BackendUserController@account')->name('backend.user.account');
	Route::any('user/details', 'BackendUserController@details')->name('backend.user.details');

    Route::get('usergroups/list', array('as' => 'usergroups', 'uses' => 'BackendUserController@listUsergroups'));
    Route::any('usergroups/manage/{id}', array('as' => 'edit_usergroup', 'uses' => 'BackendUserController@manageUsergroup'));
    Route::any('usergroups/manage/add', array('as' => 'add_usergroup', 'uses' => 'BackendUserController@manageUsergroup'));
    Route::any('usergroups/manage/delete/{id}', array('as' => 'delete_usergroup', 'uses' => 'BackendUserController@deleteUsergroup'));

    Route::get('permissions/list', array('as' => 'permissions', 'uses' => 'BackendUserController@managePermissions'));

    Route::get('languages/list', array('as' => 'languages_list', 'uses' => 'BackendLanguagesController@listLangs'));
    Route::get('languages/status/{locale}/{status}', array('as' => 'language_set_status', 'uses' => 'BackendLanguagesController@setStatus'))->where('status', '[0-1]');
    Route::get('languages/default/{locale}/{area}', array('as' => 'language_set_default', 'uses' => 'BackendLanguagesController@setDefault'))->where('area', '(site|admin)');
    Route::get('languages/fallback/{locale}/{area}', array('as' => 'language_set_fallback', 'uses' => 'BackendLanguagesController@setFallback'))->where('area', '(site|admin)');

    Route::get('languages/translations/list/{file?}', array('as' => 'translations_list', 'uses' => 'BackendLanguagesController@listTranslations'));
    Route::any('languages/translations/save/{locale}/{file}', array('as' => 'translations_save', 'uses' => 'BackendLanguagesController@saveTranslations'));
    Route::any('languages/translations/delete/{path}/{file}/{key}', array('as' => 'translations_delete', 'uses' => 'BackendLanguagesController@deleteTranslation'));

    Route::get('pingEngines', function(){
        \Dounasth\Backend\Util::pingEngines();
        exit;
    });

	Route::middleware(['web', 'auth', 'auth.admin'])->namespace('Config')->prefix('config')->group(function() {
		Route::get('/', 'ConfigDashboardController@dashboard')->name('config');
	});
});




