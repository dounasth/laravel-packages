<?php

\Eventy::addFilter('admin.left-menu', function($tree) {
	$tree = $tree + [
		'content_menu' => '<li class="header">CONTENT</li>',
		'tags' => array(
			'label' => 'Tags',
			'href' => route('content.tags.index'),
			'icon' => 'fa-cog',
		),
		'photos' => array(
			'label' => 'Photos',
			'href' => route('content.photos.index'),
			'icon' => 'fa-cog',
		),
	];
	return $tree;
}, 20, 1);

\Eventy::addFilter('config.menu', function($tree) {
	$tree = $tree + [
			'content' => array(
				'label' => 'Content',
				'href' => '#',
				'icon' => 'fa-cogs',
				'submenu' => array(
				)
			),
		];
	return $tree;
}, 20, 1);



Route::middleware(['web', 'auth', 'auth.admin'])->namespace('Dounasth\Content\App\Controllers\Admin')->prefix('admin')->group(function() {
    Route::any('content/tags/index', 'TagsController@index')->name('content.tags.index');
    Route::any('content/photos/index', 'PhotosController@index')->name('content.photos.index');
});
