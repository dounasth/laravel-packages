<?php

namespace Dounasth\Content\App\Controllers\Admin;

use Dounasth\Content\App\Models\Photo;
use Dounasth\Crud\App\Controllers\BaseCrudController;

class PhotosController extends BaseCrudController
{
    public $model = Photo::class;
    public $crudName = 'photos';
    public $routesPrefix = 'content.photos.';
    public $route = 'content.photos.index';

    public function init()
    {
        parent::init();
        $this->data['fields']['list'] = [
            'path' => [ 'title' => 'Path', 'value' => 'path', 'sorter'=>'path' ],
            'link_type' => [ 'title' => 'Link Type', 'value' => 'link_type', 'sorter'=>'link_type' ],
            'imageable_id' => [ 'title' => 'imageable_id', 'value' => 'imageable_id' ],
            'imageable_type' => [ 'title' => 'imageable_type', 'value' => 'imageable_type' ],
        ];
    }

}
