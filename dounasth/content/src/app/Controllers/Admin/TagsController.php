<?php

namespace Dounasth\Content\App\Controllers\Admin;

use Dounasth\Content\App\Models\Tag;
use Dounasth\Crud\App\Controllers\BaseCrudController;
use Dounasth\Crud\Traits\CrudDeleteTrait;
use Dounasth\Crud\Traits\CrudEditTrait;

class TagsController extends BaseCrudController
{
    public $model = Tag::class;
    public $crudName = 'tags';
    public $route = 'content.tags.index';
    public $routesPrefix = 'content.tags.';

    public function init()
    {
        parent::init();
        $this->data['fields']['list'] = [
            'name' => [ 'title' => 'Tag', 'value' => 'name', 'sorter'=>'name' ],
            'normalized' => [ 'title' => 'Slug', 'value' => 'normalized', 'sorter'=>'normalized' ],
        ];
    }

}
