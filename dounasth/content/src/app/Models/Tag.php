<?php namespace Dounasth\Content\App\Models;

use Dounasth\Crud\Traits\CrudModelTrait;
use Illuminate\Support\Collection;

class Tag extends \Cviebrock\EloquentTaggable\Models\Tag
{
    use CrudModelTrait;

    protected function select2($query=null) {
        $data = self::select();
        if ($query) {
//            $data = $data->where('arzygologiounew', 'like', "%$query%")
//                ->orWhere('carnumber', 'like', "%$query%")
//                ->orWhere('cardriver', 'like', "%$query%");
        }
        $data = $data->orderBy($this->getKeyName(), 'desc')->get();
        $select2 = [];
        foreach ($data as $datum) {
            $select2[] = [
                'id' => $datum->selectboxValue(),
                'text' => $datum->selectboxText(),
            ];
        }
        return new Collection($select2);
    }


    public function selectboxText() {
        return $this->name;
    }

    public function selectboxValue() {
        return $this->name;
    }
}
