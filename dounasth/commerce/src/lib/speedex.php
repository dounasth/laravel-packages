<?php

define('SPEEDEX_TEST_MODE', true);

//define('SPEEDEX_USERNAME', 'eoro');
//define('SPEEDEX_PASSWORD', 'safety-SELL-COOL-GENTLE');
//define('SPEEDEX_CUSTOMER_ID', 'ΠΕ145031');
//define('SPEEDEX_AGREEMENT_ID', '80499');
//define('SPEEDEX_BRANCH_ID', '1000;0101');

//define('SPEEDEX_USERNAME', 'mypharmacy');
//define('SPEEDEX_PASSWORD', 'supply-break-TONE-warsaw');
//define('SPEEDEX_CUSTOMER_ID', 'ΠΕ112847');
//define('SPEEDEX_AGREEMENT_ID', '83872');
//define('SPEEDEX_BRANCH_ID', '1000;0812');

define('SPEEDEX_USERNAME', 'WebOnDemand');
define('SPEEDEX_PASSWORD', 'MIDDLE-DOLLARS-boat-ocean');
define('SPEEDEX_CUSTOMER_ID', 'ΠΕ145031');
define('SPEEDEX_AGREEMENT_ID', '80499');
define('SPEEDEX_BRANCH_ID', '1000;0101');

define('SPEEDEX_TABLE_FLAG', '3');


class Speedex {

    protected $testMode;
    protected $username;
    protected $password;
    protected $customerId;
    protected $agreementId;
    protected $branchId;
    protected $tableFlag;

    private $url;
    const DOMAIN_LIVE = 'http://www.speedex.gr/AccessPoint/AccessPoint.asmx?WSDL';
    const DOMAIN_TEST = 'http://www.speedex.gr/accesspointtest/accesspoint.asmx?WSDL';

    public $client;
    protected $options = array(
        'style'=>SOAP_RPC,
        'use'=>SOAP_ENCODED,
        'soap_version'=>SOAP_1_2,
        'cache_wsdl'=>WSDL_CACHE_NONE,
        'connection_timeout'=>15,
        'trace'=>true,
        'encoding'=>'UTF-8',
        'exceptions'=>true,
    );

    public $latestResponse;
    protected $sessionId;

    public function __construct()
    {
    }

    public static function init($username, $password, $customerId, $agreementId, $branchId, $tableFlag, $testMode=false)
    {
        $instance = new Speedex();
        $instance->username = $username;
        $instance->password = $password;
        $instance->customerId = $customerId;
        $instance->agreementId = $agreementId;
        $instance->branchId = $branchId;
        $instance->tableFlag = $tableFlag;
        $instance->testMode = $testMode;

        if ($instance->testMode) {
            $instance->url = self::DOMAIN_TEST;
        }
        else $instance->url = self::DOMAIN_LIVE;
        $instance->initClientAndSession();
        return $instance;
    }

    public static function initWithDefines()
    {
        $instance = new Speedex();
        $instance->username = SPEEDEX_USERNAME;
        $instance->password = SPEEDEX_PASSWORD;
        $instance->customerId = SPEEDEX_CUSTOMER_ID;
        $instance->agreementId = SPEEDEX_AGREEMENT_ID;
        $instance->branchId = SPEEDEX_BRANCH_ID;
        $instance->tableFlag = SPEEDEX_TABLE_FLAG;
        $instance->testMode = SPEEDEX_TEST_MODE;

        if ($instance->testMode) {
            $instance->url = self::DOMAIN_TEST;
        }
        else $instance->url = self::DOMAIN_LIVE;
        $instance->initClientAndSession();
        return $instance;
    }

    public function initClientAndSession() {
        $this->client = new SoapClient($this->url);
        if ($this->client) {
            $this->createSession();
        }
        return $this;
    }

    public function createSession(){
        $data = array(
            'CreateSession' => array(
                'username' => $this->username,
                'password' => $this->password,
            )
        );
        $this->latestResponse = $result = $this->client->__call('CreateSession', $data);
        if ($result->returnCode == 1 && fn_is_not_empty($result->sessionId)) {
            $this->sessionId = $result->sessionId;
        }

        return $this;
    }

    public function destroySession(){
        $data = array(
            'DestroySession' => array(
                'sessionID' => $this->sessionId,
            )
        );
        $this->latestResponse = $result = $this->client->__call('CreateSession', $data);
        if ($result->returnCode == 1) {
            $this->sessionId = false;
        }

        return $this;
    }

    public function createVoucher($data){
        $data = array_merge( array(
                /* Mandatory */
                'EnterBranchId' => $this->branchId,
                'SND_Customer_Id' => $this->customerId,
                'Snd_agreement_id' => $this->agreementId,
                'RCV_Name' => '',
                'RCV_Addr1' => '',
                'RCV_Zip_Code' => '',
                'RCV_City' => '',
                'RCV_Country' => '',
                'RCV_Tel1' => '',
                'Voucher_Weight' => '',
                'Pod_Amount_Cash' => '',
                /* Optional */
                'voucher_code' => '',
                'RCV_Afm' => '',
                'RCV_DOY' => '',
                'RCV_Company' => '',
                'RCV_Addr2' => '',
                'RCV_Tel2' => '',
                'Security_Value' => '',
                'Express_Delivery' => '',
                'Saturday_Delivery' => '',
                'Time_Limit' => '',
                'Comments_2853_1' => '',
                'Comments_2853_2' => '',
                'Comments_2853_3' => '',
                'Comments' => '',
                'Voucher_Volume' => '',
                'Pod_Amount_Description' => '',
                'PayCode_Flag' => 1,
                'BranchBankCode' => '',
                'Paratiriseis_2853_1' => '',
                'Paratiriseis_2853_2' => '',
                'Paratiriseis_2853_3' => '',
                'email' => '',
                'BasicService' => '',
                'Items' => '',
                'Vouc_descr' => '',
                '_cust_Flag' => '',
                'Comments_new' => '',
            ),
            $data
        );
        $envelope = array(
            'CreateBOL' => array(
                'sessionID' => $this->sessionId,
                'inListPod' => array(
                    'BOL' => array(
                        $data
                    ),
                ),
                'tableFlag' => $this->tableFlag
            )
        );
        try {
            $this->latestResponse = $result = $this->client->__call('CreateBOL', $envelope);
        }
        catch (Exception $e) {
            echo "<pre>".$e->getMessage()."</pre>";
        }

        if ($result->returnCode == 1 && fn_is_not_empty($result->outListPod)) {
            $voucherCode = $result->outListPod->BOL->voucher_code;
            return $voucherCode;
        }
        else return false;
    }

    public function cancelVoucher($voucherCode){
        $data = array(
            'CancelBOL' => array(
                'sessionID' => $this->sessionId,
                'voucherID' => $voucherCode
            )
        );
        try {
            $this->latestResponse = $result = $this->client->__call('CancelBOL', $data);
        }
        catch (Exception $e) {
            echo "<pre>".$e->getMessage()."</pre>";
        }

        if ($result->returnCode == 1) {
            return true;
        }
        else return false;
    }

    public function getVoucherPdf($voucherCodes, $perVoucher=false, $paperType=1){
        if (!is_array($voucherCodes)) {
            $voucherCodes = array($voucherCodes);
        }
        $data = array(
            'GetBOLPdf' => array(
                'sessionID' => $this->sessionId,
                'voucherIDs' => $voucherCodes,
                'perVoucher' => $perVoucher,    //  If  is true then you get one pdf  per shipement – if is false you get one pdf with all shipments
                'paperType' => $paperType,      //  if paperType = 1 then you need A4 if is 2 you get A5 parer size
            )
        );
        try {
            $this->latestResponse = $result = $this->client->__call('GetBOLPdf', $data);
        }
        catch (Exception $e) {
            echo "<pre>".$e->getMessage()."</pre>";
        }

        if ($result->returnCode == 1 && fn_is_not_empty($result->GetBOLPdfResult->Voucher)) {
            return $result->GetBOLPdfResult->Voucher->pdf;
        }
        else return false;
    }

    public function getSummaryPdf($beginDate, $endDate){
        $data = array(
            'GetBOLSummaryPdf' => array(
                'sessionID' => $this->sessionId,
                'beginDate' => $beginDate->format('c'),
                'endDate' => $endDate->format('c'),
            )
        );
        try {
            $this->latestResponse = $result = $this->client->__call('GetBOLSummaryPdf', $data);
        }
        catch (Exception $e) {
            echo "<pre>".$e->getMessage()."</pre>";
        }

        if ($result->returnCode == 1 && fn_is_not_empty($result->GetBOLSummaryPdfResult)) {
            return $result->GetBOLSummaryPdfResult;
        }
        else return false;
    }

    public function traceByVoucher($voucherCode) {
        $data = array(
            'GetTraceByVoucher' => array(
                'sessionID' => $this->sessionId,
                'VoucherID' => $voucherCode,
            )
        );
        try {
            $this->latestResponse = $result = $this->client->__call('GetTraceByVoucher', $data);
        }
        catch (Exception $e) {
            echo "<pre>".$e->getMessage()."</pre>";
        }

        if ($result->returnCode == 1 && fn_is_not_empty($result->checkpoints->Checkpoint)) {
            return $result->checkpoints->Checkpoint;
        }
        else return false;
    }

    public function getLatestMessage() {
        if (fn_is_not_empty($this->latestResponse)) {
            return 'Code: ' . $this->latestResponse->returnCode . ' | Message: ' . $this->latestResponse->returnMessage;
        }
        else return 'No latest response found';
    }

}