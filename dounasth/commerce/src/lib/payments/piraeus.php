<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

error_reporting(E_ERROR);


if (Route::currentRouteAction() == 'DashboardController@creditsBuy') {
    header('Location:'.BASE_DOMAIN.'/order/pay?id='.$order->hash.'&t='.get_class($order).'&s='.SITE_ID);
    exit;
}


function fn_finish_payment($order, $pp_response, $is_paid) {
    $order->is_paid = $is_paid;
    $order->status_id = $pp_response['order_status'];
    $order->payment_response = $pp_response;
    $order->save();
    if (get_class($order) == 'CreditOrder') {
        if ($is_paid == 1) {
            Site::addCreditsToSite($order->site_id, $order->id, $order->credits, 'successful credits order');
            Session::flash('buy_credits_message', [
                'type' => 'success',
                'text' => trans('generic.payments.creditorder_success'),
            ]);
        }
    }
    elseif (get_class($order) == 'Order') {
        if ($is_paid == 1) {
            Site::removeCreditsFromSite(SITE_ID, $order->id, $order->items()->sum('quantity'), 'successful cases order');
        }
    }
}



if (defined('PAYMENT_NOTIFICATION')) {

    $is_paid = 2;

    if ($mode == 'notify') {

        $order_id = $hash;

//        $order = Order::where('hash', '=', $order_id)->firstOrFail();
        $processor_data = $order->payment->toArray();

        $pp_response = array();
        $pp_response['transaction_id'] = $_REQUEST['TransactionId'];
        $pp_response['transaction_datetime'] = $_REQUEST['TransactionDateTime'];
        $pp_response['reason_text'] = $_REQUEST['ResponseDescription'];

        if (isset($_REQUEST['ResultCode']) && $_REQUEST['ResultCode'] == 0) {
            if ( $_REQUEST['StatusFlag'] == 'Success' && in_array($_REQUEST['ResponseCode'], array('00', '08', '10', '11', '16')) ) {

//                $tran_ticket = db_get_field("SELECT data FROM ?:order_data WHERE type = 'E' AND order_id = ?i", $order_id);
                $tran_ticket = $order->data['data'];

                $cart_hashkey_str = $tran_ticket . $processor_data['params']['posid']. $processor_data['params']['acquirerid'] . $_REQUEST['MerchantReference'] . $_REQUEST['ApprovalCode'] . $_REQUEST['Parameters'] . $_REQUEST['ResponseCode'] . $_REQUEST['SupportReferenceID'] . $_REQUEST['AuthStatus'] . $_REQUEST['PackageNo'] . $_REQUEST['StatusFlag'];
                $cart_hashkey = strtoupper(hash( 'sha256', $cart_hashkey_str ));

                if ($cart_hashkey == $_REQUEST['HashKey']) {
                    $is_paid = 1;
                    $pp_response['order_status'] = $processor_data['params']['statuses']['captured'];

//                    db_query("DELETE FROM ?:order_data WHERE type = 'E' AND order_id = ?i", $order_id);
//                    $order->data = '';
//                    $order->save();

                    if (!empty($_REQUEST['SupportReferenceID'])) {
                        $pp_response['reason_text'] .= '; SupportReferenceID: ' . $_REQUEST['SupportReferenceID'];
                    }

                    if (!empty($_REQUEST['ApprovalCode'])) {
                        $pp_response['reason_text'] .= '; ApprovalCode: ' . $_REQUEST['ApprovalCode'];
                    }
                } else {
                    $is_paid = 2;
                    $pp_response['order_status'] = $processor_data['params']['statuses']['canceled'];
                    $pp_response['reason_text'] .= 'Hash value is incorrect';
                }
            } elseif ($_REQUEST['StatusFlag'] == 'Failure') {
                $is_paid = 2;
                $pp_response['order_status'] = $processor_data['params']['statuses']['canceled'];

            } elseif ($_REQUEST['StatusFlag'] == 'Asynchronous') {
                $is_paid = 1;
                $pp_response['order_status'] = $processor_data['params']['statuses']['captured'];
                $pp_response['reason_text'] .= '; Asynchronous';
            }

        } else {
            $is_paid = 2;
            $pp_response['order_status'] = $processor_data['params']['statuses']['canceled'];
        }

        if (get_class($order) == 'CreditOrder' && $is_paid != 1) {
            Session::flash('buy_credits_message', [
                'type'=>'danger',
                'text'=> trans('generic.payments.gereral_problem').'<br/>'.$pp_response['reason_text']
            ]);
        }


    } elseif ($mode == 'cancel') {

        if (!empty($_SESSION['stored_piraeus_orderid'])) {
            $order_id = Session::put('stored_piraeus_orderid');
            unset($_SESSION['stored_piraeus_orderid']);
            unset($_SESSION['stored_piraeus_orderid_type']);
            unset($_SESSION['stored_piraeus_orderid_site']);

            $is_paid = 2;
            $pp_response['order_status'] = $processor_data['params']['statuses']['refused'];
            $pp_response["reason_text"] = trans('generic.payments.transaction_refused');

        }

        if (get_class($order) == 'CreditOrder') {
            Session::flash('buy_credits_message', [
                'type'=>'danger',
                'text'=> trans('generic.payments.transaction_cancelled')
            ]);
        }
    }

    if ($order->payment->php_file == 'piraeus.php') {
        fn_finish_payment($order, $pp_response, $is_paid);
    }


} else {

    $ticketing_data = Array (
        'AcquirerId' => $processor_data['params']['acquirerid'],
        'MerchantId' => $processor_data['params']['merchantid'],
        'PosId' => $processor_data['params']['posid'],
        'Username' => $processor_data['params']['username'],
        'Password' => md5($processor_data['params']['password']),
        'RequestType' => $processor_data['params']['requesttype'],
        'CurrencyCode' => $processor_data['params']['currencycode'],
        'MerchantReference' => (($order['repaid']) ? ($order->id . '_' . $order['repaid']) : $order->id),
        'Amount' => $order['total'],
        'Installments' => 0,
        'Bnpl' => 0,
        'ExpirePreauth' => (($processor_data['params']['requesttype'] == '00') ? $processor_data['params']['expirepreauth'] : '0'),
        'Parameters' => ''
    );

    $str = <<<EOT
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Body>
<IssueNewTicket xmlns="http://piraeusbank.gr/paycenter/redirection">
<Request>
EOT;
    $str .= fn_array_to_xml($ticketing_data);
    $str .= <<<EOT
</Request>
</IssueNewTicket>
</soap:Body>
</soap:Envelope>
EOT;
    $str = str_replace(array("\t", "\n", "\r"), '', $str);

    $response_data = Http::post("https://paycenter.piraeusbank.gr/services/tickets/issuer.asmx", $str, array(
        'headers' => array(
            'Content-type: text/xml; charset=utf-8',
            'SOAPAction: http://piraeusbank.gr/paycenter/redirection/IssueNewTicket'
        )
    ));

    $resultcode = true;
    $pp_response = array();

    if (strpos($response_data, '<ResultCode') !== false) {
        if (preg_match('!<ResultCode[^>]*>([^>]+)</ResultCode>!', $response_data, $matches)) {
            $resultcode = $matches[1];
        }
    }


    if ($resultcode == "0") {

        if (strpos($response_data, '<TranTicket') !== false) {
            if (preg_match('!<TranTicket[^>]*>([^>]+)</TranTicket>!', $response_data, $matches)) {
                $data = array (
                    'order_id' => $order->id,
                    'type' => 'E',
                    'data' => $matches[1],
                );
                $order->data = $data;
                $order->save();
            }
        }

        $post_url = 'https://paycenter.piraeusbank.gr/redirection/pay.aspx';

        $post_data = array (
            'AcquirerId' => $processor_data['params']['acquirerid'],
            'MerchantId' => $processor_data['params']['merchantid'],
            'PosId' => $processor_data['params']['posid'],
            'User' => $processor_data['params']['username'],
            'LanguageCode' => $processor_data['params']['languagecode'],
            'MerchantReference' => (($order['repaid']) ? ($order->id . '_' . $order['repaid']) : $order->id),
            'ParamBackLink' => ""
        );

        Session::put('stored_piraeus_orderid', $order->hash);
        Session::put('stored_piraeus_orderid_type', get_class($order));
        Session::put('stored_piraeus_orderid_site', Input::get('s', SITE_ID));
        Session::save();

        fn_create_payment_form($post_url, $post_data, 'Piraeus server');
    exit;

    } else {
        $pp_response['order_status'] = $processor_data['params']['statuses']['canceled'];
        $pp_response["ResultCode"] = $resultcode;

        if (strpos($response_data, '<ResultDescription') !== false) {
            if (preg_match('!<ResultDescription[^>]*>([^>]+)</ResultDescription>!', $response_data, $matches)) {
                $pp_response["reason_text"] = $matches[1];
            }
        }
    }

}
