<?php

use \Illuminate\Support\Facades\Session;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ( !defined('PAYMENT_NOTIFICATION') ) {

    if (get_class($order) == 'Dounasth\Commerce\App\Models\Order\CreditOrder') {
        $is_paid = 1;
        $order->is_paid = $is_paid;
        $order->status_id = 2;
        $order->save();
//        Site::addCreditsToSite($order->site_id, $order->id, $order->credits, 'successful credits order');
        header('Location:'.route('credits_buy.notify', ['notify', 'id='.$order->hash]));
        exit;
    }
    elseif (get_class($order) == 'Dounasth\Commerce\App\Models\Order\Order') {
        $is_paid = 1;
        $order->is_paid = $is_paid;
        $order->status_id = 2;
        $order->save();
//        Site::removeCreditsFromSite(SITE_ID, $order->id, $order->items()->sum('quantity'), 'successful cases order');
        header('Location:'.route('order.pay.notify', ['notify', 'id='.$order->hash]));
        exit;
    }

}
else {
    Session::flash('buy_credits_message', [
        'type' => 'success',
        'text' => trans('generic.payments.creditorder_success'),
    ]);
}
