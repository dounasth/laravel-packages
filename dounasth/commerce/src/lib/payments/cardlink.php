<?php

if (!defined('BOOTSTRAP') ) { die('Access denied'); }

function fn_finish_payment($order, $pp_response, $is_paid) {
    $order->is_paid = $is_paid;
    $order->status_id = $pp_response['order_status'];
    $order->payment_response = $pp_response;
    $order->save();
    if ($is_paid == 1 && get_class($order) == 'CreditOrder') {
        Site::addCreditsToSite($order->site_id, $order->id, $order->credits, 'successful credits order');
    }
    elseif ($is_paid == 1 && get_class($order) == 'Order') {
        Site::removeCreditsFromSite(SITE_ID, $order->id, $order->items()->sum('quantity'), 'successful cases order');
    }
}

if (defined('PAYMENT_NOTIFICATION')) {

    //	Verify response digest with our key
    $post_data_array = array();
    if (isset($_POST['mid'])) {$post_data_array[0] = $_POST['mid'];}
    if (isset($_POST['orderid'])) {$post_data_array[1] = $_POST['orderid'];}
    if (isset($_POST['status'])) {$post_data_array[2] = $_POST['status'];}
    if (isset($_POST['orderAmount'])) {$post_data_array[3] = $_POST['orderAmount'];}
    if (isset($_POST['currency'])) {$post_data_array[4] = $_POST['currency'];}
    if (isset($_POST['paymentTotal'])) {$post_data_array[5] = $_POST['paymentTotal'];}
    if (isset($_POST['message'])) {$post_data_array[6] = $_POST['message'];}
    if (isset($_POST['riskScore'])) {$post_data_array[7] = $_POST['riskScore'];}
    if (isset($_POST['payMethod'])) {$post_data_array[8] = $_POST['payMethod'];}
    if (isset($_POST['txId'])) {$post_data_array[9] = $_POST['txId'];}
    if (isset($_POST['paymentRef'])) {$post_data_array[10] = $_POST['paymentRef'];}
    $post_data_array[11] = $processor_data['params']['shared_secret_key'];
    $post_DIGEST = $_POST['digest'];
    $post_data = implode("", $post_data_array);
    $digest_verfication = base64_encode(sha1($post_data,true));

    // fn_print_r($_POST);
    // fn_print_r($post_data_array);
    // fn_print_r($post_data);
    // fn_print_r($processor_data);
//    niceprintr($_POST['digest']);
//    niceprintr($digest_verfication);

    if ($digest_verfication != $_POST['digest']) {
        //	Not Verified, not same digests so stop everything
        fn_set_notification('E', fn_get_lang_var('fraud_suspected'), fn_get_lang_var('fraud_suspected_msg'));
        // fn_redirect('');
        exit;
    }

    //	Verified, continue

    $is_paid = true;
    $bank_response = $_REQUEST;

    $pp_response["order_status"] = $processor_data['params']['statuses'][strtolower($bank_response['status'])];

    if ($bank_response['status'] != 'CAPTURED') {
        $is_paid = 2;
        @$pp_response["reason_text"] = implode(' :: ', array($bank_response['status'], $bank_response['message']));
    }

    $pp_response["transaction_id"] = $bank_response['txId'];

    if ( $order->payment->php_file == 'cardlink.php' ) {
        fn_finish_payment($order, $pp_response, $is_paid);
    }

} else {

    $amount = str_replace('.', ',', $order->total);

    $form_data = "";
    $form_data_array = array();

    $form_mid = $processor_data['params']['merchant_id'];					$form_data_array[1] = $form_mid;					//Req
    $form_lang = strtolower($processor_data['params']['lang_code']);		$form_data_array[2] = $form_lang;					//Opt
    $form_device_cate = "0"; 												$form_data_array[3] = $form_device_cate;			//Opt
    $form_order_id = $order->id.'T'.time();		            				$form_data_array[4] = $form_order_id;				//Req
    $form_order_desc = '';                                  				$form_data_array[5] = $form_order_desc;				//Opt
    $form_order_amount = $amount;											$form_data_array[6] = $form_order_amount;			//Req
    $form_currency = $processor_data['params']['currency'];					$form_data_array[7] = $form_currency;				//Req
    $form_email = $order->addresses['email'];								$form_data_array[8] = $form_email;					//Req
    $form_phone = $order->addresses['phone_mobile'];						$form_data_array[9] = $form_phone;					//Opt

    $form_bill_country = ''; /*$_POST['billCountry'];*/						$form_data_array[10] = $form_bill_country;			//Opt
    $form_bill_state = ''; /*$_POST['billState'];*/							$form_data_array[11] = $form_bill_state;			//Opt
    $form_bill_zip = ''; /*$_POST['billZip'];*/								$form_data_array[12] = $form_bill_zip;				//Opt
    $form_bill_city = ''; /*$_POST['billCity'];*/							$form_data_array[13] = $form_bill_city;				//Opt
    $form_bill_addr = ''; /*$_POST['billAddress'];*/						$form_data_array[14] = $form_bill_addr;				//Opt
    $form_weight = ''; /*$_POST['weight'];*/								$form_data_array[15] = $form_weight;				//Opt
    $form_dimension = ''; /*$_POST['dimensions'];*/							$form_data_array[16] = $form_dimension;				//Opt

    $form_ship_counrty = ''; /*$_POST['shipCountry'];*/						$form_data_array[17] = $form_ship_counrty;			//Opt
    $form_ship_state = ''; /*$_POST['shipState'];*/							$form_data_array[18] = $form_ship_state;			//Opt
    $form_ship_zip = ''; /*$_POST['shipZip'];*/								$form_data_array[19] = $form_ship_zip;				//Opt
    $form_ship_city = ''; /*$_POST['shipCity'];*/							$form_data_array[20] = $form_ship_city;				//Opt
    $form_ship_addr = ''; /*$_POST['shipAddress'];*/						$form_data_array[21] = $form_ship_addr;				//Opt

    $form_add_fraud_score = ''; /*$_POST['addFraudScore']*/;				$form_data_array[22] = $form_add_fraud_score;		//Opt
    $form_max_pay_retries = ''; /*$_POST['maxPayRetries']*/;				$form_data_array[23] = $form_max_pay_retries;		//Opt
    $form_reject3dsU = ''; /*$_POST['reject3dsU']*/;						$form_data_array[24] = $form_reject3dsU;			//Opt

    $form_pay_method = ''; /*$_POST['payMethod'];*/							$form_data_array[25] = $form_pay_method;			//Opt
    $form_trytpe = 1;														$form_data_array[26] = $form_trytpe;				//Opt

    $installments = ''; //($payment_info['installments'] > 0 ) ? $payment_info['installments'] : '' ;
    $offset = ($installments > 0 ) ? 0 : '' ;
    $form_ext_install_offset = $offset; /*$_POST['extInstallmentoffset'];*/		$form_data_array[27] = $form_ext_install_offset;	//Opt
    $form_ext_install_period = $installments; /*$_POST['extInstallmentperiod'];*/		$form_data_array[28] = $form_ext_install_period;	//Opt

    $form_ext_reccuring_freq = ''; /*$_POST['extRecurringfrequency'];*/		$form_data_array[29] = $form_ext_reccuring_freq;	//Opt
    $form_ext_reccuring_enddate = ''; /*$_POST['extRecurringenddate'];*/	$form_data_array[30] = $form_ext_reccuring_enddate;	//Opt

    $form_block_score = ''; /*$_POST['blockScore']*/;						$form_data_array[31] = $form_block_score;			//Opt
    $form_cssurl = ''; /*$_POST['cssUrl'];*/								$form_data_array[32] = $form_cssurl;				//Opt


    if (get_class($order) == 'CreditOrder') {
        $form_confirm_url = route('credits_buy.notify', ['confirm', 'id='.$order->hash]);
        $form_data_array[33] = $form_confirm_url;

        $form_cancel_url = route('credits_buy.notify', ['cancel', 'id='.$order->hash]);
        $form_data_array[34] = $form_cancel_url;
    }
    elseif (get_class($order) == 'Order') {
        $form_confirm_url = route('order.pay.notify', ['confirm', 'id='.$order->hash]);
        $form_data_array[33] = $form_confirm_url;

        $form_cancel_url = route('order.pay.notify', ['cancel', 'id='.$order->hash]);
        $form_data_array[34] = $form_cancel_url;
    }



    $form_var1 = '';														$form_data_array[35] = $form_var1;
    $form_var2 = '';														$form_data_array[36] = $form_var2;
    $form_var3 = '';														$form_data_array[37] = $form_var3;
    $form_var4 = '';														$form_data_array[38] = $form_var4;
    $form_var5 = '';														$form_data_array[39] = $form_var5;

    $form_secret = $processor_data['params']['shared_secret_key'];			$form_data_array[40] = $form_secret;				//Req

    $form_data = implode("", $form_data_array);

    $digest = base64_encode(sha1($form_data,true));

    // fn_print_r($form_data);
    // fn_print_r($form_data_array);
    // fn_print_r($digest);

    $_data = array(
        'order_id' => $order->id,
        'type' => 'Y',
        'data' => $digest
    );
    $order->data = $_data;
    $order->save();

    // $send_it_2 = "https://alpha.test.modirum.com/vpos/shophandlermpi";
    $send_it_2 = $processor_data['params']['gateway'];
    if ($send_it_2 == 'alphabank_test') {
        $send_it_2 = 'https://alpha.test.modirum.com/vpos/shophandlermpi';
    }
    elseif ($send_it_2 == 'alphabank_live') {
        $send_it_2 = 'https://www.alphaecommerce.gr/vpos/shophandlermpi';
    }
    elseif ($send_it_2 == 'eurobank_test') {
        $send_it_2 = 'https://euro.test.modirum.com/vpos/shophandlermpi';
    }
    elseif ($send_it_2 == 'eurobank_live') {
        $send_it_2 = 'https://vpos.eurocommerce.gr/vpos/shophandlermpi';
    }
    // fn_print_r($send_it_2);
    /*
    style="display: none; visibility: hidden;"
    javascript: document.process.submit();
    */

    $msg = '';

    echo <<<EOT
<html>
<body onLoad="javascript: document.process.submit();">
<form name="process" method="POST" action="{$send_it_2}" accept-charset="UTF-8" style="display: none; visibility: hidden;">
	<input type="text" name="mid" value="{$form_mid}"/>
	<input type="text" name="lang" value="{$form_lang}"/>
	<input type="text" name="deviceCategory" value="{$form_device_cate}"/>
	<input type="text" name="orderid" value="{$form_order_id}"/>
	<input type="text" name="orderDesc" value="{$form_order_desc}"/>
	<input type="text" name="orderAmount" value="{$form_order_amount}"/>
	<input type="text" name="currency" value="{$form_currency}"/>
	<input type="text" name="payerEmail" value="{$form_email}"/>
	<input type="text" name="payerPhone" value="{$form_phone}"/>
	<input type="text" name="billCountry" value="{$form_bill_country}"/>
	<input type="text" name="billState" value="{$form_bill_state}"/>
	<input type="text" name="billZip" value="{$form_bill_zip}"/>
	<input type="text" name="billCity" value="{$form_bill_city}"/>
	<input type="text" name="billAddress" value="{$form_bill_addr}"/>
	<input type="text" name="weight" value="{$form_weight}"/>
	<input type="text" name="dimensions" value="{$form_dimension}"/>
	<input type="text" name="shipCountry" value="{$form_ship_counrty}"/>
	<input type="text" name="shipState" value="{$form_ship_state}"/>
	<input type="text" name="shipZip" value="{$form_ship_zip}"/>
	<input type="text" name="shipCity" value="{$form_ship_city}"/>
	<input type="text" name="shipAddress" value="{$form_ship_addr}"/>
	<input type="text" name="addFraudScore" value="{$form_add_fraud_score}"/>
	<input type="text" name="maxPayRetries" value="{$form_max_pay_retries}"/>
	<input type="text" name="reject3dsU" value="{$form_reject3dsU}"/>
	<input type="text" name="payMethod" value="{$form_pay_method}"/>
	<input type="text" name="trType" value="{$form_trytpe}"/>
	<input type="text" name="extInstallmentoffset" value="{$form_ext_install_offset}"/>
	<input type="text" name="extInstallmentperiod" value="{$form_ext_install_period}"/>
	<input type="text" name="extRecurringfrequency" value="{$form_ext_reccuring_freq}"/>
	<input type="text" name="extRecurringenddate" value="{$form_ext_reccuring_enddate}"/>
	<input type="text" name="blockScore" value="{$form_block_score}"/>
	<input type="text" name="cssUrl" value="{$form_cssurl}"/>
	<input type="text" name="confirmUrl" value="{$form_confirm_url}"/>
	<input type="text" name="cancelUrl" value="{$form_cancel_url}"/>
	<input type="text" name="var1" value="{$form_var1}"/>
	<input type="text" name="var2" value="{$form_var2}"/>
	<input type="text" name="var3" value="{$form_var3}"/>
	<input type="text" name="var4" value="{$form_var4}"/>
	<input type="text" name="var5" value="{$form_var5}"/>
	<input type="text" name="digest" value="{$digest}"/>
</form>
   <p>{$msg}</p>
 </body>
</html>
EOT;
    exit;
}