<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

define('MAX_PAYPAL_PRODUCTS', 100);



function fn_finish_payment($order, $pp_response, $is_paid) {
    $order->is_paid = $is_paid;
    $order->status_id = $pp_response['order_status'];
    $order->payment_response = $pp_response;
    $order->save();
    if ($is_paid == 1 && get_class($order) == 'CreditOrder') {
        Site::addCreditsToSite($order->site_id, $order->id, $order->credits, 'successful credits order');
    }
    elseif ($is_paid == 1 && get_class($order) == 'Order') {
        Site::removeCreditsFromSite(SITE_ID, $order->id, $order->items()->sum('quantity'), 'successful cases order');
    }
}

// Return from paypal website
if (defined('PAYMENT_NOTIFICATION')) {

    $is_paid = 2;

    if ( ($mode == 'notify' || $mode == 'return') && $order->id) {

        if ( $order->payment->php_file == 'paypal.php' ) {

            $pp_response = array();

            $paypal_statuses = $processor_data['params']['statuses'];
            $account_type = fn_validate_email($processor_data['params']['account']) ? 'receiver_email' : 'receiver_id';
            if ($_REQUEST[$account_type] != $processor_data['params']['account']) {
                $pp_response['order_status'] = $paypal_statuses['denied'];
                $pp_response['reason_text'] = 'paypal_security_error';
                $is_paid = 2;
            }
            $pp_mc_gross = !empty($_REQUEST['mc_gross']) ? $_REQUEST['mc_gross'] : 0;

            if (stristr($_REQUEST['payment_status'], 'Refunded')) {
                $_order = db_get_row("SELECT status, total FROM ?:orders WHERE order_id = ?i", $order->id);

                $pp_response['order_status'] =
                    (floatval($_order['total']) - abs(floatval($_REQUEST['payment_gross'])) == 0)
                    ? $paypal_statuses['refunded'] : $_order['status'];

                $is_paid = 2;
            }

            if (($pp_mc_gross*1) != ($order->total*1)) {
                $pp_response['order_status'] = $paypal_statuses['denied'];
                $pp_response['reason_text'] = 'order_total_not_correct';
                $pp_response['transaction_id'] = $_REQUEST['txn_id'];
                $is_paid = 2;
            } elseif (stristr($_REQUEST['payment_status'], 'Completed')) {
                $params = $processor_data['params'];
                $paypal_host = ($params['mode'] == 'test' ? "www.sandbox.paypal.com" : "www.paypal.com");

                $paypal_post = $_REQUEST;
                $paypal_post[$account_type] = $processor_data['params']['account'];
                $paypal_post['cmd'] = '_notify-validate';
                unset($paypal_post['dispatch']);

                /*$result = Http::post("https://$paypal_host:443/cgi-bin/webscr", $paypal_post, array(
                    'headers' => array(
                        'Connection: close'
                    ))
                );

                if (stristr($result, 'VERIFIED')) {
                    $pp_response['order_status'] = $paypal_statuses['completed'];
                    $pp_response['reason_text'] = '';
                    $pp_response['transaction_id'] = $_REQUEST['txn_id'];
                    $is_paid = true;
                } elseif (stristr($result, 'INVALID')) {
                    $pp_response['order_status'] = $paypal_statuses['denied'];
                    $pp_response['reason_text'] = '';
                    $pp_response['transaction_id'] = $_REQUEST['txn_id'];
                    $is_paid = 2;
                } else {
                    $pp_response['order_status'] = $paypal_statuses['expired'];
                    $pp_response['reason_text'] = '';
                    $pp_response['transaction_id'] = $_REQUEST['txn_id'];
                    $is_paid = 2;
                }*/

                $pp_response['order_status'] = $paypal_statuses['completed'];
                $pp_response['reason_text'] = '';
                $pp_response['transaction_id'] = $_REQUEST['txn_id'];
                $is_paid = true;


            } elseif (stristr($_REQUEST['payment_status'], 'Pending')) {
                $pp_response['order_status'] = $paypal_statuses['pending'];
                $pp_response['reason_text'] = 'pending' . (!empty($_REQUEST['pending_reason'])? ": " .  $_REQUEST['pending_reason'] : "");
                $pp_response['transaction_id'] = $_REQUEST['txn_id'];
                $is_paid = true;
            } else {
                $pp_response['order_status'] = $paypal_statuses['denied'];
                $pp_response['reason_text'] = '';
                $pp_response['transaction_id'] = $_REQUEST['txn_id'];
                $is_paid = 2;
            }

            if (!empty($_REQUEST['payer_email'])) {
                $pp_response['customer_email'] = $_REQUEST['payer_email'];
            }
            if (!empty($_REQUEST['payer_id'])) {
                $pp_response['client_id'] = $_REQUEST['payer_id'];
            }
            if (!empty($_REQUEST['memo'])) {
                $pp_response['customer_notes'] = $_REQUEST['memo'];
            }
        }

    }
    elseif ($mode == 'return') {
        if ( $order->payment->php_file == 'paypal.php' ) {
            $is_paid = true;
        }
    }
    elseif ($mode == 'cancel') {
        $is_paid = 2;
        $pp_response['order_status'] = 1;
        $pp_response["reason_text"] = 'text_transaction_cancelled';

        if (!empty($_REQUEST['payer_email'])) {
            $pp_response['customer_email'] = $_REQUEST['payer_email'];
        }
        if (!empty($_REQUEST['payer_id'])) {
            $pp_response['client_id'] = $_REQUEST['payer_id'];
        }
        if (!empty($_REQUEST['memo'])) {
            $pp_response['customer_notes'] = $_REQUEST['memo'];
        }
    }

    fn_finish_payment($order, $pp_response, $is_paid);

} else {

    $paypal_account = $processor_data['params']['account'];

    if ($processor_data['params']['mode'] == 'test') {
        $paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
    } else {
        $paypal_url = "https://www.paypal.com/cgi-bin/webscr";
    }

    $paypal_currency = $processor_data['params']['currency'];
    $paypal_item_name = $processor_data['params']['item_name'];
    //Order Total

    $paypal_shipping = $order->shipping_cost;
    $paypal_total = euro($order->total - $paypal_shipping);
    $paypal_shipping = euro($paypal_shipping);
    $paypal_order_id = $processor_data['params']['order_prefix'].$order->id.time();

    $_phone = preg_replace('/[^\d]/', '', $order->addresses['phone_mobile']);
    $_ph_a = $_ph_b = $_ph_c = '';

    $_b_state = $order->addresses['other_billing_address'] ? $order->addresses['billing_county'] : $order->addresses['county'];
    $_b_country = $order->addresses['other_billing_address'] ? $order->addresses['billing_country'] : $order->addresses['country'];

    if ($_b_country == 'US') {
        $_phone = substr($_phone, -10);
        $_ph_a = substr($_phone, 0, 3);
        $_ph_b = substr($_phone, 3, 3);
        $_ph_c = substr($_phone, 6, 4);
    } elseif ($_b_country == 'GB') {
        if ((strlen($_phone) == 11) && in_array(substr($_phone, 0, 2), array('01', '02', '07', '08'))) {
            $_ph_a = '44';
            $_ph_b = substr($_phone, 1);
        } elseif (substr($_phone, 0, 2) == '44') {
            $_ph_a = '44';
            $_ph_b = substr($_phone, 2);
        } else {
            $_ph_a = '44';
            $_ph_b = $_phone;
        }
    } elseif ($_b_country == 'AU') {
        if ((strlen($_phone) == 10) && $_phone[0] == '0') {
            $_ph_a = '61';
            $_ph_b = substr($_phone, 1);
        } elseif (substr($_phone, 0, 2) == '61') {
            $_ph_a = '61';
            $_ph_b = substr($_phone, 2);
        } else {
            $_ph_a = '61';
            $_ph_b = $_phone;
        }
    } else {
        $_ph_a = substr($_phone, 0, 3);
        $_ph_b = substr($_phone, 3);
    }

    if (get_class($order) == 'CreditOrder') {
        $return_url = route('credits_buy.notify', ['return', 'id='.$order->hash/*.'&t='.get_class($order)*/]);
        $cancel_url = route('credits_buy.notify', ['cancel', 'id='.$order->hash/*.'&t='.get_class($order)*/]);
        $notify_url = route('credits_buy.notify', ['notify', 'id='.$order->hash/*.'&t='.get_class($order)*/]);
    }
    elseif (get_class($order) == 'Order') {
        $return_url = route('order.pay.notify', ['return', 'id='.$order->hash]);//fn_url("payment_notification.return?payment=paypal&order_id={$order->id}", AREA, 'current');
        $cancel_url = route('order.pay.notify', ['cancel', 'id='.$order->hash]);//fn_url("payment_notification.cancel?payment=paypal&order_id={$order->id}", AREA, 'current');
        $notify_url = route('order.pay.notify', ['notify', 'id='.$order->hash]);//fn_url("payment_notification.notify?payment=paypal&order_id={$order->id}", AREA, 'current');
    }


    $post_data = array(
        'charset' => 'utf-8',
        'cmd' => '_cart',
        'custom' => $order->id,
        'invoice' => $paypal_order_id,
        'redirect_cmd' => '_xclick',
        'rm' => 2,
        'email' => $order->addresses['email'],
        'first_name' => $order->addresses['other_billing_address'] ? $order->addresses['billing_firstname'] : $order->addresses['firstname'] ,
        'last_name' => $order->addresses['other_billing_address'] ? $order->addresses['billing_surname'] : $order->addresses['surname'],
        'address1' => $order->addresses['other_billing_address'] ? $order->addresses['billing_address'] : $order->addresses['address'],
//        'address2' => $order->addresses['other_billing_address'] ? $order->addresses['billing_address2'] : $order->addresses['address2'],
        'country' => $_b_country,
        'city' => $order->addresses['other_billing_address'] ? $order->addresses['billing_city'] : $order->addresses['city'],
        'state' => $_b_state,
        'zip' => $order->addresses['other_billing_address'] ? $order->addresses['billing_zip'] : $order->addresses['zip'],
        'day_phone_a' => $_ph_a,
        'day_phone_b' => $_ph_b,
        'day_phone_c' => $_ph_c,
        'night_phone_a' => $_ph_a,
        'night_phone_b' => $_ph_b,
        'night_phone_c' => $_ph_c,
        'business' => $paypal_account,
        'item_name' => $paypal_item_name,
        'amount' => $paypal_total,
        'upload' => '1',
        'handling_cart' => $paypal_shipping,
        'currency_code' => $paypal_currency,
        'return' => $return_url,
        'cancel_return' => $cancel_url,
        'notify_url' => $notify_url,
        'bn' => 'ST_ShoppingCart_Upload_US',
    );

    $i = 1;

    if (get_class($order) == 'CreditOrder') {
        $suffix = '_'.($i++);
        $post_data["item_name$suffix"] = htmlspecialchars(strip_tags("Credits Order #{$order->hash}"));
        $post_data["amount$suffix"] = $order->total;
        $post_data["quantity$suffix"] = 1;
    }
    elseif (get_class($order) == 'Order') {
        foreach ($order->items as $v) {
            $suffix = '_'.($i++);
            $post_data["item_name$suffix"] = htmlspecialchars(strip_tags('Custom Case for '.$v->design->device->name));
            $post_data["amount$suffix"] = $v->price;
            $post_data["quantity$suffix"] = $v->quantity;
        }
    }


    fn_create_payment_form($paypal_url, $post_data, 'PayPal server', false);
}