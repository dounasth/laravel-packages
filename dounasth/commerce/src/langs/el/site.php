<?php

return array(
    'home_text' => 'Αρχική',
    'OrderTracking' => 'Παρακολούθηση Παραγγελίας',
    'Contact' => 'Επικοινωνία',
    'Contact-header' => 'Επικοινωνια',
    'Contact-help' => '
        Παρακαλούμε επικοινωνήστε μαζί μας εάν έχετε οποιαδήποτε απορία!
    ',
    'contact-email' => 'Το e-mail σας',
    'contact-message' => 'Το μήνυμά σας',
    'contact-send' => 'Αποστολή',

    'ConnectionError' => array(
        'title' => 'Πρόβλημα σύνδεσης',
        'message' => 'Υπήρξε ένα πρόβλημα στην επικοινωνία με τον server. Παρακαλώ ελέγξτε τη σύνδεσή σας στο Internet και δοκιμάστε ξανά, ή ανανεώστε τη σελίδα.',
        'more' => 'Σε περίπτωση που τα παραπάνω δεν βοηθούν δοκιμάστε ξανά σε λιγάκι ή  <a href="/contactus.html">επικοινωνήστε μαζί μας</a> για να ελέγξουμε το πρόβλημά σας. Ευχαριστούμε.',
    ),
    'buttons' => array(
        'close' =>'Κλείσιμο',
        'edit' => 'Επεξεργασία',
        'edit_data' => 'Επεξεργασία δεδομένων',
        'change_password' => 'Αλλαγή κωδικού',
        'save' => 'Αποθήκευση',
    ),
    'loading' => 'Φορτωση...',
    'choose' => 'Επιλέξτε',
    'other-cases-message' => 'Για οποιαδήποτε άλλη συσκευή που αφορά τα μοντέλα: ALCATEL, HUAWEI,MOTOROLA, BLACKBERRY, SAMSUNG, SONY, LG, NOKIA, HTC, τα οποία δεν υπάρχουν παραπάνω χρησιμοποιήστε τη θήκη αυτή και θα επικοινωνήσουμε μαζί σας για τις λεπτομέρειες της παραγγελίας σας.',
    'or-choose-from-our-suggestions' => 'Ή επιλέξτε από τις προτάσεις μας',
    'footer.titles.info' => 'Πληροφορίες',
    'footer.titles.follow' => 'Ακολουθήστε μας στα Social Media',

    'epelekse-to-montelo-tou-kinitou-sou' => 'Επέλεξε τη συσκευή σου',
    'des-edw-tis-protaseis-mas' => 'Έτοιμα σχέδια για σένα',
    'create-custom' => 'Custom Θήκη',
    'no_designs' => 'Δεν υπάρχουν ακόμα έτοιμα σχέδια για αυτή τη συσκευή',

    'designs-header' => 'Έτοιμα σχέδια',
    'designs-header-for' => 'Έτοιμα σχέδια για',
    'designs-header-brand' => 'Έτοιμα σχέδια για',

    'brands-header' => 'Συσκευές για Custom Θήκες',
    'brands-header-brand' => 'Συσκευές για Custom',

    'cart' => array(
        'minitop' => 'Καλάθι',
        'continue-shopping' => 'Συνεχίστε τις αγορές σας',
        'cart-header' => 'ΚΑΛΑΘΙ - ΤΑΜΕΙΟ',
        'cart-is-empty' => 'Το καλάθι σου είναι άδειο. <br/><br/> Δημιούργησε τη δική σου θήκη. <br/><br/> <a class="btn btn-blue big " href="/brands/apple-cases">Ξεκίνησε τώρα</a> <br/><br/><br/>',

        'quantity' => 'Ποσότητα',
        'quantity_not_available' => 'Η συνολική ποσότητα που ζητάτε δεν είναι διαθέσιμη.',
        'max_available_quantity' => 'Μπορείτε να παραγγείλετε το <b>μέγιστο :max θηκες</b>.',
        'ajust_quantity' => 'Παρακαλούμε αλλάξτε τα αποθέματα αναλόγως.',

        'price' => 'Price',
        'total' => 'Total',
        'remove' => 'Remove',

        'shipping-info' => 'Στοιχεία αποστολής',
        'billing-info' => 'Στοιχεία Χρέωσης',
        'other_billing' => 'Θέλω να εισάγω διαφορετική διεύθυνση χρέωσης',
        'firstname' => 'Όνομα',
        'lastname' => 'Επώνυμο',
        'email' => 'E-mail',
        'address1' => 'Διεύθυνση',
//        'address2' => 'Διεύθυνση (γραμμή 2)',
        'zip' => 'Ταχυδρομικός Κώδικας',
        'city' => 'Πόλη',
        'phone' => 'Τηλέφωνο σταθερό',
        'phone_mobile' => 'Κινητό Τηλέφωνο',
        'county' => 'Νομός',
        'country' => 'Χώρα',
        'company' => 'Όνομα εταιρίας',

        'payments' => 'Τρόπος Πληρωμής',
        'shippings' => 'Τρόπος Αποστολής',

        'total_items' => 'Σύνολο ειδών',
        'total_payment' => 'Έξοδα πληρωμής',
        'total_shipping' => 'Μεταφορικά',
        'total_order' => 'Σύνολο παραγγελίας',

        'complete_order' => 'Ολοκλήρωση Παραγγελίας',

        'validation-messages' => array(
            'firstname' => 'συμπληρωστε το πεδιο',
            'surname' => 'συμπληρωστε το πεδιο',
            'email' => 'εισαγετε το e-mail σας',
            'phone' => 'εισαγετε το τηλεφωνο σας',
            'phone_mobile' => 'εισαγετε το τηλεφωνο σας',
            'phone_mobile2' => 'εισαγετε τουλάχιστον 10 χαρακτήρες',
//            'company_name' => 'συμπληρωστε το πεδιο',
            'address' => 'συμπληρωστε το πεδιο',
//            'address2' => 'συμπληρωστε το πεδιο',
            'zip' => 'εισαγετε τον ΤΚ σας',
            'zip2' => 'εισαγετε τουλάχιστον 5 χαρακτήρες',
            'city' => 'συμπληρωστε το πεδιο',
            'county' => 'συμπληρωστε το πεδιο',
            'country' => 'συμπληρωστε το πεδιο',
            'other_billing_address' => 'ναι η οχι',
            'billing_firstname' => 'συμπληρωστε το πεδιο',
            'billing_surname' => 'συμπληρωστε το πεδιο',
            'billing_address' => 'συμπληρωστε το πεδιο',
            'billing_zip' => 'εισαγετε τον ΤΚ',
            'billing_zip2' => 'εισαγετε τουλάχιστον 5 χαρακτήρες',
            'billing_city' => 'συμπληρωστε το πεδιο',
            'billing_county' => 'συμπληρωστε το πεδιο',
            'billing_country' => 'συμπληρωστε το πεδιο',
//            'billing_address2' => 'συμπληρωστε το πεδιο',
        ),

        'invoice_type' => 'Τύπος παραστaτικού',
        'invoice_type_T' => 'Τιμολόγιο',
        'invoice_type_A' => 'Απόδειξη',
        'invoice_type_AT' => 'Ακυρωτικό Τιμολόγιο',
        'invoice_type_AA' => 'Ακυρωτική Απόδειξη',
        'num_of_invoice_type_T' => 'Αριθμός Τιμολογίου',
        'num_of_invoice_type_A' => 'Αριθμός Απόδειξης',
        'num_of_invoice_type_AT' => 'Αριθμός Ακυρωτικού Τιμολογίου',
        'num_of_invoice_type_AA' => 'Αριθμός Ακυρωτικού Απόδειξης',
        'job_title' => 'Επάγγελμα',
        'job_afm' => 'ΑΦΜ',
        'job_doy' => 'ΔΟΥ',
    ),
    'order' => array(
        'order' => 'Παραγγελία',
        'status' => 'Κατάσταση',
        'pay-order' => 'Πληρωμή παραγγελίας',
        'redirection1' => 'Θα γίνει ανακατεύθυνση για ολοκλήρωση της πληρωμής της παραγγελίας.',
        'redirection2' => 'Παρακαλώ περιμένετε ή',
        'redirection3' => 'πατήστε εδώ',
        'redirection4' => 'αν δεν γίνει αυτόματη ανακατεύθυνση',

        'shipping-info' => 'Στοιχεία αποστολής',
        'billing-info' => 'Στοιχεία Χρέωσης',
        'same-as-shipping' => 'Ίδια με τα στοιχεία αποστολής',
        'firstname' => 'Όνομα',
        'lastname' => 'Επώνυμο',
        'email' => 'E-mail',
        'address1' => 'Διεύθυνση',
//        'address2' => 'Διεύθυνση (γραμμή 2)',
        'zip' => 'Ταχυδρομικός Κώδικας',
        'city' => 'Πόλη',
        'phone' => 'Τηλέφωνο σταθερό',
        'phone_mobile' => 'Κινητό Τηλέφωνο',
        'county' => 'Νομός',
        'country' => 'Χώρα',
        'company' => 'Όνομα εταιρίας',

        'order-items' => 'Είδη Παραγγελίας',
        'title' => 'Όνομα',
        'quantity' => 'Ποσότητα',
        'price' => 'Τιμη',
        'total' => 'Σύνολο',

        'payments' => 'Τρόπος Πληρωμής',
        'shippings' => 'Τρόπος Αποστολής',
        'cost' => 'Κόστος',
        'change-payment' => 'Δοκίμασε πάλι',

        'synopsis' => 'Σύνοψη Παραγγελίας',

        'total_items' => 'Items total',
        'total_payment' => 'Payment charge',
        'total_shipping' => 'Shipping cost',
        'total_order' => 'Order total',

        'invoice_type' => 'Τύπος παραστ.',
        'invoice_type_T' => 'Τιμολόγιο',
        'invoice_type_A' => 'Απόδειξη',
        'invoice_type_AT' => 'Ακυρωτικό Τιμολόγιο',
        'invoice_type_AA' => 'Ακυρωτική Απόδειξη',
        'num_of_invoice_type_T' => 'Αριθμός Τιμολογίου',
        'num_of_invoice_type_A' => 'Αριθμός Απόδειξης',
        'num_of_invoice_type_AT' => 'Αριθμός Ακυρωτικού Τιμολογίου',
        'num_of_invoice_type_AA' => 'Αριθμός Ακυρωτικού Απόδειξης',
        'job_title' => 'Επάγγελμα',
        'job_afm' => 'ΑΦΜ',
        'job_doy' => 'ΔΟΥ',

        'thanx' => array(
            'title' => 'Ευχαριστούμε για την παραγγελία σου!',
            'text' => '
            <br/>
            <p style="text-align: left;">Ευχαριστούμε πολύ για την εμπιστοσύνη σου στο www.domain.gr  και ελπίζουμε να μείνεις 100% ευχαριστημένος με την επιλογή σου.<br/>
            Σε ενημερώνουμε ότι έχουμε λάβει την παραγγελία σου και το αρμόδιο τμήμα έχει ξεκινήσει ήδη την υλοποίηση της.
            </p>
            <h4 style="text-align: left;">Πότε θα παραλάβεις την παραγγελίας σου;</h4>
            <p style="text-align: left;">Την παραγγελία σου θα την παραλάβεις εντός 2-3 εργάσιμων ημερών.<br/>
            Για περισσότερες πληροφορίες σχετικά με  την παραγγελία σου μπορείς να κάνεις κλικ στην σελίδα <a href="/pages/apostoli-proionton.html">Αποστολή προϊόντων</a>.<br/>
            <br/>
            Ευχαριστούμε και πάλι για την προτίμηση σου,<br/>
            Η ομάδα του <a href="https://www.domain.gr">www.domain.gr</a>
            </p>
            ',
            'goback' => 'Συνέχεια αγορών',

            'your-order' => 'Η παραγγελία σου',
            'your-order-is' => 'Ο κωδικός της παραγγελίας σου είναι:',
        ),

        'repay' => array(
            'pending' => 'Η παραγγελία σου εκρεμμεί!',
//            'pending_sub' => 'Επέλεξε νέο τρόπο πληρωμής',
            'pending_text' => '
            <p style="text-align: left;">
            <br/>
            Παρακαλούμε επέλεξε έναν από τους παρακάτω τρόπους πληρωμής για να ολοκληρώσεις την παραγγελία σου
            <br/><br/>
            </p>
            ',
        )
    ),
    'editor' => array(
        'more' => 'περισσότερα...',
        'edit' => 'Επεξεργασία',
        'change' => 'Αλλαγή',
        'finirisma' => 'Φινίρισμα',
        'preview' => 'Προεπισκόπηση',
        'saving' => 'Γίνεται αποθήκευση...',
        'add_to_cart' => 'Αποθήκευση στο καλάθι',
        'upload_info' => 'Πατήστε εδώ για να επιλέξετε μια εικόνα απο τον υπολογιστή ή τη συσκευή σας',
        'image' => 'Φωτογραφία',
        'collage' => 'Κολάζ',
        'without_collage' => 'Χωρίς Κολάζ',
        'text' => 'Κείμενο',
        'edit_text' => 'Επεξεργασία κειμένου',
        'background' => 'Φόντο',
        'clipart' => 'Κλιπάρτ',
        'popular' => 'Δημοφιλή',
        'best_result_info' => '
                <b>Για το καλυτερο δυνατό αποτέλεσμα</b>
                <br/>
                <span style="background: rgb(0, 150, 255);"></span> Τοποθετήστε τις φωτογραφίες μέχρι την μπλε γραμμή
                <br/>
                <span style="background: rgb(255,0,255);"></span> Τοποθετήστε τα κείμενα μέχρι την ροζ γραμμή
                <br/>
                <br/>
                - Μην τοποθετείτε πρόσωπα και γράμματα κοντά στις <br/>
                &nbsp;&nbsp;&nbsp;τρύπες της κάμερας,του ηχείου κτλ<br/>
                - Αποφύγετε εντελώς μονόχρωμες θήκες<br/>
                - Οι θήκες μας ΔΕΝ είναι διάφανες<br/>
                - Επιλέξτε σωστά το μοντέλο σας<br/>
                - Προτιμήστε πιστωτική/χρεωστική κάρτα,τραπεζική<br/>
                &nbsp;&nbsp;&nbsp;κατάθεση ή PayPal ως τρόπο πληρωμής<br/>
                <br/>
                <b>
                    Οι θήκες κατασκευάζονται αποκλειστικά για εσάς<br/>
                    και συνήθως ζητείται η προπληρωμή τους με τους<br/>
                    παραπάνω τρόπους!
                </b>
        ',
    ),

    'home' => array(
        'banners' => array(
            'top' => array(
                'title' => 'Δημιούργησε τη θήκη σου',
                'button' => 'Ξεκίνησε τώρα',
            ),
            'bottom' => array(
                'title' => 'Επέλεξε από τις προτάσεις μας',
                'button' => 'Διάλεξε εδώ',
            ),
        )
    )

);
