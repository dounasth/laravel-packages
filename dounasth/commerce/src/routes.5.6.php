<?php

\Eventy::addFilter('admin.left-menu', function($tree) {
	$tree = $tree + [
		'commerce_menu' => '<li class="header">E-COMMERCE</li>',

		'commerce1' => array(
			'label' => 'Catalog',
			'href' => '#',
			'icon' => 'fa-cogs',
			'submenu' => array(
				'products' => array(
					'label' => 'Products',
					'href' => route('commerce.products.index'),
					'icon' => 'fa-dashboard',
				),
				'categories' => array(
					'label' => 'Categories',
					'href' => route('commerce.categories.index'),
					'icon' => 'fa-dashboard',
				),
			)
		),

		'commerce2' => array(
			'label' => 'Orders',
			'href' => '#',
			'icon' => 'fa-cogs',
			'submenu' => array(
				'orders' => array(
					'label' => 'Orders',
					'href' => route('commerce.orders.index'),
					'icon' => 'fa-dashboard',
				),
				'invoices' => array(
					'label' => 'Invoices',
					'href' => route('commerce.invoices.index'),
					'icon' => 'fa-dashboard',
				),
			)
		),
	];
	return $tree;
}, 30, 1);


\Eventy::addFilter('config.menu', function($tree) {
	$tree = $tree + [
			'commerce' => array(
				'label' => 'Commerce',
				'href' => '#',
				'icon' => 'fa-cogs',
				'submenu' => array(
					'filters' => array(
						'label' => 'Filters',
						'href' => route('commerce.filters.index'),
						'icon' => 'fa-dashboard',
					),
					'order_statuses' => array(
						'label' => 'Order Statuses',
						'href' => route('commerce.order_statuses.index'),
						'icon' => 'fa-dashboard',
					),
					'countries' => array(
						'label' => 'Countries',
						'href' => route('commerce.countries.index'),
						'icon' => 'fa-dashboard',
					),
					'states' => array(
						'label' => 'States',
						'href' => route('commerce.states.index'),
						'icon' => 'fa-dashboard',
					),
					'shippings' => array(
						'label' => 'Shippings',
						'href' => route('commerce.shippings.index'),
						'icon' => 'fa-dashboard',
					),
					'payments' => array(
						'label' => 'Payments',
						'href' => route('commerce.payments.index'),
						'icon' => 'fa-dashboard',
					),
				)
			),
		];
	return $tree;
}, 30, 1);


Event::listen('admin.dashboard.widgets', function() {
	$vars = dashboardCommonBefore();
	$widget = new \Dounasth\Widgets\Base\ViewWidget('day-totals', 'commerce::widgets.header', $vars, 'col-lg-12');
	return $widget;
}, 1000000);

use Dounasth\Commerce\App\Models\Order\OrderTotals;
Event::listen('admin.dashboard.widgets', function() {
	$vars = dashboardCommonBefore();
	$vars['todayOrderTotals'] = OrderTotals::whereIn('order_id', $vars['todayOrderIds'])->get();
	$widget = new \Dounasth\Widgets\Base\ViewWidget('day-totals', 'commerce::widgets.day-totals', $vars, 'col-lg-12');
	return $widget;
}, 1000000);

use Dounasth\Commerce\App\Models\Order\Order;
Event::listen('admin.dashboard.widgets', function() {
	$o = new Order();
	$dbkey = $o->getKeyName();
	$o = Order::orderBy($o->getKeyName(), 'desc')->take(5)->get();
	$orders = [];
	foreach ($o as $row) {
		$k = $row->{$row->getKeyName()};
		$orders[$k] = [
			$row->getKeyName() => $k,
			'created_at|hash' => $row->created_at.'<br/><span class="label label-default">'.$row->hash.'</span>',
			'email|fullname' => $row->getEmail().'<br/>'.$row->getFullName(),
			'payment|shipping' => "<img src='".asset($row->payment->image)."'/>"."<img src='".asset($row->shipping->image)."'/>",
			'total' => euro($row->total),
			'status' => '<span style="color: '.$row->status->color.';">'.$row->status->title.'</span>',
		];
	}
	$widget = new \Dounasth\Widgets\Base\TableWidget(
		'', 'Latest Orders',$orders,
		$dbkey.',status,created_at|hash,email|fullname,payment|shipping,total','col-lg-6 col-xs-6'
	);
	return $widget;
}, 1000000);

Event::listen('admin.dashboard.widgets', function() {
	$vars = dashboardCommonBefore();
	if (!$vars['daterange']) {
		$vars['charts']['orders'] = array();
		foreach (range(10, 0, -1) as $i) {
			$idate = date('Y-m-d', strtotime("-{$i} days"));
			$irow = Order::selectRaw('
                DATE(created_at) as create_date,
                COUNT(created_at) as count,
                SUM(items_cost) as items_cost_sum,
                SUM(items_cost) / COUNT(created_at) AS items_cost_median
            ')
			             ->whereRaw("DATE(created_at) = '{$idate}'")
			             ->groupBy('create_date')
			             ->get()->toArray();
			$irow = reset($irow);
			if (!$irow) {
				$irow = array(
					'create_date' => $idate,
					'count' => 0,
					'items_cost_sum' => 0,
					'items_cost_median' => 0,
				);
			}
			$vars['charts']['orders'][] = $irow;
		}
	}
	else {
		foreach ( getDatesBetween($vars['daterange']) as $idate ) {
			$irow = Order::selectRaw('
                DATE(created_at) as create_date,
                COUNT(created_at) as count,
                SUM(items_cost) as items_cost_sum,
                SUM(items_cost) / COUNT(created_at) AS items_cost_median
            ')
			             ->whereRaw("DATE(created_at) = '{$idate}'")
			             ->groupBy('create_date')
			             ->get()->toArray();
			$irow = reset($irow);
			if (!$irow) {
				$irow = array(
					'create_date' => $idate,
					'count' => 0,
					'items_cost_sum' => 0,
					'items_cost_median' => 0,
				);
			}
			$vars['charts']['orders'][] = $irow;
		}
	}
	$widget = new \Dounasth\Widgets\Base\ViewWidget('order-graph', 'commerce::widgets.order-graph', $vars, 'col-lg-6');
	return $widget;
}, 1000000);



Route::middleware(['web', 'auth', 'auth.admin'])->namespace('Dounasth\Commerce\App\Controllers\Admin')->prefix('admin')->group(function() {

    Route::any('commerce/products/index', 'ProductsController@index')->name('commerce.products.index');
    Route::any('commerce/products/add', 'ProductsController@edit')->name('commerce.products.add');
    Route::any('commerce/products/edit/{id}', 'ProductsController@edit')->name('commerce.products.edit');
    Route::any('commerce/products/save/{id?}', 'ProductsController@save')->name('commerce.products.save');
    Route::any('commerce/products/delete/{id}', 'ProductsController@delete')->name('commerce.products.delete');
    Route::any('commerce/products/restore/{id}', 'ProductsController@restoreTrashed')->name('commerce.products.restore');

    Route::any('commerce/categories/index', 'CategoriesController@index')->name('commerce.categories.index');
    Route::any('commerce/categories/add', 'CategoriesController@edit')->name('commerce.categories.add');
    Route::any('commerce/categories/edit/{id}', 'CategoriesController@edit')->name('commerce.categories.edit');
    Route::any('commerce/categories/save/{id?}', 'CategoriesController@save')->name('commerce.categories.save');
    Route::any('commerce/categories/delete/{id}', 'CategoriesController@delete')->name('commerce.categories.delete');
    Route::any('commerce/categories/restore/{id}', 'CategoriesController@restoreTrashed')->name('commerce.categories.restore');

    Route::any('commerce/categories/save-pos-category', 'CategoriesController@savePositionedNode')->name('commerce.categories.save-pos-category');

    Route::any('commerce/countries/index', 'CountriesController@index')->name('commerce.countries.index');
    Route::any('commerce/countries/add', 'CountriesController@edit')->name('commerce.countries.add');
    Route::any('commerce/countries/edit/{id}', 'CountriesController@edit')->name('commerce.countries.edit');
    Route::any('commerce/countries/save/{id?}', 'CountriesController@save')->name('commerce.countries.save');
    Route::any('commerce/countries/delete/{id}', 'CountriesController@delete')->name('commerce.countries.delete');
    Route::any('commerce/countries/restore/{id}', 'CountriesController@restoreTrashed')->name('commerce.countries.restore');

    Route::any('commerce/states/index', 'StatesController@index')->name('commerce.states.index');
    Route::any('commerce/states/add', 'StatesController@edit')->name('commerce.states.add');
    Route::any('commerce/states/edit/{id}', 'StatesController@edit')->name('commerce.states.edit');
    Route::any('commerce/states/save/{id?}', 'StatesController@save')->name('commerce.states.save');
    Route::any('commerce/states/delete/{id}', 'StatesController@delete')->name('commerce.states.delete');
    Route::any('commerce/states/restore/{id}', 'StatesController@restoreTrashed')->name('commerce.states.restore');

    Route::any('commerce/shippings/index', 'ShippingsController@index')->name('commerce.shippings.index');
    Route::any('commerce/shippings/add', 'ShippingsController@edit')->name('commerce.shippings.add');
    Route::any('commerce/shippings/edit/{id}', 'ShippingsController@edit')->name('commerce.shippings.edit');
    Route::any('commerce/shippings/save/{id?}', 'ShippingsController@save')->name('commerce.shippings.save');
    Route::any('commerce/shippings/delete/{id}', 'ShippingsController@delete')->name('commerce.shippings.delete');
    Route::any('commerce/shippings/restore/{id}', 'ShippingsController@restoreTrashed')->name('commerce.shippings.restore');

    Route::any('commerce/payments/index', 'PaymentsController@index')->name('commerce.payments.index');
    Route::any('commerce/payments/add', 'PaymentsController@edit')->name('commerce.payments.add');
    Route::any('commerce/payments/edit/{id}', 'PaymentsController@edit')->name('commerce.payments.edit');
    Route::any('commerce/payments/save/{id?}', 'PaymentsController@save')->name('commerce.payments.save');
    Route::any('commerce/payments/delete/{id}', 'PaymentsController@delete')->name('commerce.payments.delete');
    Route::any('commerce/payments/restore/{id}', 'PaymentsController@restoreTrashed')->name('commerce.payments.restore');

    Route::any('commerce/filters/index', 'FiltersController@index')->name('commerce.filters.index');
    Route::any('commerce/filters/add', 'FiltersController@edit')->name('commerce.filters.add');
    Route::any('commerce/filters/edit/{id}', 'FiltersController@edit')->name('commerce.filters.edit');
    Route::any('commerce/filters/save/{id?}', 'FiltersController@save')->name('commerce.filters.save');
    Route::any('commerce/filters/delete/{id}', 'FiltersController@delete')->name('commerce.filters.delete');
    Route::any('commerce/filters/restore/{id}', 'FiltersController@restoreTrashed')->name('commerce.filters.restore');

    Route::any('commerce/orders/index', 'OrdersController@index')->name('commerce.orders.index');
    Route::any('commerce/orders/add', 'OrdersController@edit')->name('commerce.orders.add');
    Route::any('commerce/orders/edit/{id}', 'OrdersController@edit')->name('commerce.orders.edit');
    Route::any('commerce/orders/save/{id?}', 'OrdersController@save')->name('commerce.orders.save');
    Route::any('commerce/orders/delete/{id}', 'OrdersController@delete')->name('commerce.orders.delete');
    Route::any('commerce/orders/restore/{id}', 'OrdersController@restoreTrashed')->name('commerce.orders.restore');

	Route::any('commerce/orders/{id}/renotify', 'OrdersController@renotifyForOrder')->name('commerce.order.renotify');
	Route::any('commerce/orders/{id}/previewStatusMail', 'OrdersController@previewStatusMail')->name('commerce.order.previewStatusMail');
	Route::any('commerce/orders/{id}/makeInvoice', 'OrdersController@makeOrderInvoice')->name('commerce.order.make_invoice');
	Route::any('commerce/orders/{id}/invoice', 'OrdersController@invoice')->name('commerce.order.invoice');

    Route::any('commerce/invoices/index', 'InvoicesController@index')->name('commerce.invoices.index');
    Route::any('commerce/invoices/add', 'InvoicesController@edit')->name('commerce.invoices.add');
    Route::any('commerce/invoices/edit/{id}', 'InvoicesController@edit')->name('commerce.invoices.edit');
    Route::any('commerce/invoices/save/{id?}', 'InvoicesController@save')->name('commerce.invoices.save');
    Route::any('commerce/invoices/delete/{id}', 'InvoicesController@delete')->name('commerce.invoices.delete');
    Route::any('commerce/invoices/restore/{id}', 'InvoicesController@restoreTrashed')->name('commerce.invoices.restore');

    Route::any('commerce/order_statuses/index', 'OrderStatusesController@index')->name('commerce.order_statuses.index');
    Route::any('commerce/order_statuses/add', 'OrderStatusesController@edit')->name('commerce.order_statuses.add');
    Route::any('commerce/order_statuses/edit/{id}', 'OrderStatusesController@edit')->name('commerce.order_statuses.edit');
    Route::any('commerce/order_statuses/save/{id?}', 'OrderStatusesController@save')->name('commerce.order_statuses.save');
    Route::any('commerce/order_statuses/delete/{id}', 'OrderStatusesController@delete')->name('commerce.order_statuses.delete');
    Route::any('commerce/order_statuses/restore/{id}', 'OrderStatusesController@restoreTrashed')->name('commerce.order_statuses.restore');
});

Route::middleware(['web'])->namespace('Dounasth\Commerce\App\Controllers\Site')->prefix( config('frontend-site.routes_prefix') )->group(function() {
    Route::get('category/{slug}', 'CategoryController@viewCategory')->name('site.commerce.category.view');
    Route::get('tag/{slugs}', 'CategoryController@viewTags')->name('site.commerce.tag.view');
    Route::get('product/{slug}', 'ProductController@viewProduct')->name('site.commerce.product.view');
    Route::get('product/aff/{slug}', 'ProductController@affiliateRedirect')->name('site.commerce.product.affiliate');

    Route::any('cart/',                             'CartController@cart')->name('site.cart.view');
    Route::any('cart/add/{model}/{id}',              'CartController@addToCart')->name('site.cart.add');
    Route::any('cart/remove/{rowId}',               'CartController@removeFromCart')->name('site.cart.remove');
    Route::any('cart/updateQuantity/{rowId}/{quantity}', 'CartController@updateQuantity')->name('site.cart.updateQuantity');
    Route::any('cart/updateAddresses/',             'CartController@updateAddresses')->name('site.cart.updateAddresses');
    Route::any('cart/updateOrderExtra/',            'CartController@updateOrderExtra')->name('site.cart.updateOrderExtra');
    Route::post('cart/checkout-complete',           'CartController@checkoutComplete')->name('site.cart.checkout.complete');

    Route::any('order/pay',                     'CartController@payOrder')                ->name('order.pay');
    Route::any('order/notify/{mode}',           'CartController@payOrderNotification')    ->name('order.pay.notify');
    Route::any('order/{hash}',                  'CartController@viewOrder')               ->name('order.view');
    Route::any('order/{hash}/updatePayment',    'CartController@updatePayment')           ->name('order.updatePayment');
});


