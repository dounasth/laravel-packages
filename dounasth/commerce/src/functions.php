<?php

use \Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use Dounasth\Commerce\App\Models\Order\Order;

require_once('lib/simplehtmldom_1_5/simple_html_dom.php');

function itemsPerPage($fallback=10) {
    return \Illuminate\Support\Facades\Input::get('items_per_page', Session::get(Route::currentRouteName().'.items_per_page', $fallback));
}
function orderType($default='id') {
    return \Illuminate\Support\Facades\Input::get('orderBy', Session::get(Route::currentRouteName().'.orderBy', $default));
}
function orderDirection($default='desc') {
    return \Illuminate\Support\Facades\Input::get('orderDir', Session::get(Route::currentRouteName().'.orderDir', $default));
}
function otherOrderDirection($default='desc') {
    return orderDirection($default) == 'asc' ? 'desc' : 'asc';
}

function addParamToCurrentUrl($param, $value) {
    $url = Request::fullUrl();
    $url = parse_url($url);
    if ( isset($url['query']) && fn_is_not_empty($url['query'])) {} else { $url['query'] = ''; }
    parse_str($url['query'], $url['query']);
    $url['query'][$param] = $value;
    $url['query'] = http_build_query($url['query']);
    @$url = "{$url['path']}?{$url['query']}";
    return $url;
}
function queryParamsForLinks() {
    parse_str($_SERVER['QUERY_STRING'], $q);
    unset($q['page']);
    unset($q['orderBy']);
    unset($q['orderDir']);
    return $q;
}
function sortButton($field, $label='', $icon='fa-sort-amount'){
    $route_name = Route::currentRouteName();
    $link = route($route_name);    //
    if (orderType() == $field) {
        $class = 'btn-info';
        $icon = $icon.'-'.strtolower(orderDirection());
        $link .= '?orderBy='.$field.'&orderDir='. ( otherOrderDirection() );
    }
    else {
        $class = 'btn-default';
        $icon='fa-sort-amount-'.orderDirection();
        $link .= '?orderBy='.$field.'&orderDir='. ( orderDirection() );
    }
    $link .= '&'.http_build_query(queryParamsForLinks());

    $orderByTootip = trans('generic.orderby') . ' ' . trans("generic.fields.{$route_name}.{$field}") .' '.trans('generic.ordertype.'.orderDirection());
    $button = "<a href='{$link}' class=\"btn btn-xs btn-block {$class}\" data-toggle=\"tooltip\" title=\"{$orderByTootip}\" ><i class=\"fa {$icon}\"></i> {$label}</a>";
    return $button;
}
function sortLabel($field, $label='', $icon='fa-sort-amount'){
    $route_name = Route::currentRouteName();
    $link = route($route_name);    //
    if (orderType() == $field) {
        $icon = $icon.'-'.strtolower(orderDirection());
        $link .= '?orderBy='.$field.'&orderDir='. ( otherOrderDirection() );
    }
    else {
        $icon='fa-sort-amount-'.orderDirection();
        $link .= '?orderBy='.$field.'&orderDir='. ( orderDirection() );
    }
    $link .= '&'.http_build_query(queryParamsForLinks());

    $orderByTootip = trans('generic.orderby') . ' ' . trans("generic.fields.{$route_name}.{$field}") .' '.trans('generic.ordertype.'.orderDirection());
    $button = "<a href='{$link}' data-toggle=\"tooltip\" title=\"{$orderByTootip}\" ><i class=\"fa {$icon}\"></i> {$label}</a>";
    return $button;
}




function euro($price, $with_span_id='') {
//    number_format ( float $number , int $decimals = 0 , string $dec_point = "." , string $thousands_sep = "," )
    if (fn_is_not_empty(trim($with_span_id))) {
        return '<span id="'.$with_span_id.'">'. number($price) . '</span> &euro;' ;
    }
    else return number($price) . '&euro;' ;
}

function withTax($amount) {
    return $amount * (1+TAX_PERCENTAGE);
}
function taxOf($amount) {
    return $amount * TAX_PERCENTAGE;
}
function creditsCost($amount) {
    return $amount * 1;
}

function deTax($amount) {
    return $amount / (1+TAX_PERCENTAGE);
}
function deTaxedAmount($amount) {
    return $amount - deTax($amount);
}

function deTax2($amount, $tax) {
    return $amount / (1+$tax);
}
function deTaxedAmount2($amount, $tax) {
    return $amount - deTax($amount, $tax);
}

function integer($number, $com_delim=',', $th_delim='.') {
    return number_format( intval($number), 0, $com_delim, $th_delim );
}
function number($number, $decimals=2, $com_delim=',', $th_delim='.') {
    return number_format( floatval($number), $decimals, $com_delim, $th_delim );
}
function numberDB($number, $decimals=2, $com_delim='.', $th_delim='') {
    return number_format( floatval($number), $decimals, $com_delim, $th_delim );
}
function theLocale() {
    return \Illuminate\Support\Facades\Input::get('sl', Session::get('current_locale', Config::get('app.locale')));
}







function dashboardCommonBefore($vars = array()) {
	$vars['today'] = date('Y-m-d');
	$vars['one_month_ago'] = date('Y-m-d', strtotime('-30 month'));
	$daterange = Input::get('daterange', false);

	if ($daterange) {
		$daterange = explode('-', $daterange);
		if ($daterange[0] && $daterange[1]) {
			$vars['daterange']['from'] = dateConvert($daterange[0]);
			$vars['daterange']['to'] = dateConvert($daterange[1]);
		}
		else $vars['daterange'] = false;
	}
	else $vars['daterange'] = false;

	if (!$vars['daterange']) {
		$todayOrderIds = Order::whereRaw("DATE(created_at) = '{$vars['today']}'")->pluck('id');
	}
	else {
		$todayOrderIds = Order::whereRaw("DATE(created_at) >= '{$vars['daterange']['from']}'")->whereRaw("DATE(created_at) <= '{$vars['daterange']['to']}'")->pluck('id');
	}
	$vars['todayOrderIds'] = $todayOrderIds->toArray();

	return $vars;
}




function notifyForOrder($order, $notify_customer = true, $notify_admin = true)
{
	//  Replace placeholders
	$order->status->mail = str_ireplace('[order_url]', route('order.view', [$order->hash]), $order->status->mail);
	$order->status->mail = str_ireplace('[domain]', Config::get('app.url'), $order->status->mail);
	$order->status->mail = str_ireplace('[site]', Config::get('app.url'), $order->status->mail);

	//  Send to customer
	if ($notify_customer) {
		Mail::send('commerce::emails.order', array('order' => $order), function ($message) use ($order) {
			$message->to($order->addresses['email'])->subject('Εξέλιξη παραγγελίας #' . $order->hash);
		});
		if ($order->invoice()) {
			Mail::send('commerce::emails.invoice', array('order' => $order), function ($message) use ($order) {
				$message->to($order->addresses['email'])->subject('Παραστατικό παραγγελίας #' . $order->hash);
				foreach($order->invoice()->irs_files as $irs_file) {
					$message->attach($irs_file);
				}
			});
		}
	}

	//  Send to admin
	if ($notify_admin) {
		/*Mail::send('commerce::emails.order', array('order' => $order), function ($message) use ($order) {
			$message->to(Config::get('mail.from.address'))->subject('Εξέλιξη παραγγελίας #' . $order->hash);
		});
		Mail::send('commerce::emails.invoice', array('order' => $order), function ($message) use ($order) {
			$message->to(Config::get('mail.from.address'))->subject('Παραστατικό παραγγελίας #' . $order->hash);
			foreach($order->invoice()->irs_files as $irs_file) {
				$message->attach($irs_file);
			}
		});*/
		Mail::send('commerce::emails.order_admin', array('order' => $order), function ($message) use ($order) {
			$message->to(Config::get('mail.from.address')/*, Config::get('mail.from.name')*/)
			        ->subject('Νέα παραγγελία #' . $order->hash);
			foreach($order->invoice()->irs_files as $irs_file) {
				$message->attach($irs_file);
			}
		});
	}
}