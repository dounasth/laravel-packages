<?php

namespace Dounasth\Commerce\App\Controllers\Admin;

use Dounasth\Backend\App\Models\Meta;
use Dounasth\Commerce\App\Models\Filter;
use Dounasth\Crud\App\Controllers\AdminCrudController;

class FiltersController extends AdminCrudController
{
	public $model = Filter::class;
	public $title = 'Filters';
	public $subtitle = '';
	public $crudName = 'filters';
    public $route = 'commerce.filters.index';
    public $routesPrefix = 'commerce.filters.';

    public function init()
    {
        parent::init();
        $this->data['fields']['list'] = [
            'id' => [ 'title' => 'id', 'value' => 'id', 'sorter'=>'id' ],
            'title' => [ 'title' => 'title', 'value' => 'title', 'sorter'=>'title' ],
            'status' => columntype_status(),
        ];
        $this->data['fields']['update'] = [
			'meta_id' => [ 'title' => 'meta_id', 'name' => 'meta_id', 'type' => 'text', 'value' => 'meta_id' ],
			'meta_id' => [
				'title' => 'meta_id', 'name' => 'meta_id', 'value' => 'meta_id',
				'type' => 'select_one_from_array',
				'values' => Meta::whereIn('name', array_keys(\Config::get('commerce.product-meta')))
				                ->get()->pluck('name', 'id')
			],
			'title' => [ 'title' => 'title', 'name' => 'title', 'type' => 'text', 'value' => 'title' ],
			'slug' => [ 'title' => 'slug', 'name' => 'slug', 'type' => 'text', 'value' => 'slug' ],
			'show_on_canonical' => [ 'title' => 'show_on_canonical', 'name' => 'show_on_canonical', 'type' => 'text', 'value' => 'show_on_canonical' ],
			'indexing' => [ 'title' => 'indexing', 'name' => 'indexing', 'type' => 'text', 'value' => 'indexing' ],
			'follow' => [ 'title' => 'follow', 'name' => 'follow', 'type' => 'text', 'value' => 'follow' ],
            'status' => fieldtype_status(),
        ];
    }

}
