<?php

namespace Dounasth\Commerce\App\Controllers\Admin;

use Dounasth\Backend\App\Helpers\AlertMessage;
use Dounasth\Backend\App\Models\Seo;
use Dounasth\Commerce\App\Models\Product\Product;
use Dounasth\Commerce\App\Models\Product\ProductData;
use Dounasth\Crud\App\Controllers\AdminCrudController;
use Illuminate\Support\Facades\Input;

class ProductsController extends AdminCrudController
{
    public $model = Product::class;
    public $title = 'Products';
    public $subtitle = 'add, edit, search your products from this page';
    public $crudName = 'products';
    public $route = 'commerce.products.index';
    public $routesPrefix = 'commerce.products.';
//    protected $with = ['prices'];

    public $view_add = 'commerce::admin.products.update';
    public $view_edit = 'commerce::admin.products.update';

    public function joins($query) {
//        $query->leftJoin('cart_product_prices', 'cart_products.id', '=', 'cart_product_prices.product_id');
        return $query;
    }

    public function init()
    {
        $this->data['row_buttons'][] = [
            'icon' => 'fa fa-external-link', 'class' => 'btn-default', 'route' => function ($routesPrefix, Product $row) {
                return $row->route();
            }
        ];
        $this->data['fields']['list'] = [
            'id' => [ 'title' => 'id', 'value' => 'id', 'sorter'=>'cart_products.id' ],
            'image' => [ 'title' => 'image', 'value' => function(Product $row) {
                return "<img src='{$row->mainPhoto()->photon(40)}' />";
            } ],
            'title' => [ 'title' => 'title', 'value' => function(Product $row) {
                return "
                {$row->title}
                <p class='text-sm text-muted'>
                    {$row->slug}
                </p>
                ";
            }, 'sorter'=>'title' ],
            'taxonomies' => [ 'title' => 'taxonomies', 'value' => function(Product $row) {
                return "
				{$row->mainCategory()->path()}
                <p class='text-sm text-muted'>
                    {$row->taglist}
                </p>
                ";
            }, 'sorter'=>'title' ],
            'price' => [ 'title' => 'price', 'value' => function(Product $row) {
                return $row->prices->price;
            }],
            'list_price' => [ 'title' => 'list_price', 'value' => function(Product $row) {
                return $row->prices->list_price;
            }],
//            'slug' => [ 'title' => 'slug', 'value' => 'slug', 'sorter'=>'slug' ],
            'sku' => [ 'title' => 'sku', 'value' => 'sku', 'sorter'=>'sku' ],
            'status' => columntype_status(),
        ];
//        $this->data['fields']['update'] = [
//            'id' => [ 'title' => 'id', 'type' => 'text', 'name' => 'id', 'value' => 'id'],
//            'title' => [ 'title' => 'title', 'type' => 'text' , 'name' => 'title', 'value' => 'title'],
//            'sku' => [ 'title' => 'sku', 'type' => 'text' , 'name' => 'sku', 'value' => 'sku'],
//        ];
    }

    public function save($id=0) {
        $data = ProductData::from(Input::get('product', []));

        if (Input::get('saveNew', 0)) {
            $product = new Product();
            $data->slug = '';
        }
        else {
            $product = Product::find($id);
            if (!$product) {
                $product = new Product();
            }
        }

        $product->saveFromData($data);

        $meta = Input::get('meta', []);
        $product->saveMeta($meta);

        $seo = Input::get('seo', []);
        if ($product->seo) {
            $product->seo->fill($seo)->save();
        }
        else {
            $seo['seoble_id'] = $product->id;
            $seo['seoble_type'] = $this->model;
            $seo = Seo::create($seo);
            $product->seo()->save($seo);
        }

        return redirect()->route('commerce.products.edit', [$product->id])->withMessage( AlertMessage::success('Product saved') );
    }

}
