<?php

namespace Dounasth\Commerce\App\Controllers\Admin;

use Dounasth\Commerce\App\Models\Order\Order;
use Dounasth\Crud\App\Controllers\AdminCrudController;

class OrdersController extends AdminCrudController
{
    public $model = Order::class;
	public $title = 'Orders';
	public $subtitle = '';
    public $crudName = 'orders';
    public $route = 'commerce.orders.index';
    public $routesPrefix = 'commerce.orders.';

    public $view_add = 'commerce::admin.orders.update';
    public $view_edit = 'commerce::admin.orders.update';

    public function init()
    {
        parent::init();
        $this->data['fields']['list'] = [
            'id' => [ 'title' => 'id', 'value' => 'id', 'sorter'=>'id' ],
//            'hash' => [ 'title' => 'hash', 'value' => 'hash' ],
            'created_at' => [ 'title' => 'hash / created_at', 'value' => function(Order $row){
        	    $url = route("commerce.orders.edit", [$row->id]);
                return "
                    <a target='_blank' href='{$url}'><i class='glyphicon glyphicon-eye-open'></i></a>
                    <a href='{$url}'>{$row->hash}</a>
                    <br/>
                    <minitext>{$row->created_at}</minitext>
                ";
            }, 'sorter'=>'created_at' ],
            'email' => [ 'title' => 'email / fullname', 'value' => function(Order $row){
                return "
                    <minitext>
                    <a href='mailto:{$row->getEmail()}'>
                        <i class='glyphicon glyphicon-envelope'></i>
                        {$row->getEmail()}
                    </a>
                    </minitext>
                    <p>{$row->getFullName()}</p>
                ";
            }, ],
            'payship' => [ 'title' => 'payment / shipping', 'value' => function(Order $row){
                return "
                    <minitext>
                        {$row->payment->name}<br/>
                        {$row->shipping->name}
                    </minitext>
                ";
            }, ],
            'items_cost' => [ 'title' => 'items_cost', 'value' => function(Order $row){
                return euro($row->items_cost)."<br/>
                    <p class='text-danger'>(<b>{$row->items->sum('quantity')}</b>)</p>";
            }, ],
            'shipping_cost' => [ 'title' => 'shipping_cost', 'value' => function(Order $row){
                return euro($row->shipping_cost);
            }, 'sorter'=>'shipping_cost' ],
            'payment_cost' => [ 'title' => 'payment_cost', 'value' => function(Order $row){
                return euro($row->payment_cost);
            }, 'sorter'=>'payment_cost' ],
            'total' => [ 'title' => 'total', 'value' => function(Order $row){
                return euro($row->total);
            }, 'sorter'=>'total' ],
            'status' => [ 'title' => 'status', 'value' => function(Order $row){
                //($row->status) ? $row->status->title : '--no status--';
                return view('commerce::admin.orders.order_statuses', [
                    'name'=>"status_ids[{$row->id}]", 'selected_status'=>$row->status, 'no_select2'=>true, 'no_title'=>true
                ]);
            }, 'sorter'=>'order_status' ],
        ];
//        $this->data['fields']['update'] = [
//            'id' => [ 'title' => 'id', 'name' => 'id', 'type' => 'text', 'value' => 'id' ],
//            'title' => [ 'title' => 'title', 'name' => 'title', 'type' => 'text', 'value' => 'title' ],
//            'mail' => [ 'title' => 'mail', 'name' => 'mail', 'type' => 'textarea', 'value' => 'mail' ],
//            'color' => [ 'title' => 'color', 'name' => 'color', 'type' => 'color', 'value' => 'color' ],
//            'valid_order' => [ 'title' => 'valid_order', 'name' => 'valid_order', 'type' => 'text', 'value' => 'valid_order' ],
//        ];

    }

	public function makeOrderInvoice($id) {
		$invoice = Order::makeInvoiceOf($id);
		return redirect()->back();
	}

	protected function invoice($order_id)
	{
		$order = Order::findOrFail($order_id);
		if ($order->invoice()->id) {
			$order->status->mail = str_ireplace('[order_url]', route('order.view', [$order->hash]), $order->status->mail);
			$order->status->mail = str_ireplace('[domain]', config('app.url'), $order->status->mail);
			return view()->make('commerce::emails.invoice', array('invoice' => $order->invoice()));
		}
		else die('');
	}

	protected function previewStatusMail($order_id)
	{
		$order = Order::findOrFail($order_id);
		$order->status->mail = str_ireplace('[order_url]', route('order.view', [$order->hash]), $order->status->mail);
		$order->status->mail = str_ireplace('[domain]', config('app.url'), $order->status->mail);
		return view()->make('commerce::emails.order', array('order' => $order));
	}

	public function renotifyForOrder($order_id)
	{
		$order = Order::findOrFail($order_id);
		notifyForOrder($order, false, false);
		return redirect()->back();
	}

}
