<?php

namespace Dounasth\Commerce\App\Controllers\Admin;

use Dounasth\Commerce\App\Models\Payment;
use Dounasth\Crud\App\Controllers\AdminCrudController;

class PaymentsController extends AdminCrudController
{
    public $model = Payment::class;
    public $title = 'Payments';
    public $subtitle = '';
    public $crudName = 'payments';
    public $route = 'commerce.payments.index';
    public $routesPrefix = 'commerce.payments.';

    public $view_add = 'commerce::admin.payments.update';
    public $view_edit = 'commerce::admin.payments.update';

    public function init()
    {
        parent::init();
        $this->data['fields']['list'] = [
            'id' => [ 'title' => 'id', 'value' => 'id', 'sorter'=>'id' ],
            'name' => [ 'title' => 'name', 'value' => 'name', 'sorter'=>'name' ],
            'cost' => [ 'title' => 'cost', 'value' => 'cost', 'sorter'=>'cost' ],
            'status' => columntype_status(),
        ];

        $this->data['fields']['update'] = [
            'name' => [ 'title' => 'name', 'name' => 'name', 'type' => 'text',  'value' => 'name', ],
            'php_file' => [ 'title' => 'php_file', 'name' => 'php_file', 'type' => 'text',  'value' => 'php_file', ],
            'admin_file' => [ 'title' => 'admin_file', 'name' => 'admin_file', 'type' => 'text',  'value' => 'admin_file', ],
            'cost' => [ 'title' => 'cost', 'name' => 'cost', 'type' => 'text',  'value' => 'cost', ],
            'site_costs' => [ 'title' => 'site_costs', 'name' => 'site_costs', 'type' => 'text',  'value' => 'site_costs', ],
            'image' => [ 'title' => 'image', 'name' => 'image', 'type' => 'text',  'value' => 'image', ],
            'position' => [ 'title' => 'position', 'name' => 'position', 'type' => 'text',  'value' => 'position', ],
            'status' => fieldtype_status(),
        ];

    }

}
