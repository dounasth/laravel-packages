<?php

namespace Dounasth\Commerce\App\Controllers\Admin;

use Dounasth\Commerce\App\Models\Countries;
use Dounasth\Crud\App\Controllers\AdminCrudController;

class CountriesController extends AdminCrudController
{
    public $model = Countries::class;
    public $title = 'Countries';
    public $subtitle = '';
    public $crudName = 'countries';
    public $route = 'commerce.countries.index';
    public $routesPrefix = 'commerce.countries.';

    public function init()
    {
        parent::init();
        $this->data['fields']['list'] = [
            'id' => ['title' => 'id', 'value' => 'id', 'sorter' => 'id'],
            'code' => ['title' => 'code', 'value' => 'code', 'sorter' => 'code'],
            'name' => ['title' => 'name', 'value' => 'name', 'sorter' => 'name'],
        ];
        $this->data['fields']['update'] = [
            'code' => ['title' => 'code', 'name' => 'code', 'type' => 'text', 'value' => 'code', 'sorter' => 'code'],
            'name' => ['title' => 'name', 'name' => 'name', 'type' => 'text', 'value' => 'name', 'sorter' => 'name'],
        ];
    }

}
