<?php

namespace Dounasth\Commerce\App\Controllers\Admin;

use Dounasth\Commerce\App\Models\Countries;
use Dounasth\Commerce\App\Models\States;
use Dounasth\Crud\App\Controllers\AdminCrudController;

class StatesController extends AdminCrudController
{
    public $model = States::class;
    public $title = 'States';
    public $subtitle = '';
    public $crudName = 'states';
    public $route = 'commerce.states.index';
    public $routesPrefix = 'commerce.states.';

    public function init()
    {
        parent::init();
        $this->data['fields']['list'] = [
            'id' => [ 'title' => 'id', 'value' => 'id', 'sorter'=>'id' ],
            'code' => [ 'title' => 'code', 'value' => 'code', 'sorter'=>'code' ],
            'state' => [ 'title' => 'state', 'value' => 'state', 'sorter'=>'state' ],
            'country_code' => [ 'title' => 'country_code', 'value' => function(States $row){
                return $row->country->name;
            }, 'sorter'=>'country_code' ],
            'status' => columntype_status(),
        ];

        $this->data['fields']['update'] = [
            'code' => [ 'title' => 'code', 'name' => 'code', 'type' => 'text', 'value' => 'code'],
            'state' => [ 'title' => 'state', 'name' => 'state', 'type' => 'text', 'value' => 'state'],
            'country_code' => [
                'title' => 'country_code', 'name' => 'country_code', 'value' => 'country_code',
                'type' => 'select2_one_from_relation', 'relation' => 'country', 'relation_texts' => 'name',
            ],
            'status' => fieldtype_status(),
        ];

    }

}
