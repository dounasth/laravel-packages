<?php

namespace Dounasth\Commerce\App\Controllers\Admin;

use Dounasth\Commerce\App\Models\Category;
use Dounasth\Commerce\App\Models\Price;
use Dounasth\Crud\App\Controllers\AdminCrudController;
use Dounasth\Crud\App\Controllers\BaseCrudController;
use Dounasth\Crud\Traits\CrudDeleteTrait;
use Dounasth\Crud\Traits\CrudEditTrait;
use Dounasth\Backend\App\Helpers\AlertMessage;
use Dounasth\Backend\App\Models\Seo;
use Illuminate\Support\Facades\Input;

class CategoriesController extends AdminCrudController
{
    public $model = Category::class;
    public $title = 'Categories';
    public $subtitle = 'add, edit, search, rearrange from this page';
    public $crudName = 'categories';
    public $route = 'commerce.categories.index';
    public $routesPrefix = 'commerce.categories.';
    public $isNested = true;

    public $view_add = 'commerce::admin.categories.update';
    public $view_edit = 'commerce::admin.categories.update';

    public function init()
    {
//        foreach (Category::all() as $category) {
//            $category->save();
//        }
//        exit;

        $this->data['row_buttons'][] = [
            'icon'=>'fa fa-external-link', 'class'=>'btn-default', 'route'=> function($routesPrefix, $row){  return route('site.commerce.category.view', [$row->slug]); }
        ];
        $this->data['fields']['list'] = [
            'title' => [
                'title' => 'title',
                'field' => 'title',
                'value'=>'title',
                'sorter'=>'title',
            ],
            'slug' => [
                'title' => 'slug',
                'field' => 'slug',
                'value'=>'slug',
                'sorter'=>'slug',
            ],
            'status' => columntype_status(),
        ];
    }

    public function save($id=0) {

        if (Input::get('saveNew', 0)) {
            $category = Category::create(Input::get('category', []));
        }
        else {
            $category = Category::findOrNew($id);
            $category->fill(Input::get('category', []));
            $category->save();
        }

        $seo = Input::get('seo', []);
        if ($category->seo) {
            $category->seo->fill($seo)->save();
        }
        else {
            $seo['seoble_id'] = $category->id;
            $seo['seoble_type'] = $this->model;
            $seo = Seo::create($seo);
            $category->seo()->save($seo);
        }

        if (Input::ajax()) {
            return view()->make('backend::parts.messages')->withMessages( AlertMessage::success('Category saved') );
        }
        else return redirect()->route('commerce.categories.edit', [$category->id])->withMessage( AlertMessage::success('Category saved') );
    }
}
