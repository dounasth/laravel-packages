<?php

namespace Dounasth\Commerce\App\Controllers\Admin;

use Dounasth\Commerce\App\Models\Shipping;
use Dounasth\Crud\App\Controllers\AdminCrudController;

class ShippingsController extends AdminCrudController
{
    public $model = Shipping::class;
    public $title = 'Shippings';
    public $subtitle = '';
    public $crudName = 'shippings';
    public $route = 'commerce.shippings.index';
    public $routesPrefix = 'commerce.shippings.';

//    public $view_add = 'commerce::admin.shippings.update';
//    public $view_edit = 'commerce::admin.shippings.update';

    public function init()
    {
        parent::init();
        $this->data['fields']['list'] = [
            'id' => [ 'title' => 'id', 'value' => 'id', 'sorter'=>'id' ],
            'name' => [ 'title' => 'name', 'value' => 'name', 'sorter'=>'name' ],
            'cost' => [ 'title' => 'cost', 'value' => 'cost', 'sorter'=>'cost' ],
            'status' => columntype_status(),
        ];
        $this->data['fields']['update'] = [
//            'hash' => [ 'title' => 'hash', 'name' => 'hash', 'type' => 'text',  'value' => 'hash', ],
            'name' => [ 'title' => 'name', 'name' => 'name', 'type' => 'text',  'value' => 'name', ],
            'estimated_time' => [ 'title' => 'estimated_time', 'name' => 'estimated_time', 'type' => 'text',  'value' => 'estimated_time', ],
            'cost' => [ 'title' => 'cost', 'name' => 'cost', 'type' => 'text',  'value' => 'cost', ],
            'site_costs' => [ 'title' => 'site_costs', 'name' => 'site_costs', 'type' => 'text',  'value' => 'site_costs', ],
            'image' => [ 'title' => 'image', 'name' => 'image', 'type' => 'text',  'value' => 'image', ],
            'position' => [ 'title' => 'position', 'name' => 'position', 'type' => 'text',  'value' => 'position', ],
            'status' => fieldtype_status(),
        ];
    }

}
