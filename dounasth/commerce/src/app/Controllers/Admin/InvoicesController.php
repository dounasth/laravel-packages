<?php

namespace Dounasth\Commerce\App\Controllers\Admin;

use Dounasth\Commerce\App\Models\Order\Invoice;
use Dounasth\Commerce\App\Models\Order\Order;
use Dounasth\Crud\App\Controllers\AdminCrudController;

class InvoicesController extends AdminCrudController
{
    public $model = Invoice::class;
	public $title = 'Invoices';
	public $subtitle = '';
    public $crudName = 'invoices';
    public $route = 'commerce.invoices.index';
    public $routesPrefix = 'commerce.invoices.';

    public $view_add = 'commerce::admin.invoices.update';
    public $view_edit = 'commerce::admin.invoices.update';

    public function init()
    {
        parent::init();
        $this->data['fields']['list'] = [
            'id' => [ 'title' => 'id', 'value' => 'id', 'sorter'=>'id' ],
            'created_at' => [ 'title' => 'created_at', 'value' => 'created_at', 'sorter'=>'created_at' ],
            'invoice_num' => [ 'title' => 'invoice_num', 'value' => function(Invoice $invoice){
	            return $invoice->invoice_type.$invoice->invoice_num;
            }, 'sorter'=>'invoice_num' ],
            'cancels_num' => [ 'title' => 'cancels_num', 'value' => function(Invoice $invoice){
	            return $invoice->cancels_type.$invoice->cancels_num;
            }, 'sorter'=>'cancels_num' ],
            'irs_hash' => [ 'title' => 'irs_hash', 'value' => 'irs_hash', 'sorter'=>'irs_hash' ],
            'order_date' => [ 'title' => 'order_date', 'value' => function(Invoice $invoice){
	            return $invoice->order_data->created_at;
            }, 'sorter'=>'order_date' ],

        ];
//        $this->data['fields']['update'] = [
//            'id' => [ 'title' => 'id', 'name' => 'id', 'type' => 'text', 'value' => 'id' ],
//            'title' => [ 'title' => 'title', 'name' => 'title', 'type' => 'text', 'value' => 'title' ],
//            'mail' => [ 'title' => 'mail', 'name' => 'mail', 'type' => 'textarea', 'value' => 'mail' ],
//            'color' => [ 'title' => 'color', 'name' => 'color', 'type' => 'color', 'value' => 'color' ],
//            'valid_invoice' => [ 'title' => 'valid_invoice', 'name' => 'valid_invoice', 'type' => 'text', 'value' => 'valid_invoice' ],
//        ];

    }

}
