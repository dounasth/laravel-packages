<?php

namespace Dounasth\Commerce\App\Controllers\Admin;

use Dounasth\Commerce\App\Models\Order\OrderStatus;
use Dounasth\Crud\App\Controllers\AdminCrudController;

class OrderStatusesController extends AdminCrudController
{
    public $model = OrderStatus::class;
	public $title = 'Order Statuses';
	public $subtitle = '';
    public $crudName = 'orders_statuses';
    public $route = 'commerce.order_statuses.index';
    public $routesPrefix = 'commerce.order_statuses.';

    public function init()
    {
        parent::init();
        $this->data['fields']['list'] = [
            'id' => [ 'title' => 'id', 'value' => 'id', 'sorter'=>'id' ],
            'title' => [ 'title' => 'title', 'value' => 'title', 'sorter'=>'title' ],
            'color' => [ 'title' => 'color', 'value' => function(OrderStatus $row) {
                return "<input type='color' disabled value='$row->color'/> {$row->color}";
            }, 'sorter'=>'color' ],
            'valid_order' => [
                'title' => 'valid_order', 'value' => function($row) {
                    if ($row->valid_order == 1) { return "<i class='fa fa-check text-success'></i>"; }
                    elseif ($row->valid_order == 0) { return "<i class='fa fa-ban text-danger'></i>"; }
                    else return $row->valid_order;
                }, 'sorter'=>'valid_order'
            ]
        ];
        $this->data['fields']['update'] = [
            'title' => [ 'title' => 'title', 'name' => 'title', 'type' => 'text', 'value' => 'title' ],
            'mail' => [ 'title' => 'mail', 'name' => 'mail', 'type' => 'textarea', 'value' => 'mail' ],
            'color' => [ 'title' => 'color', 'name' => 'color', 'type' => 'color', 'value' => 'color' ],
            'valid_order' => [ 'title' => 'valid_order', 'name' => 'valid_order', 'type' => 'text', 'value' => 'valid_order' ],
        ];

    }

}
