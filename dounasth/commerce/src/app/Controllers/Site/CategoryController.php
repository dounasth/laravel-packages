<?php
namespace Dounasth\Commerce\App\Controllers\Site;

use Conner\Tagging\Model\Tagged;
use Dounasth\Commerce\App\Models\Category;
use Dounasth\Commerce\App\Models\Filter;
use Dounasth\Commerce\App\Models\Product\Product;
use Dounasth\Commerce\App\Models\Product\ProductMeta;
use Dounasth\Frontend\App\Http\Controllers\FrontendBaseController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;

class CategoryController extends FrontendBaseController
{

    public function viewProduct($slug) {
        $product = Product::enabled()->whereSlug($slug)->first();
        if ($product) {
            return view('commerce::site.product')->withProduct( $product );
        }
        else return redirect()->route('home', [], 301);
    }

    public function affiliateRedirect($slug) {
        $product = Product::enabled()->whereSlug($slug)->first();
        if ($product) {
            return redirect()->to($product->getAffiliateUrl());
        }
        else return redirect()->route('home', [], 301);
    }

    public function viewCategory($slug, $tags=false) {
        set_time_limit(1800);
        ini_set('memory_limit', '1024M');
        $category = Category::whereSlug($slug)->withDepth()->with('seo')//->remember(Config::get('commerce::general.cache.category'))
            ->first();

        if ($category) {

            $allcats = [$category->id];
            $allcats = array_merge($allcats, $category->descendants()/*->remember(Config::get('commerce::general.cache.category'))*/->pluck('id')->toArray());

            $product_ids_q[] = 'select product_id from cart_products_categories where category_id in ('.implode(',', $allcats).') AND product_id IN (SELECT id FROM cart_products WHERE status = \'A\')';

            $filters = Filter::current2();
            $withTags = array_filter(explode(',', Input::get('with', $tags)));
            $tags = [];

            $products = Product::select('cart_products.*')->with('seo', 'photos', 'affiliateUrl', 'prices', 'metas');

            if ($filters) {
                $product_ids_q = array_merge($product_ids_q, ProductMeta::productsWithMeta($filters));
            }

            if ($withTags) {
                $tags_pids = Tagged::getProductIdsQueryForTags($withTags);
                if ($tags_pids) {
                    $product_ids_q = array_merge($product_ids_q, $tags_pids);
                }
                $products->leftJoin('taggable_taggables', 'cart_products.id', '=', 'taggable_id')->whereIn('tag_slug', $withTags);
            }

            foreach ($product_ids_q as $q) {
                $products->whereRaw("cart_products.id IN ($q)");
            }

            if (Input::get('orderBy', '') == 'price') {
                $products = $products
                    ->join('cart_product_prices', 'cart_products.id', '=', 'cart_product_prices.product_id')
                    ->orderBy('price', Input::get('orderType', 'asc'));
            }

            $fckey = md5(implode('-', $product_ids_q));
            $filters = Cache::remember('filters-'.$category->id.'-'.$fckey, 3600*24, function() use ($product_ids_q) {
                return Filter::forBlock($product_ids_q);
            });

//            $tckey = md5(implode('-', $withTags));
            $tckey = md5(URL::full());
            $tags = Cache::remember('tags-'.$category->id.'-'.$tckey, 3600*24, function() use ($product_ids_q, $withTags) {
                $tags = [];//   Tagged::tagsForProductsAndSlugs($product_ids_q, $withTags);
                //  remove tags that will not filter products and will create duplicates
                if ($tags) {
//                $c = $products->count();
//                $tags->filter(function($tag) use ($c) { return ($c != $tag->count); });
                }
                return $tags;
            });

            $products = $products->orderBy('cart_products.id', 'desc')//->with('seo', 'photos', 'affiliateUrl', 'prices', 'metas', 'imported', 'imported.merchant')
            //->remember(Config::get('commerce::general.cache.category'))
            ->paginate(24);

//            if (Debugbar::isEnabled()) {
//                niceprintr($product_ids_q);
//                return '1';
//            }

            if ($this->isRss()) {
                $data = array();
                foreach ($products as $product) {
                    $description = '
                    <a href="'.$product->route().'">
                    <img src="http:'.$product->mainPhoto()->photon(260).'" alt="'.$product->title.'">
                    </a>';
                    $description .= ($product->description) ? '<br/><br/>'.$product->descriptions->short : '';
                    $data[] = array(
                        'title' => htmlspecialchars($product->title),
                        'description' => $description,
                        'link' => $product->route(),
                        'image' => array(
                            'url' => 'http:'.$product->mainPhoto()->photon(260),
                            'length' => 100,
                            'type' => 'image/jpeg',
                        )
                    );
                }
                return $this->rss($data);
            }
            else return view('commerce::site.category')
                ->withCategory( $category )
                ->withProducts( $products )
                ->withTags( $tags )
                ->withFilters( $filters )
                ->with('product_ids', $product_ids_q );
        }
        else return redirect()->route('home', [], 301);
    }

    public function viewTags($slugs) {
        set_time_limit(1800);
        ini_set('memory_limit', '1024M');
        $slugs = explode(',', $slugs);
        list($tags, $tagObjects, $product_ids) = $this->getProductIdsForTags($slugs);

        if (fn_is_not_empty($tags)) {
            $enabled_ids = Product::enabled()->pluck('id');
            $product_ids = array_intersect($product_ids, $enabled_ids);
//            $products = Product::whereIn('id', $product_ids);
//            $products = $products->paginate(24);
            $products = $this->paginate($product_ids, 24);

            if ($products->count() == 0 && count($slugs) > 1) {
                array_pop($slugs);
                return redirect()->route('site.cart.tag.view', [implode(',', $slugs)], 301);
            }

            $mtags = Cache::remember("tagged-".implode(',', $slugs), Config::get('commerce::general.cache.category'), function() use ($products, $product_ids, $slugs) {
                $mtags = Tagged::tagsForProductsAndSlugs($product_ids, $slugs);
                //  remove tags that will not filter products and will create duplicates
                if ($mtags) {
                    $c = $products->count();
                    $mtags->filter(function($tag) use ($c) { return ($c != $tag->count); });
                }
                return $mtags;
            });

            return view('commerce::site.tag')
                ->withTags( $tagObjects )
                ->withMtags( $mtags )
                ->withSlugs( implode(',', $slugs) )
                ->withProducts( $products  );
        }
        else return redirect()->route('home', [], 301);
    }

    private function getProductIdsForTags($slugs) {
        $product_ids = array();
        $tags = array();
        $tagObjects = array();
        foreach ($slugs as $slug) {
            $tag = Tag::whereSlug($slug)->first();
            if ($tag) {
                $tags[] = $tag->name;
                $tagObjects[] = $tag;
                $product_ids = Cache::remember('tag-'.$slug.'pids', Config::get('commerce::general.cache.category'), function() use ($product_ids, $tag) {
                    if (fn_is_empty($product_ids)) {
                        $product_ids = Tagged::where('tag_slug', '=', $tag->slug)
                            ->where('taggable_type', '=', 'Dounasth\Commerce\App\Models\Product\Product')
                            ->distinct()->pluck('taggable_id');
                    }
                    else {
                        $product_ids = array_intersect(Tagged::where('tag_slug', '=', $tag->slug)
                            ->where('taggable_type', '=', 'Dounasth\Commerce\App\Models\Product\Product')
                            ->distinct()->pluck('taggable_id'), $product_ids);
                    }
                    return $product_ids;
                });
            }
        }
        return [$tags, $tagObjects, $product_ids];
    }

    protected function paginate($product_ids, $perPage) {
        $count = count($product_ids);
        $pagination = Paginator::make($product_ids, $count, $perPage);

        $page = $pagination->getCurrentPage($count);
        $product_ids = array_slice($product_ids, ($page - 1) * $perPage, $perPage);

        $pagination = Paginator::make($product_ids, $count, $perPage);
        return $pagination;
    }
}
