<?php
namespace Dounasth\Commerce\App\Controllers\Site;

use Dounasth\Commerce\App\Models\Product\Product;
use Dounasth\Frontend\App\Http\Controllers\FrontendBaseController;

class ProductController extends FrontendBaseController
{

    public function viewProduct($slug) {
        $product = Product::enabled()->whereSlug($slug)->first();
        if ($product) {
            return view('commerce::site.product')->withProduct( $product );
        }
        else return redirect()->route('home', [], 301);
    }

    public function affiliateRedirect($slug) {
        $product = Product::enabled()->whereSlug($slug)->first();
        if ($product) {
            return redirect()->to($product->getAffiliateUrl());
        }
        else return redirect()->route('home', [], 301);
    }

}
