<?php
namespace Dounasth\Commerce\App\Controllers\Site;

use Dounasth\Commerce\App\Cart\MyCartFacade as MyCart;
use Dounasth\Commerce\App\Models\Order\Order;
use Dounasth\Commerce\App\Models\Order\OrderItem;
use Dounasth\Commerce\App\Models\Payment;
use Dounasth\Commerce\App\Models\Product\Product;
use Dounasth\Commerce\App\Models\Shipping;
use Dounasth\Frontend\App\Http\Controllers\FrontendBaseController;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


class CartController extends FrontendBaseController {

    public function cart(){

        $addresses = Session::get('addresses', array());
        $messages = array();

        $addressesDef = array_flip(array_keys(Config::get('cart.checkout.validation.addresses', [])));
        foreach ($addressesDef as $dato_name => $dato_value) {
            if (!isset($addresses[$dato_name])) {
                $addresses[$dato_name] = '';
            }
            if (!isset($messages[$dato_name])) {
                $messages[$dato_name] = '';
            }
        }

        if (fn_is_not_empty($addresses)) {
            foreach ($addresses as $dato_name => $dato_value) {
                $validator = Config::get('cart.checkout.validation.addresses.'.$dato_name, false);
                $addresses[$dato_name] = $dato_value;
                if ( !startsWith($dato_name, 'billing_') && $validator && $validator !== true && !$validator->validate($dato_value)) {
                    $messages[$dato_name] = Config::get('cart.checkout.validation.messages.'.$dato_name, '');
                }
                elseif (startsWith($dato_name, 'billing_') && $addresses['other_billing_address']) {
                    if ( $validator && $validator !== true && !$validator->validate($dato_value)) {
                        $messages[$dato_name] = Config::get('cart.checkout.validation.messages.'.$dato_name, '');
                    }
                }
            }
        }

        $extra = Session::get('extra', array());
        if (isset($extra['shipping_type'])) {
            $shipping = Shipping::findOrNew($extra['shipping_type']);
        }
        else {
            $shipping = Shipping::theDefault();
            $extra['shipping_type'] = $shipping->id;
        }
        if (isset($extra['payment_method'])) {
            $payment = Payment::findOrNew($extra['payment_method']);
        }
        else {
            $payment = Payment::theDefault();
            $extra['payment_method'] = $payment->id;
        }

        $max_quantity = 10000000;//   todo set max quantity;
        $cart_message = '';
        if (MyCart::getContent()->count() > $max_quantity) {
            $cart_message = [
                trans('site.cart.quantity_not_available'),
                trans('site.cart.max_available_quantity', ['max'=>$max_quantity]),
                trans('site.cart.ajust_quantity'),
            ];
            $cart_message = implode(' ', $cart_message);
        }

        $data = [
            'addresses' => $addresses,
            'cart_message' => $cart_message,
            'messages' => $messages,
            'extra' => $extra,
            'selectedshipping' => $shipping,
            'selectedpayment' => $payment,
        ];
        return view('commerce::site.cart.onepage-checkout', $data);
    }

    public function cartJSON(){
        $view = $this->cart();
        $html = str_get_html($view->render());

        $ret = $html->find('li.cart-button');
        $response['html']['li.cart-button'] = (string)$ret[count($ret)-1];

        $ret = $html->find('div.checkout');
        $response['html']['div.checkout'] = (string)$ret[count($ret)-1];

        $response['cec'] = count(array_filter(Session::get('messages', [])));

        $headers = array(
            'Content-type'=> 'application/json; charset=utf-8',
            'Cache-Control' => 'max-age='.Config::get('api::general.jsonCacheControl', 0),
        );
        return response()->json($response, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    public function removeFromCart($rowId) {
        MyCart::remove($rowId);
        return redirect()->route('site.cart.view');
//        return $this->cartJSON();
    }

    public function updateQuantity($rowId, $quantity) {
        $response = array();
        MyCart::update($rowId, ['quantity'=>[
        	'value' => $quantity,
        	'relative' => false,
        ]]);
        return $this->cartJSON();
    }

    public function updateAddresses() {
        $addresses = Session::get('addresses', array());
        $messages = array();

        $data = Input::get('addresses', array());

        if (fn_is_not_empty($data)) {
            foreach ($data as $name => $value) {
                $validator = Config::get('cart.checkout.validation.addresses.'.$name, false);
                $addresses[$name] = $value;
                if ( !startsWith($name, 'billing_') && $validator && $validator !== true && !$validator->validate($value)) {
                    $messages[$name] = Config::get('cart.checkout.validation.messages.'.$name, '');
                }
                elseif (startsWith($name, 'billing_') && $addresses['other_billing_address']) {
                    if ( $validator && $validator !== true && !$validator->validate($value)) {
                        $messages[$name] = Config::get('cart.checkout.validation.messages.'.$name, '');
                    }
                }
            }
        }

        Session::put('latest_county', $addresses['county'], '');
        Session::put('latest_billing_county', $addresses['billing_county'], '');

        Session::put('addresses', $addresses);
        Session::put('messages', $messages);
        return $this->cartJSON();
    }

    public function updateOrderExtra() {
        $extra = Session::get('extra', array());
        $data = Input::get('extra', array());

        if (fn_is_not_empty($data)) {
            foreach ($data as $name => $value) {
                $extra[$name] = $value;
            }
        }

        Session::put('extra', $extra);
        return $this->cartJSON();
    }

    public function addToCart($model, $id, $quantity=1) {
        $model = base64_decode($model);
        $object = $model::findOrFail($id);
	    $data = array(
			'id'       => $object->id,
			'name'     => $object->title,
			'quantity' => $quantity,
			'price'    => $object->prices->price,
			[],
		    'model' => $object
	    );
        MyCart::associate($model);
        MyCart::add($data);
        return redirect()->route('site.cart.view');
    }

    public function checkoutComplete(){

        $this->updateAddresses();
        $this->updateOrderExtra();

        $max_quantity = 10000000;//   todo set max quantity;
        if (MyCart::getContent()->count() > $max_quantity) {
            return redirect()->route('site.cart.view');
            exit;
        }

        //  if messages are left in the session they are validation errors
        if (fn_is_not_empty(Session::get('messages', array()))) {
            return redirect()->route('site.cart.view');
            exit;
        }

        $addresses = Session::get('addresses', array());

        $extra = Session::get('extra', array());
        $data = Input::all();
        $extra = array_merge($extra, $data);
        if (isset($extra['shipping_type'])) {
            $shipping = Shipping::findOrNew($extra['shipping_type']);
        }
        else {
            $shipping = Shipping::theDefault();
            $extra['shipping_type'] = $shipping->id;
        }
        if (isset($extra['payment_method'])) {
            $payment = Payment::findOrNew($extra['payment_method']);
        }
        else {
            $payment = Payment::theDefault();
            $extra['payment_method'] = $payment->id;
        }

        $order = new Order();
        $order->status_id = INCOMPLETE_ORDER;
        $order->addresses = $addresses;
        $order->email = $addresses['email'];
        $order->fill($extra);
        $order->items_cost = MyCart::getTotal();
        $order->payment_cost = $payment->cost;
        $order->shipping_cost = $shipping->cost;
        $order->total = MyCart::getTotal() + $shipping->cost + $payment->cost;
        $order->save();

        foreach (MyCart::getContent() as $item) {
            $oi = new OrderItem();
            $oi->order_id = $order->id;
            $oi->model_id = $item->id;
            $oi->model_type = Product::class;
            $oi->quantity = $item->quantity;
            $oi->price = $item->price;
            $oi->save();
        }

        MyCart::clear();

        return redirect()->route('order.view', [$order->hash]);
    }

    public function viewOrder($hash) {
        $order = Order::where('hash', '=', $hash)->firstOrFail();
//        $order->changeIsPaid(2);
        return view('commerce::site.order.order')->withOrder($order);
    }

    public function payOrder() {

        $t = Input::get('t', 'Order');
        $hash = Input::get('id', 0);


        if (!$hash) {
            return redirect()->home();
        }

        if (fn_is_not_empty($t) && $t == 'CreditOrder') {
            $order = CreditOrder::where('hash', '=', $hash)->firstOrFail();
        }
        else {
            $order = Order::where('hash', '=', $hash)->firstOrFail();
        }


        $order->changeIsPaid(2);


        $processor_data = $order->payment->toArray();
        $payment_file = __DIR__.'/../../../lib/payments/'.$order->payment->php_file;

//        $x = exec("curl -s checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//'",$out);
//        echo $x;
//        exit;

        define('BOOTSTRAP', true);
        include_once($payment_file);
        exit;
    }

    public function payOrderNotification($mode) {
        //  mode is used in the payment processor
        //  type is used to distinguish orders for various types
        $t = Input::get('t', '');
        $s = Input::get('s', '');
        $hash = Input::get('id', '');

        if (!is_numeric($hash) && Session::get($hash, false)) {
            $t = Session::get($hash.'_type', '');
            $s = Session::get($hash.'_site', '');
            $hash = Session::get($hash);
        }
        if (!$hash) {
            return redirect()->home();
        }

        if (fn_is_not_empty($t)) {
            if ($t == 'CreditOrder') {
                $req = Input::all();
                $req['id'] = $hash;
                $backToSite = Site::findOrFail($s);
                $backto = $backToSite->fullDomain().'/admin/manage/credits/notify/'.$mode.'?'.http_build_query($req);
                header('Location:'.$backto);
                exit;
            }
        }

        $order = Order::where('hash', '=', $hash)->firstOrFail();
        $processor_data = $order->payment->toArray();
        $payment_file = __DIR__.'/../../../lib/payments/'.$order->payment->php_file;
        define('BOOTSTRAP', true);
        define('PAYMENT_NOTIFICATION', true);
        include_once($payment_file);

        notifyForOrder($order, false, false);
        Session::put('show_thanx', true);
        return redirect()->route('order.view', [$order->hash]);
    }

    public function updatePayment($hash) {
        $order = Order::where('hash', '=', $hash)->firstOrFail();
        $payment = Payment::find(Input::get('payment_method', $order->payment_method));

        if (!$payment) {
            return redirect()->route('order.view', [$order->hash]);
        }

        $oldPaymentCost = $order->payment_cost;
        $newPaymentCost = $payment->cost;
        $newPaymentTotal = $order->total - $oldPaymentCost + $newPaymentCost;

        $order->is_paid = 0;
        $order->payment_cost = $payment->cost;
        $order->payment_method = $payment->id;
        $order->total = $newPaymentTotal;
        $order->save();
        return redirect()->route('order.pay', ['id='.$order->hash]);
    }
}

