<?php namespace Dounasth\Commerce\App\Cart;

use Illuminate\Support\Facades\Facade;

class MyCartFacade extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'mycart';
    }
}