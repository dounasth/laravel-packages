<?php namespace Dounasth\Commerce\App\Cart;

use Darryldecode\Cart\Exceptions\InvalidConditionException;
use Darryldecode\Cart\Exceptions\InvalidItemException;
use Darryldecode\Cart\Helpers\Helpers;
use Darryldecode\Cart\ItemAttributeCollection;
use Darryldecode\Cart\Validators\CartItemValidator;

/**
 * Class Cart
 * @package Darryldecode\Cart
 */
class MyCart extends \Darryldecode\Cart\Cart
{

	/**
	 * The Eloquent model a cart is associated with
	 *
	 * @var string
	 */
	protected $associatedModel;
	/**
	 * An optional namespace for the associated model
	 *
	 * @var string
	 */
	protected $associatedModelNamespace;

	/**
	 * Set the associated model
	 *
	 * @param  string $modelName The name of the model
	 * @param  string $modelNamespace The namespace of the model
	 *
	 * @return void
	 */
	public function associate( $modelName, $modelNamespace = null ) {
		$this->associatedModel          = $modelName;
		$this->associatedModelNamespace = $modelNamespace;

		if ( ! class_exists( $modelNamespace . '\\' . $modelName ) ) {
			throw new Exceptions\ShoppingcartUnknownModelException;
		}

		// Return self so the method is chainable
		return $this;
	}

	public function getAssociatedModel() {
		return $this->associatedModel;
	}

	public function getAssociatedModelNamespace() {
		return $this->associatedModelNamespace;
	}


	/**
	 * add item to the cart, it can be an array or multi dimensional array
	 *
	 * @param string|array $id
	 * @param string $name
	 * @param float $price
	 * @param int $quantity
	 * @param array $attributes
	 * @param CartCondition|array $conditions
	 * @return $this
	 * @throws InvalidItemException
	 */
	public function add($id, $name = null, $price = null, $quantity = null, $attributes = array(), $conditions = array(), $model = null)
	{
		// if the first argument is an array,
		// we will need to call add again
		if (is_array($id)) {
			// the first argument is an array, now we will need to check if it is a multi dimensional
			// array, if so, we will iterate through each item and call add again
			if (Helpers::isMultiArray($id)) {
				foreach ($id as $item) {
					$this->add(
						$item['id'],
						$item['name'],
						$item['price'],
						$item['quantity'],
						Helpers::issetAndHasValueOrAssignDefault($item['attributes'], array()),
						Helpers::issetAndHasValueOrAssignDefault($item['conditions'], array()),
						$item['model']
					);
				}
			} else {
				$this->add(
					$id['id'],
					$id['name'],
					$id['price'],
					$id['quantity'],
					Helpers::issetAndHasValueOrAssignDefault($id['attributes'], array()),
					Helpers::issetAndHasValueOrAssignDefault($id['conditions'], array()),
					$id['model']
				);
			}

			return $this;
		}

		// validate data
		$item = $this->validate(array(
			'id' => $id,
			'name' => $name,
			'price' => Helpers::normalizePrice($price),
			'quantity' => $quantity,
			'attributes' => new ItemAttributeCollection($attributes),
			'conditions' => $conditions,
			'model' => $model,
		));

		// get the cart
		$cart = $this->getContent();

		// if the item is already in the cart we will just update it
		if ($cart->has($id)) {
			$this->update($id, $item);
		} else {
			$this->addRow($id, $item);
		}

		return $this;
	}

}
