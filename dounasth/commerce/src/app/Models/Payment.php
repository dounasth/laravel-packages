<?php
namespace Dounasth\Commerce\App\Models;

//use Dimsav\Translatable\Translatable;

use Dounasth\Crud\Traits\CrudModelTrait;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model {

    use CrudModelTrait;
//    use Translatable;
//    protected $translationForeignKey = 'payment_id';
    protected $translatedAttributes = array('name');

	protected $table = 'cart_payment_methods';
    protected $fillable = array('name', 'php_file', 'admin_file', 'cost', 'site_costs', 'status', 'image', 'position', 'params');
    protected $guarded = array('id');

    public function scopeEnabled($query)
    {
        return $query->where('status', '=', 'A');
    }

    public function scopePositioned($query)
    {
        return $query->orderBy('position','asc');
    }

    public function scopeTheDefault($query)
    {
        return $query->enabled()->orderBy('position', 'asc')->first();
    }

    public function orders()
    {
        return $this->hasMany('Order', 'payment_method', 'id');
    }

    public function getParamsAttribute($value)
    {
        return unserialize($value);
    }

    public function setParamsAttribute($value)
    {
        $this->attributes['params'] = serialize($value);
    }

    public function calculateSiteCosts($order_total) {
        $site_costs_total = array();
        $site_costs = array_filter(explode('/', $this->site_costs));
        foreach ($site_costs as $site_cost) {
            if (endsWith($site_cost, '%')) {
                $site_cost = ($site_cost*1) / 100 ;
                $site_costs_total[] = $order_total * $site_cost;
            }
            else $site_costs_total[] = $site_cost;
        }
        return array_sum($site_costs_total);
    }
}
