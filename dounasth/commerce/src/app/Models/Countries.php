<?php
namespace Dounasth\Commerce\App\Models;

use Dounasth\Crud\Traits\CrudModelTrait;
use Illuminate\Database\Eloquent\Model;

class Countries extends Model {

    use CrudModelTrait;

	protected $table = 'cart_countries';
    protected $fillable = array('code', 'name');
    protected $guarded = array('id');
    public $timestamps = false;

}
