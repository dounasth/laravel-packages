<?php

namespace Dounasth\Commerce\App\Models;


use Cviebrock\EloquentSluggable\Sluggable;
use Dounasth\Crud\Traits\CrudModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model {

    use CrudModelTrait;
    use NodeTrait;
    use SoftDeletes;

    protected $table = 'cart_categories';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $guarded = array('id');
    protected $fillable = array('title', 'slug', 'description', 'status', 'parent_id', 'keyword', 'path_ids', 'path', 'count');

    public function save(array $options = array())
    {
//        $this->path_ids = $this->path_ids();
//        $this->path = $this->path();
//        $this->count = $this->enabledProductsCount();
        $this->tree_count = $this->categoryTreeCount();
        return parent::save($options);
    }

    public function products() {
        return $this->belongsToMany('Dounasth\Commerce\App\Models\Product\Product', 'cart_products_categories', 'category_id', 'product_id');
    }

    public function enabledProductsCount() {
        return $this->products()->enabled()->count();
    }
    public function categoryTreeCount() {
        $sum  = Category::where('path_ids','=',$this->path_ids)
            ->orWhere('path_ids','like',$this->path_ids.'/%')
            ->sum('count')
        ;
//        SELECT sum(count) FROM `cart_categories`
//        where path_ids = '262' or path_ids like '262/%'
//        ORDER BY `cart_categories`.`path_ids` ASC
        return $sum;
    }

    public function route() {
        return route('site.commerce.category.view', [$this->slug]);
    }

    public function path()
    {
        $parent = $this->parent;
        return $parent ? $parent->path().' / '.$this->title : $this->title;
    }

    public function path_ids()
    {
        $parent = $this->parent;
        return $parent ? $parent->path_ids().'/'.$this->id : $this->id;
    }

    public function seo()
    {
        return $this->morphOne('Dounasth\Backend\App\Models\Seo', 'seoble');
    }

    public static function scopeBasics($query) {
        return $query->withDepth()->having('depth', '=', 0);
    }

    public static function scopeLevel($query, $level) {
        return $query->withDepth()->having('depth', '=', $level);
    }

    public function scopeEnabled($query)
    {
        return $query->whereStatus('A');
    }

    public function scopeSorted($query, $type='asc')
    {
        return $query->orderBy('_lft', 'asc');
    }

    public static function cachedTree() {
        $t = new static;
        return $t->defaultOrder()->enabled()->remember(3600*24, 'site-categories')->get()->toTree();
    }

    public static function cachedRoot() {
        $t = new static;
        return $t->defaultOrder()->basics()->remember(3600*24, 'site-categories-root')->take(5)->get();
    }



    protected static function select2($query=null) {
        $query = implode('%', explode(' ', $query));
        $data = self::select();
        if ($query) {
            $data = $data->where('path', 'like', "%$query%")
                ->orWhere('title', 'like', "%$query%");
        }
        $data = $data->defaultOrder()->get();
        $select2 = [];
        foreach ($data as $datum) {
            $select2[] = [
                'id' => $datum->selectboxValue(),
                'text' => $datum->selectboxText(),
            ];
        }
        return new Collection($select2);
    }


    public function selectboxText() {
        return $this->path();
    }

    public function selectboxValue() {
        return $this->id;
    }

}