<?php

namespace Dounasth\Commerce\App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Description extends Model {

    protected $table = 'cart_product_descriptions';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = array('product_id', 'short', 'full');

    public function product() {
        return $this->belongsTo('Dounasth\Commerce\App\Models\Product\Product', 'product_id', 'id');
    }

/*    public function setTitle($title){
        $this->title = $title;
    }
    public function setShort($description){
        $this->short = $description;
    }
    public function setFull($description){
        $this->full = $description;
    }*/

} 