<?php
/**
 * Created by PhpStorm.
 * User: nimda
 * Date: 5/22/15
 * Time: 11:49 AM
 */

namespace Dounasth\Commerce\App\Models\Product;


class ProductsCategories extends \Eloquent {

//    use \SoftDeletingTrait;
    protected $table = 'cart_products_categories';
//    protected $dates = ['deleted_at'];

    public function products()
    {
        return $this->morphMany('Dounasth\Commerce\App\Models\Product\Product', 'product', 'id', 'product_id');
    }

    public static function rawdb(){
        $t = new ProductsCategories();
        return \DB::table($t->table);
    }
}