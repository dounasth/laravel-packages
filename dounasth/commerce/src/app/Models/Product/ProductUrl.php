<?php

namespace Dounasth\Commerce\App\Models\Product;

use Baum\Extensions\Eloquent\Model;

class ProductUrl extends Model {

    protected $table = 'cart_product_aff_urls';
    protected $primaryKey = 'product_id';
    public $timestamps = false;

    protected $fillable = array('product_id', 'url');

    public function product() {
        return $this->belongsTo('Dounasth\Commerce\App\Models\Product\Product', 'product_id', 'id');
    }

}