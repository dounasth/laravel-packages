<?php

namespace Dounasth\Commerce\App\Models\Product;

use Cviebrock\EloquentSluggable\Services\SlugService;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentTaggable\Taggable;
use Dounasth\Backend\App\Helpers\GreekSlugGenerator;
use Dounasth\Backend\App\Models\Metable;
use Dounasth\Commerce\App\Models\Category;
use Dounasth\Content\App\Models\Photo;
use Dounasth\Crud\Traits\CrudModelTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\FacadesInput;

class Product extends Model{

    use SoftDeletes;
    use CrudModelTrait;
    use Taggable;

    use Metable;
    protected $meta_model = 'Dounasth\Backend\App\Models\Meta';
    protected $metable_table = 'cart_product_meta';

    /**
     * @var bool
     */
    public $timestamps = true;
    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
    /**
     * @var string
     */
    protected $table = 'cart_products';
    /**
     * @var string
     */
    protected $primaryKey = 'id';

	use Sluggable;
    public function sluggable() {
	    return [
		    'slug' => [
			    'source'             => 'title',
			    'maxLength'          => null,
			    'maxLengthKeepWords' => true,
			    'method'             => ['Dounasth\Backend\App\Helpers\GreekSlugGenerator','get_slug'],
			    'separator'          => '-',
			    'unique'             => true,
			    'uniqueSuffix'       => null,
			    'includeTrashed'     => true,
			    'reserved'           => null,
			    'onUpdate'           => false,

		    ]
	    ];
    }

	protected $fillable = array('title', 'sku', 'slug', 'status');

    /**
     * @param mixed $id
     * @param array $columns
     * @return Product
     */
    public static function findOrNew($id, $columns = array('*'))
    {
        return parent::findOrNew($id, $columns);
    }

    public function saveFromData(ProductData $data, array $options = array())
    {
        $isNew = !($this->id > 0);
        $this->fill((array)$data);
        $saved = parent::save($options);
        if ($saved) {

            $this->categories()->sync($data->categories);

            $prices = $this->prices()->first();
            if (!$prices) {
                $prices = new Price(['product_id'=>$this->id]);
            }
            $prices->price = ($data->price);
            $prices->list_price = ($data->list_price);
            $this->prices()->save($prices);

            $descriptions = $this->descriptions()->first();
            if (!$descriptions) {
                $descriptions = new Description(['product_id'=>$this->id]);
            }
            $descriptions->full = $data->full_description;
            $descriptions->short = $data->short_description;
            $this->descriptions()->save($descriptions);

            $mainImage = Input::file('files.main_image');
            if ($mainImage && $mainImage->isValid())
            {
                $filename = md5($mainImage->getClientOriginalName().time()) . '.' . $mainImage->getClientOriginalExtension();
                Input::file('files.main_image')->move(public_path().'/uploads/products', $filename);
                $this->mainPhoto('/uploads/products' . '/' . $filename);
            }
            elseif ($data->main_image) {
                $this->mainPhoto($data->main_image);
            }

            if ($data->tags) {
                $this->retag($data->tags);
            }

            if ($data->affiliateUrl) {
                if ($this->affiliateUrl) {
                    $this->affiliateUrl->url = $data->affiliateUrl;
                    $this->affiliateUrl->save();
                }
                else {
                    $url = ProductUrl::create([
                        'product_id' => $this->id,
                        'url' => $data->affiliateUrl,
                    ]);
                    $this->affiliateUrl()->save($url);
                }
            }
            else {
                $this->affiliateUrl()->delete();
            }
        }

//        if ($this->needsSlugging() || $isNew) {
//            $this->resluggify();
//        }

        return $this;
    }

    public function saveMeta($meta) {
        if (fn_is_not_empty($meta)) {
            $this->setMeta($meta);
        }
    }

    public function delete()
    {
        if ($this->forceDeleting) {
            $this->categories()->sync([]);
            $this->descriptions()->delete();
            $this->prices()->delete();
            $this->affiliateUrl()->delete();
            foreach ($this->seo() as $seo) {
                $seo->delete();
            }
            foreach ($this->photos() as $photo) {
                $photo->delete();
            }
            //  also delete meta
        }
        parent::delete();
    }

    public function prices() {
        return $this->hasOne('Dounasth\Commerce\App\Models\Product\Price', 'product_id', 'id');
    }

    public function affiliateUrl() {
        return $this->hasOne('Dounasth\Commerce\App\Models\Product\ProductUrl', 'product_id', 'id');
    }

    public function getAffiliateUrl() {
        if (fn_is_not_empty($this->affiliateUrl)) {
            $subID = \Config::get("laraffiliate::general.subID");
            if (fn_is_not_empty($subID)) {
                $subID = '&subid1='.$subID;
            }
            return $this->affiliateUrl->url.$subID;
        }
        else return '';
    }

    public function route() {
        if (fn_is_not_empty($this->affiliateUrl)) {
            return route('site.commerce.product.affiliate', [$this->slug]);
        }
        else return route('site.commerce.product.view', [$this->slug]);
    }

    public function descriptions() {
        return $this->hasOne('Dounasth\Commerce\App\Models\Product\Description', 'product_id', 'id');
    }

    public function photos()
    {
        return $this->morphMany('Dounasth\Content\App\Models\Photo', 'imageable');
    }

    /**
     * @param null $path
     * @return Photo
     */
    public function mainPhoto($path=null) {
        if ($path) {
            $link = $this->photos()->where('link_type', '=', 'M')->first();
            if (!$link) {
                $link = new Photo();
            }
            $link->path = $path;
            $link->link_type = 'M';
            $this->photos()->save($link);
        }
        else {
//            $link = $this->photos()->where('link_type', '=', 'M')->first();
            if (isset($this->photos[0])) {
                $link = $this->photos[0];
            }
            else $link = new Photo();
        }
        return $link;
    }

    public function categories() {
        return $this->belongsToMany('Dounasth\Commerce\App\Models\Category', 'cart_products_categories', 'product_id', 'category_id')->withPivot('type');
    }

    /**
     * @param null $category_id
     * @return Category
     */
    public function mainCategory($category_id=null) {
        if ($category_id){
            $link = $this->categories()->wherePivot('type', '=', 'M')->wherePivot('product_id', '=', $this->id)->first()->pivot;
            if ($link) {
                $this->categories()->detach($link->category_id);
            }
            $this->categories()->attach([$category_id => ['type'=>'M']]);
        }
        $mainCat = $this->categories()->wherePivot('type', '=', 'M')->first();
        if (!$mainCat) {
            $mainCat = new Category();
        }
        return $mainCat;
    }

    /**
     * @param null $category_ids
     * @param string $delimiter
     * @param string $action
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function additionalCategories($category_ids=null, $delimiter=',', $action='sync') {
        if ($category_ids && !is_array($category_ids)){
            $category_ids = explode($delimiter, $category_ids);
        }
        if ($category_ids && is_array($category_ids)){
            $category_ids = array_flip($category_ids);
            foreach ($category_ids as $k => $v) {
                $category_ids[$k] = ['type'=>'A'];
            }
            $this->categories()->$action($category_ids);
        }
        return $this->categories()->wherePivot('type', '=', 'A')->get();
    }

    /**
     * @param $category_ids
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function addAdditionalCategory($category_ids){
        return $this->additionalCategories($category_ids, ',', $action='attach');
    }

    /**
     * @param $category_ids
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function removeAdditionalCategory($category_ids){
        return $this->additionalCategories($category_ids, ',', $action='detach');
    }

    public function tagNamesInline($delimiter=',') {
        return implode($delimiter, $this->tagArray);
    }

    /**
     * @return string
     */
    function __toString()
    {
        return $this->title ;
    }

    public function seo()
    {
        return $this->morphOne('Dounasth\Backend\App\Models\Seo', 'seoble');
    }

    public static function tagsFor($products) {
        $tags = array();
        $sorter = array();
        foreach ($products as $product) {
            foreach ($product->tagged as $tag) {
                $count = (isset($tags[$tag->tag_slug]->count)) ? $tags[$tag->tag_slug]->count : 0 ;
//                $tags[$tag->tag_slug] = array(
//                    'slug' => $tag->tag_slug,
//                    'name' => $tag->tag_name,
//                    'count' =>  $count+1,
//                );
                $tags[$tag->tag_slug] = new \stdClass();
                $tags[$tag->tag_slug]->slug = $tag->tag_slug;
                $tags[$tag->tag_slug]->name = $tag->tag_name;
                $tags[$tag->tag_slug]->count = $count+1;
                $sorter[$tag->tag_slug] = $count+1;
            }
        }
        array_multisort($sorter, SORT_DESC, $tags);
        return $tags;
    }

    public function scopeEnabled($query)
    {
        return $query->whereStatus('A');
    }

    public function scopeRecent($query)
    {
        return $query->orderBy('id', 'desc');
    }

    public static function getIdsForCategories($catids) {
        if (fn_is_not_empty($catids)) {
            $t = new static;
//            $product_ids = ProductsCategories::whereIn('category_id', $catids)->remember(3600*24)->pluck('product_id');
//            if (fn_is_not_empty($product_ids)) {
//                $query[] = "SELECT id FROM " . $t->table;
//                $query[] = "WHERE status = 'A' AND deleted_at is null AND id IN ( " . implode(',', $product_ids) . " )";
//                $query = implode(' ', $query);
//                $data = \DB::select(\DB::raw($query))->remember(3600*24);
                $data = \DB::table($t->table)
                    ->select('id')
                    ->where('status','=','A')
                    ->whereNull('deleted_at')
                    ->whereRaw(\DB::raw('id IN (select `product_id` from `cart_products_categories` where `category_id` in ('.implode(',', $catids).'))'))
                    ->orderBy('id', 'desc')
                    ->remember(3600*24)->get();
                $data = Collection::make($data)->pluck('id');
                return $data;
//            }
//            else return false;
        }
        else return false;
    }

    public static function rawdb(){
        $t = new Product();
        return \DB::table($t->table);
    }

//    public function imported() {
//        return $this->hasOne('ImportProducts', 'product_id', 'id');
//    }

//    public function getSql()
//    {
//        $builder = $this->getBuilder();
//        $sql = $builder->toSql();
//        foreach($builder->getBindings() as $binding)
//        {
//            $value = is_numeric($binding) ? $binding : "'".$binding."'";
//            $sql = preg_replace('/\?/', $value, $sql, 1);
//        }
//        return $sql;
//    }

    public function scopeSetOrder($query) {
        return $query->orderBy(
            orderType('cart_products.id'),
            orderDirection()
        );
    }

}