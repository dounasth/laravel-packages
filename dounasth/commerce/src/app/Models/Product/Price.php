<?php

namespace Dounasth\Commerce\App\Models\Product;

class Price extends \Illuminate\Database\Eloquent\Model{

    const LIST_PRICE = 'ListPrice';
    const PRICE = 'Price';

    protected $table = 'cart_product_prices';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = array('product_id', 'type', 'price');

    public function product() {
        return $this->belongsTo('Dounasth\Commerce\App\Models\Product\Product', 'product_id', 'id');
    }

    public function discount() {
        if ($this->list_price > 0 && $this->list_price > $this->price) { 
            return (1- round(($this->price / $this->list_price), 2)) * 100;
        }
        else return false;
    }

}