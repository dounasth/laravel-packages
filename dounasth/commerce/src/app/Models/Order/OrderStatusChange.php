<?php
namespace Dounasth\Commerce\App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class OrderStatusChange extends Model {

	protected $table = 'cart_order_status_changes';
    protected $fillable = array('order_id', 'status_id', 'printer_status_id', 'printer_paid_id', 'timestamp');
    protected $guarded = array('id');

    public $timestamps = false;

    public function order()
    {
        return $this->belongsTo('Dounasth\Commerce\App\Models\Order\Order', 'id', 'order_id');
    }

    public function user()
    {
        return $this->belongsTo('Dounasth\Backend\App\Models\User', 'user_id', 'id');
    }

    public function status()
    {
        return $this->hasOne('Dounasth\Commerce\App\Models\Order\OrderStatus', 'id', 'status_id');
    }

}
