<?php

namespace Dounasth\Commerce\App\Models\Order;
use Illuminate\Database\Eloquent\Model;

/**
 * OrderItem
 *
 * @property integer $id 
 * @property integer $order_id 
 * @property integer $model_id
 * @property integer $quantity 
 * @property float $price 
 * @property float $print_price
 * @property string $case_type
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $created_at 
 * @property-read \Order $order 
 * @property-read \Design $design 
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereDesignId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem wherePrintPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereCaseType($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderItem whereCreatedAt($value)
 */
class OrderItem extends Model {

	protected $table = 'cart_order_items';
    protected $fillable = array('order_id', 'model_id', 'quantity', 'price');
    protected $guarded = array('id');

    public function order()
    {
        return $this->belongsTo('Dounasth\Commerce\App\Models\Order\Order', 'id', 'order_id');
    }

    public function model()
    {
        return $this->morphTo();
    }

}
