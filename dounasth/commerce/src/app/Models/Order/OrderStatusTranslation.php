<?php
namespace Dounasth\Commerce\App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class OrderStatusTranslation extends Model {

    protected $table = 'cart_order_statuses_translations';
    public $timestamps = false;
    protected $fillable = array('title', 'mail');

}
