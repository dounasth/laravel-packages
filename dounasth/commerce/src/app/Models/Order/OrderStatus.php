<?php
namespace Dounasth\Commerce\App\Models\Order;

use Dimsav\Translatable\Translatable;
use Dounasth\Crud\Traits\CrudModelTrait;
use Illuminate\Database\Eloquent\Model;


class OrderStatus extends Model {

    use CrudModelTrait;
    use Translatable;

	protected $table = 'cart_order_statuses';
    protected $fillable = array('title', 'color', 'valid_order', 'mail');
    protected $translatedAttributes = array('title', 'mail');
    protected $guarded = array('id');
    public $timestamps = false;

    public function orders()
    {
        return $this->hasMany('Order', 'status', 'id');
    }

}
