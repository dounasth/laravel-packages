<?php
namespace Dounasth\Commerce\App\Models\Order;


use Illuminate\Database\Eloquent\Model;

/**
 * OrderTotals
 *
 * @property integer id
 * @property integer order_id
 * @property integer items_quantity
 * @property float unit_cost
 * @property float vat_perc
 * @property float items_in_net
 * @property float items_in_vat
 * @property float payment_in_net
 * @property float payment_in_vat
 * @property float shipping_in_net
 * @property float shipping_in_vat
 * @property float total_in_net
 * @property float total_in_vat
 * @property float items_out_net
 * @property float items_out_vat
 * @property float payment_out_net
 * @property float payment_out_vat
 * @property float shipping_out_net
 * @property float shipping_out_vat
 * @property float total_out_net
 * @property float total_out_vat
 * @property float profit_net
 * @property float profit_vat
 *
 */
class OrderTotals extends Model {

	protected $table = 'cart_order_totals';
//    protected $fillable = array('status_id', 'addresses', 'data', 'payment_method', 'is_paid', 'payment_response', 'shipping_type', 'items_cost', 'payment_cost', 'shipping_cost', 'total', 'printed');
    protected $guarded = array('id');
    public $timestamps = false;

}




