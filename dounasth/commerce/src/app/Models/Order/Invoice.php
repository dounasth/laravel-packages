<?php
namespace Dounasth\Commerce\App\Models\Order;

use Dounasth\Backend\Searchable;
use Dounasth\Commerce\App\Models\Countries;
use Dounasth\Commerce\App\Models\States;
use Dounasth\Crud\Traits\CrudModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Invoice
 */
class Invoice extends Model {

    use SoftDeletes;
    use Searchable;
	use CrudModelTrait;

	protected $table = 'cart_orders_invoices';
    protected $fillable = array('order_id','invoice_type','invoice_num','is_cancel','cancels_type','cancels_num','email', 'status', 'addresses', 'order_data', 'carrier', 'voucher', 'payment_method', 'is_paid', 'payment_response', 'shipping_type', 'items_cost', 'payment_cost', 'shipping_cost', 'total', 'printed', 'printer_paid','items','totals');
    protected $guarded = array('id');

    public function getAddressesAttribute($value)
    {
        return unserialize(base64_decode($value));
    }

    public function setAddressesAttribute($value)
    {
        $this->attributes['addresses'] = base64_encode(serialize($value));
    }

    public function getIrsFilesAttribute($value)
    {
        return unserialize(base64_decode($value));
    }

    public function setIrsFilesAttribute($value)
    {
        $this->attributes['irs_files'] = base64_encode(serialize($value));
    }

    public function getOrderDataAttribute($value)
    {
        return unserialize(base64_decode($value));
    }

    public function setOrderDataAttribute($value)
    {
        $this->attributes['order_data'] = base64_encode(serialize($value));
    }

    public function getPaymentResponseAttribute($value)
    {
        return unserialize(base64_decode($value));
    }

    public function setPaymentResponseAttribute($value)
    {
        $this->attributes['payment_response'] = base64_encode(serialize($value));
    }

    public function payment()
    {
        return $this->belongsTo('Dounasth\Commerce\App\Models\Payment', 'payment_method', 'id');
    }

    public function shipping()
    {
        return $this->belongsTo('Dounasth\Commerce\App\Models\Shipping', 'shipping_type', 'id');
    }

//    public function items()
//    {
//        return $this->hasMany('OrderItem', 'order_id', 'id');
//    }
//
//    public function totals()
//    {
//        return $this->hasOne('OrderTotals', 'order_id', 'id');
//    }
    public function getItemsAttribute($value) {
        return unserialize(base64_decode($value));
    }
    public function setItemsAttribute($value) {
        $this->attributes['items'] = base64_encode(serialize($value));
    }

    public function getTotalsAttribute($value) {
        return unserialize(base64_decode($value));
    }
    public function setTotalsAttribute($value) {
        $this->attributes['totals'] = base64_encode(serialize($value));
    }

    public function getFullName() {
        $fullname = @implode(' ', array_filter(array($this->addresses['surname'], $this->addresses['firstname'])));
        return  $fullname;
    }
    public function getEmail() {
        return isset($this->addresses['email']) ? $this->addresses['email'] : '' ;
    }

    public function save(array $options = array()) {
        $saved = parent::save($options);
        return $saved;
    }

//    public function makeIRSHash() {
//        $irs_hash = '';
////        do {
////            $irs_hash = md5(Hash::make($this->id.time().str_random()));
////        } while (Invoice::where('irs_hash', '=', $irs_hash)->exists());
//        $this->irs_hash = $irs_hash;//"INV_$irs_hash.txt";
//        return $this;
//    }

//    public function writeForologikosTriggerFile(){
//        if ( in_array($this->invoice_type, ['A', 'AA']) ) {
//            $receipt = View::make('parts.invoice_trigger_'.strtolower($this->invoice_type))->withInvoice($this);
//            $forologikos_path = '/home/csdevpl/forologikos';
//            $md5 = md5(Hash::make($this->id.time().str_random()));
//            File::put($forologikos_path.'/'."INV_".$md5.".txt", $receipt);
//            File::put($forologikos_path.'/test/'."INV_".$md5.".txt", $receipt);
//        }
//        elseif ( in_array($this->invoice_type, ['T', 'AT']) ) {
//
//        }
//    }

    public function theState() {
        $theVar = States::whereCountryCode($this->addresses['country'])->whereCode($this->addresses['county'])->first();
        if (fn_is_not_empty($theVar)) {
            return $theVar->state;
        }
        else return $this->addresses['county'];
    }
    public function theBillingState() {
        $theVar = States::whereCountryCode($this->addresses['billing_country'])->whereCode($this->addresses['billing_county'])->first();
        if (fn_is_not_empty($theVar)) {
            return $theVar->state;
        }
        else return $this->addresses['billing_county'];
    }

    public function theCountry() {
        return Countries::whereCode($this->addresses['country'])->first()->name;
    }
    public function theBillingCountry() {
        return Countries::whereCode($this->addresses['billing_country'])->first()->name;
    }

}
