<?php

namespace Dounasth\Commerce\App\Models\Order;

use Carbon\Carbon;
use Dounasth\Backend\Searchable;
use Dounasth\Commerce\App\Models\Countries;
use Dounasth\Commerce\App\Models\States;
use Dounasth\Crud\Traits\CrudModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * Order
 *
 * @property integer $id
 * @property integer $status_id
 * @property string $addresses
 * @property integer $payment_method
 * @property integer $is_paid
 * @property integer $payment_response
 * @property integer $shipping_type
 * @property float $items_cost
 * @property float $payment_cost
 * @property float $shipping_cost
 * @property integer $total
 * @property integer $printed
 * @property integer $printer_paid
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $created_at
 * @property-read \Payment $payment
 * @property-read \Shipping $shipping
 * @property-read \OrderStatus $status
 * @method static \Illuminate\Database\Query\Builder|\Order whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereAddresses($value)
 * @method static \Illuminate\Database\Query\Builder|\Order wherePaymentMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereShippingType($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereItemsCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereShippingCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotal($value)
 * @method static \Illuminate\Database\Query\Builder|\Order wherePrinted($value)
 * @method static \Illuminate\Database\Query\Builder|\Order wherePrinterPaid($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCreatedAt($value)
 */
class Order extends Model {

    use SoftDeletes;
    use Searchable;
    use CrudModelTrait;

    /*use Searchable;
    protected $searchable = [
        'columns' => [
            'larapp_orders.id' => 100,
//            'larapp_orders.hash' => 90,
//            'larapp_orders.status_id' => 1,
//            'larapp_orders.payment_method' => 1,
//            'cart_product_meta.value' => 1,
        ],
//        'joins' => [
//            'cart_product_meta' => ['cart_products.id','cart_product_meta.xref_id'],
//        ],
    ];*/

	protected $table = 'cart_orders';
    protected $fillable = array('status_id', 'addresses', 'data', 'carrier', 'voucher', 'payment_method', 'is_paid', 'payment_response', 'shipping_type', 'items_cost', 'payment_cost', 'shipping_cost', 'total', 'printed', 'printer_paid');
    protected $guarded = array('id');

    protected $logStatusChanges = true;

    public function getAddressesAttribute($value)
    {
        return unserialize($value);
    }

    public function setAddressesAttribute($value)
    {
        $this->attributes['addresses'] = serialize($value);
    }

    public function getDataAttribute($value)
    {
        return unserialize($value);
    }

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = serialize($value);
    }

    public function getPaymentResponseAttribute($value)
    {
        return unserialize($value);
    }

    public function setPaymentResponseAttribute($value)
    {
        $this->attributes['payment_response'] = serialize($value);
    }

    public function payment()
    {
        return $this->belongsTo('Dounasth\Commerce\App\Models\Payment', 'payment_method', 'id');
    }

    public function shipping()
    {
        return $this->belongsTo('Dounasth\Commerce\App\Models\Shipping', 'shipping_type', 'id');
    }

    public function status()
    {
        return $this->belongsTo('Dounasth\Commerce\App\Models\Order\OrderStatus', 'status_id', 'id');
    }

    public function items()
    {
        return $this->hasMany('Dounasth\Commerce\App\Models\Order\OrderItem', 'order_id', 'id');
    }
    public function invoices()
    {
        return $this->hasMany('Dounasth\Commerce\App\Models\Order\Invoice', 'order_id', 'id');
    }

    public function invoice()
    {
        return $this->invoices()->where('is_cancel', '=', 0)->where('status', '=', 1)->orderBy('created_at', 'desc')->first();
    }

    public function items_names()
    {
        niceprintr($this->items);
        exit;
    }

    public function totals()
    {
        return $this->hasOne('Dounasth\Commerce\App\Models\Order\OrderTotals', 'order_id', 'id');
    }

    public function calculateTotals() {
        if ($this->id > 0) {
            $totals = $this->totals;
            if (!$totals) {
                $totals = new OrderTotals();
                $totals->order_id = $this->id;
            }
            $totals->vat_perc = TAX_PERCENTAGE;
            $totals->unit_cost = 1;
            $totals->items_quantity = $this->items()->sum('quantity');

            $totals->items_in_net = numberDB(deTax($this->items_cost));
            $totals->items_in_vat = numberDB(deTaxedAmount($this->items_cost));
            $totals->payment_in_net = numberDB(deTax($this->payment_cost));
            $totals->payment_in_vat = numberDB(deTaxedAmount($this->payment_cost));
            $totals->shipping_in_net = numberDB(deTax($this->shipping_cost));
            $totals->shipping_in_vat = numberDB(deTaxedAmount($this->shipping_cost));
            $totals->total_in_net = numberDB(deTax($this->total));
            $totals->total_in_vat = numberDB(deTaxedAmount($this->total));

            $totals->items_out_net = numberDB(creditsCost($totals->items_quantity));
            $totals->items_out_vat = numberDB(taxOf(creditsCost($totals->items_quantity)));
            $totals->payment_out_net = numberDB(deTax($this->payment->calculateSiteCosts($this->total)));
            $totals->payment_out_vat = numberDB(deTaxedAmount($this->payment->calculateSiteCosts($this->total)));
            $totals->shipping_out_net = numberDB(deTax($this->shipping->calculateSiteCosts($this->total)));
            $totals->shipping_out_vat = numberDB(deTaxedAmount($this->shipping->calculateSiteCosts($this->total)));
            $totals->total_out_net = $totals->items_out_net + $totals->payment_out_net + $totals->shipping_out_net ;
            $totals->total_out_vat = $totals->items_out_vat + $totals->payment_out_vat + $totals->shipping_out_vat ;

            $totals->profit_net = $totals->total_in_net - $totals->total_out_net ;
            $totals->profit_vat = $totals->total_in_vat - $totals->total_out_vat ;

            $totals->save();
        }

//        niceprintr($this->totals->toArray());
//        niceprintr($this->payment->toArray());
//        niceprintr($this->toArray());
    }

    public function statuses()
    {
        return $this->hasMany('Dounasth\Commerce\App\Models\Order\OrderStatusChange', 'order_id', 'id')->orderBy('timestamp', 'desc');
    }

    public function getFullName() {
        $fullname = @implode(' ', array_filter(array($this->addresses['surname'], $this->addresses['firstname'])));
        return  $fullname;
    }
    public function getEmail() {
        return isset($this->addresses['email']) ? $this->addresses['email'] : '' ;
    }

    public function save(array $options = array()) {
        if (fn_is_empty($this->hash)) {
            $this->makeHash();
        }
        if ( $this->logStatusChanges && $this->status_id && $this->getOriginal('status_id') != $this->status_id  ) {
            $this->logStatusChange();
        }
        $saved = parent::save($options);
        if ($saved) {
            $this->calculateTotals();
        }
        return $saved;
    }

    public function makeHash() {
        $hash = '';
        do {
            $hash = md5(Hash::make($this->id.time().str_random()));
        } while (Order::where('hash', '=', $hash)->exists());
        $this->hash = $hash;
//        $this->hash = $this->id;
        return $this;
    }

    public function changeIsPaid($val) {
        $this->is_paid = $val;
        $this->save();
        return $this;
    }

    public function logStatusChange($order_id=false, $status_id=false) {
        $log = OrderStatusChange::findOrNew(0);
        $log->order_id = ($order_id) ? $order_id : $this->id ;
        $log->status_id = ($status_id) ? $status_id : $this->status_id ;
        $log->printer_status_id = ($this->printed) ? $this->printed : 1 ;
        $log->printer_paid_id = ($this->printer_paid) ? $this->printer_paid : 1 ;
        if ($log->order_id && $log->order_id) {
            $log->user_id = Auth::user() ? Auth::user()->id : 0 ;
            $log->timestamp = Carbon::now();
            $saved = $log->save();
            return $saved;
        }
        else return false;
    }

    public function scopeSetOrder($query) {
        return $query->orderBy(
            orderType(),
            orderDirection()
        );
    }

    public function scopeSearchAnything($query, $params)
    {
        $params = $this->searchDefaultParams($params);
        extract($params);

        if (fn_is_not_empty($id)) {
            $query->where('orders.id', $id_op, $id);
        }
        if (fn_is_not_empty($hash)) {
            $query->where('orders.hash', 'LIKE', "%{$hash}%");
        }
        if (fn_is_not_empty($email)) {
//            $query->where('orders.addresses', 'LIKE', '%"email";s:%:"%'.$email.'%";%');
            $query->where('orders.email', 'LIKE', '%'.$email.'%');
        }

        if (fn_is_not_empty($date_from)) {
            $date_from = "{$date_from} 00:00:00";
            $query->where('orders.created_at', '>=', $date_from);
        }
        if (fn_is_not_empty($date_to)) {
            $date_to = "{$date_to} 24:60:60";
            $query->where('orders.created_at', '<=', $date_to);
        }

        if (fn_is_not_empty($order_status)) {
            $query->whereIN('orders.status_id', $order_status);
        }
        if (fn_is_not_empty($printed)) {
            $query->whereIN('orders.printed', $printed);
        }
        if (fn_is_not_empty($printer_paid)) {
            $query->whereIN('orders.printer_paid', $printer_paid);
        }
        if (fn_is_not_empty($payment)) {
            $query->whereIN('orders.payment_method', $payment);
        }
        if (fn_is_not_empty($shipping)) {
            $query->whereIN('orders.shipping_type', $shipping);
        }

        if (fn_is_not_empty($items_count)) {
            $query->whereIn('orders.id', function($query) use ($items_count, $items_count_op){
                $query->select("orders.id")->from('orders')
                    ->leftJoin(DB::raw("(SELECT order_id, COUNT(order_id) as items_count FROM larapp_order_items GROUP BY order_id ) items"), 'orders.id', '=', DB::raw('items.order_id'))
                    ->whereRaw("items.items_count {$items_count_op} {$items_count}");
            });
        }
        if (fn_is_not_empty($items_cost)) {
            $query->where('orders.items_cost', $items_cost_op, $items_cost);
        }
        if (fn_is_not_empty($total)) {
            $query->where('orders.total', $total_op, $total);
        }

        return $query;
    }

    protected function searchDefaultParams($params=[]) {
        $params = $params != null ? $params : [];
        return $params = array_merge(array(
            'doing_search' => 0,

            'id' => '',
            'id_op' => '=',
            'hash' => '',
            'email' => '',

            'date_from' => '',
            'date_to' => '',

            'order_status' => array(),
            'printed' => array(),
            'printer_paid' => array(),
            'payment' => array(),
            'shipping' => array(),

            'items_count' => '',
            'items_count_op' => '=',
            'items_cost' => '',
            'items_cost_op' => '=',
            'total' => '',
            'total_op' => '=',
        ), $params);
    }

    public function theState() {
        $theVar = States::whereCountryCode($this->addresses['country'])->whereCode($this->addresses['county'])->first();
        if (fn_is_not_empty($theVar)) {
            return $theVar->state;
        }
        else return $this->addresses['county'];
    }
    public function theBillingState() {
        $theVar = States::whereCountryCode($this->addresses['billing_country'])->whereCode($this->addresses['billing_county'])->first();
        if (fn_is_not_empty($theVar)) {
            return $theVar->state;
        }
        else return $this->addresses['billing_county'];
    }

    public function theCountry() {
        return Countries::whereCode($this->addresses['country'])->first()->name;
    }
    public function theBillingCountry() {
        return Countries::whereCode($this->addresses['billing_country'])->first()->name;
    }

    public function makeInvoice() {
        $invoices = Invoice::where('order_id', '=', $this->id)->where('is_cancel', '=', 0)->get();
        foreach ($invoices as $invoice) {
            $cancel = Invoice::where('is_cancel', '=', 1)
                ->where('cancels_type','=',$invoice->invoice_type)
                ->where('cancels_num','=',$invoice->invoice_num)
                ->first();
            if (!$cancel) {
                $cancel = Invoice::findOrNew(0);
                $cancel->fill($invoice->toArray());
            }
            $cancel->addresses = $invoice->addresses;
            $cancel->order_data = $invoice->order_data;
            $cancel->payment_response = $invoice->payment_response;
            $cancel->items = $invoice->items;
            $cancel->totals = $invoice->totals;
            $cancel->invoice_type = 'A'.$cancel->invoice_type;
            $maxId = Invoice::where('invoice_type', '=', $cancel->invoice_type)->max('invoice_num');
            $maxId = $maxId ? $maxId : 0;
            $cancel->invoice_num = $maxId + 1;
            $cancel->status = 1;
            $cancel->is_cancel = $invoice->id;
            $cancel->cancels_type = $invoice->invoice_type;
            $cancel->cancels_num = $invoice->invoice_num;
            $cancel->save();
//            $cancel->writeForologikosTriggerFile();

            $invoice->status = 0;
            $invoice->save();
        }

//        $invoice = Invoice::where('order_id', '=', $id)->first();
//        if (!$invoice) {
        $invoice = new Invoice();
//        }
        $invoice->fill($this->toArray());
//        $invoice->makeIRSHash();

        $invoice->invoice_type = strtoupper($this->addresses['invoice_type']);
        $maxId = Invoice::where('invoice_type', '=', $invoice->invoice_type)->max('invoice_num');
        $maxId = $maxId ? $maxId : 0;
        $invoice->invoice_num = $maxId + 1;

        $invoice->order_id = $this->id;
        $invoice->status = 1;
        $invoice->order_data = $this;
        $invoice->items = $this->items;
        $invoice->totals = $this->totals;
        $invoice->save();
//        $invoice->writeForologikosTriggerFile();
        return $invoice;
    }

    public static function makeInvoiceOf($id) {
        $order = Order::findOrFail($id);
        $invoice = $order->makeInvoice();
        return $invoice;
    }

}
