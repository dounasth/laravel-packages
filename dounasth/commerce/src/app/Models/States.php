<?php
namespace Dounasth\Commerce\App\Models;

use Dounasth\Crud\Traits\CrudModelTrait;
use Illuminate\Database\Eloquent\Model;

class States extends Model {

    use CrudModelTrait;

	protected $table = 'cart_states';
    protected $fillable = array('country_code', 'code', 'status', 'state');
    protected $guarded = array('id');
    public $timestamps = false;


//    use Translatable;
//    protected $translatedAttributes = array('state');

    public function scopeSortedList($query, $country, $withSelectOption=true) {
        $query
//            ->join('cart_states_translation', 'cart_states_translation.states_id', '=', 'cart_states.id')
            ->where('cart_states.country_code', ( (!empty($country)) ? $country : 'gr' ))
//            ->where('cart_states_translation.locale', theLocale())
//            ->groupBy('cart_states.id')
//            ->orderBy('cart_states_translation.state', 'asc')
//            ->with('translations')
        ;
        $states = $query->pluck('state', 'code');
        if ($withSelectOption) {
            $states = array('0'=>trans('site.choose')) + $states->toArray();
        }
        return $states;
    }

    public function country() {
        return $this->belongsTo(Countries::class, 'country_code','code');
    }

}
