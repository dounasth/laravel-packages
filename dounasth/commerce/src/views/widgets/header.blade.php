<header class="head">
    <div class="main-bar">
        <div class="pull-right">
            <form action="{{ route(Route::currentRouteName()) }}" class="form-horizontal">
                <div class="input-group">
                    <input  id="daterange-input" type="hidden" name="daterange" value="{{ printDateRangeValue($daterange, $today) }}" />
                    <button id="daterange-btn" type="button" class="btn btn-default">
                    <span>
                      <i class="fa fa-calendar"></i> <text>{{ printDateRangeValue($daterange, $today) }}</text>
                    </span>
                        <i class="fa fa-caret-down"></i>
                    </button>
                    <button type="submit" class="btn btn-success btn-sm inline">
                        <i class="fa fa-check"></i>
                    </button>
                    <a href="{{ route(Route::currentRouteName()) }}" class="btn btn-warning btn-sm inline">
                        <i class="fa fa-refresh"></i>
                    </a>
                </div>
            </form>
        </div>
        <h3>
            <i class="glyphicon glyphicon-dashboard"></i>&nbsp; Eshop Admin Dashboard
        </h3>
    </div><!-- /.main-bar -->
</header><!-- /.head -->


@push('after_scripts')
<link rel="stylesheet" href="{{ asset('vendor/adminlte') }}/plugins/daterangepicker/daterangepicker.css">
<script src="{{ asset('vendor/adminlte') }}/plugins/daterangepicker/moment.js"></script>
<script src="{{ asset('vendor/adminlte') }}/plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">
    jQuery(function () {
        jQuery('#daterange-btn').daterangepicker(
            {
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate  : moment()
            },
            function (start, end) {
                jQuery('#daterange-btn span text')   .html(start.format('DD/MM/YYYY') + '-' + end.format('DD/MM/YYYY'));
                jQuery('#daterange-input')           .val (start.format('DD/MM/YYYY') + '-' + end.format('DD/MM/YYYY'));
            }
        );
    })
</script>
@endpush