<div class="box">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>
    <script src="https://cdn.jsdelivr.net/npm/fusioncharts@3.12.2/fusioncharts.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.2/echarts-en.min.js" charset="utf-8"></script>
    <script src="https://cdn.jsdelivr.net/npm/frappe-charts@1.1.0/dist/frappe-charts.min.iife.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/5.7.0/d3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.6.7/c3.min.js"></script>
	<?php
	//                ff(get_array_values_as_dates($charts['orders'], 'create_date', 'd/m'));
	//                ff(get_array_values_as_numbers($charts['orders'], 'count'));
	//                ff(get_array_values_as_euro($charts['orders'], 'items_cost_sum'));
	//                ff(get_array_values_as_euro($charts['orders'], 'items_cost_median'));
	$chart = new ConsoleTVs\Charts\Classes\Frappe\Chart();
	$chart->labels(get_array_values_as_dates($charts['orders'], 'create_date', 'd/m'));
	$chart->dataset('# Orders', 'line', get_array_values_as_numbers($charts['orders'], 'count'));
	$chart->dataset('# Items', 'line', get_array_values_as_euro($charts['orders'], 'items_cost_sum'));
	$chart->dataset('Total Median', 'line', get_array_values_as_euro($charts['orders'], 'items_cost_median'));
	?>
    {!! $chart->container() !!}
    {!! $chart->script() !!}
</div>

@push('after_scripts')
<link rel="stylesheet" href="{{ asset('vendor/adminlte') }}/plugins/daterangepicker/daterangepicker.css">
<script src="{{ asset('vendor/adminlte') }}/plugins/daterangepicker/moment.js"></script>
<script src="{{ asset('vendor/adminlte') }}/plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">
    jQuery(function () {
        jQuery('#daterange-btn').daterangepicker(
            {
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate  : moment()
            },
            function (start, end) {
                jQuery('#daterange-btn span text')   .html(start.format('DD/MM/YYYY') + '-' + end.format('DD/MM/YYYY'));
                jQuery('#daterange-input')           .val (start.format('DD/MM/YYYY') + '-' + end.format('DD/MM/YYYY'));
            }
        );
    })
</script>
@endpush