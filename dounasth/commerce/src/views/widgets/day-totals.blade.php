<div class="box">
    <div class="box-header">
        <h3 class="box-title">
            {{trans('imeras.title')}}
            {{ printDateRange($daterange, $today) }}
        </h3>
    </div>
    <div class="body ">
        <div class="row-fluid clearfix">
            <div class="col-sm-12 col-xs-12 margin-bottom-small">
                <table class="table table-responsive table-condensed table-striped" width="100%" border="0" align="center">
                    <thead>
                    <tr>
                        <th rowspan="2" class="text-center">&nbsp;</th>
                        <th colspan="3" class="text-center text-info">Είδη</th>
                        <th colspan="3" class="text-center text-warning">Πληρ</th>
                        <th colspan="3" class="text-center text-danger">Μεταφ</th>
                        <th colspan="3" class="text-center text-success">Σύνολο</th>
                    </tr>
                    <tr>
                        <th class="text-center text-info">ΕΣ</th>
                        <th class="text-center text-info">ΕΞ</th>
                        <th class="text-center text-info">ΚΡΔ</th>

                        <th class="text-center text-warning">ΕΣ</th>
                        <th class="text-center text-warning">ΕΞ</th>
                        <th class="text-center text-warning">ΚΡΔ</th>

                        <th class="text-center text-danger">ΕΣ</th>
                        <th class="text-center text-danger">ΕΞ</th>
                        <th class="text-center text-danger">ΚΡΔ</th>

                        <th class="text-center text-success">ΕΣ</th>
                        <th class="text-center text-success">ΕΞ</th>
                        <th class="text-center text-success">ΚΡΔ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-center">Καθαρό</td>

                        <td class="text-center text-info"> {!! euro( $todayOrderTotals->sum('items_in_net') )  !!} </td>
                        <td class="text-center text-info"> {!! euro( $todayOrderTotals->sum('items_out_net') )  !!} </td>
                        <td class="text-center text-info"> {!! euro( $todayOrderTotals->sum('items_in_net') - $todayOrderTotals->sum('items_out_net') )  !!} </td>

                        <td class="text-center text-warning"> {!! euro( $todayOrderTotals->sum('payment_in_net') )  !!} </td>
                        <td class="text-center text-warning"> {!! euro( $todayOrderTotals->sum('payment_out_net') )  !!} </td>
                        <td class="text-center text-warning"> {!! euro( $todayOrderTotals->sum('payment_in_net') - $todayOrderTotals->sum('payment_out_net') )  !!} </td>

                        <td class="text-center text-danger"> {!! euro( $todayOrderTotals->sum('shipping_in_net') )  !!} </td>
                        <td class="text-center text-danger"> {!! euro( $todayOrderTotals->sum('shipping_out_net') )  !!} </td>
                        <td class="text-center text-danger"> {!! euro( $todayOrderTotals->sum('shipping_in_net') - $todayOrderTotals->sum('shipping_out_net') )  !!} </td>

                        <td class="text-center text-success"> {!! euro( $todayOrderTotals->sum('total_in_net') )  !!} </td>
                        <td class="text-center text-success"> {!! euro( $todayOrderTotals->sum('total_out_net') )  !!} </td>
                        <td class="text-center text-success"> {!! euro( $todayOrderTotals->sum('total_in_net') - $todayOrderTotals->sum('total_out_net') )  !!} </td>
                    </tr>
                    <tr>
                        <td class="text-center">ΦΠΑ</td>

                        <td class="text-center text-info"> {!! euro( $todayOrderTotals->sum('items_in_vat') )  !!} </td>
                        <td class="text-center text-info"> {!! euro( $todayOrderTotals->sum('items_out_vat') )  !!} </td>
                        <td class="text-center text-info"> {!! euro( $todayOrderTotals->sum('items_in_vat') - $todayOrderTotals->sum('items_out_vat') )  !!} </td>

                        <td class="text-center text-warning"> {!! euro( $todayOrderTotals->sum('payment_in_vat') )  !!} </td>
                        <td class="text-center text-warning"> {!! euro( $todayOrderTotals->sum('payment_out_vat') )  !!} </td>
                        <td class="text-center text-warning"> {!! euro( $todayOrderTotals->sum('payment_in_vat') - $todayOrderTotals->sum('payment_out_vat') )  !!} </td>

                        <td class="text-center text-danger"> {!! euro( $todayOrderTotals->sum('shipping_in_vat') )  !!} </td>
                        <td class="text-center text-danger"> {!! euro( $todayOrderTotals->sum('shipping_out_vat') )  !!} </td>
                        <td class="text-center text-danger"> {!! euro( $todayOrderTotals->sum('shipping_in_vat') - $todayOrderTotals->sum('shipping_out_vat') )  !!} </td>

                        <td class="text-center text-success"> {!! euro( $todayOrderTotals->sum('total_in_vat') )  !!} </td>
                        <td class="text-center text-success"> {!! euro( $todayOrderTotals->sum('total_out_vat') )  !!} </td>
                        <td class="text-center text-success"> {!! euro( $todayOrderTotals->sum('total_in_vat') - $todayOrderTotals->sum('total_out_vat') )  !!} </td>
                    </tr>
                    <tr>
                        <td class="text-center">Σύνολο</td>

                        <td class="text-center text-info"> {!! euro( $todayOrderTotals->sum('items_in_net') + $todayOrderTotals->sum('items_in_vat') )  !!} </td>
                        <td class="text-center text-info"> {!! euro( $todayOrderTotals->sum('items_out_net') + $todayOrderTotals->sum('items_out_vat') )  !!} </td>
                        <td class="text-center text-info"> <b>{!! euro( $todayOrderTotals->sum('items_in_net') + $todayOrderTotals->sum('items_in_vat') - ( $todayOrderTotals->sum('items_out_net') + $todayOrderTotals->sum('items_out_vat') ) )  !!}</b> </td>

                        <td class="text-center text-warning"> {!! euro( $todayOrderTotals->sum('payment_in_net') + $todayOrderTotals->sum('payment_in_vat') )  !!} </td>
                        <td class="text-center text-warning"> {!! euro( $todayOrderTotals->sum('payment_out_net') + $todayOrderTotals->sum('payment_out_vat') )  !!} </td>
                        <td class="text-center text-warning"> <b> {!! euro( $todayOrderTotals->sum('payment_in_net') + $todayOrderTotals->sum('payment_in_vat') - ( $todayOrderTotals->sum('payment_out_net') + $todayOrderTotals->sum('payment_out_vat') ) )  !!} </b> </td>

                        <td class="text-center text-danger"> {!! euro( $todayOrderTotals->sum('shipping_in_net') + $todayOrderTotals->sum('shipping_in_vat') )  !!} </td>
                        <td class="text-center text-danger"> {!! euro( $todayOrderTotals->sum('shipping_out_net') + $todayOrderTotals->sum('shipping_out_vat') )  !!} </td>
                        <td class="text-center text-danger"> <b> {!! euro( $todayOrderTotals->sum('shipping_in_net') + $todayOrderTotals->sum('shipping_in_vat') - ( $todayOrderTotals->sum('shipping_out_net') + $todayOrderTotals->sum('shipping_out_vat') ) )  !!} </b> </td>

                        <td class="text-center text-success"> {!! euro( $todayOrderTotals->sum('total_in_net') + $todayOrderTotals->sum('total_in_vat') )  !!} </td>
                        <td class="text-center text-success"> {!! euro( $todayOrderTotals->sum('total_out_net') + $todayOrderTotals->sum('total_out_vat') )  !!} </td>
                        <td class="text-center text-success"> <b> {!! euro( $todayOrderTotals->sum('total_in_net') + $todayOrderTotals->sum('total_in_vat') - ( $todayOrderTotals->sum('total_out_net') + $todayOrderTotals->sum('total_out_vat') ) )  !!} </b> </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>