<?php
$logo_base64 = 'data:image/png;base64, ';//.base64_encode(file_get_contents(APP_URL.LOGO_IMAGE))
?>

<table border="0" cellspacing="0" cellpadding="0" width="700" align="center">
    <tr>
        <td>
            <p style="text-align: center;">
                <img src="{{ $logo_base64 }}" />
            </p>
            <hr/>
            <br/>
            <h3 style="margin: 0px;">Παραγγελια #<a href="{{route('order.view', [$order->hash])}}">{{$order->hash}}</a></h3>
            <br/>
            Κατάσταση: {{$order->status->title}}<br/>
            <br/>
            {!! $order->status->mail !!}<br/>
            <br/>
        </td>
    </tr>
</table>

<div class="page-break"></div>

{{--@include('emails.invoice')--}}