<style type="text/css">
    * {
        font-family: Arial !important;
    }
</style>

<?php
$logo_base64 = 'data:image/png;base64, ';//.base64_encode(file_get_contents(APP_URL.LOGO_IMAGE));
?>

<table border="1" cellspacing="0" cellpadding="10" width="700" align="center">
    <tr>
        <td align="center">
            <table border="0" cellspacing="5" cellpadding="5" width="100%" align="center">
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
                            <tr>
                                <td>
                                    <img height="40" src="{{ $logo_base64 }}" />
                                </td>
                                <td align="right" valign="center" style="font-size: 12px;">
                                    <br/>
                                    {{ config('app.name') }}<br/>
                                    {{--{{ $thisSite->billing_info['eponymia'] }}<br/>--}}
                                    {{--@if (fn_is_not_empty(trim($thisSite->billing_info['drastiriotita'])))--}}
                                        {{--{{ $thisSite->billing_info['drastiriotita'] }}<br/>--}}
                                    {{--@endif--}}
                                    {{--{{ $thisSite->billing_info['address'] }}<br/>--}}
                                    {{--Α.Φ.Μ.: {{ $thisSite->billing_info['afm'] }}<br/>--}}
                                    {{--Δ.Ο.Υ.:{{ $thisSite->billing_info['doy'] }}<br/>--}}
                                    {{--@if (fn_is_not_empty(trim($thisSite->billing_info['contact_tel'])))--}}
                                        {{--{{ $thisSite->billing_info['contact_tel'] }}<br/>--}}
                                    {{--@endif--}}
                                    {{--{{ $thisSite->billing_info['contact_mail'] }}<br/>--}}
                                    <br/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td><hr style="margin: 0;"/></td></tr>
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
                            <tr>
                                <td>
                                    <h2 style="margin: 0;">{{ trans('site.order.invoice_type_'.$invoice->invoice_type) }}</h2>
                                </td>
                                <td align="right" valign="center">
                                    {{ trans('site.order.num_of_invoice_type_'.strtoupper($invoice->invoice_type)) }}: <b style="font-size: 25px;">#{{ $invoice->invoice_num }}</b>
                                </td>
                            </tr>
                            @if ($invoice->is_cancel)
                            <tr>
                                <td colspan="2" style="font-size: 12px;">
                                    Ακυρώνει:
                                    <a href="/admin/manage/orders/invoice/{{$invoice->is_cancel}}">
                                        {{ trans('site.order.invoice_type_'.strtoupper($invoice->cancels_type)) }}: #{{ $invoice->cancels_num }}
                                    </a>
                                </td>
                            </tr>
                            @endif
                        </table>
                    </td>
                </tr>
                <tr><td><hr style="margin: 0;"/></td></tr>
                <tr>
                    <td>
                        Κωδικός Παραγγ.: <b>{{$invoice->order_data->hash}}</b>
                    </td>
                </tr>
                <tr><td><hr style="margin: 0;"/></td></tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td valign="top" width="50%">
                                    <h4>Στοιχεία Αποστολής</h4>
                                    <table border="0" cellspacing="3" cellpadding="0" width="100%">
                                        <tr>
                                            <td>Όνομα:</td>
                                            <td>{{$invoice->addresses['firstname']}}</td>
                                        </tr>
                                        <tr>
                                            <td>Επώνυμο:</td>
                                            <td>{{$invoice->addresses['surname']}}</td>
                                        </tr>
                                        <tr>
                                            <td>E-mail:</td>
                                            <td>{{$invoice->addresses['email']}}</td>
                                        </tr>
                                        <tr>
                                            <td>Τηλ Σταθερό:</td>
                                            <td>{{$invoice->addresses['phone']}}</td>
                                        </tr>
                                        <tr>
                                            <td>Τηλ Κινητό:</td>
                                            <td>{{$invoice->addresses['phone_mobile']}}</td>
                                        </tr>
                                        <tr>
                                            <td>Διεύθυνση:</td>
                                            <td>{{$invoice->addresses['address']}}</td>
                                        </tr>
                                        <tr>
                                            <td>ΤΚ:</td>
                                            <td>{{$invoice->addresses['zip']}}</td>
                                        </tr>
                                        <tr>
                                            <td>Πόλη:</td>
                                            <td>{{$invoice->addresses['city']}}</td>
                                        </tr>
                                        <tr>
                                            <td>Νομός:</td>
                                            <td>{{ $invoice->theState() }}</td>
                                        </tr>
                                        <tr>
                                            <td>Χώρα:</td>
                                            <td>{{ $invoice->theCountry() }}</td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" width="50%">
                                    <h4>Στοιχεία Χρέωσης</h4>
                                    @if (!$invoice->addresses['other_billing_address'])
                                        Ίδια με τα στοιχεία αποστολής
                                    @else
                                        <table border="0" cellspacing="3" cellpadding="0" width="100%">
                                            <tr>
                                                <td>Όνομα:</td>
                                                <td>{{$invoice->addresses['billing_firstname']}}</td>
                                            </tr>
                                            <tr>
                                                <td>Επώνυμο:</td>
                                                <td>{{$invoice->addresses['billing_surname']}}</td>
                                            </tr>
                                            <tr>
                                                <td>Διεύθυνση:</td>
                                                <td>{{$invoice->addresses['billing_address']}}</td>
                                            </tr>
                                            {{--<tr>--}}
                                            {{--<td>Διεύθυνση (γραμμή 2):</td>--}}
                                            {{--<td>{{$invoice->addresses['billing_address2']}}</td>--}}
                                            {{--</tr>--}}
                                            <tr>
                                                <td>ΤΚ:</td>
                                                <td>{{$invoice->addresses['billing_zip']}}</td>
                                            </tr>
                                            <tr>
                                                <td>Πόλη:</td>
                                                <td>{{$invoice->addresses['billing_city']}}</td>
                                            </tr>
                                            <tr>
                                                <td>Νομός:</td>
                                                <td>{{ $invoice->theBillingState() }}</td>
                                            </tr>
                                            <tr>
                                                <td>Χώρα:</td>
                                                <td>{{ $invoice->theBillingCountry() }}</td>
                                            </tr>
                                        </table>
                                    @endif

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td><hr/></td></tr>
                <tr>
                    <td>
                        <h4>Είδη Παραγγελίας</h4>
                        <table border="0" cellspacing="" cellpadding="0" width="100%">
                            <tr>
                                <td colspan="2"></td>
                                <td><b>Ποσότητα</b></td>
                                <td><b>Τιμή</b></td>
                                <td><b>Σύνολο</b></td>
                            </tr>
                            @foreach ($invoice->order_data->items as $item)
                                <tr>
                                    <td>
                                        <img border="0" class="img-thumbnail" src="{{$item->model->mainPhoto()->photon(100)}}"/>
                                    </td>
                                    <td>
                                        {{ $item->model->title }}
                                    </td>
                                    <td>
                                        {{$item->quantity}}
                                    </td>
                                    <td>
                                        {!! euro($item->price) !!}
                                    </td>
                                    <td>
                                        {!! euro($item->quantity * $item->price) !!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </td>
                </tr>
                <tr><td><hr/></td></tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td width="60%">
                                    <p style="line-height: 24px;">
                                        <b>Τρόπος Πληρωμής</b><br/>
                                        {{$invoice->payment->name}} / Κοστος: {!! euro($invoice->payment->cost)  !!}
                                    </p>
                                    <p style="line-height: 24px;">
                                        <b>Τρόπος Αποστολής</b><br/>
                                        {{$invoice->shipping->name}} :: {{$invoice->shipping->estimated_time}} / Κόστος: {!! euro($invoice->shipping->cost)  !!}
                                    </p>
                                </td>
                                <td width="40%">
                                    <table border="0" width="100%">
                                        <tr>
                                            <td>Καθαρό σύνολο ειδών:</td>
                                            <td align="right">{!! euro($invoice->order_data->totals->items_in_net)  !!}</td>
                                        </tr>
                                        <tr>
                                            <td>ΦΠΑ ειδών:</td>
                                            <td align="right">{!! euro($invoice->order_data->totals->items_in_vat)  !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Σύνολο ειδών:</td>
                                            <td align="right">{!! euro($invoice->items_cost)  !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Έξοδα πληρωμής:</td>
                                            <td align="right">{!! euro($invoice->payment_cost)  !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Μεταφορικά:</td>
                                            <td align="right">{!! euro($invoice->shipping_cost)  !!}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Σύνολο παραγγελίας:</b></td>
                                            <td align="right"><b>{!! euro($invoice->total)  !!}</b></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            @if ($invoice->irs_hash)
            <br>
            <p style="text-align: left; font-family: 'Courier New', Courier, monospace;">
            ΠΑΗΨΣ: {{ $invoice->irs_hash }}
            </p>
            @endif

        </td>
    </tr>
</table>

