@extends('backend::layout')

@section('page-title')
Manage Categories
@stop

@section('page-subtitle')
dashboard subtitle, some description must be here
@stop

@section('breadcrumb')
@parent
<li class="active">Manage Categories</li>
@stop

@section('page-menu')
<li role="presentation"><a role="menuitem" tabindex="-1" href="{{ route('cart.categories.list') }}"><i class="fa fa-list"></i> Tree list</a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="{{ route('cart.categories.update') }}"><i class="fa fa-plus"></i> Add a new category</a></li>
<li role="presentation" class="divider"></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="#"><i class="fa fa-trash-o"></i> Delete selected categories</a></li>
<li role="presentation" class="divider"></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="{{ route('cart.categories.trashed') }}"><i class="fa fa-trash-o"></i> Trashed</a></li>

<li role="presentation" class="pull-right"><a role="menuitem" tabindex="-1" href="{{ route('cart.categories.makeoldcats') }}"><i class="fa fa-trash-o"></i> makeoldcats</a></li>
@stop

@section('styles')
<link href="{{ Config::get('backend::general.asset_path') }}/css/jQueryUI/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css" />
<style>
    ul.connectedSortable {
        min-height: 10px;
    }
</style>
@stop

@section('scripts')
<!-- DATA TABES SCRIPT -->
<script src="{{ Config::get('backend::general.asset_path') }}/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
    jQuery(document).ready(function(){
        jQuery( ".connectedSortable" ).sortable({
            connectWith: ".connectedSortable",
            placeholder: "ui-state-highlight",
            greedy: true
        })
        .on( "sortstop", function( e, ui ) {
            e.stopPropagation();
            console.info(e);
            console.info(ui);
            console.info(ui.item);
        } )
        .disableSelection();
        jQuery(".subleveler").click(function(e){
            e.stopPropagation();
            jQuery(this).closest('li').children('ul').slideToggle();
            if (jQuery(this).find('i').hasClass('fa-level-down')) {
                jQuery(this).find('i').switchClass('fa-level-down', 'fa-level-up');
            }
            else {
                jQuery(this).find('i').switchClass('fa-level-up', 'fa-level-down');
            }
        }).click();
    });
</script>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-body table-responsive">
        @include('laracart::categories.list-ul', array('categories'=>$categories, 'prefix'=>''))
    </div>
</div>
@stop