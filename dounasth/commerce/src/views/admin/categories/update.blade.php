@extends('backend::layout')

@section('page-title')
{{ !$row->id ? 'Add' : 'Edit' }} Category
@stop

@section('page-subtitle')
{{ $row->name }} ({{ $row->id }})
@stop

@section('breadcrumb')
@parent
<li><a href="{{ route('commerce.categories.index') }}" class="goOnCancel"><i class="fa fa-group"></i> Manage Categories</a></li>
<li class="active">{{ !$row->id ? 'Add' : 'Edit' }} Category</li>
@stop

@section('page-menu')
@stop

@include('crud::parts.show_fields_scripts')

@section('content')
<div class="box box-primary">
    {{ Form::open(array('route' => ['commerce.categories.save', $row->id], 'method' => 'POST', 'role' => 'form', 'files' => true)) }}
    <div class="box-body">
        <div class="row">
        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                <h2>General</h2>
                <div class="form-group">
                    {{ Form::label('parent_id', 'Parent:') }}
                    <select class="form-control select2-ajax" data-addclass="select2-ajax" id="parent_category" name="category[parent_id]"
                            data-ajax--url="{{ url('/data/models/Dounasth.Commerce.App.Models.Category/select2') }}" data-ajax--cache="true">
                        @if ($row->parent)
                            <option value="{{$row->parent->selectboxValue()}}" selected>{{$row->parent->selectboxText()}}</option>
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    {{ Form::label('title', 'Title:') }}
                    {{ Form::text('category[title]', $row->title, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('slug', 'Slug:') }}
                    {{ Form::text('category[slug]', $row->slug, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('status', 'Status:') }}
                    {{ Form::select('category[status]', array('A' => 'Active', 'D' => 'Disabled'), $row->status, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('keyword', 'Keyword:') }}
                    {{ Form::text('category[keyword]', $row->keyword, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('description', 'Description:') }}
                    {{ Form::textarea('category[description]', $row->description, array('class' => 'form-control')) }}
                </div>
                <h2>SEO</h2>
                <div class="form-group">
                    {{ Form::label('seo-title', 'SEO Title:') }}
                    {{ Form::text('seo[title]', ($row->seo) ? $row->seo->title : '', array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('seo-description', 'Meta Description:') }}
                    {{ Form::textarea('seo[description]', ($row->seo) ? $row->seo->description : '', array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('seo-keywords', 'Meta Keywords:') }}
                    {{ Form::textarea('seo[keywords]', ($row->seo) ? $row->seo->keywords : '', array('class' => 'form-control')) }}
                </div>
        	</div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                <h2>Timestamps</h2>
            	<div class="form-group">
                    {{ Form::label('', 'Updated@: '.$row->updated_at) }}
            	</div>
            	<div class="form-group">
                    {{ Form::label('', 'Created@: '.$row->created_at) }}
            	</div>
            	<div class="form-group">
                    {{--{{ \Dounasth\Backend\Util::keywordio($row->keyword) }}--}}
            	</div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <button class="btn btn-success" type="submit"><i class="fa fa-check-square-o"></i> Save</button>
        <a class="btn btn-danger btn-cancel"><i class="fa fa-square-o"></i> Cancel</a>
        <button class="btn btn-warning pull-right" type="submit" name="saveNew" value="1"><i class="fa fa-plus"></i> Save as new</button>
    </div>
    {{ Form::close() }}
</div>
@stop