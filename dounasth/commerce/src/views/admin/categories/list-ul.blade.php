<ul class="connectedSortable">
@foreach ($categories as $category)
    <li class="">
        <div class="btn-group">
            <a class="btn btn-default subleveler""><i class="fa fa-level-up"></i></a>
            <a class="btn btn-default" href="{{route('cart.categories.update', [$category->id])}}">{{ $prefix }} {{ $category->title }}</a>
            <a class="btn btn-default" href="{{route('cart.categories.update', [$category->id])}}">{{ $category->slug }}</a>
            <a class="btn btn-default" href="{{route('cart.categories.update', [$category->id])}}">{{ @$category->seo->title }}</a>
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu{{$category->id}}" data-toggle="dropdown">
                <i class="fa fa-gear"></i>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu{{$category->id}}">
                @if ($category->trashed())
                <li role="presentation">
                    <a class="" href="{{route('cart.categories.restore', [$category->id])}}"><i class="fa fa-refresh"></i> {{trans('backend::actions.restore')}}</a>
                </li>
                @endif
                <li role="presentation">
                    <a class="" href="{{route('cart.categories.update', [$category->id])}}"><i class="fa fa-edit"></i> {{trans('backend::actions.edit')}}</a>
                </li>
                <li role="presentation" class="divider"></li>
                <li role="presentation">
                    <a class="" href="{{route('cart.categories.delete', [$category->id])}}"><i class="fa fa-trash-o"></i> {{trans('backend::actions.delete')}}</a>
                </li>
            </ul>
        </div>
        @if (fn_is_not_empty($category->children()->get()))
        @include('laracart::categories.list-ul', array('categories'=>$category->children()->get(), 'prefix'=>$prefix.''))
        @endif
    </li>
@endforeach
@if ($categories->count() == 0)
    <li class=""><button type="button" class="btn">no subcategories</button></li>
@endif
</ul>