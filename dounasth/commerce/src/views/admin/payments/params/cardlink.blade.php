<h3>Ρυθμισεις</h3>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="merchant_id">merchant_id:</label>
    <div class="col-lg-9">
    <input type="text" name="params[merchant_id]" id="merchant_id" value="{{$row->params['merchant_id']}}" class="form-control" size="60" />
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="merchant_id">shared_secret_key:</label>
    <div class="col-lg-9">
    <input type="text" name="params[shared_secret_key]" id="shared_secret_key" value="{{$row->params['shared_secret_key']}}" class="form-control" size="60" />
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="currency">currency:</label>
    <div class="col-lg-9">
    <select name="params[currency]" id="currency" class="form-control">
        <option value="EUR"{{ $row->params['currency'] == 'EUR' ? 'selected="selected"' : '' }}>Euro</option>
    </select>
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="lang_code">lang_code:</label>
    <div class="col-lg-9">
    <select name="params[lang_code]" id="lang_code" class="form-control">
        <option value="el"{{ $row->params['lang_code'] == 'el' ? 'selected="selected"' : '' }}>Ελληνικα</option>
    </select>
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="order_prefix">order_prefix:</label>
    <div class="col-lg-9">
    <input type="text" name="params[order_prefix]" id="order_prefix" value="{{$row->params['order_prefix']}}" class="form-control" size="60" />
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="gateway">gateway:</label>
    <div class="col-lg-9">
    <select name="params[gateway]" id="gateway" class="form-control">
        <option value="alphabank_test" {{ $row->params['gateway'] == 'alphabank_test' ? 'selected="selected"' : '' }}>ALPHABANK TEST env (https://alpha.test.modirum.com/vpos/shophandlermpi)</option>
        <option value="alphabank_live" {{ $row->params['gateway'] == 'alphabank_live' ? 'selected="selected"' : '' }}>ALPHABANK env (https://www.alphaecommerce.gr/vpos/shophandlermpi)</option>
        <option value="eurobank_test" {{ $row->params['gateway'] == 'eurobank_test' ? 'selected="selected"' : '' }}>EUROBANK TEST env (https://euro.test.modirum.com/vpos/shophandlermpi)</option>
        <option value="eurobank_live" {{ $row->params['gateway'] == 'eurobank_live' ? 'selected="selected"' : '' }}>EUROBANK env (https://vpos.eurocommerce.gr/vpos/shophandlermpi)</option>
    </select>
    </div>
</div>

<h3>Καταστασεις</h3>

<div id="text_paypal_status_map" class="in collapse">

    <?php $statuses = array_flip( array_merge( ['Επιλεξτε'=>'0']  , \Dounasth\Commerce\App\Models\Order\OrderStatus::withTranslation()->get()->pluck('id', 'title')->toArray() ) ) ?>

    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_captured">{__("captured")}:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][captured]', $statuses, $row->params['statuses']['captured'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>

    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_canceled">{__("canceled")}:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][canceled]', $statuses, $row->params['statuses']['canceled'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>

    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_refused">{__("refused")}:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][refused]', $statuses, $row->params['statuses']['refused'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>

    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_error">{__("error")}:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][error]', $statuses, $row->params['statuses']['error'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>
    
</div>