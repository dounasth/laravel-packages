
<h3>Ρυθμισεις</h3>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="account">Λογαριασμός:</label>
    <div class="col-lg-9">
        <input type="text" class="form-control" name="params[account]" id="account" value="{{$row->params['account']}}" >
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="item_name">Όνομα Είδους (ή ένα Όνομα για το Καλάθι Αγορών).:</label>
    <div class="col-lg-9">
        <input type="text" class="form-control" name="params[item_name]" id="item_name" value="{{$row->params['item_name']}}" >
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="currency">Νόμισμα:</label>
    <div class="col-lg-9">
        <select class="form-control" name="params[currency]" id="currency">
            <option {{ $row->params['currency'] == 'CAD' ? 'selected="selected"' : "" }} value="CAD">Δολάριο Καναδά</option>
            <option {{ $row->params['currency'] == 'EUR' ? 'selected="selected"' : "" }} value="EUR">Ευρώ</option>
            <option {{ $row->params['currency'] == 'GBP' ? 'selected="selected"' : "" }} value="GBP">Λίρα Αγγλίας</option>
            <option {{ $row->params['currency'] == 'USD' ? 'selected="selected"' : "" }} value="USD">Δολάριο ΗΠΑ</option>
            <option {{ $row->params['currency'] == 'JPY' ? 'selected="selected"' : "" }} value="JPY">Ιαπωνικά γεν</option>
            <option {{ $row->params['currency'] == 'RUB' ? 'selected="selected"' : "" }} value="RUB">Ρούβλι Ρωσίας</option>
            <option {{ $row->params['currency'] == 'AUD' ? 'selected="selected"' : "" }} value="AUD">Δολάριο Αυστραλίας</option>
            <option {{ $row->params['currency'] == 'NZD' ? 'selected="selected"' : "" }} value="NZD">Δολάριο Νέας Ζηλανδίας</option>
            <option {{ $row->params['currency'] == 'CHF' ? 'selected="selected"' : "" }} value="CHF">Ελβετικό Φράγκο</option>
            <option {{ $row->params['currency'] == 'HKD' ? 'selected="selected"' : "" }} value="HKD">Δολάριο Χονγκ Κονγκ</option>
            <option {{ $row->params['currency'] == 'SGD' ? 'selected="selected"' : "" }} value="SGD">Δολάριο Σιγκαπούρης</option>
            <option {{ $row->params['currency'] == 'SEK' ? 'selected="selected"' : "" }} value="SEK">Κορόνα Σουηδίας</option>
            <option {{ $row->params['currency'] == 'DKK' ? 'selected="selected"' : "" }} value="DKK">Δανικές κορώνες</option>
            <option {{ $row->params['currency'] == 'PLN' ? 'selected="selected"' : "" }} value="PLN">Πολωνικά Zlotych</option>
            <option {{ $row->params['currency'] == 'NOK' ? 'selected="selected"' : "" }} value="NOK">Κορώνα Νορβηγίας</option>
            <option {{ $row->params['currency'] == 'HUF' ? 'selected="selected"' : "" }} value="HUF">Φιορίνι Ουγγαρίας</option>
            <option {{ $row->params['currency'] == 'CZK' ? 'selected="selected"' : "" }} value="CZK">Κορώνα Τσεχίας</option>
            <option {{ $row->params['currency'] == 'ILS' ? 'selected="selected"' : "" }} value="ILS">Νέο Ισραηλινό Shequel</option>
            <option {{ $row->params['currency'] == 'MXN' ? 'selected="selected"' : "" }} value="MXN">Μεξικάνικο πέσο</option>
            <option {{ $row->params['currency'] == 'BRL' ? 'selected="selected"' : "" }} value="BRL">Βραζιλιάνικο Real</option>
            <option {{ $row->params['currency'] == 'MYR' ? 'selected="selected"' : "" }} value="MYR">Ρίνγκιτ Μαλαισίας</option>
            <option {{ $row->params['currency'] == 'PHP' ? 'selected="selected"' : "" }} value="PHP">Φιλιπινέζικο Peso</option>
            <option {{ $row->params['currency'] == 'TWD' ? 'selected="selected"' : "" }} value="TWD">Νέο Ταϊβανέζικο dollar</option>
            <option {{ $row->params['currency'] == 'THB' ? 'selected="selected"' : "" }} value="THB">Ταϊλανδικό μπατ</option>
            <option {{ $row->params['currency'] == 'TRY' ? 'selected="selected"' : "" }} value="TRY">Τουρκική Λίρα</option>
        </select>
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="mode">Τέστ/Live Mode::</label>
    <div class="col-lg-9">
        <select class="form-control" name="params[mode]" id="mode">
            <option value="test" {{ $row->params['mode'] == "test" ? 'selected="selected"' : '' }}>Test</option>
            <option value="live" {{ $row->params['mode'] == "live" ? 'selected="selected"' : '' }}>Live</option>
        </select>
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="order_prefix">Πρόθεμα παραγγελίας:</label>
    <div class="col-lg-9">
        <input type="text" class="form-control" name="params[order_prefix]" id="order_prefix" value="{{$row->params['order_prefix']}}" >
    </div>
</div>

<h3>Καταστασεις</h3>

<div id="text_paypal_status_map" class="in collapse">

    <?php $statuses = array_flip( array_merge( ['Επιλεξτε'=>'0']  , \Dounasth\Commerce\App\Models\Order\OrderStatus::withTranslation()->get()->pluck('id', 'title')->toArray() ) ) ?>

    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_refunded">refunded:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][refunded]', $statuses, $row->params['statuses']['refunded'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>
    
    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_completed">completed:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][completed]', $statuses, $row->params['statuses']['completed'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>
    
    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_pending">pending:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][pending]', $statuses, $row->params['statuses']['pending'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>
    
    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_canceled_reversal">canceled_reversal:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][canceled_reversal]', $statuses, $row->params['statuses']['canceled_reversal'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>
    
    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_created">created:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][created]', $statuses, $row->params['statuses']['created'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>
    
    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_denied">denied:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][denied]', $statuses, $row->params['statuses']['denied'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>
    
    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_expired">expired:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][expired]', $statuses, $row->params['statuses']['expired'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>
    
    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_reversed">reversed:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][reversed]', $statuses, $row->params['statuses']['reversed'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>
    
    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_processed">processed:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][processed]', $statuses, $row->params['statuses']['processed'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>
    
    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_voided">voided:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][voided]', $statuses, $row->params['statuses']['voided'], array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>
</div>