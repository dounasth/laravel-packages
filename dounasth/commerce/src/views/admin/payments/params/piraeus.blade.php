<h3>Ρυθμισεις</h3>

<hr>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="posid">URLs:</label>
    <div class="col-lg-9">
        website_url:    {{ $_SERVER['HTTP_HOST'] }}/<br/>
        referrer_url:   {{ $_SERVER['HTTP_HOST'] }}/order/pay<br/>
        success:        {{ $_SERVER['HTTP_HOST'] }}/order/notify/notify?id=stored_piraeus_orderid<br/>
        failure:        {{ $_SERVER['HTTP_HOST'] }}/order/notify/notify?id=stored_piraeus_orderid<br/>
        cancel:         {{ $_SERVER['HTTP_HOST'] }}/order/notify/cancel?id=stored_piraeus_orderid<br/>
    </div>
</div>
<hr>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="acquirerid">acquirerid:</label>
    <div class="col-lg-9">
        <input type="text" name="params[acquirerid]" id="acquirerid" value="{{$row->params['acquirerid']}}"  size="60">
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="merchantid">merchantid:</label>
    <div class="col-lg-9">
        <input type="text" name="params[merchantid]" id="merchantid" value="{{$row->params['merchantid']}}"  size="60">
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="posid">posid:</label>
    <div class="col-lg-9">
        <input type="text" name="params[posid]" id="posid" value="{{$row->params['posid']}}"  size="60">
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="username">username:</label>
    <div class="col-lg-9">
        <input type="text" name="params[username]" id="username" value="{{$row->params['username']}}"  size="60">
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="password">password:</label>
    <div class="col-lg-9">
        <input type="text" name="params[password]" id="password" value="{{$row->params['password']}}"  size="60">
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="requesttype">requesttype:</label>
    <div class="col-lg-9">
        <select name="params[requesttype]" id="requesttype">
            <option value="02" {{ ($row->params['requesttype'] == "02") ? 'selected="selected"' : '' }}>sale</option>
            <option value="00" {{ ($row->params['requesttype'] == "00") ? 'selected="selected"' : '' }}>preauthorization</option>
        </select>
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="expirepreauth">expirepreauth:</label>
    <div class="col-lg-9">
        <input type="text" name="params[expirepreauth]" id="expirepreauth" value="{{$row->params['expirepreauth']}}"  size="60">
        <p><small>expirepreauth_description</small></p>
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="currencycode">currencycode:</label>
    <div class="col-lg-9">
        <select name="params[currencycode]" id="currencycode">
            <option value="978" {{ ($row->params['currencycode'] == "978") ? 'selected="selected"' : '' }}>currency_code_eur</option>
        </select>
    </div>
</div>

<div class="form-group clearfix">
    <label class="control-label col-lg-3" for="languagecode">language:</label>
    <div class="col-lg-9">
        <select name="params[languagecode]" id="languagecode">
            <option value="el-GR" {{ ($row->params['languagecode'] == "el-GR") ? 'selected="selected"' : '' }}>greek</option>
            <option value="en-US" {{ ($row->params['languagecode'] == "en-US") ? 'selected="selected"' : '' }}>english</option>
        </select>
    </div>
</div>


<h3>Καταστασεις</h3>

<div id="text_paypal_status_map" class="in collapse">

    <?php $statuses = array_flip( array_merge( ['Επιλεξτε'=>'0']  , \Dounasth\Commerce\App\Models\Order\OrderStatus::withTranslation()->get()->pluck('id', 'title')->toArray() ) ) ?>

    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_captured">{__("captured")}:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][captured]', $statuses, (isset($row->params['statuses'])) ? $row->params['statuses']['captured'] : '' , array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>

    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_canceled">{__("canceled")}:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][canceled]', $statuses, (isset($row->params['statuses'])) ? $row->params['statuses']['canceled'] : '', array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>

    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_refused">{__("refused")}:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][refused]', $statuses,  (isset($row->params['statuses'])) ? $row->params['statuses']['refused'] : '' , array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>

    <div class="form-group clearfix">
        <label class="control-label col-lg-3" for="elm_paypal_error">{__("error")}:</label>
        <div class="col-lg-9">
            {{ Form::select('params[statuses][error]', $statuses, (isset($row->params['statuses'])) ? $row->params['statuses']['error'] : '', array('class'=>'form-control chzn-select', 'size'=>1)) }}
        </div>
    </div>
</div>