@extends('backend::layout')

@section('page-title')
    {{ !$row->id ? 'Add' : 'Editing' }}
@stop

@section('page-subtitle')
    {{ !$row->id ? 'Add' : "$row->name ($row->id)" }}
@stop

@push('breadcrumb')
<li><a href="{{ route($crud->routesPrefix.'index') }}" class="goOnCancel"><i class="fa fa-group"></i> Manage Payments</a></li>
<li class="active">{{ !$row->id ? 'Add' : 'Edit' }} Payment</li>
@endpush

@section('page-menu')
@stop

@push('after_styles')
<link href="{{ asset('vendor/adminlte/plugins/select2/select2.css') }}" rel="stylesheet">
@endpush

@push('after_scripts')
@endpush

@section('content')
{{ Form::open(array('route' => [$crud->routesPrefix.'save', $row->getKey()], 'method' => 'POST', 'role' => 'form', 'enctype'=>'multipart/form-data')) }}
@include('crud::parts.show_fields', ['fields' => $fields['update']])

@if ($row->admin_file && view()->exists($row->admin_file))
    @include($row->admin_file)
@endif

<div class="box-footer">
    <button class="btn btn-success" type="submit"><i class="fa fa-check-square-o"></i> Save</button>
    <a class="btn btn-danger btn-cancel"><i class="fa fa-square-o"></i> Cancel</a>
    <button class="btn btn-warning pull-right" type="submit" name="saveNew" value="1"><i class="fa fa-plus"></i> Save as new</button>
</div>
{{ Form::close() }}
@stop