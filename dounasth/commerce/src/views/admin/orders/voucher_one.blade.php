

<div class="input-group" style="{{$styles or ''}}">
    <input type="text" name="voucher" value="{{$order->voucher or ''}}" placeholder="voucher" class="form-control input-sm" />
    <div class="input-group-btn btn-group btn-group-sm {{ isset($dropup) && $dropup ? 'dropup' : '' }}">
        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu dropdown-menu-right list-group carriers-dropdown">
            @foreach ( Config::get('admin.carriers.active', []) as $carrier )
                <li>
                    <div class="btn-group" role="group" aria-label="...">
                        <a class="btn btn-default btn-sm">
                            <input type="radio" id="{{$carrier['id']}}_radio" name="carrier" value="{{$carrier['id']}}" {{ $order->carrier == $carrier['id'] ? 'checked' : '' }}>
                        </a>
                        <label class="btn btn-default" for="{{$carrier['id']}}_radio">{{$carrier['name']}}</label>
                        <a class="btn btn-default make-voucher" data-orderid="{{$order->id}}"><i class="fa fa-refresh"></i></a>
                    </div>
                </li>
            @endforeach
            <li>
                <div class="btn-group" role="group" aria-label="...">
                    <a class="btn btn-default btn-sm">
                        <input type="radio" id="other_radio" name="carrier" value="other" {{ !in_array($order->carrier, Config::get('admin.carriers.keys', [])) ? 'checked' : '' }} data-order-id="{{$order->id}}">
                    </a>
                    <label class="btn btn-default" for="other_radio">Άλλο</label>
                    <a onclick="return false;"></a>
                </div>
            </li>
            {{--<li role="separator" class="divider"></li>--}}
            {{--<li><a href="#">Separated link</a></li>--}}
        </ul>
    </div>
</div>