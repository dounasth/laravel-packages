<?php
use Dounasth\Commerce\App\Models\Order\OrderStatus;
$statuses = OrderStatus::all();
$selected_status = (isset($selected_status)) && (is_numeric($selected_status)) ? $selected_status : ( isset($selected_status->id) ? $selected_status->id : false );
$selected_status_obj = OrderStatus::find($selected_status);
$selected_statuses = (isset($selected_statuses)) && (is_array($selected_statuses)) ? $selected_statuses : [];
$float = isset($float) ? $float : 'none';
$title = isset($title) ? $title : 'Κατάσταση:';
$no_title = isset($no_title) ? true : false;
$multiple = isset($multiple) && $multiple ? 'multiple' : '';
$name = isset($name) && fn_is_not_empty($name) ? $name : 'status_id';
$varname = isset($name) && fn_is_not_empty($name) ? "name='{$name}'" : '';
$select2 = (isset($no_select2)) ? !$no_select2 : false;
?>
<span style="float: {{$float}};">
    @if (!$no_title)
    {{$title}}
    @endif
    <select {{$varname}} class="form-control {{ $select2 ? 'chzn-select' : '' }}" {{ $multiple }} data-placeholder="{{ $placeholder or 'Επιλέξτε' }}" style="color: {{ $selected_status_obj->color or 'black' }};">
        @foreach ($statuses as $orstatus)
            <option {{($selected_status == $orstatus->id || in_array($orstatus->id, $selected_statuses)) ? 'selected="selected"' : ''}} value="{{$orstatus->id}}"
                    style="color: {{ $orstatus->color or 'black' }};"
            >
                {{$orstatus->title}}
            </option>
        @endforeach
    </select>
</span>