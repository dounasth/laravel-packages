@extends('backend::layout')

@push('after_scripts')
<script type="text/javascript">
    jQuery(document).ready(function(){

        jQuery('.position-setter').bind('change', function(e){
            var url = '/admin/updatePosition/'+jQuery(this).data('object')+'/'+jQuery(this).data('id')+'/'+(jQuery(this).val()*1);
            jQuery.get(url, function(data){
                if (data.error == 0) {
                    jQuery('input[data-id='+data.obj_id+']').css({'border-color': 'green'});
                    jQuery('input[data-id='+data.obj_id+']').val(data.position);
                }
                else if (data.error == 1) {
                    jQuery('input[data-id='+data.obj_id+']').css({'border-color': 'red'});
                }
            }, 'json');
        });
    });
</script>
@endpush

@section('content')

<form class="form-horizontal" action="{{route('commerce.orders.index', [$row->id])}}" method="post">
<header class="head">
    <div class="pull-right">
        <button class="btn btn-primary btn-lg">{{ trans('generic.save') }}</button>
    </div>
    <div class="main-bar">
        <h3><i class="glyphicon glyphicon-dashboard"></i>&nbsp; Manage Orders</h3>
    </div><!-- /.main-bar -->
</header><!-- /.head -->

<div class="box dark">
    <header>
        <h5 style="width: 100%;">
            @include('commerce::admin.orders.order_statuses', array('selected_status'=>$row->status, 'float'=>'right'))
            Παραγγελία #{{$row->id}} (<a target="_blank" href="{{ route('order.view', [$row->hash]) }}">{{$row->hash}}</a>)
            <div class="btn-group">
                <a class="btn btn-default btn-xs" href="{{ route('commerce.order.previewStatusMail', [$row->id]) }}" target="_blank">
                    {{ trans('admin.orders.previewStatusMail') }}
                </a>
                <a class="btn btn-default btn-xs" href="{{ route('commerce.order.renotify', [$row->id]) }}">
                    {{ trans('admin.orders.renotify') }}
                </a>
            </div>
        </h5>
    </header>
    <div class="body collapse in">
        <div class="clearfix">

        </div>
        <hr>
        <div class="row-fluid clearfix">
            <div class="col-lg-3">

                <h3>Στοιχεια Αποστολης</h3><hr/>
                <div class="clearfix">
                    <div class="form-group clearfix">
                        <label class="control-label text-left col-lg-4">Ονομα:</label>
                        <div class="col-lg-8 checkbox address-field">{{$row->addresses['firstname']}}</div>
                        <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['firstname']}}" style="display: none;" />
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label text-left col-lg-4">Επωνυμο:</label>
                        <div class="col-lg-8 checkbox address-field">{{$row->addresses['surname']}}</div>
                        <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['surname']}}" style="display: none;" />
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label text-left col-lg-4">E-mail:</label>
                        <div class="col-lg-8 checkbox address-field">{{$row->addresses['email']}}</div>
                        <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['email']}}" style="display: none;" />
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label text-left col-lg-4">Τηλ Σταθερό:</label>
                        <div class="col-lg-8 checkbox address-field">{{$row->addresses['phone']}}</div>
                        <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['phone']}}" style="display: none;" />
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label text-left col-lg-4">Τηλ Κινητό:</label>
                        <div class="col-lg-8 checkbox address-field">{{$row->addresses['phone_mobile']}}</div>
                        <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['phone_mobile']}}" style="display: none;" />
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label text-left col-lg-4">Διευθυνση:</label>
                        <div class="col-lg-8 checkbox address-field">{{$row->addresses['address']}}</div>
                        <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['address']}}" style="display: none;" />
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label text-left col-lg-4">ΤΚ:</label>
                        <div class="col-lg-8 checkbox address-field">{{$row->addresses['zip']}}</div>
                        <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['zip']}}" style="display: none;" />
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label text-left col-lg-4">Πολη:</label>
                        <div class="col-lg-8 checkbox address-field">{{$row->addresses['city']}}</div>
                        <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['city']}}" style="display: none;" />
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label text-left col-lg-4">Νομος:</label>
                        <div class="col-lg-8 checkbox address-field address-field">{{ $row->theState() }}</div>
                        <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['county']}}" style="display: none;" />
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label text-left col-lg-4">Χωρα:</label>
                        <div class="col-lg-8 checkbox address-field">{{ $row->theCountry() }}</div>
                        <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['country']}}" style="display: none;" />
                    </div>
                </div>

                <h3>Παραστατικό</h3><hr/>
                <div class="clearfix">
                    <div class="form-group clearfix">
                        <label class="control-label text-left col-xs-6">{{trans('site.order.invoice_type')}}:</label>
                        <div class="col-lg-6 checkbox">{{trans('site.order.invoice_type_'.$row->addresses['invoice_type'])}}</div>
                    </div>
                    @if ($row->addresses['invoice_type'] == 't')
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-xs-6">{{trans('site.order.job_title')}}:</label>
                            <div class="col-lg-6 checkbox">{{$row->addresses['job_title']}}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-xs-6">{{trans('site.order.job_afm')}}:</label>
                            <div class="col-lg-6 checkbox">{{$row->addresses['job_afm']}}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-xs-6">{{trans('site.order.job_doy')}}:</label>
                            <div class="col-lg-6 checkbox">{{$row->addresses['job_doy']}}</div>
                        </div>
                    @endif
                </div>

                <h3>Στοιχεια Χρεωσης</h3><hr/>
                <div class="clearfix">
                    @if (!$row->addresses['other_billing_address'])
                        <p>Ιδια με τα στοιχεια αποστολης</p>
                    @else
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-lg-4">Ονομα:</label>
                            <div class="col-lg-8 checkbox address-field">{{$row->addresses['billing_firstname']}}</div>
                            <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['billing_firstname']}}" style="display: none;" />
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-lg-4">Επωνυμο:</label>
                            <div class="col-lg-8 checkbox address-field">{{$row->addresses['billing_surname']}}</div>
                            <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['billing_surname']}}" style="display: none;" />
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-lg-4">Διευθυνση:</label>
                            <div class="col-lg-8 checkbox address-field">{{$row->addresses['billing_address']}}</div>
                            <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['billing_address']}}" style="display: none;" />
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-lg-4">ΤΚ:</label>
                            <div class="col-lg-8 checkbox address-field">{{$row->addresses['billing_zip']}}</div>
                            <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['billing_zip']}}" style="display: none;" />
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-lg-4">Πολη:</label>
                            <div class="col-lg-8 checkbox address-field">{{$row->addresses['billing_city']}}</div>
                            <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['billing_city']}}" style="display: none;" />
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-lg-4">Νομος:</label>
                            <div class="col-lg-6 checkbox">{{ $row->theBillingState() }}</div>
                            <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['billing_county']}}" style="display: none;" />
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-lg-4">Χωρα:</label>
                            <div class="col-lg-8 checkbox address-field">{{ $row->theBillingCountry() }}</div>
                            <input type="text" class="col-lg-8 checkbox address-field" value="{{$row->addresses['billing_country']}}" style="display: none;" />
                        </div>
                    @endif
                </div>

            </div>
            <div class="col-lg-6">
                <h3>Ειδη παραγγελιας</h3><hr/>
                @foreach ($row->items as $item)
                <div class="row">
                    <div class="col-lg-2">
                        <a >
                            <img border="0" class="img-thumbnail" src="{{$item->model->mainPhoto()->photon(100)}}"/>
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <b>Ονομα</b><br/><br/>
                        <a href="{{ url('/admin/manage/designs/'.$item->id) }}">
                            {{ $item->model->title }}
                        </a>
                    </div>
                    <div class="col-lg-2">
                        <b>Ποσοτητα</b><br/><br/>
                        {{$item->quantity}}
                    </div>
                    <div class="col-lg-2">
                        <b>Τιμη</b><br/><br/>
                        {!! euro($item->price) !!}
                    </div>
                    <div class="col-lg-2">
                        <b>Συνολο</b><br/><br/>
                        {!! euro($item->quantity * $item->price) !!}
                    </div>
                </div>
                <hr>
                @endforeach
                <div class="row">
                    <div class="clearfix well col-lg-6 col-lg-offset-6">
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-lg-6">Σύνολο ειδών:</label>
                            <div class="col-lg-6 checkbox">{!! euro($row->items_cost) !!}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-lg-6">Έξοδα πληρωμής:</label>
                            <div class="col-lg-6 checkbox">{!! euro($row->payment_cost) !!}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-lg-6">Μεταφορικά:</label>
                            <div class="col-lg-6 checkbox">{!! euro($row->shipping_cost) !!}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label text-left col-lg-6">Σύνολο παραγγελίας:</label>
                            <div class="col-lg-6 checkbox">{!! euro($row->total) !!}</div>
                        </div>
                    </div>
                </div>

                @if ($row->totals)
                    <?php $totals = $row->totals->toArray() ?>
                    <h3>Ανάλυση Ποσών</h3><hr/>
                    <table class="table table-responsive table-condensed table-striped" style="margin:0;">
                        <tbody>
                        <tr><td> {{ trans('generic.fields.order_totals.unit_cost') }} </td> <td align="right"> {!! euro($totals['unit_cost']) !!} </td> </tr>
                        <tr><td> {{ trans('generic.fields.order_totals.vat_perc') }} </td> <td align="right"> {{ $totals['vat_perc']*100 }}% </td> </tr>
                        </tbody>
                    </table>
                    <table class="table table-responsive table-condensed table-striped" style="margin:0;">
                        <tbody>
                            <tr>
                                <td></td>
                                <td>Έσοδα</td>
                                <td>ΦΠΑ</td>
                                <td>Έξοδα</td>
                                <td>ΦΠΑ</td>
                            </tr>
                            <tr><td>Είδη</td>
                                <td> {!! euro($totals['items_in_net']) !!} </td>
                                <td> {!! euro($totals['items_in_vat']) !!} </td>
                                <td> {!! euro($totals['items_out_net']) !!} </td>
                                <td> {!! euro($totals['items_out_vat']) !!} </td>
                            </tr>
                            <tr><td>Πληρωμές</td>
                                <td> {!! euro($totals['payment_in_net']) !!} </td>
                                <td> {!! euro($totals['payment_in_vat']) !!} </td>
                                <td> {!! euro($totals['payment_out_net']) !!} </td>
                                <td> {!! euro($totals['payment_out_vat']) !!} </td>
                            </tr>
                            <tr><td>Μεταφορικά</td>
                                <td> {!! euro($totals['shipping_in_net']) !!} </td>
                                <td> {!! euro($totals['shipping_in_vat']) !!} </td>
                                <td> {!! euro($totals['shipping_out_net']) !!} </td>
                                <td> {!! euro($totals['shipping_out_vat']) !!} </td>
                            </tr>
                            <tr><td>Σύνολο</td>
                                <td> {!! euro($totals['total_in_net']) !!} </td>
                                <td> {!! euro($totals['total_in_vat']) !!} </td>
                                <td> {!! euro($totals['total_out_net']) !!} </td>
                                <td> {!! euro($totals['total_out_vat']) !!} </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-responsive table-condensed table-striped" style="margin:0;">
                        <tbody>
                        <tr><td> {{ trans('generic.fields.order_totals.profit_net') }} </td> <td align="right"> {!! euro($totals['profit_net']) !!} </td> </tr>
                        <tr><td> {{ trans('generic.fields.order_totals.profit_vat') }} </td> <td align="right"> {!! euro($totals['profit_vat']) !!} </td> </tr>
                        </tbody>
                    </table>
                @endif
            </div>
            <div class="col-lg-3">
                <div class="clearfix">
                    <h3>Τροπος πληρωμης</h3><hr/>
                    {{$row->payment->name}} / Κόστος: {!! euro($row->payment->cost) !!}
                    <h3>Τροπος αποστολης</h3><hr/>
                    {{$row->shipping->name}} :: {{$row->shipping->estimated_time}} / Κόστος: {!! euro($row->shipping->cost) !!}
                    <h3>Voucher</h3><hr/>
                    @include('commerce::admin.orders.voucher_one', ['order'=>$row])
                    @if ( in_array($row->carrier, Config::get('admin.carriers.keys', [])) && $row->voucher )
                        <a class="btn btn-info btn-sm" target="_blank" href="/funcs/{{$row->carrier}}/voucher-pdf?voucher={{$row->voucher}}">
                            <i class="fa fa-file-pdf-o"></i>
                            Download
                        </a>
                        <a class="btn btn-danger btn-sm" onclick="return confirm('Sure delete?');" href="/funcs/{{$row->carrier}}/cancel-voucher?voucher={{$row->voucher}}">
                            <i class="fa fa-trash-o"></i>
                            Cancel
                        </a>
                    @endif

                </div>
                @if ($row->payment_response)
                <div class="clearfix">
                    <h3>Απόκριση συστήματος πληρωμής</h3><hr/>
                    {{niceprintr($row->payment_response)}}
                </div>
                <hr>
                @endif
            </div>
        </div>
        <div class="row-fluid clearfix">

            <div class="col-lg-6">
                <h3>Παραστατικά</h3><hr/>
                @if ($row->invoices->count())
                <table class="table table-responsive table-condensed table-striped " width="100%" border="0" cellspacing="10" cellpadding="10">
                    <thead>
                    <tr>
                        <th>ΑΡΙΘΜ</th>
                        <th>ΑΚΥΡΩΝΕΙ</th>
                        <th>ΚΑΤΑΣΤ</th>
                        <th>ΠΑΗΨΣ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($row->invoices as $invoice)
                        <tr>
                            <td>{{ $invoice->invoice_type.$invoice->invoice_num }}</td>
                            <td>
                                @if ($invoice->is_cancel)
                                {{ $invoice->cancels_type.$invoice->cancels_num }}
                                @endif
                            </td>
                            <td>{{ $invoice->status }}</td>
                            <td>{{ $invoice->irs_hash }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
                <a class="btn btn-default btn-xs" href="{{ route('commerce.order.make_invoice', [$row->id]) }}">
                    Δημιουργία παραστατικού
                </a>
                @if ($row->invoice() && $row->invoice()->id)
                    <a class="btn btn-default btn-xs" href="{{ route('commerce.order.invoice', [$row->id]) }}" target="_blank">
                        <i class="fa fa-file-pdf-o"></i> Παραστατικό
                    </a>
                @endif
            </div>


            @if ($row->statuses->count())
                <div class="col-lg-6">
                    <div class="clearfix">
                        <h3>Ιστορικό καταστάσεων</h3><hr/>
                        <table class="table table-responsive table-condensed " width="100%" border="0" cellspacing="10" cellpadding="10">
                            @foreach ($row->statuses as $sc)
                                <tr style="color: {{$sc->status->color or 'black'}}; font-size: 12px;">
                                    <td>{{ $sc->timestamp }}</td>
                                    <td>{{ $sc->status->title }}</td>
                                    <td>{{ trans('generic.printed_one_'.$sc->printer_status_id) }}</td>
                                    <td>{{ trans('generic.printer_paid_one_'.$sc->printer_paid_id) }}</td>
                                    <td>{{ $sc->user_id ? '('.( ($sc->user->name) ? $sc->user->name : $sc->user->email ).')' : '' }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
</form>
@stop
