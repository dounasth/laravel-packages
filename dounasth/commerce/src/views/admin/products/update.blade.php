@extends('backend::layout')

@section('page-title')
{{ !$row->id ? 'Add' : 'Editing' }}
@stop

@section('page-subtitle')
    {{ !$row->id ? 'Add' : "$row->title ($row->id)" }}
@stop

@push('breadcrumb')
<li><a href="{{ route('commerce.products.index') }}" class="goOnCancel"><i class="fa fa-group"></i> Manage Products</a></li>
<li class="active">{{ !$row->id ? 'Add' : 'Edit' }} Product</li>
@endpush

@section('page-menu')
@stop

@include('crud::parts.show_fields_scripts')

@section('content')
<div class="box box-primary">
    {{ Form::open(array('route' => ['commerce.products.save', $row->id], 'method' => 'POST', 'role' => 'form', 'enctype'=>'multipart/form-data')) }}
    <div class="box-body">
        <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <h2>General</h2>
                <div class="form-group">
                    {{ Form::label('title', 'Title:') }}
                    {{ Form::text('product[title]', $row->title, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('slug', 'Slug:') }}
                    {{ Form::text('product[slug]', $row->slug, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('sku', 'SKU:') }}
                    {{ Form::text('product[sku]', $row->sku, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('description', 'Short Description:') }}
                    {{ Form::textarea('product[short_description]', ($row->descriptions) ? $row->descriptions->short : '', array('class' => 'form-control ckeditor')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('description', 'Full Description:') }}
                    {{ Form::textarea('product[full_description]', ($row->descriptions) ? $row->descriptions->full : '', array('class' => 'form-control ckeditor')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('tags', 'Tags:') }}
                    {{--<select class="form-control select2-ajax-multiple compartment" multiple size="1" id="tags" name="product[tags][]"
                            data-ajax--url="{{ url('/data/models/Dounasth.Content.App.Models.Tag/select2') }}" data-ajax--cache="true">
                        @foreach( $row->tagArray as $tagName )
                            <option value="{{$tagName}}" selected>{{$tagName}}</option>
                        @endforeach
                    </select>--}}
                    <select class="form-control select2-tags compartment" multiple size="1" id="tags" name="product[tags][]">
                        @foreach( \Dounasth\Content\App\Models\Tag::pluck('name') as $tagName )
                            <option value="{{$tagName}}" {{ in_array($tagName, $row->tagArray) ? 'selected' : '' }}>{{$tagName}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    {{ Form::label('status', 'Status:') }}
                    {{ Form::select('product[status]', array('A' => 'Active', 'D' => 'Disabled'), $row->status, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('affiliate_url', 'If affiliate product, enter here the url:') }}
                    {{ Form::text('product[affiliate_url]', ($row->affiliateUrl) ? $row->affiliateUrl->url : '', array('class' => 'form-control')) }}
                </div>

                <h2>SEO Fields</h2>
                <div class="form-group">
                    {{ Form::label('seo-title', 'SEO Title:') }}
                    {{ Form::text('seo[title]', ($row->seo) ? $row->seo->title : '', array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('seo-description', 'Meta Description:') }}
                    {{ Form::textarea('seo[description]', ($row->seo) ? $row->seo->description : '', array('class' => 'form-control ckeditor')) }}
                </div>
                <div class="form-group">
                    {{ Form::label('seo-keywords', 'Meta Keywords:') }}
                    {{ Form::textarea('seo[keywords]', ($row->seo) ? $row->seo->keywords : '', array('class' => 'form-control ckeditor')) }}
                </div>

                <h2>Meta Fields</h2>
                @foreach (Config::get('commerce.product-meta', []) as $key => $name)
                <div class="form-group">
                    {{ Form::label('meta-'.$key, $name) }}
                    {{ Form::text('meta['.$key.']', $row->meta($key), array('class' => 'form-control')) }}
                </div>
                @endforeach

            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <h2>Basic Data</h2>

                <div class="form-group">
                    {{ Form::label('main_category', 'Parent:') }}
                    <select class="form-control select2-ajax" data-addclass="select2-ajax" id="main_category" name="product[main_category]"
                            data-ajax--url="{{ url('/data/models/Dounasth.Commerce.App.Models.Category/select2') }}" data-ajax--cache="true">
                        @if ($row->mainCategory()->id)
                            <option value="{{$row->mainCategory()->selectboxValue()}}" selected>{{$row->mainCategory()->selectboxText()}}</option>
                        @endif
                    </select>
                </div>

                <div class="form-group">
                    {{ Form::label('additional_categories', 'Additional:') }}
                    <select class="form-control select2-ajax-multiple compartment" multiple size="1" id="additional_categories" name="product[additional_categories][]"
                            data-ajax--url="{{ url('/data/models/Dounasth.Commerce.App.Models.Category/select2') }}" data-ajax--cache="true">
                        @foreach( $row->additionalCategories() as $additionalCategory )
                            <option value="{{$additionalCategory->selectboxValue()}}" selected>{{$additionalCategory->selectboxText()}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    {{ Form::label('price', 'Price:') }}
                    {{ Form::text('product[price]', ($row->prices) ? $row->prices->price : 0 , array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('list_price', 'List Price:') }}
                    {{ Form::text('product[list_price]', ($row->prices) ? $row->prices->list_price : 0, array('class' => 'form-control')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('main_image', 'Main Image:') }}
                    @if ($row->mainPhoto())
                    <img src="{{ $row->mainPhoto()->httpPath() }}" class="img-responsive img-rounded" />
                    @endif
                    {{ Form::text('product[main_image]', $row->mainPhoto() ? $row->mainPhoto()->path : '' , array('class' => 'form-control')) }}
{{--                    {{ Form::file('files[main_image]', '', array('class' => 'form-control')) }}--}}
                </div>

                <h2>Timestamps</h2>
                <div class="form-group">
                    {{ Form::label('', 'Updated@: '.$row->updated_at) }}
                </div>
                <div class="form-group">
                    {{ Form::label('', 'Created@: '.$row->created_at) }}
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <button class="btn btn-success" type="submit"><i class="fa fa-check-square-o"></i> Save</button>
        <a class="btn btn-danger btn-cancel"><i class="fa fa-square-o"></i> Cancel</a>
        <button class="btn btn-warning pull-right" type="submit" name="saveNew" value="1"><i class="fa fa-plus"></i> Save as new</button>
    </div>
    {{ Form::close() }}
</div>
@stop