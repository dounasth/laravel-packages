@foreach (array_chunk($products->items(), 4) as $row)
<div class="row  categoryProduct xsResponse clearfix">
    @foreach ($row as $product)
        <div class="item col-sm-3 col-lg-3 col-md-3 col-xs-3">
            @include('commerce::site.product-mini', ['product'=>$product])
        </div>
    @endforeach
</div>
@endforeach