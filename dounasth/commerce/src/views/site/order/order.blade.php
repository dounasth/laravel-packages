@extends('frontend::layout')

@if ($order->is_paid === 0)
<meta http-equiv="refresh" content="0;URL='{{ route('order.pay', ['id='.$order->hash]) }}'" />
@endif

@push('after_scripts')
<script type="text/javascript" charset="utf-8">
    jQuery(document).ready(function(){
        jQuery('#payment-methods .payment-button input').click(function(e){
            jQuery('#payment-methods .payment-button').removeClass('selected');
            jQuery('#payment-methods .payment-button .btn').removeClass('btn-gray').removeClass('btn-gray-empty').addClass('btn-gray-empty');
            jQuery('#payment-methods .payment-button input:checked').parent().addClass('selected');
            jQuery('#payment-methods .payment-button input:checked').parent().find('.btn').removeClass('btn-gray').removeClass('btn-gray-empty').addClass('btn-gray');
        });
        jQuery('#payment-methods .payment-button input:checked').click();
    });
</script>
@endpush

@section('content')
@if ($order->is_paid === 0)
<div class="dark-bg page-header">
    <div class="container">
        <p style="text-align: center; font-size: 20px; background: #f0f0f0; border: 1px solid #d4d4d4; padding: 20px;">
            {{trans('site.order.redirection1')}}<br/>
            {{trans('site.order.redirection2')}} <a href="{{ route('order.pay', ['id='.$order->hash]) }}">{{trans('site.order.redirection3')}}</a> {{trans('site.order.redirection4')}}.
        </p>
    </div>
</div>


@else



<div class="margin-top">
    <div class="container thanx-container">
        <div class="body collapse in form-horizontal">

            @if (\Illuminate\Support\Facades\Session::get('show_thanx', false))
                {{ \Illuminate\Support\Facades\Session::put('show_thanx', false) }}
                <div class="row">
                    <div class="col-lg-12 thanx-message">
                        <img src="/images/web/thank-you.png" />
                        <br/>
                        <h2><span>{{trans('site.order.thanx.title')}}</span></h2>
                        {{trans('site.order.thanx.text')}}
                        <a href="/" class="btn btn-blue btn-block">{{ trans('site.order.thanx.goback') }}</a>
                    </div>
                </div>
            @endif

            @if ($order->is_paid == 2)
                <div class="row margin-top">
                    <div class="col-lg-12 text-center ">
                        <h2><span>{{ trans('site.order.repay.pending') }}</span></h2>
                        {{--<p class="text-center">{{trans('site.order.repay.pending_sub')}}</p>--}}
                        {{trans('site.order.repay.pending_text')}}
                        <div class="form-group clearfix">
                            <form action="{{route('order.updatePayment', [$order->hash])}}" method="post">
                                <div class="row clearfix">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="margin-bottom-sm box {{--options two--}}" id="payment-methods">
                                            <?php $payments = Payment::enabled()->get(); ?>
                                            @foreach ($payments as $payment)
                                                <label id="payment-{{$payment->id}}" class="payment-button {{ $order->payment->id == $payment->id ? 'selected' : '' }}" style="width: {{ 100/count($payments) }}%">
                                                    @if ($payment->cost > 0)
                                                        <div class="price-tag sm hidden-xs">
                                                            <div class="price">{!! euro($payment->cost) !!}</div>
                                                        </div>
                                                    @endif
                                                    <input type="radio" {{ $order->payment->id == $payment->id ? 'checked=""' : '' }} value="{{$payment->id}}" name="payment_method">
                                                    <div class="inline image">
                                                        @if ($payment->image)
                                                            <img src="{{$payment->image}}" />
                                                        @endif
                                                    </div>
                                                    <span class="btn {{ $order->payment->id == $payment->id ? 'btn-gray' : 'btn-gray-empty' }} bold">{{$payment->name}}</span>
                                                    <small class="">&nbsp;</small>
                                                    <div class="price-tag visible-xs">
                                                        <div class="price">{!! euro($payment->cost) !!}</div>
                                                    </div>
                                                </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                {{--<a style="margin-bottom: 10px;" class="btn btn-primary btn-block" href="{{ route('order.pay', ['id='.$order->hash]) }}">
                                    {{trans('site.order.pay-order')}} <span class="glyphicon glyphicon-menu-right"></span>
                                </a>--}}
                                <button class="btn btn-blue big btn-block" style="margin: 10px 0 !important;">{{trans('site.order.change-payment')}}</button>
                            </form>
                        </div>
                    </div>

                </div>
            @endif

            <div class="row margin-top">
                <div class="col-lg-12">
                    <h2><span>{{trans('site.order.thanx.your-order')}}</span></h2>
                    <p class="margin-top-small">{{ trans('site.order.thanx.your-order-is') }} #<b>{{$order->hash}}</b></p>
                    <p>{{trans('site.order.status')}}: {{$order->status->title}}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-center">{{trans('site.order.shipping-info')}}</h3>
                    <div class="shadowed clearfix">
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.firstname')}}:</label>
                            <div class="col-lg-6 checkbox">{{$order->addresses['firstname']}}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.lastname')}}:</label>
                            <div class="col-lg-6 checkbox">{{$order->addresses['surname']}}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.email')}}:</label>
                            <div class="col-lg-6 checkbox">{{$order->addresses['email']}}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.phone')}}:</label>
                            <div class="col-lg-6 checkbox">{{$order->addresses['phone']}}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.phone_mobile')}}:</label>
                            <div class="col-lg-6 checkbox">{{$order->addresses['phone_mobile']}}</div>
                        </div>
                        {{--<div class="form-group clearfix">--}}
                            {{--<label class="control-label col-xs-6">{{trans('site.order.company')}}:</label>--}}
                            {{--<div class="col-lg-6 checkbox">{{$order->addresses['company_name']}}</div>--}}
                        {{--</div>--}}
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.address1')}}:</label>
                            <div class="col-lg-6 checkbox">{{$order->addresses['address']}}</div>
                        </div>
                        {{--<div class="form-group clearfix">--}}
                        {{--<label class="control-label col-xs-6">{{trans('site.order.address2')}}:</label>--}}
                        {{--<div class="col-lg-6 checkbox">{{$order->addresses['address2']}}</div>--}}
                        {{--</div>--}}
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.zip')}}:</label>
                            <div class="col-lg-6 checkbox">{{$order->addresses['zip']}}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.city')}}:</label>
                            <div class="col-lg-6 checkbox">{{$order->addresses['city']}}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.county')}}:</label>
                            <div class="col-lg-6 checkbox">{{ $order->theState() }}</div>
                        </div>
                        <div class="clearfix" style="padding-bottom: 10px;">
                            <label class="control-label col-xs-6">{{trans('site.order.country')}}:</label>
                            <div class="col-lg-6 checkbox">{{ $order->theCountry() }}</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-center">{{trans('site.order.invoice_type')}}</h3>
                    <div class="shadowed clearfix">
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.invoice_type')}}:</label>
                            <div class="col-lg-6 checkbox">{{trans('site.order.invoice_type_'.strtoupper($order->addresses['invoice_type']))}}</div>
                        </div>
                        @if ($order->addresses['invoice_type'] == 't')
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.job_title')}}:</label>
                            <div class="col-lg-6 checkbox">{{$order->addresses['job_title']}}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.job_afm')}}:</label>
                            <div class="col-lg-6 checkbox">{{$order->addresses['job_afm']}}</div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label col-xs-6">{{trans('site.order.job_doy')}}:</label>
                            <div class="col-lg-6 checkbox">{{$order->addresses['job_doy']}}</div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-center">{{trans('site.order.billing-info')}}</h3>
                    <div class="shadowed clearfix">
                        @if (!$order->addresses['other_billing_address'])
                            <p class="text-center" style="padding: 15px; margin: 0;">{{trans('site.order.same-as-shipping')}}</p>
                        @else
                            <div class="form-group clearfix">
                                <label class="control-label col-xs-6">{{trans('site.order.firstname')}}:</label>
                                <div class="col-lg-6 checkbox">{{$order->addresses['billing_firstname']}}</div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-xs-6">{{trans('site.order.lastname')}}:</label>
                                <div class="col-lg-6 checkbox">{{$order->addresses['billing_surname']}}</div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-xs-6">{{trans('site.order.address1')}}:</label>
                                <div class="col-lg-6 checkbox">{{$order->addresses['billing_address']}}</div>
                            </div>
                            {{--<div class="form-group clearfix">--}}
                            {{--<label class="control-label col-xs-6">{{trans('site.order.address2')}}:</label>--}}
                            {{--<div class="col-lg-6 checkbox">{{$order->addresses['billing_address2']}}</div>--}}
                            {{--</div>--}}
                            <div class="form-group clearfix">
                                <label class="control-label col-xs-6">{{trans('site.order.zip')}}:</label>
                                <div class="col-lg-6 checkbox">{{$order->addresses['billing_zip']}}</div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-xs-6">{{trans('site.order.city')}}:</label>
                                <div class="col-lg-6 checkbox">{{$order->addresses['billing_city']}}</div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-xs-6">{{trans('site.order.county')}}:</label>
                                <div class="col-lg-6 checkbox">{{ $order->theBillingState() }}</div>
                            </div>
                            <div class="clearfix" style="padding-bottom: 10px;">
                                <label class="control-label col-xs-6">{{trans('site.order.country')}}:</label>
                                <div class="col-lg-6 checkbox">{{ $order->theBillingCountry() }}</div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







<?php
$itemsContainerClass = count($order->items) > 1 ? 'thanx-container' : 'thanx-container';
$itemClass = count($order->items) > 1 ? 'col-md-12 margin-bottom-small' : 'col-md-12  margin-bottom-small';
?>

<div class="container {{$itemsContainerClass}} margin-bottom">
    <div class="body collapse in form-horizontal">

        <div class="row">
            <div class="col-lg-12">
                <h3 class="text-center">{{trans('site.order.order-items')}}</h3>

                <div class="items">
                @foreach ($order->items as $item)
                    <div class="col-xs-12 {{ $itemClass }}">
                        <div class="row cart-item-row" id="{{$item->id}}">

                            <div class="col-xs-12 col-md-6 cart-item-preview">
                                <div class="center-vertical">
                                    {{--<a href="{{$edit_url}}">
                                        {{$item->design->preview}}
                                    </a>--}}
                                    <img border="0" class="img-thumbnail" src="{{$item->model->mainPhoto()->photon(100)}}"/>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-6 cart-item-info bg-gray">
                                <div class="cart-item-title-cont">
                                    <p class="cart-item-title">
                                        {{$item->name}}
                                    </p>
                                </div>
                                <div class="cart-item-prices">
                                    <table width="100%">
                                        <tr class="quantity">
                                            <td>{{ trans('site.cart.quantity') }}</td>
                                            <td align="center">
                                                {{$item->quantity}}
                                            </td>
                                        </tr>
                                        <tr class="price">
                                            <td>{{ trans('site.cart.price') }}</td>
                                            <td align="center">
                                                {!! euro($item->price) !!}
                                            </td>
                                        </tr>
                                        <tr class="total">
                                            <td>{{ trans('site.cart.total') }}</td>
                                            <td align="center">
                                                {!! euro( $item->quantity * $item->price ) !!}
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
                </div>



            </div>
        </div>
    </div>
</div>


<div class="container thanx-container">
    <div class="row">
        <div class="col-lg-12">
            <div class="margin-bottom">
                <h3 class="text-center">{{trans('site.order.payments')}}</h3>
                <p class="text-center">{{$order->payment->name}} / {{trans('site.order.cost')}}: {!! euro($order->payment->cost) !!}</p>
            </div>
            <div class="margin-bottom">
                <h3 class="text-center">{{trans('site.order.shippings')}}</h3>
                <p class="text-center">{{$order->shipping->name}} :: {{$order->shipping->estimated_time}} / {{trans('site.order.cost')}}: {!! euro($order->shipping->cost) !!}</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="text-center">{{trans('site.order.synopsis')}}</h3>
            <div class="shadowed clearfix form-horizontal order-totals">
                <div class="form-group clearfix">
                    <label class="control-label col-xs-6">{{trans('site.order.total_items')}}:</label>
                    <div class="col-xs-6 checkbox">{!! euro($order->items_cost) !!}</div>
                </div>
                <div class="form-group clearfix">
                    <label class="control-label col-xs-6">{{trans('site.order.total_payment')}}:</label>
                    <div class="col-xs-6 checkbox">{!! euro($order->payment_cost) !!}</div>
                </div>
                <div class="form-group clearfix">
                    <label class="control-label col-xs-6">{{trans('site.order.total_shipping')}}:</label>
                    <div class="col-xs-6 checkbox">{!! euro($order->shipping_cost) !!}</div>
                </div>
                <div class="clearfix the-total" style="padding-bottom: 8px;">
                    <label class="control-label col-xs-6 ">{{trans('site.order.total_order')}}:</label>
                    <div class="col-xs-6 checkbox">{!! euro($order->total) !!}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid margin-top">
        <div class="form-group clearfix">
            <a href="/" class="btn btn-blue btn-block">{{ trans('site.order.thanx.goback') }}</a>
        </div>
    </div>

</div>

@endif
@stop