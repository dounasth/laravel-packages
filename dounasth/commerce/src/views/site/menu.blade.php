
@foreach ($tree as $category)
    @if ($category->tree_count > 0)
    <li class="{{ $category->children->count() > 0 ? isset($class) ? 'dropdown '.$class : 'dropdown' : '' }}">
        <a data-toggle="{{ ($category->getDescendantCount() == 0) ? '' : 'dropdown' }}" class="{{ $category->children->count() > 0 ? 'dropdown-toggle' : '' }}"
           href="{{ ($category->getDescendantCount() == 0) ? route('site.commerce.category.view', [$category->slug]) : '#' }}">
            <span>{{ $category->title }} ({{ $category->tree_count }})</span>
        </a>
        @if ( $category->children->count() > 0 )
        <ul class="dropdown-menu">
            @include('commerce::site.menu', ['tree'=>$category->children, 'class'=>'dropdown-submenu'])
        </ul>
        @endif
    </li>
    @endif
@endforeach


