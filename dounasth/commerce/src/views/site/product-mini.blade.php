@if ($product instanceof Dounasth\Commerce\App\Models\Product\Product)
@else
<?php $product = Dounasth\Commerce\App\Models\Product\Product::find($product) ?>
@endif


<div class="card product mini" data-id="{{$product->id}}">
    <img class="card-img-top" src="{{ $product->mainPhoto()->photon(365) }}" alt="{{ $product->title }}" style="width:100%">
    <div class="card-body">

        <div class="promotion">
            @if (false)
                <span class="new-product"> NEW</span>
            @endif
            @if ( $product->prices && (int)$product->prices->discount() > 0 )
                <span class="discount">{{(int)$product->prices->discount() }}% Έκπτωση</span>
            @endif
        </div>

        <div>{{$product->meta('brand')}}</div>
        <div class="">
            <h4 class="card-title">
                <a target="_blank" rel="nofollow" href="{{ $product->route() }}">{{ $product->title }}</a>
            </h4>
            {{--<p class="card-text"></p>--}}
        </div>
        <div class="price">
            <span>&euro;{{$product->prices ? $product->prices->price : -1}}</span>
            @if ($product->prices && $product->prices->list_price > $product->prices->price)
                <span class="old-price">&euro;{{$product->prices->list_price}}</span>
            @endif
        </div>

        <div class="btn-group btn-group-sm pull-right">
            @if ($product->affiliateUrl)
                <a target="_blank" rel="nofollow" href="{{ $product->route() }}" class="btn btn-primary">
                    <i class="fa fa-eye"></i> view on merchant
                </a>
            @else
                <a rel="nofollow" href="{{ $product->route() }}" class="btn btn-primary">
                    <i class="fa fa-eye"></i> view
                </a>
                <a rel="nofollow" href="{{ route('site.cart.add', [base64_encode(get_class($product)), $product->id]) }}" class="btn btn-info">
                    <i class="fa fa-shopping-cart"></i> buy
                </a>
            @endif
        </div>
    </div>
</div>
<br>