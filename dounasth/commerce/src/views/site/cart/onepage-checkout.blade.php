<?php
use Dounasth\Commerce\App\Cart\MyCartFacade as MyCart;
?>
@extends('frontend::layout')

@push('after_styles')

@endpush

@push('after_scripts')
<script src="{{ asset('vendor/backend/') }}/jquery.blockUI.js"></script>
<script src="{{ asset('vendor/backend/') }}/blockUI.init.cart.js"></script>
<script src="{{ asset('vendor/backend/') }}/jquery.validate.js"></script>

<script type="text/javascript" charset="utf-8">

    function blockTheUI() {
        if (doBlockUI) { $.blockUI(); }
    }
    function unblockTheUI() {
        initCheckoutFormValidation();
        jQuery("#complete_order_form").valid();
        $.unblockUI();
    }

    $(document).ajaxStart(blockTheUI).ajaxStop(unblockTheUI);

    function replaceHTML(data) {
        jQuery.each(data, function (index, value) {
            jQuery(index).replaceWith(value);
        });
    }

    function initCheckoutFormValidation() {
        jQuery("#complete_order_form").validate({
            rules: {
                'addresses[firstname]': "required",
                'addresses[surname]': "required",
                'addresses[phone_mobile]': {
                    required: true,
                    minlength: 10
                },
                'addresses[email]': {
                    required: true,
                    email: true
                },
                'addresses[address]': "required",
                'addresses[zip]': {
                    required: true,
                    minlength: 5
                },
                'addresses[city]': "required",
                'addresses[country]': "required",
                'addresses[county]': "required",

                'addresses[billing_firstname]': {
                    required: {
                        depends: function(element) { return jQuery("#other_billing_address").val() == 1; }
                    }
                },
                'addresses[billing_surname]': {
                    required: {
                        depends: function(element) { return jQuery("#other_billing_address").val() == 1; }
                    }
                },
                'addresses[billing_address]': {
                    required: {
                        depends: function(element) { return jQuery("#other_billing_address").val() == 1; }
                    }
                },
                'addresses[billing_country]': "required",
                'addresses[billing_county]': "required",
                'addresses[billing_zip]': {
                    required: {
                        depends: function(element) { return jQuery("#other_billing_address").val() == 1; }
                    },
                },
                'addresses[billing_city]': {
                    required: {
                        depends: function(element) { return jQuery("#other_billing_address").val() == 1; }
                    }
                }
            },
            messages: {
                /*SHIPPING*/
                'addresses[firstname]': "{{ trans('commerce::site.cart.validation-messages.firstname') }}",
                'addresses[surname]': "{{ trans('commerce::site.cart.validation-messages.surname') }}",
                'addresses[phone_mobile]': {
                    required:  "{{ trans('commerce::site.cart.validation-messages.phone_mobile') }}",
                    minlength: "{{ trans('commerce::site.cart.validation-messages.phone_mobile2') }}"
                },
                'addresses[email]': "{{ trans('commerce::site.cart.validation-messages.email') }}",
                'addresses[address]': "{{ trans('commerce::site.cart.validation-messages.address') }}",
                'addresses[zip]': {
                    required:  "{{ trans('commerce::site.cart.validation-messages.zip') }}",
                    minlength: "{{ trans('commerce::site.cart.validation-messages.zip2') }}"
                },
                'addresses[city]': "{{ trans('commerce::site.cart.validation-messages.city') }}",
                'addresses[country]': "{{ trans('commerce::site.cart.validation-messages.country') }}",
                'addresses[county]': "{{ trans('commerce::site.cart.validation-messages.county') }}",

                /*BILLING*/
                'addresses[billing_firstname]': "{{ trans('commerce::site.cart.validation-messages.firstname') }}",
                'addresses[billing_surname]': "{{ trans('commerce::site.cart.validation-messages.surname') }}",
                'addresses[billing_address]': "{{ trans('commerce::site.cart.validation-messages.address') }}",
                'addresses[billing_country]': "{{ trans('commerce::site.cart.validation-messages.country') }}",
                'addresses[billing_county]': "{{ trans('commerce::site.cart.validation-messages.county') }}",
                'addresses[billing_zip]': {
                    required:  "{{ trans('commerce::site.cart.validation-messages.billing_zip') }}",
                },
                'addresses[billing_city]': "{{ trans('commerce::site.cart.validation-messages.city') }}"
            },
            errorClass: "message text-danger",
            errorElement: "div"
        });
    }

    function updateAddresses(doAsync) {
        doAsync = typeof doAsync !== 'undefined' ? doAsync : true;
        var url = '{{route("site.cart.updateAddresses")}}';
        var data = jQuery('#complete_order_form .addresses input, #complete_order_form .addresses select').serializeArray();
        jQuery.ajax({
            type: 'GET',
            url: url,
            data: data,
            success: function (data) {
                lastFocusedInput = $(':focus').attr('tabindex') * 1;
                lastFocusedValue = $(':focus').val();
                lastcec = typeof data['cec'] !== 'undefined' ? data['cec'] : 0;
                if (doReplace) {
                    replaceHTML(data['html']);
                }
//                    jQuery('[tabindex="' + (lastFocusedInput) + '"]').val(lastFocusedValue).focus();
            },
            dataType: 'json',
            async: doAsync
        });

    }

    function updateOrderExtra() {
        var url = '{{route("site.cart.updateOrderExtra")}}';
        var data = jQuery('#complete_order_form .order-extra input, #complete_order_form .order-extra select').serializeArray();
        jQuery.get(url, data, function (data) {
            replaceHTML(data['html']);
        }, "json");
    }


    var lastAjax = null;
    var lastFocusedInput = 0;
    var lastFocusedValue = 0;
    var doReplace = false;
    var doBlockUI = false;
    var lastcec = 0;

    jQuery(document).ready(function () {

        initCheckoutFormValidation();

        jQuery('body').on('change', '.quantity', function (e) {
            e.preventDefault();
            if(lastAjax){ lastAjax.abort(); }
            doReplace = true;
            doBlockUI = false;
            var url = '{{route("site.cart.updateQuantity", ["xx", "yy"])}}';
            url = url.replace('xx', jQuery(this).data('id')).replace('yy', jQuery(this).val());
            lastAjax = jQuery.get(url, function (data) {
                lastFocusedInput = $(':focus').attr('tabindex') * 1;
                lastFocusedValue = $(':focus').val();
                lastcec = typeof data['cec'] !== 'undefined' ? data['cec'] : 0;
                if (doReplace) {
                    replaceHTML(data['html']);
                }
                jQuery('[tabindex="' + (lastFocusedInput) + '"]').val(lastFocusedValue);
            }, "json");
            return false;
        });

        jQuery('body').on('submit', '#complete_order_form', function (e) {
            return jQuery("#complete_order_form").valid();
        });

        jQuery('body').on('change', '#complete_order_form input, #complete_order_form select:not([id$=country])', function (e) {
            e.preventDefault();
            doReplace = false;
            doBlockUI = false;
            lastFocusedInput = $(':focus').attr('tabindex') * 1;
            lastFocusedValue = $(':focus').val();
            updateAddresses();
            return false;
        });

        jQuery('body').on('change', '[id$=country]', function (e) {
            e.preventDefault();
            doReplace = true;
            doBlockUI = true;
            lastFocusedInput = $(':focus').attr('tabindex') * 1;
            lastFocusedValue = $(':focus').val();
            updateAddresses();
            return false;
        });

        jQuery('body').on('click', '#other-billing-address', function (e) {
            e.preventDefault();
            doReplace = true;
            doBlockUI = true;
            jQuery('#billing-address-box').removeClass('nodisplay').show();
            jQuery('#other-billing-address').hide();
            jQuery('#other_billing_address').val(1);
            updateAddresses();
            return false;
        });

        jQuery('body').on('click', '#no-billing-address', function (e) {
            e.preventDefault();
            doReplace = true;
            doBlockUI = true;
            jQuery('#billing-address-box').addClass('nodisplay').hide();
            jQuery('#other-billing-address').show();
            jQuery('#complete_order_form [name^="addresses[billing_"]').val('');
            jQuery('#other_billing_address').val(0);
            updateAddresses();
            return false;
        });

        jQuery('body').on('click', '#complete_order_form .order-extra input[type="radio"]', function (e) {
            e.preventDefault();
            doReplace = true;
            doBlockUI = true;
            updateOrderExtra();
            return false;
        });

        jQuery('body').on('click', '#invoice_type_a', function (e) {
            jQuery('.timologio-fields').addClass('hidden');
            jQuery('.timologio-fields input').val('');
        });
        jQuery('body').on('click', '#invoice_type_t', function (e) {
            jQuery('.timologio-fields').removeClass('hidden');
        });

        jQuery('[name="addresses[invoice_type]"]:checked').click();

    });
</script>
@endpush






@section('content')
    <ul>
        <li class="cart-button">
            <a href="{{route('site.cart.view')}}" style="display: flex;align-items: center;">
                {{ trans('commerce::site.cart.minitop')  }}&nbsp;
                <svg class="cart-icon inline" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" width="20.163px" height="18.35px" viewBox="0 0 20.163 18.35" enable-background="new 0 0 20.163 18.35" xml:space="preserve">
                    <g>
                        <path d="M20.001,4.079c-0.182-0.281-0.492-0.448-0.83-0.448H9.102c-0.547,0-0.993,0.445-0.993,0.993
                            c0,0.547,0.446,0.993,0.993,0.993h8.55l-2.454,5.638H7.49L4.579,0.73C4.464,0.3,4.069,0,3.62,0H0.993C0.445,0,0,0.445,0,0.993
                            c0,0.548,0.445,0.993,0.993,0.993h1.878l2.911,10.526c0.116,0.431,0.51,0.729,0.96,0.729h9.114c0.395,0,0.742-0.227,0.91-0.596
                            l3.316-7.627C20.213,4.71,20.183,4.359,20.001,4.079z"/>
                        <circle cx="6.052" cy="16.437" r="1.913"/>
                        <path d="M16.342,14.521c-0.043,0-0.087,0.001-0.128,0.004c-0.51,0.033-0.976,0.267-1.311,0.656
                            c-0.333,0.386-0.498,0.877-0.464,1.384c0.07,1.001,0.905,1.785,1.901,1.785h0.131l0.012-0.001c0.513-0.043,0.977-0.277,1.303-0.658
                            c0.33-0.369,0.494-0.863,0.467-1.393C18.185,15.301,17.346,14.521,16.342,14.521z"/>
                    </g>
                </svg>&nbsp;

                @if (MyCart::getContent()->count())
                    <span class="cart-count">{{ MyCart::getContent()->count() }}</span>
                @endif
                {!! euro(MyCart::getTotal()) !!}
            </a>
        </li>
    </ul>

    <div class="dark-bg page-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-3 col-lg-6 col-xs-offset-0 col-xs-12">
                    <h1 style="text-align: center; margin: 0px;">{{ trans('commerce::site.cart.cart-header') }}</h1>
                </div>
                <div class="col-lg-3 hidden-xs">
                    <a style="margin: 6px;" class="btn btn-blue-empty pull-right " href="{{ config('app.url') }}">
                        {{ trans('commerce::site.cart.continue-shopping') }} <span class="glyphicon glyphicon-menu-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div class="container checkout margin-top">

        @if (MyCart::getContent()->count())

            @if ($cart_message)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ $cart_message }}
                </div>
            @endif

            <div class="items clearfix">


                @foreach (MyCart::getContent() as $k => $item)
                    <div class="row cart-item-row" id="{{$item->id}}">
                            <div class="col-xs-12 col-md-2 cart-item-preview">
                                <div class="text-center">
                                    <a href="{{$item->model->route()}}" style="max-width: 100px; max-height: 100px; overflow: hidden;">
                                        <img border="0" class="img-thumbnail" src="{{$item->model->mainPhoto()->photon(100)}}"/>
                                    </a>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-6 cart-item-info">
                                <div class="cart-item-title-cont">
                                    <p class="cart-item-title">
                                        <a href="{{$item->model->route()}}">
                                            {{$item->name}}
                                        </a>
                                        <a href="{{$item->model->route()}}" class="btn btn-text">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <br/>
                                        <a class="text-sm text-muted" href="{{$item->model->mainCategory()->route()}}">{{$item->model->mainCategory()->path()}}</a> <br/>
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-3 cart-item-info">
                                <div class="cart-item-prices">
                                    <table width="100%" class="table table-condensed">
                                        <tr class="price">
                                            <td>{{ trans('commerce::site.cart.price') }}</td>
                                            <td align="center">{!! euro( $item->price ) !!}</td>
                                        </tr>
                                        <tr class="quantity">
                                            <td>{{ trans('commerce::site.cart.quantity') }}</td>
                                            <td align="center">
                                                <input type="number" step="1" data-trigger="manual" value="{{$item->quantity}}"
                                                      {{--data-toggle="tooltip" data-title="Lowest value is $1" old="1" data-original-title="" title=""--}}
                                                      data-id="{{$item->id}}" min="1"
                                                      style="max-width:80px; height: 20px;"
                                                      class=" text-center quantity form-control input-sm">
                                            </td>
                                        </tr>
                                        <tr class="total">
                                            <td>{{ trans('commerce::site.cart.total') }}</td>
                                            <td align="center">{!! euro( $item->quantity * $item->price ) !!}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-1 ">
                                <a class="item-btn remove-btn btn btn-xs btn-blue pull-right"
                                   title="{{ trans('commerce::site.cart.remove') }}" data-id="1"
                                   href="{{ route('site.cart.remove', [$item->id]) }}">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                            </div>
                    </div>
                    <hr/>
                @endforeach


            </div>

            <div class="row">

                <form id="complete_order_form" action="{{route('site.cart.checkout.complete')}}" method="post">
                    @csrf
                    <div class="addresses row-fluid">
                        <div class="col-sm-12 ">
                            <h4 class="cart-header">{{ trans('commerce::site.cart.shipping-info') }}</h4>

                            {{--<form id="dibs-form" method="post" action="{{route("site.cart.updateAddresses")}}">--}}
                            <div id="shipping-address">
                                <div class="box margin-bottom-sm">


                                    {{--Στοιχεία αποστολής--}}
                                    <div class="row">

                                        {{--row 1--}}
                                        <div class="form-group col-sm-12 col-lg-3">
                                            <label class="control-label" for="firstname">{{ trans('commerce::site.cart.firstname') }}</label>
                                            <input type="text" value="{{$addresses['firstname']}}" class="form-control" id="firstname" name="addresses[firstname]" tabindex="1">
                                            <div class="message text-danger hidden">{{ $messages['firstname'] }}</div>
                                        </div>
                                        <div class="form-group col-sm-12 col-lg-3">
                                            <label class="control-label" for="surname">{{ trans('commerce::site.cart.lastname') }}</label>
                                            <input type="text" value="{{$addresses['surname']}}" class="form-control" id="surname" name="addresses[surname]" tabindex="2">
                                            <div class="message text-danger hidden">{{ $messages['surname'] }}</div>
                                        </div>
                                        <div class="form-group col-sm-12 col-lg-3">
                                            <label class="control-label" for="phone">{{ trans('commerce::site.cart.phone') }}</label>
                                            <input type="text" value="{{$addresses['phone']}}" class="form-control" id="phone" name="addresses[phone]" tabindex="3">
                                            <div class="message text-danger hidden">{{ $messages['phone'] }}</div>
                                        </div>
                                        <div class="form-group col-sm-12 col-lg-3">
                                            <label class="control-label" for="phone_mobile">{{ trans('commerce::site.cart.phone_mobile') }}</label>
                                            <input type="text" value="{{$addresses['phone_mobile']}}" class="form-control" id="phone_mobile" name="addresses[phone_mobile]" tabindex="3">
                                            <div class="message text-danger hidden">{{ $messages['phone_mobile'] }}</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        {{--row 2--}}
                                        <div class="form-group col-sm-12 col-lg-4">
                                            <label class="control-label" for="email">{{ trans('commerce::site.cart.email') }}</label>
                                            <input type="email" value="{{$addresses['email']}}" class="form-control" id="email" name="addresses[email]" tabindex="4">
                                            <div class="message text-danger hidden">{{ $messages['email'] }}</div>
                                        </div>
                                        <div class="form-group col-sm-12 col-lg-4">
                                            <label class="control-label" for="address">{{ trans('commerce::site.cart.address1') }}</label>
                                            <input type="text" value="{{$addresses['address']}}" class="form-control" id="address" name="addresses[address]" tabindex="6">
                                            <div class="message text-danger hidden">{{ $messages['address'] }}</div>
                                        </div>
                                        <div class="form-group col-sm-12 col-lg-4">
                                            <label class="control-label" for="zip">{{ trans('commerce::site.cart.zip') }}</label>
                                            <input type="text" value="{{$addresses['zip']}}" class="form-control" id="zip" name="addresses[zip]" tabindex="7">
                                            <div class="message text-danger hidden">{{ $messages['zip'] }}</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        {{--row 3--}}
                                        <?php $countries = \Dounasth\Commerce\App\Models\Countries::pluck('name', 'code'); ?>
                                        <div class="form-group col-sm-12 col-lg-4">
                                            <label class="control-label" for="country">{{ trans('commerce::site.cart.country') }}</label>
                                            {{ Form::select('addresses[country]', $countries, ( (!empty($addresses['country'])) ? $addresses['country'] : 'gr' ), array('id'=>'country', 'class'=>'form-control', 'size'=>1, 'tabindex'=>8)) }}
                                            <div class="message text-danger hidden">{{ $messages['country'] }}</div>
                                        </div>
                                        <?php $states = \Dounasth\Commerce\App\Models\States::sortedList($addresses['country']); ?>
                                        @if (fn_is_not_empty($states))
                                            <div class="form-group col-sm-12 col-lg-4">
                                                <label class="control-label" for="county">{{ trans('commerce::site.cart.county') }}</label>
                                                {{ Form::select('addresses[county]', $states, ( (!empty($addresses['county'])) ? $addresses['county'] : Session::get('latest_county', '') ), array('id'=>'county', 'class'=>'form-control', 'size'=>1, 'tabindex'=>9)) }}
                                                <div class="message text-danger hidden">{{ $messages['county'] }}</div>
                                            </div>
                                        @else
                                            <div class="form-group col-sm-12 col-lg-4">
                                                <label class="control-label" for="county">{{ trans('commerce::site.cart.county') }}</label>
                                                <input list="counties" type="text" value="{{ !is_numeric($addresses['county']) ? $addresses['county'] : '' }}" id="county" class="form-control" name="addresses[county]" tabindex="10">
                                                <div class="message text-danger hidden">{{ $messages['county'] }}</div>
                                            </div>
                                        @endif
                                        <div class="form-group col-sm-12 col-lg-4">
                                            <label class="control-label" for="city">{{ trans('commerce::site.cart.city') }}</label>
                                            <input type="text" value="{{$addresses['city']}}" class="form-control" id="city" name="addresses[city]" tabindex="11">
                                            <div class="message text-danger hidden">{{ $messages['city'] }}</div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 margin-bottom">
                                <h4 class="cart-header">{{ trans('commerce::site.cart.invoice_type') }}</h4>

                                <div class="row">
                                    <div class="form-group col-sm-12 col-lg-4 col-lg-offset-4">
                                        <label class="radio-inline" for="invoice_type_a"><input id="invoice_type_a" type="radio" name="addresses[invoice_type]" value="a" {{$addresses['invoice_type'] == 'a' || !$addresses['invoice_type'] ? 'checked' : ''}}>{{ trans('commerce::site.cart.invoice_type_A') }}</label>
                                        <label class="radio-inline" for="invoice_type_t"><input id="invoice_type_t" type="radio" name="addresses[invoice_type]" value="t" {{$addresses['invoice_type'] == 't' ? 'checked' : ''}}>{{ trans('commerce::site.cart.invoice_type_T') }}</label>
                                        <div class="message text-danger hidden">{{ $messages['job_title'] }}</div>
                                    </div>
                                </div>
                                <div class="row timologio-fields">
                                    {{--row 2--}}
                                    <div class="form-group col-sm-12 col-lg-4">
                                        <label class="control-label" for="job_title">{{ trans('commerce::site.cart.job_title') }}</label>
                                        <input type="text" value="{{$addresses['job_title']}}" class="form-control" id="job_title" name="addresses[job_title]" tabindex="12">
                                        <div class="message text-danger hidden">{{ $messages['job_title'] }}</div>
                                    </div>
                                    <div class="form-group col-sm-12 col-lg-4">
                                        <label class="control-label" for="job_afm">{{ trans('commerce::site.cart.job_afm') }}</label>
                                        <input type="text" value="{{$addresses['job_afm']}}" class="form-control" id="job_afm" name="addresses[job_afm]" tabindex="13">
                                        <div class="message text-danger hidden">{{ $messages['job_afm'] }}</div>
                                    </div>
                                    <div class="form-group col-sm-12 col-lg-4">
                                        <label class="control-label" for="job_doy">{{ trans('commerce::site.cart.job_doy') }}</label>
                                        <input type="text" value="{{$addresses['job_doy']}}" class="form-control" id="job_doy" name="addresses[job_doy]" tabindex="14">
                                        <div class="message text-danger hidden">{{ $messages['job_doy'] }}</div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center margin-top">
                                <a class="btn btn-blue-empty" id="other-billing-address" href="#!" {{$addresses['other_billing_address'] ? 'style="display: none;"' : '' }}>{{trans('commerce::site.cart.other_billing')}}</a>
                                <input id="other_billing_address" type="hidden" value="{{$addresses['other_billing_address']}}" name="addresses[other_billing_address]" tabindex="15"/>
                            </div>

                            {{--Στοιχεία Χρέωσης--}}
                            <div class="margin-bottom {{@!$addresses['other_billing_address'] ? 'nodisplay' : '' }}" id="billing-address-box" {{@!$addresses['other_billing_address'] ? 'style="display: none;"' : '' }}>
                                <h4 class="cart-header">
                                    {{ trans('commerce::site.cart.billing-info') }}
                                    <a class="btn btn-xs btn-blue" id="no-billing-address" href="#!">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </a>
                                </h4>

                                <div class="box">

                                    <div class="row">

                                        {{--row 1--}}
                                        <div class="form-group col-sm-12 col-lg-4">
                                            <label class="control-label" for="billing-firstname">{{ trans('commerce::site.cart.firstname') }}</label>
                                            <input type="text" value="{{$addresses['billing_firstname']}}" id="billing-firstname" class="form-control" name="addresses[billing_firstname]" tabindex="16">
                                            <div class="message text-danger hidden">{{ $messages['billing_firstname'] }}</div>
                                        </div>

                                        <div class="form-group col-sm-12 col-lg-4">
                                            <label class="control-label" for="billing-surname">{{ trans('commerce::site.cart.lastname') }}</label>
                                            <input type="text" value="{{$addresses['billing_surname']}}" id="billing-surname" class="form-control" name="addresses[billing_surname]" tabindex="17">
                                            <div class="message text-danger hidden">{{ $messages['billing_surname'] }}</div>
                                        </div>

                                        <div class="form-group col-sm-12 col-lg-4">
                                            <label class="control-label" for="billing-address">{{ trans('commerce::site.cart.address1') }}</label>
                                            <input type="text" value="{{$addresses['billing_address']}}" id="billing-address" class="form-control" name="addresses[billing_address]" tabindex="18">
                                            <div class="message text-danger hidden">{{ $messages['billing_address'] }}</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        {{--row 2--}}
                                        <div class="form-group col-sm-12 col-lg-3">
                                            <label class="control-label" for="billing-country">{{ trans('commerce::site.cart.country') }}</label>
                                            {{ Form::select('addresses[billing_country]', $countries, ( (!empty($addresses['billing_country'])) ? $addresses['billing_country'] : 'gr' ), array('id'=>'billing_country', 'class'=>'form-control', 'size'=>1, 'tabindex'=>19)) }}
                                            <div class="message text-danger hidden">{{ $messages['billing_country'] }}</div>
                                        </div>
                                        @if (fn_is_not_empty($states))
                                            <div class="form-group col-sm-12 col-lg-3">
                                                <label class="control-label" for="billing-county">{{ trans('commerce::site.cart.county') }}</label>
                                                {{ Form::select('addresses[billing_county]', array_merge(array(''=>trans('commerce::site.choose')), $states), ( (!empty($addresses['billing_county'])) ? $addresses['billing_county'] : Session::get('latest_billing_county', '') ), array('id'=>'billing_county', 'class'=>'form-control', 'size'=>1, 'tabindex'=>20)) }}
                                                <div class="message text-danger hidden">{{ $messages['billing_county'] }}</div>
                                            </div>
                                        @else
                                            <div class="form-group col-sm-12 col-lg-3">
                                                <label class="control-label"
                                                       for="billing-county">{{ trans('commerce::site.cart.county') }}</label>
                                                <input list="counties" type="text"
                                                       value="{{$addresses['billing_county']}}"
                                                       id="billing_county" class="form-control"
                                                       name="addresses[billing_county]" tabindex="21">
                                                <div class="message text-danger hidden">{{ $messages['billing_county'] }}</div>
                                            </div>
                                        @endif
                                        <div class="form-group col-sm-12 col-lg-3">
                                            <label class="control-label"
                                                   for="billing-zip">{{ trans('commerce::site.cart.zip') }}</label>
                                            <input type="text" value="{{$addresses['billing_zip']}}"
                                                   id="billing-zip" class="form-control" name="addresses[billing_zip]"
                                                   tabindex="22">
                                            <div class="message text-danger hidden">{{ $messages['billing_zip'] }}</div>
                                        </div>
                                        <div class="form-group col-sm-12 col-lg-3">
                                            <label class="control-label"
                                                   for="billing-city">{{ trans('commerce::site.cart.city') }}</label>
                                            <input type="text" value="{{$addresses['billing_city']}}"
                                                   id="billing-city" class="form-control" name="addresses[billing_city]"
                                                   tabindex="23">
                                            <div class="message text-danger hidden">{{ $messages['billing_city'] }}</div>
                                        </div>




                                    </div>

                                </div>

                            </div>

                            {{--</form>--}}

                        </div>
                    </div>

                    <div class="order-extra row-fluid">
                        <div class="col-sm-12 col-md-6 " >
                            <h4 class="cart-header">{{trans('commerce::site.cart.payments')}}</h4>
                            <div class="row-fluid margin-bottom-sm box {{--options two--}}" id="payment-methods">
                                <?php $payments = \Dounasth\Commerce\App\Models\Payment::enabled()->get(); ?>
                                @foreach ($payments as $payment)
                                    <label id="payment-{{$payment->id}}" class="payment-button col-xs-12" style="width: {{ 100/count($payments) }}%">
                                        @if ($payment->cost > 0)
                                            <div class="price-tag sm hidden-xs">
                                                <div class="price">{!! euro($payment->cost) !!}</div>
                                            </div>
                                        @endif
                                        <input type="radio" {{ $extra['payment_method'] == $payment->id ? 'checked=""' : '' }} value="{{$payment->id}}" name="extra[payment_method]">
                                        <div class="inline image">
                                            @if ($payment->image)
                                                <img src="{{asset($payment->image)}}" />
                                            @endif
                                        </div>
                                        <span class="btn {{ $extra['payment_method'] == $payment->id ? 'btn-gray' : 'btn-gray-empty' }} bold">{{$payment->name}}</span>
                                        <small class="">&nbsp;</small>
                                        <div class="price-tag visible-xs">
                                            <div class="price">{!! euro($payment->cost) !!}</div>
                                        </div>
                                    </label>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <h4 class="cart-header">{{trans('commerce::site.cart.shippings')}}</h4>
                            <div class="box margin-bottom" id="shipping-types">
                                <?php $shippings = \Dounasth\Commerce\App\Models\Shipping::enabled()->get(); ?>
                                @foreach ($shippings as $shipping)
                                    <label id="shipping-{{$shipping->id}}" class="shipping-button" style="width: {{ 100/count($shippings) }}%" >
                                        @if ($shipping->cost > 0)
                                            <div class="price-tag sm hidden-xs">
                                                <div class="price">{!! euro($shipping->cost) !!}</div>
                                            </div>
                                        @endif
                                        <input type="radio" {{ $selectedshipping->id == $shipping->id ? 'checked=""' : '' }} value="{{$shipping->id}}" name="extra[shipping_type]">
                                        <div class="inline image">
                                            @if ($shipping->image)
                                                <img src="{{asset($shipping->image)}}" />
                                            @endif
                                        </div>
                                        <span class="btn {{ $selectedshipping->id == $shipping->id ? 'btn-gray' : 'btn-gray-empty' }} bold">
                                {{$shipping->name}}
                                            <small class="days visible-xs">{{$shipping->estimated_time}}</small>
                            </span>
                                        <small class="days hidden-xs">{{$shipping->estimated_time}}</small>
                                        <div class="price-tag visible-xs">
                                            <div class="price">{!! euro($shipping->cost) !!}</div>
                                        </div>
                                    </label>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="summary row-fluid">
                        <div class="col-sm-12 col-md-offset-3 col-md-6">
                            <div id="summary">
                                <div class="box margin-bottom-xs">
                                    <div class="row">
                                        <div class="col-xs-6 left-text">{{trans('commerce::site.cart.total_items')}}</div>
                                        <div class="col-xs-6 right-text sub-total">{!! euro( MyCart::getTotal() ) !!}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-6 left-text">{{trans('commerce::site.cart.total_payment')}}</div>
                                        <div class="col-xs-6 right-text payment-total">{!! euro($selectedpayment->cost) !!}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-6 left-text">{{trans('commerce::site.cart.total_shipping')}}</div>
                                        <div class="col-xs-6 right-text shipping-total">{!! euro( $selectedshipping->cost ) !!}</div>
                                    </div>

                                    <div class="row the-total">
                                        <div class="col-xs-6 left-text">{{trans('commerce::site.cart.total_order')}}</div>
                                        <div class="col-xs-6 right-text " id="total">{!! euro( MyCart::getTotal() + $selectedshipping->cost + $selectedpayment->cost ) !!}</div>
                                    </div>

                                    <div class="alert alert-danger nodisplay" id="dibs-error"></div>

                                </div>

                                <div class=" margin-bottom-xs">
                                    <button class="btn btn-blue" {{--{{ (@fn_is_empty($messages)) ? '' : 'disabled="disabled"' }}--}} id="dibs-submit" type="submit">
                                        {{trans('commerce::site.cart.complete_order')}} <i class="glyphicon glyphicon-menu-right"></i>
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>

                </form>

                @else

                    <div class="container">
                        <div class="row" style="text-align: center;">
                            {!! trans('commerce::site.cart.cart-is-empty') !!}
                        </div>

                    </div>

                @endif
            </div>
    </div>


    </div>
@stop