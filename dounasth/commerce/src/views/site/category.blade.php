<?php
use Illuminate\Support\Facades\Input;
?>
@extends('frontend::layout')

@section('page-title')
    <?php $catTitle = ($category->seo && $category->seo->title) ? $category->seo->title : $category->title ?>
    {{ \Dounasth\Commerce\App\Models\Filter::currentAsText($catTitle) }} {{ date('Y', time()) }}
@stop

@section('page-subtitle')
    {{ \Dounasth\Commerce\App\Models\Filter::currentAsText($catTitle) }} στις καλύτερες τιμές απο τα μεγαλύτερα καταστήματα στην Ελλάδα και το εξωτερικό
@stop

@push('breadcrumb')
@foreach ($category->getAncestors() as $i => $ancestor)
    <li class=""><a href="{{ route('site.commerce.category.view', $ancestor->slug) }}">{{ $ancestor->title  }}</a></li>
@endforeach
<li class="active">{{ $category->title }}</li>
@endpush

@section('page-menu')
@stop

@section('meta')
<link rel="canonical" href="{{ route('site.commerce.category.view', $category->slug).\Dounasth\Commerce\App\Models\Filter::makeCanonicalLink() }}" />
{{--<meta name="robots" content="{{\Dounasth\Commerce\App\Models\Filter::makeIndexFollow()}}" />--}}
<meta name="description" content="{{ ($category->description) ? "<p>{$category->description}</p>" : trim($__env->yieldContent('page-subtitle')) }}" >
@stop

@push('after_styles')
    <style type="text/css">
        .subCategoryList.collapsed { height : 95px;}
    </style>
@endpush

@push('after_scripts')
<?php $j=0 ?>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [
  @foreach ($category->getAncestors() as $i => $ancestor)
    {
      "@type": "ListItem",
      "position": {{ $i+1 }},
      "item": {
        "@id": "{{ route('site.commerce.category.view', $ancestor->slug) }}",
        "name": "{{ $ancestor->title }}"
      }
    },
    <?php $j=$i+1 ?>
  @endforeach
    {
      "@type": "ListItem",
      "position": {{ $j+1 }},
      "item": {
        "@id": "{{ route('site.commerce.category.view', $category->slug) }}",
        "name": "{{ $category->title }}"
      }
    }
  ]
}
</script>
<script type="text/javascript" charset="utf-8">
    jQuery('.moreSubCats').click(function(e){
        jQuery('.subCategoryList').toggleClass('collapsed');
        if ( jQuery('.subCategoryList').hasClass('collapsed') ) {
            jQuery(this).find('span').text('θέλω να δω όλες τις');
        }
        else jQuery(this).find('span').text('θέλω να δω λιγότερες');
    });
</script>
@endpush

@section('content')

<div class="container main-container headerOffset">

{{--<div class="row">
    <div class="breadcrumbDiv col-lg-12">
        <ul class="breadcrumb">
            @include('frontend::parts.breadcrumb')
        </ul>
    </div>
</div>--}}

<div class="row">

    <div class="col-lg-3 col-md-3 col-sm-12">
        <div class="panel-group" id="accordionNo">
            @include('commerce::site.filters', ['product_ids'=>$product_ids])
        </div>
    </div>

    <div class="col-lg-9 col-md-12 col-sm-12">
        <div class="w100 clearfix category-top">
            <h2> @yield('page-title') </h2>
            @if ($category->description)
            <p>{{ $category->description }}</p>
            @endif

            @if ($category->children->count() > 0)
            <div class="row subCategoryList collapsed clearfix">
                @foreach ($category->children()->with('seo')/*->remember(3600*24)*/->get()->whereNotIn('tree_count', [0]) as $sub)
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6  text-center ">
                    <div class="thumbnail equalheight" style="height: 123px;">
                        <!-- <a href="{{ route('site.commerce.category.view', $sub->slug) }}" class="subCategoryThumb">
                            <img alt="{{ ($sub->seo && $sub->seo->title) ? $sub->seo->title : $sub->title }}" class="img-rounded " src="images/product/3.jpg">
                        </a> -->
                        <a href="{{ route('site.commerce.category.view', $sub->slug) }}" class="subCategoryTitle">
                            <span>{{ ($sub->seo && $sub->seo->title) ? $sub->seo->title : $sub->title }}</span>
                            <span>{{ $sub->tree_count }}</span>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            @if ($category->children->count() > 6 )
            <a class="moreSubCats btn btn-default btn-sm pull-right"><span>θέλω να δώ όλες τις</span> υποκατηγορίες ({{ $category->children->count() }} συνολικά)</a>
            @endif
            @endif
        </div>

        @if (fn_is_not_empty($products->items()))
        <div class="w100 productFilter clearfix">
            <p class="pull-left">
                Εμφάνιση {{--<b>{{ $products->getFrom() }}-{{ $products->getTo() }}</b>--}} από {{ $products->total() }} προϊόντα
                @if (Input::get('orderBy', false))
                    | Ταξινόμηση: {{ implode('_', [Input::get('orderBy', false), Input::get('orderType', false)]) }}
                @endif
            </p>
            <div class="pull-right ">
                <div class="change-order pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" style="background: white;border: 1px solid lightgray;"
                                aria-haspopup="true" aria-expanded="false">Ταξινόμηση <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="{{ URL::current() }}?{{ http_build_query(array_merge(Input::all(), ['orderBy'=>null, 'orderType'=>null])) }}">Προεπιλογή</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ URL::current() }}?{{ http_build_query(array_merge(Input::all(), ['orderBy'=>'price', 'orderType'=>'asc'])) }}">Με τιμή αύξουσα</a></li>
                            <li><a href="{{ URL::current() }}?{{ http_build_query(array_merge(Input::all(), ['orderBy'=>'price', 'orderType'=>'desc'])) }}">Με τιμή φθίνουσα</a></li>
                        </ul>
                    </div>
                </div>
                {{--<div class="change-view pull-right"><a href="#" title="Grid" class="grid-view"> <i class="fa fa-th-large"></i> </a> <a href="#" title="List" class="list-view "><i class="fa fa-th-list"></i></a></div>--}}
            </div>
        </div>

        @include('commerce::site.category-products-grid')

        <div class="w100 categoryFooter">
            <div class="pagination pull-left no-margin-top">
                {{ $products->appends(\Dounasth\Commerce\App\Models\Filter::getLinkParams())->links() }}
            </div>
        </div>
        @else
            <div class="well text-center">
                Δεν υπάρχουν ακόμα προϊόντα σε αυτή την κατηγορία.<br/>
                @if ($category->parent && $category->parent->children->count() > 0)
                    Μήπως σας ενδιαφέρουν οι παρακάτω κατηγορίες;<br/><br/>
                    <div class="row subCategoryList clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  text-center ">
                            <div class="thumbnail equalheight" style="height: 123px;">
                                <!-- <a href="{{ route('site.commerce.category.view', $category->parent->slug) }}" class="subCategoryThumb">
                            <img alt="{{ ($category->parent->seo && $category->parent->seo->title) ? $category->parent->seo->title : $category->parent->title }}" class="img-rounded " src="images/product/3.jpg">
                        </a> -->
                                <a href="{{ route('site.commerce.category.view', $category->parent->slug) }}" class="subCategoryTitle"><span>{{ ($category->parent->seo && $category->parent->seo->title) ? $category->parent->seo->title : $category->parent->title }}</span></a>
                            </div>
                        </div>
                        @foreach ($category->parent->children()->with('seo')/*->remember(3600*24)*/->get() as $sub)
                            @if ($sub->id != $category->id)
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6  text-center ">
                                    <div class="thumbnail equalheight" style="height: 123px;">
                                        <!-- <a href="{{ route('site.commerce.category.view', $sub->slug) }}" class="subCategoryThumb">
                            <img alt="{{ ($sub->seo && $sub->seo->title) ? $sub->seo->title : $sub->title }}" class="img-rounded " src="images/product/3.jpg">
                        </a> -->
                                        <a href="{{ route('site.commerce.category.view', $sub->slug) }}" class="subCategoryTitle"><span>{{ ($sub->seo && $sub->seo->title) ? $sub->seo->title : $sub->title }}</span></a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @endif
            </div>
        @endif

    </div>

</div>

</div>


@stop