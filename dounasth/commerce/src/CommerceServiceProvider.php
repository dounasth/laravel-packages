<?php

namespace Dounasth\Commerce;

use Illuminate\Support\ServiceProvider;

class CommerceServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot(\Illuminate\Routing\Router $router)
    {
        // LOAD THE VIEWS
        // - first the published views (in case they have any changes)
        $this->loadViewsFrom(resource_path('views/vendor/dounasth/commerce'), 'commerce');
        // - then the stock views that come with the package, in case a published view might be missing
        $this->loadViewsFrom(realpath(__DIR__.'/views'), 'commerce');

        $this->loadTranslationsFrom(realpath(__DIR__.'/langs'), 'commerce');

        // use the vendor configuration file as fallback
        $this->mergeConfigFrom(realpath(__DIR__ . '/../config/commerce.php'), 'commerce');
        $this->mergeConfigFrom(realpath(__DIR__ . '/../config/cart.php'), 'cart');

        $this->setupRoutes($router);
        $this->loadHelpers();
    }

    public function registerMiddleware(\Illuminate\Routing\Router $router)
    {
    }

    public function setupRoutes(\Illuminate\Routing\Router $router)
    {
        // by default, use the routes file provided in vendor
        $routeFilePathInUse = __DIR__ . '/routes.5.6.php';
        $this->loadRoutesFrom($routeFilePathInUse);

        // but if there's a file with the use that one
        if (file_exists(base_path().'/routes/commerce.php')) {
            $routeFilePathInUse = base_path().'/routes/commerce.php';
            $this->loadRoutesFrom($routeFilePathInUse);
        }
    }

    public function publishFiles()
    {
        // publish config file
        $this->publishes([
            realpath(__DIR__ . '/../config/commerce.php') => config_path('commerce.php'),
            realpath(__DIR__ . '/../config/cart.php') => config_path('cart.php'),
        ], 'config');
    }

    public function loadHelpers()
    {
        require_once realpath(__DIR__ . '/functions.php');
    }

    public function register()
    {
	    $this->app->singleton('mycart', function($app)
	    {
		    $storageClass = config('shopping_cart.storage');
		    $eventsClass = config('shopping_cart.events');

		    $storage = $storageClass ? new $storageClass() : $app['session'];
		    $events = $eventsClass ? new $eventsClass() : $app['events'];
		    $instanceName = 'mycart';

		    // default session or cart identifier. This will be overridden when calling Cart::session($sessionKey)->add() etc..
		    // like when adding a cart for a specific user name. Session Key can be string or maybe a unique identifier to bind a cart
		    // to a specific user, this can also be a user ID
		    $session_key = '4yTlTDKu3oJOfzD';

		    return new App\Cart\MyCart(
			    $storage,
			    $events,
			    $instanceName,
			    $session_key,
			    config('shopping_cart')
		    );
	    });
    }

    public function provides()
    {
        return array(
        );
    }

}
