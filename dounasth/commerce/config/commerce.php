<?php

return [
    'general' => [
        'config_path' => dirname(__FILE__),
        'cache' => array(
            // minutes
            'category' => 60,
            'product-meta' => 60
        )
    ],
    'product-meta' => [
        'is_affiliate' => 'Is Affiliate',
        'brand' => 'Brrand',
        'size' => 'Size',
        'color' => 'Color',
        'discount' => 'Discount',
    ],
    'slugs' => [
        'category' => 'category',
        'product' => 'product',
        'tag' => 'tagged',
    ],

];