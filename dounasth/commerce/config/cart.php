<?php

use Respect\Validation\Validator as v;

return array(
    'pagination' => array(
        'items_per_page' => array(10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 150, 200),
    ),
    'checkout' => array(
        'validation' => array(
            'addresses' => array(
                'firstname' => v::stringType()->notEmpty()->not( v::numeric() ),
                'surname' => v::stringType()->notEmpty()->not( v::numeric() ),
                'email' => v::stringType()->notEmpty()->email(),
                'phone' => v::alwaysValid(),
                'phone_mobile' => v::stringType()->notEmpty()->phone(),
//                'company_name' => v::stringType(),
                'address' => v::stringType()->notEmpty()->not( v::numeric() ),
//                'address2' => v::stringType(),
                'zip' => v::stringType()->notEmpty()->length(5,10)->numeric(),
                'city' => v::stringType()->notEmpty()->not( v::numeric() ),
                'county' => v::stringType()->notEmpty(),
                'country' => v::stringType()->notEmpty()->not( v::numeric() ),
                'other_billing_address' => v::between(0,1, true),
                'billing_firstname' => v::stringType()->notEmpty()->not( v::numeric() ),
                'billing_surname' => v::stringType()->notEmpty()->not( v::numeric() ),
                'billing_address' => v::stringType()->notEmpty()->not( v::numeric() ),
                'billing_zip' => v::stringType()->notEmpty()->length(5,10)->numeric(),
                'billing_city' => v::stringType()->notEmpty()->not( v::numeric() ),
                'billing_county' => v::stringType()->notEmpty(),
                'billing_country' => v::stringType()->notEmpty()->not( v::numeric() ),
//                'billing_address2' => v::stringType()->notEmpty()->not( v::numeric() ),
                'invoice_type' => v::alwaysValid(),
                'job_title' => v::alwaysValid(),
                'job_afm' => v::alwaysValid(),
                'job_doy' => v::alwaysValid(),
            ),
            'messages' => array(
                'firstname' => trans('site.cart.validation-messages.firstname'),
                'surname' => trans('site.cart.validation-messages.surname'),
                'email' => trans('site.cart.validation-messages.email'),
                'phone' => trans('site.cart.validation-messages.phone'),
                'phone_mobile' => trans('site.cart.validation-messages.phone_mobile'),
//                'company_name' => trans('site.cart.validation-messages.company_name'),
                'address' => trans('site.cart.validation-messages.address'),
//                'address2' => trans('site.cart.validation-messages.address2'),
                'zip' => trans('site.cart.validation-messages.zip'),
                'city' => trans('site.cart.validation-messages.city'),
                'county' => trans('site.cart.validation-messages.county'),
                'country' => trans('site.cart.validation-messages.country'),
                'other_billing_address' => trans('site.cart.validation-messages.other_billing_address'),
                'billing_firstname' => trans('site.cart.validation-messages.billing_firstname'),
                'billing_surname' => trans('site.cart.validation-messages.billing_surname'),
                'billing_address' => trans('site.cart.validation-messages.billing_address'),
                'billing_zip' => trans('site.cart.validation-messages.billing_zip'),
                'billing_city' => trans('site.cart.validation-messages.billing_city'),
                'billing_county' => trans('site.cart.validation-messages.billing_county'),
                'billing_country' => trans('site.cart.validation-messages.billing_country'),
//                'billing_address2' => trans('site.cart.validation-messages.billing_address2'),
            ),
        )
    )
);